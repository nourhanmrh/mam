-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 19, 2020 at 07:29 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mamgram`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `reply_message` text DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `idx-auth_assignment-user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '2', 1572250688),
('client', '11', 1574757076),
('client', '3', 1582791351),
('client', '4', 1559124569),
('client', '5', 1559135734),
('client', '6', 1559137237),
('client', '7', 1559137376),
('client', '8', 1559197480),
('copy_admin_accounts_deposit', '2', 1572424066),
('copy_admin_add_accounts', '2', 1572270665),
('copy_admin_add_accounts', '21', 1564147391),
('copy_admin_add_accounts', '22', 1564147456),
('copy_admin_add_accounts', '24', 1564147618),
('copy_admin_columns', '2', 1572270665),
('copy_admin_delete_accounts', '2', 1572270665),
('copy_admin_delete_accounts', '21', 1564147391),
('copy_admin_delete_accounts', '22', 1564147456),
('copy_admin_delete_accounts', '24', 1564147618),
('copy_admin_destroy_trades', '2', 1572270665),
('copy_admin_destroy_trades', '21', 1564147391),
('copy_admin_destroy_trades', '22', 1564147456),
('copy_admin_destroy_trades', '24', 1564147618),
('copy_admin_refresh_accounts', '2', 1572270665),
('copy_admin_update_trades', '2', 1572270665),
('copy_admin_update_trades', '21', 1564147391),
('copy_admin_update_trades', '22', 1564147456),
('copy_admin_update_trades', '24', 1564147618),
('copy_admin_view_group', '2', 1572270665),
('copy_admin_view_group', '21', 1564147391),
('copy_admin_view_group', '22', 1564147456),
('copy_admin_view_group', '24', 1564147618),
('copy_client_edit_copytypes', '10', 1563885143),
('copy_client_edit_copytypes', '11', 1563885146),
('copy_client_edit_copytypes', '12', 1564039288),
('copy_client_edit_copytypes', '2', 1571219268),
('copy_client_edit_copytypes', '21', 1564147391),
('copy_client_edit_copytypes', '22', 1564147456),
('copy_client_edit_copytypes', '24', 1564147618),
('copy_client_edit_copytypes', '3', 1563885137),
('copy_client_edit_copytypes', '7', 1563885131),
('copy_client_edit_copytypes', '8', 1563885133),
('copy_client_edit_copytypes', '9', 1563885140),
('copy_ib_active_accounts', '2', 1572270667),
('copy_ib_delete_relations', '10', 1563885142),
('copy_ib_delete_relations', '11', 1563885145),
('copy_ib_delete_relations', '12', 1564039288),
('copy_ib_delete_relations', '2', 1572270667),
('copy_ib_delete_relations', '21', 1564147391),
('copy_ib_delete_relations', '22', 1564147456),
('copy_ib_delete_relations', '24', 1564147618),
('copy_ib_delete_relations', '3', 1563885136),
('copy_ib_delete_relations', '9', 1563885140),
('copy_ib_edit_account_type', '2', 1572270668),
('copy_ib_edit_master_account', '10', 1563885142),
('copy_ib_edit_master_account', '11', 1563885145),
('copy_ib_edit_master_account', '12', 1564039288),
('copy_ib_edit_master_account', '2', 1572270668),
('copy_ib_edit_master_account', '21', 1564147392),
('copy_ib_edit_master_account', '22', 1564147456),
('copy_ib_edit_master_account', '24', 1564147619),
('copy_ib_edit_master_account', '3', 1563885136),
('copy_ib_edit_master_account', '9', 1563885140),
('copy_ib_sync_accounts', '2', 1572270668),
('copy_ib_users_edits', '2', 1572270668),
('copy_ib_view_relations', '10', 1563885142),
('copy_ib_view_relations', '11', 1563885145),
('copy_ib_view_relations', '12', 1564039288),
('copy_ib_view_relations', '21', 1564147392),
('copy_ib_view_relations', '22', 1564147456),
('copy_ib_view_relations', '24', 1564147619),
('copy_ib_view_relations', '3', 1563885136),
('copy_ib_view_relations', '8', 1571038051),
('copy_ib_view_relations', '9', 1563885140),
('ib', '10', 1560004859),
('ib', '12', 1564039288),
('ib', '22', 1564146749),
('ib', '25', 1578907773),
('ib', '9', 1560004739),
('super admin', '1', 1557215505),
('super admin', '21', 1564147391),
('super admin', '22', 1564147456),
('super admin', '23', 1564147297),
('super admin', '24', 1564147618);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1561019979, 1561019979),
('/accounts/*', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/accounts/accounts', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/accounts/captcha', 2, NULL, NULL, NULL, 1565945389, 1565945389),
('/accounts/create', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/accounts/destroy', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/accounts/drop', 2, NULL, NULL, NULL, 1565944700, 1565944700),
('/accounts/error', 2, NULL, NULL, NULL, 1565947371, 1565947371),
('/accounts/index', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/accounts/master', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/accounts/read', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/accounts/refresh-accounts', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/accounts/sync', 2, NULL, NULL, NULL, 1566975465, 1566975465),
('/accounts/update', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/accounts/update-account', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/accounts/users', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/admin/*', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/assignment/*', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/assignment/assign', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/assignment/index', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/assignment/revoke', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/assignment/view', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/default/*', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/default/index', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/menu/*', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/menu/create', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/menu/delete', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/menu/index', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/menu/update', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/menu/view', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/permission/*', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/permission/assign', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/permission/create', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/permission/delete', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/permission/index', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/permission/remove', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/permission/update', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/permission/view', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/role/*', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/role/assign', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/role/create', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/role/delete', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/role/index', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/role/remove', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/role/update', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/role/view', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/admin/route/*', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/route/assign', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/route/create', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/route/index', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/route/refresh', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/route/remove', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/rule/*', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/rule/create', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/rule/delete', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/rule/index', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/rule/update', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/rule/view', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/*', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/activate', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/change-password', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/delete', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/index', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/login', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/logout', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/request-password-reset', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/reset-password', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/signup', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/admin/user/view', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/codes-type/*', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/codes-type/index', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/codes/*', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/codes/create', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/codes/delete', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/codes/index', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/codes/update', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/codes/view', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/companies/index', 2, NULL, NULL, NULL, 1572957314, 1572957314),
('/debug/default/*', 2, NULL, NULL, NULL, 1565941732, 1565941732),
('/debug/default/index', 2, NULL, NULL, NULL, 1565938580, 1565938580),
('/debug/default/toolbar', 2, NULL, NULL, NULL, 1565940507, 1565940507),
('/debug/default/view', 2, NULL, NULL, NULL, 1565938584, 1565938584),
('/debug/user/*', 2, NULL, NULL, NULL, 1565939875, 1565939875),
('/debug/user/set-identity', 2, NULL, NULL, NULL, 1565941735, 1565941735),
('/email-templates/*', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/email-templates/default/*', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/email-templates/default/create', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/email-templates/default/delete', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/email-templates/default/index', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/email-templates/default/update', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/email-templates/default/view', 2, NULL, NULL, NULL, 1561019977, 1561019977),
('/gii/*', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/gii/default/*', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/gii/default/action', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/gii/default/diff', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/gii/default/index', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/gii/default/preview', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/gii/default/view', 2, NULL, NULL, NULL, 1561019976, 1561019976),
('/language/*', 2, NULL, NULL, NULL, 1564574378, 1564574378),
('/language/index', 2, NULL, NULL, NULL, 1564574376, 1564574376),
('/leaderboard/chart', 2, NULL, NULL, NULL, 1571121916, 1571121916),
('/leaderboard/index', 2, NULL, NULL, NULL, 1571121915, 1571121915),
('/leaderboard/ph', 2, NULL, NULL, NULL, 1573546106, 1573546106),
('/leaderboard/rating', 2, NULL, NULL, NULL, 1571121916, 1571121916),
('/leaderboard/read', 2, NULL, NULL, NULL, 1571121916, 1571121916),
('/mam/*', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/mam/captcha', 2, NULL, NULL, NULL, 1565939134, 1565939134),
('/mam/create', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/mam/destroy', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/mam/error', 2, NULL, NULL, NULL, 1565945698, 1565945698),
('/mam/index', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/mam/read', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/mam/update', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/mamgram/*', 2, NULL, NULL, NULL, 1571036834, 1571036834),
('/mamgram/captcha', 2, NULL, NULL, NULL, 1571036833, 1571036833),
('/mamgram/configuration', 2, NULL, NULL, NULL, 1571036907, 1571036907),
('/mamgram/create', 2, NULL, NULL, NULL, 1571036834, 1571036834),
('/mamgram/destroy', 2, NULL, NULL, NULL, 1571036833, 1571036833),
('/mamgram/error', 2, NULL, NULL, NULL, 1571036833, 1571036833),
('/mamgram/index', 2, NULL, NULL, NULL, 1571036833, 1571036833),
('/mamgram/read', 2, NULL, NULL, NULL, 1571036833, 1571036833),
('/mamgram/update', 2, NULL, NULL, NULL, 1571036833, 1571036833),
('/managers/*', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/managers/active', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/managers/create', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/managers/delete', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/managers/index', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/managers/update', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/managers/users-list', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/managers/view', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/my/*', 2, NULL, NULL, NULL, 1565940594, 1565940594),
('/my/captcha', 2, NULL, NULL, NULL, 1565945816, 1565945816),
('/my/error', 2, NULL, NULL, NULL, 1565947436, 1565947436),
('/operation/*', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/operation/balance', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/operation/delete', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/operation/managers-list', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/permissions/*', 2, NULL, NULL, NULL, 1561019979, 1561019979),
('/permissions/index', 2, NULL, NULL, NULL, 1561019979, 1561019979),
('/permissions/permissions', 2, NULL, NULL, NULL, 1561019978, 1561019978),
('/permissions/re', 2, NULL, NULL, NULL, 1565942696, 1565942696),
('/permissions/read', 2, NULL, NULL, NULL, 1561019979, 1561019979),
('/permissions/readp', 2, NULL, NULL, NULL, 1561019979, 1561019979),
('/permissions/updatep', 2, NULL, NULL, NULL, 1561019979, 1561019979),
('/relations/display', 2, NULL, NULL, NULL, 1561538629, 1561538629),
('/relations/link', 2, NULL, NULL, NULL, 1564145791, 1564145791),
('/relations/parents', 2, NULL, NULL, NULL, 1565702601, 1565702601),
('/relations/read', 2, NULL, NULL, NULL, 1565702383, 1565702383),
('/relations/relations', 2, NULL, NULL, NULL, 1561473598, 1561473598),
('/relations/update', 2, NULL, NULL, NULL, 1571212807, 1571212807),
('/relations/update_relations', 2, NULL, NULL, NULL, 1561564302, 1561564302),
('/select-broker/*', 2, NULL, NULL, NULL, 1572960038, 1572960038),
('/select-broker/brokers', 2, NULL, NULL, NULL, 1573034050, 1573034050),
('/select-broker/create', 2, NULL, NULL, NULL, 1572960013, 1572960013),
('/select-broker/index', 2, NULL, NULL, NULL, 1572958394, 1572958394),
('/select-broker/view', 2, NULL, NULL, NULL, 1572960030, 1572960030),
('/traders/index', 2, NULL, NULL, NULL, 1564145816, 1564145816),
('/traders/read', 2, NULL, NULL, NULL, 1561717439, 1561717439),
('/trades/index', 2, NULL, NULL, NULL, 1571122482, 1571122482),
('/trades/masters', 2, NULL, NULL, NULL, 1571123148, 1571123148),
('/trades/read', 2, NULL, NULL, NULL, 1571122488, 1571122488),
('/trades/read-master', 2, NULL, NULL, NULL, 1571121941, 1571121941),
('/trading/rating', 2, NULL, NULL, NULL, 1562327938, 1562327938),
('/translatemanager/*', 2, NULL, NULL, NULL, 1564574133, 1564574133),
('/translatemanager/language/*', 2, NULL, NULL, NULL, 1564574301, 1564574301),
('/translatemanager/language/change-status', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/create', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/delete', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/delete-source', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/dialog', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/error', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/export', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/import', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/list', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/message', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/optimizer', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/save', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/scan', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/translate', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/update', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/translatemanager/language/view', 2, NULL, NULL, NULL, 1565247601, 1565247601),
('/users/create', 2, NULL, NULL, NULL, 1564145502, 1564145502),
('/users/index', 2, NULL, NULL, NULL, 1564145584, 1564145584),
('/users/set', 2, NULL, NULL, NULL, 1574079449, 1574079449),
('/users/update', 2, NULL, NULL, NULL, 1564145670, 1564145670),
('/users/view', 2, NULL, NULL, NULL, 1564145512, 1564145512),
('admin', 1, 'Admin', NULL, NULL, 1552036239, 1560869010),
('admin_copy', 2, NULL, NULL, NULL, 1560757142, 1560757142),
('client', 1, 'Client', NULL, NULL, 1555422648, 1560869054),
('copy_admin_accounts_deposit', 2, 'Accounts Deposits', NULL, NULL, 1571055230, 1571055230),
('copy_admin_add_accounts', 2, 'Add Accounts', NULL, NULL, 1559809938, 1560176874),
('copy_admin_columns', 2, 'Show Columns', NULL, NULL, 1571040974, 1571040974),
('copy_admin_delete_accounts', 2, 'Delete Accounts', NULL, NULL, 1559809349, 1560176895),
('copy_admin_destroy_trades', 2, 'Delete Trades', NULL, NULL, 1561359346, 1561359346),
('copy_admin_refresh_accounts', 2, 'refresh accounts', NULL, NULL, 1571039825, 1571039936),
('copy_admin_update_trades', 2, 'Update Trades', NULL, NULL, 1561359322, 1561359322),
('copy_admin_view_group', 2, 'View Group', NULL, NULL, 1559806608, 1560176921),
('copy_client_edit_copytypes', 2, 'Edit CopyTypes', NULL, NULL, 1559828734, 1560176853),
('copy_ib_active_accounts', 2, 'Show Active Accounts', NULL, NULL, 1571041450, 1571041640),
('copy_ib_delete_relations', 2, 'Delete Relations', NULL, NULL, 1559829664, 1560176942),
('copy_ib_edit_account_type', 2, 'Edit Accounts Type', NULL, NULL, 1571042688, 1571042708),
('copy_ib_edit_master_account', 2, 'Edit Master Account', NULL, NULL, 1559829998, 1560176975),
('copy_ib_sync_accounts', 2, 'Synchronize accounts', NULL, NULL, 1571043589, 1571043589),
('copy_ib_users_edits', 2, 'Users Accounts', NULL, NULL, 1571055517, 1571055517),
('copy_ib_view_relations', 2, 'View Relations', NULL, NULL, 1559829305, 1560237730),
('en/admin/route/*', 2, NULL, NULL, NULL, NULL, NULL),
('en/route/*', 2, NULL, NULL, NULL, NULL, NULL),
('ib', 1, 'IB', NULL, NULL, 1552036253, 1560869043),
('ib_copy', 2, NULL, NULL, NULL, 1560757131, 1560757131),
('super admin', 1, 'Super Admin', NULL, NULL, 1552036268, 1560869086);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('access own permission', '/users/view'),
('admin', '/accounts/*'),
('admin', '/accounts/accounts'),
('admin', '/accounts/captcha'),
('admin', '/accounts/create'),
('admin', '/accounts/destroy'),
('admin', '/accounts/drop'),
('admin', '/accounts/error'),
('admin', '/accounts/index'),
('admin', '/accounts/master'),
('admin', '/accounts/read'),
('admin', '/accounts/refresh-accounts'),
('admin', '/accounts/update'),
('admin', '/accounts/update-account'),
('admin', '/accounts/users'),
('admin', '/companies/index'),
('admin', '/leaderboard/ph'),
('admin', '/mam/create'),
('admin', '/mam/destroy'),
('admin', '/mam/index'),
('admin', '/mam/read'),
('admin', '/mam/update'),
('admin', '/mamgram/*'),
('admin', '/mamgram/captcha'),
('admin', '/mamgram/configuration'),
('admin', '/mamgram/create'),
('admin', '/mamgram/destroy'),
('admin', '/mamgram/error'),
('admin', '/mamgram/index'),
('admin', '/mamgram/read'),
('admin', '/mamgram/update'),
('admin', '/managers/*'),
('admin', '/managers/active'),
('admin', '/managers/create'),
('admin', '/managers/delete'),
('admin', '/managers/index'),
('admin', '/managers/update'),
('admin', '/managers/users-list'),
('admin', '/managers/view'),
('admin', '/operation/balance'),
('admin', '/operation/managers-list'),
('admin', '/relations/delete'),
('admin', '/relations/display'),
('admin', '/relations/link'),
('admin', '/relations/parents'),
('admin', '/relations/read'),
('admin', '/relations/relations'),
('admin', '/relations/type'),
('admin', '/relations/update'),
('admin', '/relations/update_relations'),
('admin', '/select-broker/*'),
('admin', '/select-broker/brokers'),
('admin', '/select-broker/create'),
('admin', '/select-broker/index'),
('admin', '/select-broker/view'),
('admin', '/site/*'),
('admin', '/site/about'),
('admin', '/site/captcha'),
('admin', '/site/contact'),
('admin', '/site/error'),
('admin', '/site/index'),
('admin', '/site/login'),
('admin', '/site/logout'),
('admin', '/traders/index'),
('admin', '/traders/read'),
('admin', '/trades/index'),
('admin', '/trades/masters'),
('admin', '/trades/read'),
('admin', '/trades/read-master'),
('admin', '/trading/rating'),
('admin', '/translatemanager/*'),
('admin', '/translatemanager/language/*'),
('admin', '/translatemanager/language/change-status'),
('admin', '/translatemanager/language/create'),
('admin', '/translatemanager/language/delete'),
('admin', '/translatemanager/language/delete-source'),
('admin', '/translatemanager/language/dialog'),
('admin', '/translatemanager/language/error'),
('admin', '/translatemanager/language/export'),
('admin', '/translatemanager/language/import'),
('admin', '/translatemanager/language/list'),
('admin', '/translatemanager/language/message'),
('admin', '/translatemanager/language/optimizer'),
('admin', '/translatemanager/language/save'),
('admin', '/translatemanager/language/scan'),
('admin', '/translatemanager/language/translate'),
('admin', '/translatemanager/language/update'),
('admin', '/translatemanager/language/view'),
('admin', '/users/*'),
('admin', '/users/create'),
('admin', '/users/delete'),
('admin', '/users/index'),
('admin', '/users/update'),
('admin', '/users/view'),
('admin', 'add_accounts'),
('admin', 'admin_copy'),
('admin', 'copy_ib_view_relations'),
('admin', 'delete_accounts'),
('admin', 'delete_relations'),
('admin', 'edit_copytypes'),
('admin', 'edit_master_account'),
('admin', 'ib'),
('admin', 'view_group'),
('admin', 'view_relations'),
('balance access', '/operation/*'),
('balance access', '/operation/balance'),
('balance access', '/operation/delete'),
('balance access', '/operation/managers-list'),
('client', '/traders/index'),
('client', '/traders/read'),
('client', '/trading/rating'),
('client', '/users/password'),
('client', '/users/set'),
('client', '/users/update'),
('client', '/users/view'),
('credit_perm', 'balance access'),
('ib', '/credit/managers-list'),
('ib', '/debug/*'),
('ib', '/debug/default/*'),
('ib', '/debug/default/db-explain'),
('ib', '/debug/default/download-mail'),
('ib', '/debug/default/index'),
('ib', '/debug/default/toolbar'),
('ib', '/debug/default/view'),
('ib', '/debug/user/*'),
('ib', '/debug/user/reset-identity'),
('ib', '/debug/user/set-identity'),
('ib', '/mam/read'),
('ib', '/managers/create'),
('ib', '/managers/index'),
('ib', '/managers/users-list'),
('ib', '/permission/permissions'),
('ib', '/permissions/index'),
('ib', '/permissions/permissions'),
('ib', '/permissions/permissions/read'),
('ib', '/permissions/permissions/readp'),
('ib', '/permissions/read'),
('ib', '/permissions/readp'),
('ib', '/permissions/updatep'),
('ib', '/relations/delete'),
('ib', '/relations/link'),
('ib', '/relations/parents'),
('ib', '/relations/type'),
('ib', '/site/about'),
('ib', '/site/contact'),
('ib', '/site/index'),
('ib', '/site/login'),
('ib', '/site/logout'),
('ib', '/traders/index'),
('ib', '/traders/read'),
('ib', '/trades/read-master'),
('ib', '/trading/rating'),
('ib', '/translatemanager/*'),
('ib', '/translatemanager/language/*'),
('ib', '/translatemanager/language/change-status'),
('ib', '/translatemanager/language/create'),
('ib', '/translatemanager/language/delete'),
('ib', '/translatemanager/language/delete-source'),
('ib', '/translatemanager/language/dialog'),
('ib', '/translatemanager/language/error'),
('ib', '/translatemanager/language/export'),
('ib', '/translatemanager/language/import'),
('ib', '/translatemanager/language/list'),
('ib', '/translatemanager/language/message'),
('ib', '/translatemanager/language/optimizer'),
('ib', '/translatemanager/language/save'),
('ib', '/translatemanager/language/scan'),
('ib', '/translatemanager/language/translate'),
('ib', '/translatemanager/language/update'),
('ib', '/translatemanager/language/view'),
('ib', '/users/*'),
('ib', '/users/create'),
('ib', '/users/index'),
('ib', '/users/password'),
('ib', 'ib_copy'),
('mam_access', '/mam/read'),
('mam_access', '/mam/update'),
('manager', 'access own permission'),
('manager', 'balance access'),
('manager', 'managers own access'),
('managers own access', '/managers/update'),
('managers own access', '/managers/view'),
('super admin', '/*'),
('super admin', '/accounts/*'),
('super admin', '/accounts/accounts'),
('super admin', '/accounts/create'),
('super admin', '/accounts/destroy'),
('super admin', '/accounts/drop'),
('super admin', '/accounts/index'),
('super admin', '/accounts/master'),
('super admin', '/accounts/read'),
('super admin', '/accounts/refresh-accounts'),
('super admin', '/accounts/update'),
('super admin', '/accounts/update-account'),
('super admin', '/accounts/users'),
('super admin', '/admin/*'),
('super admin', '/admin/assignment/*'),
('super admin', '/admin/assignment/assign'),
('super admin', '/admin/assignment/index'),
('super admin', '/admin/assignment/revoke'),
('super admin', '/admin/assignment/view'),
('super admin', '/admin/default/*'),
('super admin', '/admin/default/index'),
('super admin', '/admin/menu/*'),
('super admin', '/admin/menu/create'),
('super admin', '/admin/menu/delete'),
('super admin', '/admin/menu/index'),
('super admin', '/admin/menu/update'),
('super admin', '/admin/menu/view'),
('super admin', '/admin/permission/*'),
('super admin', '/admin/permission/assign'),
('super admin', '/admin/permission/create'),
('super admin', '/admin/permission/delete'),
('super admin', '/admin/permission/index'),
('super admin', '/admin/permission/remove'),
('super admin', '/admin/permission/update'),
('super admin', '/admin/permission/view'),
('super admin', '/admin/role/*'),
('super admin', '/admin/role/assign'),
('super admin', '/admin/role/create'),
('super admin', '/admin/role/delete'),
('super admin', '/admin/role/index'),
('super admin', '/admin/role/remove'),
('super admin', '/admin/role/update'),
('super admin', '/admin/role/view'),
('super admin', '/admin/route/*'),
('super admin', '/admin/route/assign'),
('super admin', '/admin/route/create'),
('super admin', '/admin/route/index'),
('super admin', '/admin/route/refresh'),
('super admin', '/admin/route/remove'),
('super admin', '/admin/rule/*'),
('super admin', '/admin/rule/create'),
('super admin', '/admin/rule/delete'),
('super admin', '/admin/rule/index'),
('super admin', '/admin/rule/update'),
('super admin', '/admin/rule/view'),
('super admin', '/admin/user/*'),
('super admin', '/admin/user/activate'),
('super admin', '/admin/user/change-password'),
('super admin', '/admin/user/delete'),
('super admin', '/admin/user/index'),
('super admin', '/admin/user/login'),
('super admin', '/admin/user/logout'),
('super admin', '/admin/user/request-password-reset'),
('super admin', '/admin/user/reset-password'),
('super admin', '/admin/user/signup'),
('super admin', '/admin/user/view'),
('super admin', '/codes-type/*'),
('super admin', '/codes-type/index'),
('super admin', '/codes/*'),
('super admin', '/codes/create'),
('super admin', '/codes/delete'),
('super admin', '/codes/index'),
('super admin', '/codes/update'),
('super admin', '/codes/view'),
('super admin', '/companies/index'),
('super admin', '/credit/*'),
('super admin', '/credit/create'),
('super admin', '/credit/credit'),
('super admin', '/credit/delete'),
('super admin', '/credit/index'),
('super admin', '/credit/managers-list'),
('super admin', '/credit/update'),
('super admin', '/credit/view'),
('super admin', '/debug/*'),
('super admin', '/debug/default/*'),
('super admin', '/debug/default/db-explain'),
('super admin', '/debug/default/download-mail'),
('super admin', '/debug/default/index'),
('super admin', '/debug/default/toolbar'),
('super admin', '/debug/default/view'),
('super admin', '/debug/user/*'),
('super admin', '/debug/user/reset-identity'),
('super admin', '/debug/user/set-identity'),
('super admin', '/email-templates/*'),
('super admin', '/email-templates/default/*'),
('super admin', '/email-templates/default/create'),
('super admin', '/email-templates/default/delete'),
('super admin', '/email-templates/default/index'),
('super admin', '/email-templates/default/update'),
('super admin', '/email-templates/default/view'),
('super admin', '/gii/*'),
('super admin', '/gii/default/*'),
('super admin', '/gii/default/action'),
('super admin', '/gii/default/diff'),
('super admin', '/gii/default/index'),
('super admin', '/gii/default/preview'),
('super admin', '/gii/default/view'),
('super admin', '/language/*'),
('super admin', '/language/index'),
('super admin', '/mam/*'),
('super admin', '/mam/configuration'),
('super admin', '/mam/create'),
('super admin', '/mam/destroy'),
('super admin', '/mam/index'),
('super admin', '/mam/read'),
('super admin', '/mam/update'),
('super admin', '/mamgram/*'),
('super admin', '/mamgram/captcha'),
('super admin', '/mamgram/configuration'),
('super admin', '/mamgram/create'),
('super admin', '/mamgram/destroy'),
('super admin', '/mamgram/error'),
('super admin', '/mamgram/index'),
('super admin', '/mamgram/read'),
('super admin', '/mamgram/update'),
('super admin', '/managers/*'),
('super admin', '/managers/active'),
('super admin', '/managers/create'),
('super admin', '/managers/delete'),
('super admin', '/managers/index'),
('super admin', '/managers/update'),
('super admin', '/managers/users-list'),
('super admin', '/managers/view'),
('super admin', '/operation/*'),
('super admin', '/operation/balance'),
('super admin', '/operation/delete'),
('super admin', '/operation/managers-list'),
('super admin', '/permission/*'),
('super admin', '/permission/permissions'),
('super admin', '/permissions/*'),
('super admin', '/permissions/index'),
('super admin', '/permissions/permissions'),
('super admin', '/permissions/permissions/read'),
('super admin', '/permissions/permissions/readp'),
('super admin', '/permissions/read'),
('super admin', '/permissions/readp'),
('super admin', '/permissions/updatep'),
('super admin', '/relations/display'),
('super admin', '/relations/relations'),
('super admin', '/relations/update_relations'),
('super admin', '/select-broker/*'),
('super admin', '/select-broker/brokers'),
('super admin', '/select-broker/create'),
('super admin', '/select-broker/index'),
('super admin', '/select-broker/view'),
('super admin', '/site/*'),
('super admin', '/site/about'),
('super admin', '/site/captcha'),
('super admin', '/site/contact'),
('super admin', '/site/error'),
('super admin', '/site/index'),
('super admin', '/site/login'),
('super admin', '/site/logout'),
('super admin', '/traders/index'),
('super admin', '/traders/read'),
('super admin', '/trades/index'),
('super admin', '/trades/masters'),
('super admin', '/trades/read'),
('super admin', '/trades/read-master'),
('super admin', '/trading/rating'),
('super admin', '/translatemanager/*'),
('super admin', '/translatemanager/language/*'),
('super admin', '/translatemanager/language/change-status'),
('super admin', '/translatemanager/language/create'),
('super admin', '/translatemanager/language/delete'),
('super admin', '/translatemanager/language/delete-source'),
('super admin', '/translatemanager/language/dialog'),
('super admin', '/translatemanager/language/error'),
('super admin', '/translatemanager/language/export'),
('super admin', '/translatemanager/language/import'),
('super admin', '/translatemanager/language/list'),
('super admin', '/translatemanager/language/message'),
('super admin', '/translatemanager/language/optimizer'),
('super admin', '/translatemanager/language/save'),
('super admin', '/translatemanager/language/scan'),
('super admin', '/translatemanager/language/translate'),
('super admin', '/translatemanager/language/update'),
('super admin', '/translatemanager/language/view'),
('super admin', '/users/*'),
('super admin', '/users/create'),
('super admin', '/users/delete'),
('super admin', '/users/index'),
('super admin', '/users/password'),
('super admin', '/users/update'),
('super admin', '/users/view'),
('super admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('credit rule', 0x4f3a32303a226170705c72756c65735c43726564697452756c65223a333a7b733a343a226e616d65223b733a31313a226372656469742072756c65223b733a393a22637265617465644174223b693a313535333137323330373b733a393a22757064617465644174223b693a313535333137323330373b7d, 1553172307, 1553172307),
('managers access', 0x4f3a32383a226170705c72756c65735c4d616e616765727341636365737352756c65223a333a7b733a343a226e616d65223b733a31353a226d616e616765727320616363657373223b733a393a22637265617465644174223b693a313535323339383133373b733a393a22757064617465644174223b693a313535323339383133373b7d, 1552398137, 1552398137),
('own access', 0x4f3a32343a226170705c72756c65735c5573657241636365737352756c65223a333a7b733a343a226e616d65223b733a31303a226f776e20616363657373223b733a393a22637265617465644174223b693a313535323034363133393b733a393a22757064617465644174223b693a313535323035313135363b7d, 1552046139, 1552051156);

-- --------------------------------------------------------

--
-- Table structure for table `brokers`
--

DROP TABLE IF EXISTS `brokers`;
CREATE TABLE IF NOT EXISTS `brokers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_name` varchar(100) DEFAULT NULL,
  `image_location` varchar(2083) DEFAULT NULL,
  `platforms` varchar(100) NOT NULL,
  `regulated_in` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `min_deposit` int(11) NOT NULL,
  `supp_currencies` varchar(100) NOT NULL,
  `commissions` float NOT NULL,
  `is_paid` tinyint(4) NOT NULL DEFAULT 0,
  `amount_paid` double DEFAULT NULL,
  `website` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `broker_name` (`broker_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brokers`
--

INSERT INTO `brokers` (`id`, `broker_name`, `image_location`, `platforms`, `regulated_in`, `min_deposit`, `supp_currencies`, `commissions`, `is_paid`, `amount_paid`, `website`) VALUES
(1, 'xm', '', '[\"mt4\"]', 'Europe', 300, '[\"EUR\",\"JPY\"]', 1.33, 1, 200, NULL),
(2, 'fxgrow', 'fxgrow.png', 'mt4', 'Europe', 500, 'USD', 5, 1, 9000, 'https://secure.fxgrow.com/register'),
(3, 'fxtm', 'fxtm.png', 'mt4', 'Europe', 300, 'PLN', 1.9, 1, 1000, NULL),
(4, 'fxpro', 'fxpro.png', 'mt4', 'Europe', 100, 'PLN', 2, 1, 2000, NULL),
(5, 'New Broker2', '', '[\"mt4\",\"mt5\"]', 'Europe', 10002, '[\"EUR\",\"USD\",\"GBP\",\"JPY\",\"AUD\",\"CAD\",\"PLN\"]', 12, 1, 200, 'dddd');

-- --------------------------------------------------------

--
-- Table structure for table `broker_details`
--

DROP TABLE IF EXISTS `broker_details`;
CREATE TABLE IF NOT EXISTS `broker_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) DEFAULT NULL,
  `key` varchar(128) DEFAULT NULL,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bk_index_idd` (`broker_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `broker_details`
--

INSERT INTO `broker_details` (`id`, `broker_id`, `key`, `value`) VALUES
(27, NULL, 'currency', 'fxtest'),
(28, NULL, 'currency', 'beirut'),
(29, NULL, 'currency', 'usd'),
(30, NULL, 'currency', 'fxtest'),
(31, NULL, 'currency', 'beirut'),
(32, NULL, 'currency', 'usd'),
(33, NULL, 'currency', 'tetsing'),
(34, NULL, 'currency', 'verdun'),
(35, NULL, 'currency', 'usd'),
(36, NULL, 'currency', 'fxtesthnbvbv'),
(37, NULL, 'currency', 'beirut'),
(38, NULL, 'currency', 'usd'),
(39, NULL, 'currency', 'fxtesthnbvbv'),
(40, NULL, 'currency', 'beirut'),
(41, NULL, 'currency', 'usd'),
(42, NULL, 'currency', 'fxtestinhgftge'),
(43, NULL, 'currency', 'beirut'),
(44, NULL, 'currency', 'usd'),
(45, 4, 'name', 'fxpro'),
(46, 4, 'location', 'cyprus'),
(47, 4, 'currency', 'usd'),
(48, 2, 'name', 'fxgrow'),
(49, 2, 'location', 'verdun'),
(50, 2, 'currency', 'usd'),
(51, 1, 'name', 'aaa'),
(52, 1, 'location', 'beirut'),
(53, 1, 'currency', 'usd'),
(54, NULL, NULL, NULL),
(55, NULL, NULL, NULL),
(56, NULL, NULL, NULL),
(57, 5, 'name', '2'),
(58, 5, 'location', '2'),
(59, 5, 'currency', 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

DROP TABLE IF EXISTS `codes`;
CREATE TABLE IF NOT EXISTS `codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET latin1 NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `code_type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code_type_id` (`code_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `code`, `name`, `code_type_id`, `description`, `is_active`) VALUES
(1, 'AF', 'Afghanistan', 4, '', 1),
(2, 'AL', 'Albania', 4, '', 1),
(3, 'DZ', 'Algeria', 4, '', 1),
(4, 'DS', 'American Samoa', 4, '', 1),
(5, 'AD', 'Andorra', 4, '', 1),
(6, 'AO', 'Angola', 4, '', 1),
(7, 'AI', 'Anguilla', 4, '', 1),
(8, 'AQ', 'Antarctica', 4, '', 1),
(9, 'AG', 'Antigua and Barbuda', 4, '', 1),
(10, 'AR', 'Argentina', 4, '', 1),
(11, 'AM', 'Armenia', 4, '', 1),
(12, 'AW', 'Aruba', 4, '', 1),
(13, 'AU', 'Australia', 4, '', 1),
(14, 'AT', 'Austria', 4, '', 1),
(15, 'AZ', 'Azerbaijan', 4, '', 1),
(16, 'BS', 'Bahamas', 4, '', 1),
(17, 'BH', 'Bahrain', 4, '', 1),
(18, 'BD', 'Bangladesh', 4, '', 1),
(19, 'BB', 'Barbados', 4, '', 1),
(20, 'BY', 'Belarus', 4, '', 1),
(21, 'BE', 'Belgium', 4, '', 1),
(22, 'BZ', 'Belize', 4, '', 1),
(23, 'BJ', 'Benin', 4, '', 1),
(24, 'BM', 'Bermuda', 4, '', 1),
(25, 'BT', 'Bhutan', 4, '', 1),
(26, 'BO', 'Bolivia', 4, '', 1),
(27, 'BA', 'Bosnia and Herzegovina', 4, '', 1),
(28, 'BW', 'Botswana', 4, '', 1),
(29, 'BV', 'Bouvet Island', 4, '', 1),
(30, 'BR', 'Brazil', 4, '', 1),
(31, 'IO', 'British Indian Ocean Territory', 4, '', 1),
(32, 'BN', 'Brunei Darussalam', 4, '', 1),
(33, 'BG', 'Bulgaria', 4, '', 1),
(34, 'BF', 'Burkina Faso', 4, '', 1),
(35, 'BI', 'Burundi', 4, '', 1),
(36, 'KH', 'Cambodia', 4, '', 1),
(37, 'CM', 'Cameroon', 4, '', 1),
(38, 'CA', 'Canada', 4, '', 1),
(39, 'CV', 'Cape Verde', 4, '', 1),
(40, 'KY', 'Cayman Islands', 4, '', 1),
(41, 'CF', 'Central African Republic', 4, '', 1),
(42, 'TD', 'Chad', 4, '', 1),
(43, 'CL', 'Chile', 4, '', 1),
(44, 'CN', 'China', 4, '', 1),
(45, 'CX', 'Christmas Island', 4, '', 1),
(46, 'CC', 'Cocos (Keeling) Islands', 4, '', 1),
(47, 'CO', 'Colombia', 4, '', 1),
(48, 'KM', 'Comoros', 4, '', 1),
(49, 'CD', 'Democratic Republic of the Congo', 4, '', 1),
(50, 'CG', 'Republic of Congo', 4, '', 1),
(51, 'CK', 'Cook Islands', 4, '', 1),
(52, 'CR', 'Costa Rica', 4, '', 1),
(53, 'HR', 'Croatia (Hrvatska)', 4, '', 1),
(54, 'CU', 'Cuba', 4, '', 1),
(55, 'CY', 'Cyprus', 4, '', 1),
(56, 'CZ', 'Czech Republic', 4, '', 1),
(57, 'DK', 'Denmark', 4, '', 1),
(58, 'DJ', 'Djibouti', 4, '', 1),
(59, 'DM', 'Dominica', 4, '', 1),
(60, 'DO', 'Dominican Republic', 4, '', 1),
(61, 'TP', 'East Timor', 4, '', 1),
(62, 'EC', 'Ecuador', 4, '', 1),
(63, 'EG', 'Egypt', 4, '', 1),
(64, 'SV', 'El Salvador', 4, '', 1),
(65, 'GQ', 'Equatorial Guinea', 4, '', 1),
(66, 'ER', 'Eritrea', 4, '', 1),
(67, 'EE', 'Estonia', 4, '', 1),
(68, 'ET', 'Ethiopia', 4, '', 1),
(69, 'FK', 'Falkland Islands (Malvinas)', 4, '', 1),
(70, 'FO', 'Faroe Islands', 4, '', 1),
(71, 'FJ', 'Fiji', 4, '', 1),
(72, 'FI', 'Finland', 4, '', 1),
(73, 'FR', 'France', 4, '', 1),
(74, 'FX', 'France, Metropolitan', 4, '', 1),
(75, 'GF', 'French Guiana', 4, '', 1),
(76, 'PF', 'French Polynesia', 4, '', 1),
(77, 'TF', 'French Southern Territories', 4, '', 1),
(78, 'GA', 'Gabon', 4, '', 1),
(79, 'GM', 'Gambia', 4, '', 1),
(80, 'GE', 'Georgia', 4, '', 1),
(81, 'DE', 'Germany', 4, '', 1),
(82, 'GH', 'Ghana', 4, '', 1),
(83, 'GI', 'Gibraltar', 4, '', 1),
(84, 'GK', 'Guernsey', 4, '', 1),
(85, 'GR', 'Greece', 4, '', 1),
(86, 'GL', 'Greenland', 4, '', 1),
(87, 'GD', 'Grenada', 4, '', 1),
(88, 'GP', 'Guadeloupe', 4, '', 1),
(89, 'GU', 'Guam', 4, '', 1),
(90, 'GT', 'Guatemala', 4, '', 1),
(91, 'GN', 'Guinea', 4, '', 1),
(92, 'GW', 'Guinea-Bissau', 4, '', 1),
(93, 'GY', 'Guyana', 4, '', 1),
(94, 'HT', 'Haiti', 4, '', 1),
(95, 'HM', 'Heard and Mc Donald Islands', 4, '', 1),
(96, 'HN', 'Honduras', 4, '', 1),
(97, 'HK', 'Hong Kong', 4, '', 1),
(98, 'HU', 'Hungary', 4, '', 1),
(99, 'IS', 'Iceland', 4, '', 1),
(100, 'IN', 'India', 4, '', 1),
(101, 'IM', 'Isle of Man', 4, '', 1),
(102, 'ID', 'Indonesia', 4, '', 1),
(103, 'IR', 'Iran (Islamic Republic of)', 4, '', 1),
(104, 'IQ', 'Iraq', 4, '', 1),
(105, 'IE', 'Ireland', 4, '', 1),
(106, 'IL', 'Israel', 4, '', 1),
(107, 'IT', 'Italy', 4, '', 1),
(108, 'CI', 'Ivory Coast', 4, '', 1),
(109, 'JE', 'Jersey', 4, '', 1),
(110, 'JM', 'Jamaica', 4, '', 1),
(111, 'JP', 'Japan', 4, '', 1),
(112, 'JO', 'Jordan', 4, '', 1),
(113, 'KZ', 'Kazakhstan', 4, '', 1),
(114, 'KE', 'Kenya', 4, '', 1),
(115, 'KI', 'Kiribati', 4, '', 1),
(116, 'KP', 'Korea, Democratic People\'s Republic of', 4, '', 1),
(117, 'KR', 'Korea, Republic of', 4, '', 1),
(118, 'XK', 'Kosovo', 4, '', 1),
(119, 'KW', 'Kuwait', 4, '', 1),
(120, 'KG', 'Kyrgyzstan', 4, '', 1),
(121, 'LA', 'Lao People\'s Democratic Republic', 4, '', 1),
(122, 'LV', 'Latvia', 4, '', 1),
(123, 'LB', 'Lebanon', 4, '', 1),
(124, 'LS', 'Lesotho', 4, '', 1),
(125, 'LR', 'Liberia', 4, '', 1),
(126, 'LY', 'Libyan Arab Jamahiriya', 4, '', 1),
(127, 'LI', 'Liechtenstein', 4, '', 1),
(128, 'LT', 'Lithuania', 4, '', 1),
(129, 'LU', 'Luxembourg', 4, '', 1),
(130, 'MO', 'Macau', 4, '', 1),
(131, 'MK', 'North Macedonia', 4, '', 1),
(132, 'MG', 'Madagascar', 4, '', 1),
(133, 'MW', 'Malawi', 4, '', 1),
(134, 'MY', 'Malaysia', 4, '', 1),
(135, 'MV', 'Maldives', 4, '', 1),
(136, 'ML', 'Mali', 4, '', 1),
(137, 'MT', 'Malta', 4, '', 1),
(138, 'MH', 'Marshall Islands', 4, '', 1),
(139, 'MQ', 'Martinique', 4, '', 1),
(140, 'MR', 'Mauritania', 4, '', 1),
(141, 'MU', 'Mauritius', 4, '', 1),
(142, 'TY', 'Mayotte', 4, '', 1),
(143, 'MX', 'Mexico', 4, '', 1),
(144, 'FM', 'Micronesia, Federated States of', 4, '', 1),
(145, 'MD', 'Moldova, Republic of', 4, '', 1),
(146, 'MC', 'Monaco', 4, '', 1),
(147, 'MN', 'Mongolia', 4, '', 1),
(148, 'ME', 'Montenegro', 4, '', 1),
(149, 'MS', 'Montserrat', 4, '', 1),
(150, 'MA', 'Morocco', 4, '', 1),
(151, 'MZ', 'Mozambique', 4, '', 1),
(152, 'MM', 'Myanmar', 4, '', 1),
(153, 'NA', 'Namibia', 4, '', 1),
(154, 'NR', 'Nauru', 4, '', 1),
(155, 'NP', 'Nepal', 4, '', 1),
(156, 'NL', 'Netherlands', 4, '', 1),
(157, 'AN', 'Netherlands Antilles', 4, '', 1),
(158, 'NC', 'New Caledonia', 4, '', 1),
(159, 'NZ', 'New Zealand', 4, '', 1),
(160, 'NI', 'Nicaragua', 4, '', 1),
(161, 'NE', 'Niger', 4, '', 1),
(162, 'NG', 'Nigeria', 4, '', 1),
(163, 'NU', 'Niue', 4, '', 1),
(164, 'NF', 'Norfolk Island', 4, '', 1),
(165, 'MP', 'Northern Mariana Islands', 4, '', 1),
(166, 'NO', 'Norway', 4, '', 1),
(167, 'OM', 'Oman', 4, '', 1),
(168, 'PK', 'Pakistan', 4, '', 1),
(169, 'PW', 'Palau', 4, '', 1),
(170, 'PS', 'Palestine', 4, '', 1),
(171, 'PA', 'Panama', 4, '', 1),
(172, 'PG', 'Papua New Guinea', 4, '', 1),
(173, 'PY', 'Paraguay', 4, '', 1),
(174, 'PE', 'Peru', 4, '', 1),
(175, 'PH', 'Philippines', 4, '', 1),
(176, 'PN', 'Pitcairn', 4, '', 1),
(177, 'PL', 'Poland', 4, '', 1),
(178, 'PT', 'Portugal', 4, '', 1),
(179, 'PR', 'Puerto Rico', 4, '', 1),
(180, 'QA', 'Qatar', 4, '', 1),
(181, 'RE', 'Reunion', 4, '', 1),
(182, 'RO', 'Romania', 4, '', 1),
(183, 'RU', 'Russian Federation', 4, '', 1),
(184, 'RW', 'Rwanda', 4, '', 1),
(185, 'KN', 'Saint Kitts and Nevis', 4, '', 1),
(186, 'LC', 'Saint Lucia', 4, '', 1),
(187, 'VC', 'Saint Vincent and the Grenadines', 4, '', 1),
(188, 'WS', 'Samoa', 4, '', 1),
(189, 'SM', 'San Marino', 4, '', 1),
(190, 'ST', 'Sao Tome and Principe', 4, '', 1),
(191, 'SA', 'Saudi Arabia', 4, '', 1),
(192, 'SN', 'Senegal', 4, '', 1),
(193, 'RS', 'Serbia', 4, '', 1),
(194, 'SC', 'Seychelles', 4, '', 1),
(195, 'SL', 'Sierra Leone', 4, '', 1),
(196, 'SG', 'Singapore', 4, '', 1),
(197, 'SK', 'Slovakia', 4, '', 1),
(198, 'SI', 'Slovenia', 4, '', 1),
(199, 'SB', 'Solomon Islands', 4, '', 1),
(200, 'SO', 'Somalia', 4, '', 1),
(201, 'ZA', 'South Africa', 4, '', 1),
(202, 'GS', 'South Georgia South Sandwich Islands', 4, '', 1),
(203, 'SS', 'South Sudan', 4, '', 1),
(204, 'ES', 'Spain', 4, '', 1),
(205, 'LK', 'Sri Lanka', 4, '', 1),
(206, 'SH', 'St. Helena', 4, '', 1),
(207, 'PM', 'St. Pierre and Miquelon', 4, '', 1),
(208, 'SD', 'Sudan', 4, '', 1),
(209, 'SR', 'Suriname', 4, '', 1),
(210, 'SJ', 'Svalbard and Jan Mayen Islands', 4, '', 1),
(211, 'SZ', 'Swaziland', 4, '', 1),
(212, 'SE', 'Sweden', 4, '', 1),
(213, 'CH', 'Switzerland', 4, '', 1),
(214, 'SY', 'Syrian Arab Republic', 4, '', 1),
(215, 'TW', 'Taiwan', 4, '', 1),
(216, 'TJ', 'Tajikistan', 4, '', 1),
(217, 'TZ', 'Tanzania, United Republic of', 4, '', 1),
(218, 'TH', 'Thailand', 4, '', 1),
(219, 'TG', 'Togo', 4, '', 1),
(220, 'TK', 'Tokelau', 4, '', 1),
(221, 'TO', 'Tonga', 4, '', 1),
(222, 'TT', 'Trinidad and Tobago', 4, '', 1),
(223, 'TN', 'Tunisia', 4, '', 1),
(224, 'TR', 'Turkey', 4, '', 1),
(225, 'TM', 'Turkmenistan', 4, '', 1),
(226, 'TC', 'Turks and Caicos Islands', 4, '', 1),
(227, 'TV', 'Tuvalu', 4, '', 1),
(228, 'UG', 'Uganda', 4, '', 1),
(229, 'UA', 'Ukraine', 4, '', 1),
(230, 'AE', 'United Arab Emirates', 4, '', 1),
(231, 'GB', 'United Kingdom', 4, '', 1),
(232, 'US', 'United States', 4, '', 1),
(233, 'UM', 'United States minor outlying islands', 4, '', 1),
(234, 'UY', 'Uruguay', 4, '', 1),
(235, 'UZ', 'Uzbekistan', 4, '', 1),
(236, 'VU', 'Vanuatu', 4, '', 1),
(237, 'VA', 'Vatican City State', 4, '', 1),
(238, 'VE', 'Venezuela', 4, '', 1),
(239, 'VN', 'Vietnam', 4, '', 1),
(240, 'VG', 'Virgin Islands (British)', 4, '', 1),
(241, 'VI', 'Virgin Islands (U.S.)', 4, '', 1),
(242, 'WF', 'Wallis and Futuna Islands', 4, '', 1),
(243, 'EH', 'Western Sahara', 4, '', 1),
(244, 'YE', 'Yemen', 4, '', 1),
(245, 'ZM', 'Zambia', 4, '', 1),
(246, 'ZW', 'Zimbabwe', 4, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `codes_type`
--

DROP TABLE IF EXISTS `codes_type`;
CREATE TABLE IF NOT EXISTS `codes_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `codes_type`
--

INSERT INTO `codes_type` (`id`, `code`, `name`, `description`) VALUES
(1, 'U_TY', 'User Type', 'type of registered users'),
(2, 'U_ST', 'User Status', 'status of registered user'),
(3, 'A_ST', 'Account status', 'account status'),
(4, 'CTS', 'Countries', 'list of countries');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `code` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `geoname_id` int(11) NOT NULL,
  `capital_geoname_id` int(11) NOT NULL,
  `language_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` char(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timezone_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` decimal(10,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso3` char(3) CHARACTER SET latin1 DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`code`, `geoname_id`, `capital_geoname_id`, `language_code`, `currency_code`, `timezone_code`, `latitude`, `longitude`, `name`, `iso3`, `numcode`, `phonecode`) VALUES
('AD', 3041565, 3041563, 'ca', 'EUR', 'Europe/Andorra', '42.55065918', '1.57623327', 'Andorra', 'AND', 20, 376),
('AE', 290557, 292968, 'ar', 'AED', 'Asia/Dubai', '23.68477631', '54.53664398', 'United Arab Emirates', 'ARE', 784, 971),
('AF', 1149361, 1138958, 'fa', 'AFN', 'Asia/Kabul', '33.83324814', '66.02528381', 'Afghanistan', 'AFG', 4, 93),
('AG', 3576396, 3576022, 'en', 'XCD', 'America/Antigua', '17.09273911', '-61.81040955', 'Antigua and Barbuda', 'ATG', 28, 1268),
('AI', 3573511, 3573374, 'en', 'XCD', 'America/Anguilla', '18.22646713', '-63.04735184', 'Anguilla', 'AIA', 660, 1264),
('AL', 783754, 3183875, 'sq', 'ALL', 'Europe/Tirane', '41.11113358', '20.02745247', 'Albania', 'ALB', 8, 355),
('AM', 174982, 616052, 'hy', 'AMD', 'Asia/Yerevan', '40.29266357', '44.93947220', 'Armenia', 'ARM', 51, 374),
('AO', 3351879, 2240449, 'pt', 'AOA', 'Africa/Luanda', '-12.33355522', '17.53946495', 'Angola', 'AGO', 24, 244),
('AQ', 6697173, 6696480, 'en', 'USD', 'Antarctica/McMurdo', '-82.86275200', '-135.00000000', 'Antarctica', NULL, NULL, 0),
('AR', 3865483, 3435910, 'es', 'ARS', 'America/Argentina/Buenos_Aires', '-37.07196426', '-64.85450745', 'Argentina', 'ARG', 32, 54),
('AS', 5880801, 5881576, 'en', 'USD', 'Pacific/Pago_Pago', '-14.31956673', '-170.74035645', 'American Samoa', 'ASM', 16, 1684),
('AT', 2782113, 2761369, 'de', 'EUR', 'Europe/Vienna', '47.58843994', '14.14021111', 'Austria', 'AUT', 40, 43),
('AU', 2077456, 2172517, 'en', 'AUD', 'Australia/Sydney', '-25.58524132', '134.50411987', 'Australia', 'AUS', 36, 61),
('AW', 3577279, 3577154, 'nl', 'AWG', 'America/Aruba', '12.50652313', '-69.96931458', 'Aruba', 'ABW', 533, 297),
('AX', 661882, 3041732, 'sv', 'EUR', 'Europe/Mariehamn', '60.20238113', '19.96520233', 'Aland Islands', NULL, NULL, NULL),
('AZ', 587116, 587084, 'az', 'AZN', 'Asia/Baku', '40.33100510', '47.80820084', 'Azerbaijan', 'AZE', 31, 994),
('BA', 3277605, 3191281, 'bs', 'BAM', 'Europe/Sarajevo', '44.16533279', '17.79024124', 'Bosnia and Herzegovina', 'BIH', 70, 387),
('BB', 3374084, 3374036, 'en', 'BBD', 'America/Barbados', '13.17809868', '-59.54859543', 'Barbados', 'BRB', 52, 1246),
('BD', 1210997, 1185241, 'bn', 'BDT', 'Asia/Dhaka', '23.73010445', '90.30652618', 'Bangladesh', 'BGD', 50, 880),
('BE', 2802361, 2800866, 'nl', 'EUR', 'Europe/Brussels', '50.64896393', '4.64150238', 'Belgium', 'BEL', 56, 32),
('BF', 2361809, 2357048, 'fr', 'XOF', 'Africa/Ouagadougou', '12.28498554', '-1.74556065', 'Burkina Faso', 'BFA', 854, 226),
('BG', 732800, 727011, 'bg', 'BGN', 'Europe/Sofia', '42.76610184', '25.28373337', 'Bulgaria', 'BGR', 100, 359),
('BH', 290291, 290340, 'ar', 'BHD', 'Asia/Bahrain', '26.09424019', '50.54299545', 'Bahrain', 'BHR', 48, 973),
('BI', 433561, 425378, 'fr', 'BIF', 'Africa/Bujumbura', '-3.36520815', '29.88650894', 'Burundi', 'BDI', 108, 257),
('BJ', 2395170, 2392087, 'fr', 'XOF', 'Africa/Porto-Novo', '9.62411213', '2.33773875', 'Benin', 'BEN', 204, 229),
('BL', 3578476, 3579132, 'fr', 'EUR', 'America/St_Barthelemy', '17.89626122', '-62.83061218', 'Saint Barthelemy', NULL, NULL, NULL),
('BM', 3573345, 3573197, 'en', 'BMD', 'Atlantic/Bermuda', '32.30266953', '-64.75168610', 'Bermuda', 'BMU', 60, 1441),
('BN', 1820814, 1820906, 'ms', 'BND', 'Asia/Brunei', '4.57038403', '114.74818420', 'Brunei', 'BRN', 96, 673),
('BO', 3923057, 3903987, 'es', 'BOB', 'America/La_Paz', '-16.71305466', '-64.66664886', 'Bolivia', 'BOL', 68, 591),
('BQ', 7626844, 3513563, 'nl', 'USD', 'America/Kralendijk', '12.17836100', '-68.23853400', 'Bonaire, Saint Eustatius and Saba ', NULL, NULL, NULL),
('BR', 3469034, 3469058, 'pt', 'BRL', 'America/Sao_Paulo', '-10.81045151', '-52.97311783', 'Brazil', 'BRA', 76, 55),
('BS', 3572887, 3571824, 'en', 'BSD', 'America/Nassau', '25.03564835', '-77.39512634', 'Bahamas', 'BHS', 44, 1242),
('BT', 1252634, 1252416, 'dz', 'BTN', 'Asia/Thimphu', '27.41687965', '90.43476105', 'Bhutan', 'BTN', 64, 975),
('BV', 3371123, 3371123, 'en', 'NOK', 'Europe/Oslo', '-54.43420410', '3.41025114', 'Bouvet Island', NULL, NULL, 0),
('BW', 933860, 933773, 'en', 'BWP', 'Africa/Gaborone', '-22.18675232', '23.81494141', 'Botswana', 'BWA', 72, 267),
('BY', 630336, 625144, 'be', 'BYR', 'Europe/Minsk', '53.54347229', '28.05409431', 'Belarus', 'BLR', 112, 375),
('BZ', 3582678, 3582672, 'en', 'BZD', 'America/Belize', '17.22529221', '-88.66973877', 'Belize', 'BLZ', 84, 501),
('CA', 6251999, 6094817, 'en', 'CAD', 'America/Toronto', '62.83290863', '-95.91332245', 'Canada', 'CAN', 124, 1),
('CC', 1547376, 7304591, 'ms', 'AUD', 'Indian/Cocos', '-12.20060253', '96.85894012', 'Cocos Islands', NULL, NULL, 672),
('CD', 203312, 2314302, 'fr', 'CDF', 'Africa/Kinshasa', '-2.87986612', '23.65637779', 'Democratic Republic of the Congo', 'COD', 180, 242),
('CF', 239880, 2389853, 'fr', 'XAF', 'Africa/Bangui', '6.57412338', '20.48692322', 'Central African Republic', 'CAF', 140, 236),
('CG', 2260494, 2260535, 'fr', 'XAF', 'Africa/Brazzaville', '-2.87986612', '23.65637779', 'Republic of the Congo', 'COG', 178, 242),
('CH', 2658434, 2661552, 'de', 'CHF', 'Europe/Zurich', '46.80379868', '8.22285461', 'Switzerland', 'CHE', 756, 41),
('CI', 2287781, 2279755, 'fr', 'XOF', 'Africa/Abidjan', '7.59875536', '-5.55257463', 'Ivory Coast', 'CIV', 384, 225),
('CK', 1899402, 4035715, 'en', 'NZD', 'Pacific/Rarotonga', '-21.22330666', '-159.74055481', 'Cook Islands', 'COK', 184, 682),
('CL', 3895114, 3871336, 'es', 'CLP', 'America/Santiago', '-35.78622818', '-71.67467499', 'Chile', 'CHL', 152, 56),
('CM', 2233387, 2220957, 'en', 'XAF', 'Africa/Douala', '5.68547678', '12.72287750', 'Cameroon', 'CMR', 120, 237),
('CN', 1814991, 1816670, 'zh', 'CNY', 'Asia/Shanghai', '36.55308533', '103.97543335', 'China', 'CHN', 156, 86),
('CO', 3686110, 3688689, 'es', 'COP', 'America/Bogota', '3.99760723', '-73.27796936', 'Colombia', 'COL', 170, 57),
('CR', 3624060, 3621849, 'es', 'CRC', 'America/Costa_Rica', '9.88499165', '-84.22723389', 'Costa Rica', 'CRI', 188, 506),
('CU', 3562981, 3553478, 'es', 'CUC', 'America/Havana', '22.06633568', '-79.45314789', 'Cuba', 'CUB', 192, 53),
('CV', 3374766, 3374333, 'pt', 'CVE', 'Atlantic/Cape_Verde', '15.18300247', '-23.70345116', 'Cape Verde', 'CPV', 132, 238),
('CW', 7626836, 3513090, 'nl', 'ANG', 'America/Curacao', '12.16322041', '-68.94505310', 'Curacao', NULL, NULL, NULL),
('CX', 2078138, 2078127, 'en', 'AUD', 'Indian/Christmas', '-10.49029064', '105.63275146', 'Christmas Island', NULL, NULL, 61),
('CY', 146669, 146268, 'el', 'EUR', 'Asia/Nicosia', '35.11473846', '33.48671722', 'Cyprus', 'CYP', 196, 357),
('CZ', 3077311, 3067696, 'cs', 'CZK', 'Europe/Prague', '49.73910522', '15.33150101', 'Czechia', 'CZE', 203, 420),
('DE', 2921044, 2950159, 'de', 'EUR', 'Europe/Berlin', '51.20246506', '10.38220310', 'Germany', 'DEU', 276, 49),
('DJ', 223816, 223817, 'fr', 'DJF', 'Africa/Djibouti', '11.74259186', '42.63182831', 'Djibouti', 'DJI', 262, 253),
('DK', 2623032, 2618425, 'da', 'DKK', 'Europe/Copenhagen', '56.10176086', '9.55590725', 'Denmark', 'DNK', 208, 45),
('DM', 3575830, 3575635, 'en', 'XCD', 'America/Dominica', '15.39910603', '-61.33945847', 'Dominica', 'DMA', 212, 1767),
('DO', 3508796, 3492908, 'es', 'DOP', 'America/Santo_Domingo', '19.01982498', '-70.79285431', 'Dominican Republic', 'DOM', 214, 1809),
('DZ', 2589581, 2507480, 'ar', 'DZD', 'Africa/Algiers', '28.21364594', '2.65472817', 'Algeria', 'DZA', 12, 213),
('EC', 3658394, 3652462, 'es', 'USD', 'America/Guayaquil', '-1.42152894', '-78.87104034', 'Ecuador', 'ECU', 218, 593),
('EE', 453733, 588409, 'et', 'EUR', 'Europe/Tallinn', '58.69374466', '25.24162483', 'Estonia', 'EST', 233, 372),
('EG', 357994, 360630, 'ar', 'EGP', 'Africa/Cairo', '26.75610352', '29.86229706', 'Egypt', 'EGY', 818, 20),
('EH', 2461445, 2462881, 'ar', 'MAD', 'Africa/El_Aaiun', '25.00000000', '-13.00000000', 'Western Sahara', 'ESH', 732, 212),
('ER', 338010, 343300, 'aa', 'ERN', 'Africa/Asmara', '15.39719963', '39.08718872', 'Eritrea', 'ERI', 232, 291),
('ES', 2510769, 3117735, 'es', 'EUR', 'Europe/Madrid', '40.39602661', '-3.55069256', 'Spain', 'ESP', 724, 34),
('ET', 337996, 344979, 'am', 'ETB', 'Africa/Addis_Ababa', '8.62670326', '39.63755417', 'Ethiopia', 'ETH', 231, 251),
('FI', 660013, 658225, 'fi', 'EUR', 'Europe/Helsinki', '64.28858185', '25.98940277', 'Finland', 'FIN', 246, 358),
('FJ', 2205218, 2198148, 'en', 'FJD', 'Pacific/Fiji', '-17.65816116', '178.14726257', 'Fiji', 'FJI', 242, 679),
('FK', 3474414, 3426691, 'en', 'FKP', 'Atlantic/Stanley', '-51.77312469', '-59.72790909', 'Falkland Islands', 'FLK', 238, 500),
('FM', 2081918, 2081986, 'en', 'USD', 'Pacific/Pohnpei', '6.86934900', '158.18725586', 'Micronesia', 'FSM', 583, 691),
('FO', 2622320, 2611396, 'fo', 'DKK', 'Atlantic/Faroe', '62.00955963', '-6.81825542', 'Faroe Islands', 'FRO', 234, 298),
('FR', 3017382, 2988507, 'fr', 'EUR', 'Europe/Paris', '46.63727951', '2.33826232', 'France', 'FRA', 250, 33),
('GA', 2400553, 2399697, 'fr', 'XAF', 'Africa/Libreville', '-0.63454008', '11.73860836', 'Gabon', 'GAB', 266, 241),
('GB', 2635167, 2643743, 'en', 'GBP', 'Europe/London', '54.56088638', '-2.21251178', 'United Kingdom', 'GBR', 826, 44),
('GD', 3580239, 3579925, 'en', 'XCD', 'America/Grenada', '12.17886639', '-61.64693069', 'Grenada', 'GRD', 308, 1473),
('GE', 614540, 611717, 'ka', 'GEL', 'Asia/Tbilisi', '42.32078450', '43.37136150', 'Georgia', 'GEO', 268, 995),
('GF', 3381670, 3382160, 'fr', 'EUR', 'America/Cayenne', '4.06999111', '-53.16830826', 'French Guiana', 'GUF', 254, 594),
('GG', 3042362, 3042287, 'en', 'GBP', 'Europe/Guernsey', '49.72008514', '-2.19996858', 'Guernsey', NULL, NULL, NULL),
('GH', 2300660, 2306104, 'en', 'GHS', 'Africa/Accra', '7.92133045', '-1.20438623', 'Ghana', 'GHA', 288, 233),
('GI', 2411586, 2411585, 'en', 'GIP', 'Europe/Gibraltar', '36.13584137', '-5.34924889', 'Gibraltar', 'GIB', 292, 350),
('GL', 3425505, 3421319, 'kl', 'DKK', 'America/Godthab', '74.34954834', '-41.08988953', 'Greenland', 'GRL', 304, 299),
('GM', 2413451, 2413876, 'en', 'GMD', 'Africa/Banjul', '13.44026566', '-15.49088478', 'Gambia', 'GMB', 270, 220),
('GN', 2420477, 2422465, 'fr', 'GNF', 'Africa/Conakry', '10.42930222', '-10.98954964', 'Guinea', 'GIN', 324, 224),
('GP', 3579143, 3579732, 'fr', 'EUR', 'America/Guadeloupe', '16.25673103', '-61.56741714', 'Guadeloupe', 'GLP', 312, 590),
('GQ', 2309096, 2309527, 'es', 'XAF', 'Africa/Malabo', '1.53312600', '10.37258148', 'Equatorial Guinea', 'GNQ', 226, 240),
('GR', 390903, 264371, 'el', 'EUR', 'Europe/Athens', '39.68437195', '21.89740944', 'Greece', 'GRC', 300, 30),
('GS', 3474415, 3426466, 'en', 'GBP', 'Atlantic/South_Georgia', '-54.45992279', '-36.35461807', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
('GT', 3595528, 3598132, 'es', 'GTQ', 'America/Guatemala', '15.67056561', '-90.34865570', 'Guatemala', 'GTM', 320, 502),
('GU', 4043988, 4044012, 'en', 'USD', 'Pacific/Guam', '13.42112923', '144.73971558', 'Guam', 'GUM', 316, 1671),
('GW', 2372248, 2374775, 'pt', 'XOF', 'Africa/Bissau', '12.11586285', '-14.74813652', 'Guinea-Bissau', 'GNB', 624, 245),
('GY', 3378535, 3378644, 'en', 'GYD', 'America/Guyana', '4.91731119', '-58.94346237', 'Guyana', 'GUY', 328, 592),
('HK', 1819730, 1819729, 'zh', 'HKD', 'Asia/Hong_Kong', '22.33615685', '114.18696594', 'Hong Kong', 'HKG', 344, 852),
('HM', 1547314, 1547314, 'en', 'AUD', 'Indian/Kerguelen', '-53.08010864', '73.56218719', 'Heard Island and McDonald Islands', NULL, NULL, 0),
('HN', 3608932, 3600949, 'es', 'HNL', 'America/Tegucigalpa', '14.97503281', '-86.26477051', 'Honduras', 'HND', 340, 504),
('HR', 3202326, 3186886, 'hr', 'HRK', 'Europe/Zagreb', '45.44430542', '15.73450375', 'Croatia', 'HRV', 191, 385),
('HT', 3723988, 3718426, 'ht', 'HTG', 'America/Port-au-Prince', '19.07324219', '-72.24127960', 'Haiti', 'HTI', 332, 509),
('HU', 719819, 3054643, 'hu', 'HUF', 'Europe/Budapest', '47.16573334', '19.41657448', 'Hungary', 'HUN', 348, 36),
('ID', 1643084, 1642911, 'id', 'IDR', 'Asia/Jakarta', '-1.24808908', '115.41899872', 'Indonesia', 'IDN', 360, 62),
('IE', 2963597, 2964574, 'en', 'EUR', 'Europe/Dublin', '53.18272781', '-8.19610214', 'Ireland', 'IRL', 372, 353),
('IL', 294640, 281184, 'he', 'ILS', 'Asia/Jerusalem', '31.81419373', '34.75337219', 'Israel', 'ISR', 376, 972),
('IM', 3042225, 3042237, 'en', 'GBP', 'Europe/Isle_of_Man', '54.22451401', '-4.56213331', 'Isle of Man', NULL, NULL, NULL),
('IN', 1269750, 1261481, 'en', 'INR', 'Asia/Kolkata', '23.40601158', '79.45809174', 'India', 'IND', 356, 91),
('IO', 1282588, 7503195, 'en', 'USD', 'Indian/Chagos', '-6.19626999', '71.34793091', 'British Indian Ocean Territory', NULL, NULL, 246),
('IQ', 99237, 98182, 'ar', 'IQD', 'Asia/Baghdad', '33.04458618', '43.77495575', 'Iraq', 'IRQ', 368, 964),
('IR', 130758, 112931, 'fa', 'IRR', 'Asia/Tehran', '32.50077820', '54.29420090', 'Iran', 'IRN', 364, 98),
('IS', 2629691, 3413829, 'is', 'ISK', 'Atlantic/Reykjavik', '64.92856598', '-18.96170044', 'Iceland', 'ISL', 352, 354),
('IT', 3175395, 3169070, 'it', 'EUR', 'Europe/Rome', '42.76697922', '12.49382305', 'Italy', 'ITA', 380, 39),
('JE', 3042142, 3042091, 'en', 'GBP', 'Europe/Jersey', '49.22850418', '-2.12289286', 'Jersey', NULL, NULL, NULL),
('JM', 3489940, 3489854, 'en', 'JMD', 'America/Jamaica', '18.14344406', '-77.34654999', 'Jamaica', 'JAM', 388, 1876),
('JO', 248816, 250441, 'ar', 'JOD', 'Asia/Amman', '31.27576256', '36.82838821', 'Jordan', 'JOR', 400, 962),
('JP', 1861060, 1850147, 'ja', 'JPY', 'Asia/Tokyo', '36.28164673', '139.07727051', 'Japan', 'JPN', 392, 81),
('KE', 192950, 184745, 'en', 'KES', 'Africa/Nairobi', '0.57650316', '37.83988953', 'Kenya', 'KEN', 404, 254),
('KG', 1527747, 1528675, 'ky', 'KGS', 'Asia/Bishkek', '41.46435547', '74.55522156', 'Kyrgyzstan', 'KGZ', 417, 996),
('KH', 1831722, 1821306, 'km', 'KHR', 'Asia/Phnom_Penh', '12.57042313', '104.81391144', 'Cambodia', 'KHM', 116, 855),
('KI', 4030945, 2110257, 'en', 'AUD', 'Pacific/Tarawa', '1.84283316', '-157.67582703', 'Kiribati', 'KIR', 296, 686),
('KM', 921929, 921772, 'ar', 'KMF', 'Indian/Comoro', '-11.86610222', '43.43264008', 'Comoros', 'COM', 174, 269),
('KN', 3575174, 3575551, 'en', 'XCD', 'America/St_Kitts', '17.24447250', '-62.64318466', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
('KP', 1873107, 1871859, 'ko', 'KPW', 'Asia/Pyongyang', '40.07764053', '127.13385010', 'North Korea', 'PRK', 408, 850),
('KR', 1835841, 1835848, 'ko', 'KRW', 'Asia/Seoul', '40.07764053', '127.13385010', 'South Korea', 'KOR', 410, 82),
('KW', 285570, 285787, 'ar', 'KWD', 'Asia/Kuwait', '29.32194138', '47.60246658', 'Kuwait', 'KWT', 414, 965),
('KY', 3580718, 3580661, 'en', 'KYD', 'America/Cayman', '19.30886269', '-81.25680542', 'Cayman Islands', 'CYM', 136, 1345),
('KZ', 1522867, 1526273, 'kk', 'KZT', 'Asia/Almaty', '48.14600372', '67.17916870', 'Kazakhstan', 'KAZ', 398, 7),
('LA', 1655842, 1651944, 'lo', 'LAK', 'Asia/Vientiane', '18.65074921', '104.15293884', 'Laos', 'LAO', 418, 856),
('LB', 272103, 276781, 'ar', 'LBP', 'Asia/Beirut', '33.92541122', '35.89972687', 'Lebanon', 'LBN', 422, 961),
('LC', 3576468, 3576812, 'en', 'XCD', 'America/St_Lucia', '13.86330509', '-60.96656418', 'Saint Lucia', 'LCA', 662, 1758),
('LI', 3042058, 3042030, 'de', 'CHF', 'Europe/Vaduz', '47.14126968', '9.55278301', 'Liechtenstein', 'LIE', 438, 423),
('LK', 1227603, 1248991, 'si', 'LKR', 'Asia/Colombo', '7.78913355', '80.68072510', 'Sri Lanka', 'LKA', 144, 94),
('LR', 2275384, 2274895, 'en', 'LRD', 'Africa/Monrovia', '6.41151285', '-9.32349205', 'Liberia', 'LBR', 430, 231),
('LS', 932692, 932505, 'en', 'ZAR', 'Africa/Maseru', '-29.58175278', '28.24661255', 'Lesotho', 'LSO', 426, 266),
('LT', 597427, 593116, 'lt', 'EUR', 'Europe/Vilnius', '55.33871841', '23.87092400', 'Lithuania', 'LTU', 440, 370),
('LU', 2960313, 2960316, 'lb', 'EUR', 'Europe/Luxembourg', '49.77788162', '6.09474611', 'Luxembourg', 'LUX', 442, 352),
('LV', 458258, 456172, 'lv', 'EUR', 'Europe/Riga', '56.86873245', '24.84024429', 'Latvia', 'LVA', 428, 371),
('LY', 2215636, 2210247, 'ar', 'LYD', 'Africa/Tripoli', '27.23609734', '18.04355621', 'Libya', 'LBY', 434, 218),
('MA', 2542007, 2538475, 'ar', 'MAD', 'Africa/Casablanca', '29.14059067', '-8.95338821', 'Morocco', 'MAR', 504, 212),
('MC', 2993457, 2993458, 'fr', 'EUR', 'Europe/Monaco', '43.73892975', '7.42548323', 'Monaco', 'MCO', 492, 377),
('MD', 617790, 618426, 'ro', 'MDL', 'Europe/Chisinau', '47.20370483', '28.46834373', 'Moldova', 'MDA', 498, 373),
('ME', 3194884, 3193044, 'sr', 'EUR', 'Europe/Podgorica', '42.75280380', '19.23791885', 'Montenegro', NULL, NULL, NULL),
('MF', 3578421, 3578851, 'fr', 'EUR', 'America/Marigot', '18.04222488', '-63.06623459', 'Saint Martin', NULL, NULL, NULL),
('MG', 1062947, 1070940, 'fr', 'MGA', 'Indian/Antananarivo', '-19.27239418', '46.69843292', 'Madagascar', 'MDG', 450, 261),
('MH', 2080185, 2113779, 'mh', 'USD', 'Pacific/Majuro', '7.28620768', '168.75140381', 'Marshall Islands', 'MHL', 584, 692),
('MK', 718075, 785842, 'mk', 'MKD', 'Europe/Skopje', '41.60045624', '21.70089531', 'Macedonia', 'MKD', 807, 389),
('ML', 2453866, 2460596, 'fr', 'XOF', 'Africa/Bamako', '17.35776711', '-3.52738190', 'Mali', 'MLI', 466, 223),
('MM', 1327865, 6611854, 'my', 'MMK', 'Asia/Rangoon', '20.33014297', '96.52182007', 'Myanmar', 'MMR', 104, 95),
('MN', 2029969, 2028462, 'mn', 'MNT', 'Asia/Ulaanbaatar', '46.83647919', '103.06689453', 'Mongolia', 'MNG', 496, 976),
('MO', 1821275, 1821274, 'zh', 'MOP', 'Asia/Macau', '22.14074898', '113.56034088', 'Macao', 'MAC', 446, 853),
('MP', 4041468, 7828758, 'fil', 'USD', 'Pacific/Saipan', '15.26277924', '145.80456543', 'Northern Mariana Islands', 'MNP', 580, 1670),
('MQ', 3570311, 3570675, 'fr', 'EUR', 'America/Martinique', '14.64280796', '-60.97755432', 'Martinique', 'MTQ', 474, 596),
('MR', 2378080, 2377450, 'ar', 'MRO', 'Africa/Nouakchott', '20.25899506', '-10.36443710', 'Mauritania', 'MRT', 478, 222),
('MS', 3578097, 3578069, 'en', 'XCD', 'America/Montserrat', '16.73599815', '-62.18881989', 'Montserrat', 'MSR', 500, 1664),
('MT', 2562770, 2562305, 'mt', 'EUR', 'Europe/Malta', '35.93336487', '14.38103390', 'Malta', 'MLT', 470, 356),
('MU', 934292, 934154, 'en', 'MUR', 'Indian/Mauritius', '-20.22040939', '57.58937836', 'Mauritius', 'MUS', 480, 230),
('MV', 1282028, 1282027, 'dv', 'MVR', 'Indian/Maldives', '4.18588495', '73.53071594', 'Maldives', 'MDV', 462, 960),
('MW', 927384, 927967, 'ny', 'MWK', 'Africa/Blantyre', '-13.52357769', '33.83546448', 'Malawi', 'MWI', 454, 265),
('MX', 3996063, 3530597, 'es', 'MXN', 'America/Mexico_City', '23.90909386', '-102.63339996', 'Mexico', 'MEX', 484, 52),
('MY', 1733045, 1735161, 'ms', 'MYR', 'Asia/Kuala_Lumpur', '2.54900050', '102.96261597', 'Malaysia', 'MYS', 458, 60),
('MZ', 1036973, 1040652, 'pt', 'MZN', 'Africa/Maputo', '-17.55586433', '35.95569229', 'Mozambique', 'MOZ', 508, 258),
('NA', 3355338, 3352136, 'en', 'NAD', 'Africa/Windhoek', '-22.15069962', '17.17752647', 'Namibia', 'NAM', 516, 264),
('NC', 2139685, 2139521, 'fr', 'XPF', 'Pacific/Noumea', '-21.31782341', '165.29858398', 'New Caledonia', 'NCL', 540, 687),
('NE', 2440476, 2440485, 'fr', 'XOF', 'Africa/Niamey', '17.42407417', '9.40063381', 'Niger', 'NER', 562, 227),
('NF', 2155115, 2161314, 'en', 'AUD', 'Pacific/Norfolk', '-29.03696251', '167.95523071', 'Norfolk Island', 'NFK', 574, 672),
('NG', 2328926, 2352778, 'en', 'NGN', 'Africa/Lagos', '9.55950546', '8.07788086', 'Nigeria', 'NGA', 566, 234),
('NI', 3617476, 3617763, 'es', 'NIO', 'America/Managua', '12.90377331', '-84.92182159', 'Nicaragua', 'NIC', 558, 505),
('NL', 2750405, 2759794, 'nl', 'EUR', 'Europe/Amsterdam', '52.34225845', '5.52815723', 'Netherlands', 'NLD', 528, 31),
('NO', 3144096, 3143244, 'no', 'NOK', 'Europe/Oslo', '66.76667023', '14.89992523', 'Norway', 'NOR', 578, 47),
('NP', 1282988, 1283240, 'ne', 'NPR', 'Asia/Kathmandu', '28.25913811', '83.94416046', 'Nepal', 'NPL', 524, 977),
('NR', 2110425, 7626461, 'na', 'AUD', 'Pacific/Nauru', '-0.53160650', '166.93640137', 'Nauru', 'NRU', 520, 674),
('NU', 4036232, 4036284, 'niu', 'NZD', 'Pacific/Niue', '-19.03806496', '-169.83024597', 'Niue', 'NIU', 570, 683),
('NZ', 2186224, 2179537, 'en', 'NZD', 'Pacific/Auckland', '-44.05629349', '170.35415649', 'New Zealand', 'NZL', 554, 64),
('OM', 286963, 287286, 'ar', 'OMR', 'Asia/Muscat', '20.56662178', '56.15796280', 'Oman', 'OMN', 512, 968),
('PA', 3703430, 3703443, 'es', 'PAB', 'America/Panama', '8.64624786', '-80.50607300', 'Panama', 'PAN', 591, 507),
('PE', 3932488, 3936456, 'es', 'PEN', 'America/Lima', '-9.21253300', '-74.42211914', 'Peru', 'PER', 604, 51),
('PF', 4030656, 4033936, 'fr', 'XPF', 'Pacific/Tahiti', '-17.64812279', '-149.46472168', 'French Polynesia', 'PYF', 258, 689),
('PG', 2088628, 2088122, 'en', 'PGK', 'Pacific/Port_Moresby', '-6.88915968', '146.21444702', 'Papua New Guinea', 'PNG', 598, 675),
('PH', 1694008, 1701668, 'tl', 'PHP', 'Asia/Manila', '11.11266613', '122.50947571', 'Philippines', 'PHL', 608, 63),
('PK', 1168579, 1176615, 'ur', 'PKR', 'Asia/Karachi', '29.92321968', '69.35774231', 'Pakistan', 'PAK', 586, 92),
('PL', 798544, 756135, 'pl', 'PLN', 'Europe/Warsaw', '52.14785004', '19.37775993', 'Poland', 'POL', 616, 48),
('PM', 3424932, 3424934, 'fr', 'EUR', 'America/Miquelon', '46.90594482', '-56.33658600', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
('PN', 4030699, 4030723, 'en', 'NZD', 'Pacific/Pitcairn', '-24.37211418', '-128.31124878', 'Pitcairn', 'PCN', 612, 0),
('PR', 4566966, 4568127, 'en', 'USD', 'America/Puerto_Rico', '18.24913979', '-66.62803650', 'Puerto Rico', 'PRI', 630, 1787),
('PS', 6254930, 281133, 'ar', 'ILS', 'Asia/Gaza', '31.94639206', '35.25973511', 'Palestinian Territory', NULL, NULL, 970),
('PT', 2264397, 2267057, 'pt', 'EUR', 'Europe/Lisbon', '39.64200974', '-8.00942230', 'Portugal', 'PRT', 620, 351),
('PW', 1559582, 7303944, 'pau', 'USD', 'Pacific/Palau', '7.44190073', '134.54205322', 'Palau', 'PLW', 585, 680),
('PY', 3437598, 3439389, 'es', 'PYG', 'America/Asuncion', '-23.24028969', '-58.39517212', 'Paraguay', 'PRY', 600, 595),
('QA', 289688, 290030, 'ar', 'QAR', 'Asia/Qatar', '25.41362572', '51.26026535', 'Qatar', 'QAT', 634, 974),
('RE', 935317, 935264, 'fr', 'EUR', 'Indian/Reunion', '-21.14629936', '55.63124847', 'Reunion', 'REU', 638, 262),
('RO', 798549, 683506, 'ro', 'RON', 'Europe/Bucharest', '45.83774185', '25.00593567', 'Romania', 'ROM', 642, 40),
('RS', 6290252, 792680, 'sr', 'RSD', 'Europe/Belgrade', '44.23297119', '20.79795837', 'Serbia', NULL, NULL, NULL),
('RU', 2017370, 524901, 'ru', 'RUB', 'Europe/Moscow', '63.12518692', '103.75398254', 'Russia', 'RUS', 643, 70),
('RW', 49518, 202061, 'rw', 'RWF', 'Africa/Kigali', '-1.99994981', '29.92605782', 'Rwanda', 'RWA', 646, 250),
('SA', 102358, 108410, 'ar', 'SAR', 'Asia/Riyadh', '23.99472618', '44.40135574', 'Saudi Arabia', 'SAU', 682, 966),
('SB', 2103350, 2108502, 'en', 'SBD', 'Pacific/Guadalcanal', '-9.54811287', '160.01930237', 'Solomon Islands', 'SLB', 90, 677),
('SC', 241170, 241131, 'en', 'SCR', 'Indian/Mahe', '-4.66979504', '55.47166061', 'Seychelles', 'SYC', 690, 248),
('SD', 366755, 379252, 'ar', 'SDG', 'Africa/Khartoum', '16.08578491', '30.08739090', 'Sudan', 'SDN', 736, 249),
('SE', 2661886, 2673730, 'sv', 'SEK', 'Europe/Stockholm', '62.67497253', '16.79805946', 'Sweden', 'SWE', 752, 46),
('SG', 1880251, 1880252, 'cmn', 'SGD', 'Asia/Singapore', '1.32199585', '103.82053375', 'Singapore', 'SGP', 702, 65),
('SH', 3370751, 3370903, 'en', 'SHP', 'Atlantic/St_Helena', '-15.95733452', '-5.71691418', 'Saint Helena', 'SHN', 654, 290),
('SI', 3190538, 3196359, 'sl', 'EUR', 'Europe/Ljubljana', '46.12023926', '14.82066441', 'Slovenia', 'SVN', 705, 386),
('SJ', 607072, 2729907, 'no', 'NOK', 'Arctic/Longyearbyen', '71.04893494', '-8.19574738', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
('SK', 3057568, 3060972, 'sk', 'EUR', 'Europe/Bratislava', '48.70748520', '19.48488998', 'Slovakia', 'SVK', 703, 421),
('SL', 2403846, 2409306, 'en', 'SLL', 'Africa/Freetown', '8.52144146', '-11.84389019', 'Sierra Leone', 'SLE', 694, 232),
('SM', 3168068, 3168070, 'it', 'EUR', 'Europe/San_Marino', '43.93813324', '12.46339321', 'San Marino', 'SMR', 674, 378),
('SN', 2245662, 2253354, 'fr', 'XOF', 'Africa/Dakar', '14.36251163', '-14.53164387', 'Senegal', 'SEN', 686, 221),
('SO', 51537, 53654, 'so', 'SOS', 'Africa/Mogadishu', '5.94826746', '47.47360611', 'Somalia', 'SOM', 706, 252),
('SR', 3382998, 3383330, 'nl', 'SRD', 'America/Paramaribo', '4.21692896', '-55.88921738', 'Suriname', 'SUR', 740, 597),
('SS', 7909807, 373303, 'en', 'SSP', 'Africa/Juba', '7.30385828', '30.28075218', 'South Sudan', NULL, NULL, NULL),
('ST', 2410758, 2410763, 'pt', 'STD', 'Africa/Sao_Tome', '0.27555528', '6.63162804', 'Sao Tome and Principe', 'STP', 678, 239),
('SV', 3585968, 3583361, 'es', 'USD', 'America/El_Salvador', '13.67163658', '-88.86363220', 'El Salvador', 'SLV', 222, 503),
('SX', 7609695, 3513392, 'nl', 'ANG', 'America/Lower_Princes', '18.04222488', '-63.06623459', 'Sint Maarten', NULL, NULL, NULL),
('SY', 163843, 170654, 'ar', 'SYP', 'Asia/Damascus', '35.03312683', '38.47347260', 'Syria', 'SYR', 760, 963),
('SZ', 934841, 934985, 'en', 'SZL', 'Africa/Mbabane', '-26.56513405', '31.49811363', 'Swaziland', 'SWZ', 748, 268),
('TC', 3576916, 3576994, 'en', 'USD', 'America/Grand_Turk', '21.75872612', '-71.71514893', 'Turks and Caicos Islands', 'TCA', 796, 1649),
('TD', 2434508, 2427123, 'fr', 'XAF', 'Africa/Ndjamena', '15.36765289', '18.66758156', 'Chad', 'TCD', 148, 235),
('TF', 1546748, 1546102, 'fr', 'EUR', 'Indian/Kerguelen', '-49.56386566', '69.54277802', 'French Southern Territories', NULL, NULL, 0),
('TG', 2363686, 2365267, 'fr', 'XOF', 'Africa/Lome', '8.51322651', '0.98009753', 'Togo', 'TGO', 768, 228),
('TH', 1605651, 1609350, 'th', 'THB', 'Asia/Bangkok', '14.48458195', '100.85191345', 'Thailand', 'THA', 764, 66),
('TJ', 1220409, 1221874, 'tg', 'TJS', 'Asia/Dushanbe', '38.87976456', '70.89906311', 'Tajikistan', 'TJK', 762, 992),
('TK', 4031074, 7522183, 'tkl', 'NZD', 'Pacific/Fakaofo', '-8.97920799', '-172.20170593', 'Tokelau', 'TKL', 772, 690),
('TL', 1966436, 1645457, 'tet', 'USD', 'Asia/Dili', '-8.80478668', '126.07902527', 'East Timor', NULL, NULL, 670),
('TM', 1218197, 162183, 'tk', 'TMT', 'Asia/Ashgabat', '39.20128250', '59.08225250', 'Turkmenistan', 'TKM', 795, 7370),
('TN', 2464461, 2464470, 'ar', 'TND', 'Africa/Tunis', '34.33528519', '9.24525928', 'Tunisia', 'TUN', 788, 216),
('TO', 4032283, 4032402, 'to', 'TOP', 'Pacific/Tongatapu', '-21.14761162', '-175.25067139', 'Tonga', 'TON', 776, 676),
('TR', 298795, 323786, 'tr', 'TRY', 'Europe/Istanbul', '39.05101013', '34.93033981', 'Turkey', 'TUR', 792, 90),
('TT', 3573591, 3573890, 'en', 'TTD', 'America/Port_of_Spain', '10.68574047', '-61.16406250', 'Trinidad and Tobago', 'TTO', 780, 1868),
('TV', 2110297, 2110394, 'tvl', 'AUD', 'Pacific/Funafuti', '-7.47130585', '178.67402649', 'Tuvalu', 'TUV', 798, 688),
('TW', 1668284, 1668341, 'zh', 'TWD', 'Asia/Taipei', '23.68578911', '120.89749146', 'Taiwan', 'TWN', 158, 886),
('TZ', 149590, 160196, 'sw', 'TZS', 'Africa/Dar_es_Salaam', '-6.30689716', '34.85392761', 'Tanzania', 'TZA', 834, 255),
('UA', 690791, 703448, 'uk', 'UAH', 'Europe/Kiev', '48.92656326', '31.47578239', 'Ukraine', 'UKR', 804, 380),
('UG', 226074, 232422, 'en', 'UGX', 'Africa/Kampala', '1.27732801', '32.38998413', 'Uganda', 'UGA', 800, 256),
('UM', 5854968, 5854928, 'en', 'USD', 'Pacific/Johnston', '19.28231900', '166.64704700', 'United States Minor Outlying Islands', NULL, NULL, 1),
('US', 6252001, 4140963, 'en', 'USD', 'America/New_York', '39.44325638', '-98.95733643', 'United States', 'USA', 840, 1),
('UY', 3439705, 3441575, 'es', 'UYU', 'America/Montevideo', '-32.96965408', '-56.05590820', 'Uruguay', 'URY', 858, 598),
('UZ', 1512440, 1512569, 'uz', 'UZS', 'Asia/Tashkent', '41.77239227', '63.14588928', 'Uzbekistan', 'UZB', 860, 998),
('VA', 3164670, 6691831, 'la', 'EUR', 'Europe/Vatican', '41.90308380', '12.45285225', 'Vatican', 'VAT', 336, 39),
('VC', 3577815, 3577887, 'en', 'XCD', 'America/St_Vincent', '13.21725178', '-61.19344711', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
('VE', 3625428, 3646738, 'es', 'VEF', 'America/Caracas', '7.66538858', '-66.14541626', 'Venezuela', 'VEN', 862, 58),
('VG', 3577718, 3577430, 'en', 'USD', 'America/Tortola', '18.44307137', '-64.57130432', 'British Virgin Islands', 'VGB', 92, 1284),
('VI', 4796775, 4795467, 'en', 'USD', 'America/St_Thomas', '17.75262451', '-64.73542023', 'U.S. Virgin Islands', 'VIR', 850, 1340),
('VN', 1562822, 1581130, 'vi', 'VND', 'Asia/Ho_Chi_Minh', '16.94042969', '106.81642914', 'Vietnam', 'VNM', 704, 84),
('VU', 2134431, 2135171, 'bi', 'VUV', 'Pacific/Efate', '-16.37668419', '167.56250000', 'Vanuatu', 'VUT', 548, 678),
('WF', 4034749, 4034821, 'wls', 'XPF', 'Pacific/Wallis', '-13.29961205', '-176.17012024', 'Wallis and Futuna', 'WLF', 876, 681),
('WS', 4034894, 4035413, 'sm', 'WST', 'Pacific/Apia', '-13.66897297', '-172.32202148', 'Samoa', 'WSM', 882, 684),
('XK', 831053, 786714, 'sq', 'EUR', 'Europe/Belgrade', '42.60263590', '20.90297699', 'Kosovo', NULL, NULL, NULL),
('YE', 69543, 71137, 'ar', 'YER', 'Asia/Aden', '15.88838768', '47.48988724', 'Yemen', 'YEM', 887, 967),
('YT', 1024031, 921815, 'fr', 'EUR', 'Indian/Mayotte', '-12.79636002', '45.14227295', 'Mayotte', NULL, NULL, 269),
('ZA', 953987, 964137, 'zu', 'ZAR', 'Africa/Johannesburg', '-29.04618454', '25.06287956', 'South Africa', 'ZAF', 710, 27),
('ZM', 895949, 909137, 'en', 'ZMW', 'Africa/Lusaka', '-13.45884514', '27.78809738', 'Zambia', 'ZMB', 894, 260),
('ZW', 878675, 890299, 'en', 'USD', 'Africa/Harare', '-19.00028038', '29.86876106', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `cron_job`
--

DROP TABLE IF EXISTS `cron_job`;
CREATE TABLE IF NOT EXISTS `cron_job` (
  `id_cron_job` int(11) NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `limit` int(11) DEFAULT NULL,
  `offset` int(11) DEFAULT NULL,
  `running` smallint(6) UNSIGNED NOT NULL,
  `success` smallint(6) UNSIGNED NOT NULL,
  `started_at` int(11) UNSIGNED DEFAULT NULL,
  `ended_at` int(11) UNSIGNED DEFAULT NULL,
  `last_execution_time` float DEFAULT NULL,
  PRIMARY KEY (`id_cron_job`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cron_job`
--

INSERT INTO `cron_job` (`id_cron_job`, `controller`, `action`, `limit`, `offset`, `running`, `success`, `started_at`, `ended_at`, `last_execution_time`) VALUES
(1, 'cron', 'index', 0, 0, 0, 1, 1588677501, 1588677501, 0.0355461),
(2, 'cron', 'index', 0, 0, 0, 1, 1588677542, 1588677542, 0.0337641),
(3, 'cron', 'index', 0, 0, 0, 1, 1588677577, 1588677577, 0.0142119),
(4, 'cron', 'index', 0, 0, 0, 1, 1588677728, 1588677729, 0.177749),
(5, 'cron', 'index', 0, 0, 0, 1, 1588677919, 1588677919, 0.158666),
(6, 'cron', 'index', 0, 0, 0, 1, 1588677933, 1588677933, 0.154202),
(7, 'cron', 'index', 0, 0, 0, 1, 1588677988, 1588677989, 0.154145),
(8, 'cron', 'index', 0, 0, 0, 1, 1588678048, 1588678048, 0.416149),
(9, 'cron', 'index', 0, 0, 0, 1, 1588678184, 1588678185, 0.401063);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `code` varchar(30) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `status`) VALUES
(1, 'euro', 'EUR', 1),
(2, 'US DOLLAR', 'USD', 1),
(3, 'GBP', 'GBP', 1),
(4, 'JPY', 'JPY', 1),
(5, 'AUD', 'AUD', 1),
(6, 'CAD', 'CAD', 1),
(7, 'PLN', 'PLN', 1);

-- --------------------------------------------------------

--
-- Table structure for table `drawdown`
--

DROP TABLE IF EXISTS `drawdown`;
CREATE TABLE IF NOT EXISTS `drawdown` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trader_id` int(11) NOT NULL,
  `drawdown` float NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drawdown`
--

INSERT INTO `drawdown` (`id`, `trader_id`, `drawdown`, `time`) VALUES
(6, 5, 1290000, '2020-02-06 08:52:59'),
(7, 5, 12545500, '2020-02-05 08:55:55'),
(8, 5, 125555, '2020-02-02 08:57:52'),
(9, 5, 4544540, '2020-02-04 08:59:47'),
(10, 5, 1200000, '2020-02-05 09:04:02'),
(11, 5, 522000, '2020-02-01 09:04:49'),
(12, 5, 1200000, '2020-02-02 09:19:50'),
(13, 5, 500002, '2020-02-02 09:19:50'),
(14, 5, 989999, '2020-02-01 09:21:11'),
(15, 5, 9877, '2020-02-06 09:21:11'),
(16, 5, 129789, '2020-02-05 09:27:30'),
(17, 5, 8956560, '2020-01-20 09:27:30'),
(18, 5, 454455, '2020-01-06 09:56:38'),
(19, 5, 1000, '2020-01-25 13:32:46'),
(20, 5, 1600, '2020-01-25 13:32:46'),
(21, 5, 99183.8, '2020-02-01 13:58:15'),
(22, 5, 6666670, '2020-02-26 09:07:37'),
(23, 5, 52029, '2020-02-27 08:20:09'),
(24, 5, 32108.4, '2020-02-28 08:20:03'),
(25, 5, 5205, '2020-04-20 21:02:38'),
(26, 8, 10000, '2020-05-05 11:25:33'),
(27, 5, 10000, '2020-05-05 11:27:28'),
(28, 228, 12302300, '2020-05-05 11:27:28');

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

DROP TABLE IF EXISTS `email_template`;
CREATE TABLE IF NOT EXISTS `email_template` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  UNIQUE KEY `idx-email_template-key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`id`, `key`) VALUES
(1, 'confirm-signup'),
(2, 'main-page'),
(3, 'reset-password'),
(4, 'verify');

-- --------------------------------------------------------

--
-- Table structure for table `email_template_translation`
--

DROP TABLE IF EXISTS `email_template_translation`;
CREATE TABLE IF NOT EXISTS `email_template_translation` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `templateId` int(11) UNSIGNED NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `hint` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-email_template_translation-templateId` (`templateId`),
  KEY `idx-email_template_translation-language` (`language`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_template_translation`
--

INSERT INTO `email_template_translation` (`id`, `templateId`, `language`, `subject`, `body`, `hint`) VALUES
(1, 2, 'en_US', 'main template', '<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">\r\n<style>\r\n#email-foot p{font-size:14px}\r\n</style>\r\n	 \r\n<div style=\"background-color:#f3f2f2;padding:20px\"><div class=\"adM\">\r\n    </div><div style=\"background-color:#fff;max-width:750px;margin:30px auto\"><div class=\"adM\">\r\n        \r\n        </div>\r\n		<div style=\"padding:20px\">\r\n		<div class=\"adM\">\r\n            </div>\r\n		 <img src=\"https://fxgrow.com/images/emails/email-logo.png\"  class=\"img-fluid\"><div class=\"a6S\" dir=\"ltr\" style=\"opacity: 0.01; left: 565.5px; top: 156px;\"><div id=\":1c0\" class=\"T-I J-J5-Ji aQv T-I-ax7 L3 a5q\" role=\"button\" tabindex=\"0\" aria-label=\"Download attachment logo.png\" data-tooltip-class=\"a1V\" data-tooltip=\"Download\"><div class=\"aSK J-J5-Ji aYr\"></div></div><div id=\":1c1\" class=\"T-I J-J5-Ji aQv T-I-ax7 L3 a5q\" role=\"button\" tabindex=\"0\" aria-label=\"Save attachment logo.png to Drive\" data-tooltip-class=\"a1V\" data-tooltip=\"Save to Drive\"><div class=\"wtScjd J-J5-Ji aYr aQu\"><div class=\"T-aT4\" style=\"display: none;\"><div></div><div class=\"T-aT4-JX\"></div></div></div></div></div>\r\n\r\n			<table width=\"100%\">\r\n                <tbody><tr>\r\n                    <td>\r\n                    </td>\r\n                    <td width=\"50%\">&nbsp;</td>\r\n                    \r\n                </tr>\r\n            </tbody>\r\n			</table>\r\n			<br>\r\n{body}\r\n<table width=\"100%\" id=\"email-foot\" style=\"margin:20px 0px\">\r\n                <tbody><tr>\r\n                    <td style=\"width:30%;text-align:center\">\r\n					<img class=\"img-fluid\" src=\"https://fxgrow.com/images/emails/automated-trading.png\"><br>\r\n					<p class=\"text-center\"style=\"margin-top:10px;font-weight:bold\">Automated Trading</p>\r\n				<p style=\"font-size: 9px;\">Join Mamgram and invest in the best traders in the market through your preferred broker</p>\r\n					</td>\r\n                    <td style=\"width:30%;text-align:center\">\r\n					<img class=\"img-fluid\" src=\"https://fxgrow.com/images/emails/secure-payment.png\"><br>\r\n					<p class=\"text-center\"style=\"margin-top:10px;font-weight:bold\">Automated Trading</p>\r\n				<p style=\"font-size: 9px;\">Join Mamgram and invest in the best traders in the market through your preferred broker</p>\r\n				</td>\r\n                    <td style=\"width:30%;text-align:center\">\r\n					<img class=\"img-fluid\" src=\"https://fxgrow.com/images/emails/guaranteed-investment.png\"><br>\r\n					<p class=\"text-center\"style=\"margin-top:10px;font-weight:bold\">Automated Trading</p>\r\n				<p style=\"font-size: 9px;\">Join Mamgram and invest in the best traders in the market through your preferred broker</p>\r\n				</td>\r\n				\r\n                    \r\n                </tr>\r\n            </tbody>\r\n			\r\n			</table>\r\n			</div>\r\n                   \r\n                    <table style=\"font-family:arial;font-size:12px;background:#e6e7e8!important\">\r\n                        <tbody><tr>\r\n                            <td style=\"padding:20px\">\r\n                                <p style=\"font-family:arial;font-size:12px\">\r\n                                    </p><p>\r\n									<strong>RISK WARNING</strong>\r\n                                    \r\n                                    Trading leveraged products such as Forex, CFDs involves substantial risk of loss and may not be\r\n                                    suitable for all investors, since leverage can work both to your advantage and disadvantage. As a result, trading such\r\n                                    products is risky and you may lose all of your invested capital. Therefore, you should not risk more than you are\r\n                                    prepared to lose. Before deciding to trade, you need to ensure that you understand the risks involved taking into account\r\n                                    your investment objectives and level of experience.  \r\n                                    </p>\r\n                                    <p>\r\n									<strong>CONFIDENTIALITY NOTICE</strong>\r\n                                    \r\n                                    The information in this message (e-mail) including any attachments is confidential and intended only for the addressee(s).\r\n                                    The recipient(s) of this message (e-mail) including any attachments are not allowed to share it with any other party\r\n                                    without the sender’s prior consent. If you are not the intended recipient of this message (e-mail), please promptly delete\r\n                                    this message (e-mail) from your system and notify the sender. Views or opinions presented in this message (e-mail) are\r\n                                    solely those of the author and do not necessarily represent those of FxGrow.\r\n                                    </p>\r\n                                    <p>\r\n									<strong>WARNING</strong>\r\n                                    \r\n                                    Reasonable <span class=\"il\">security</span> precautions were taken to ensure that this message (e-mail) including any attachments has been\r\n                                    scanned for malicious code (viruses). However, the sender cannot accept liability for any damage sustained as a result of\r\n                                    such code and advises you to perform your own checks before opening this message (e-mail) or any attachment.\r\n                                    </p>\r\n                                <p></p>\r\n                                <p>\r\n                                   </p>PLEASE CONSIDER THE ENVIRONMENT BEFORE PRINTING THIS E-MAIL.\r\n                                <p></p>\r\n                            </td>\r\n                        </tr>\r\n                    </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\r\n\r\n                    <p></p><p></p></div></div><div class=\"adL\">\r\n                </div></div>\r\n				<div class=\"adL\">\r\n            </div>\r\n</div>', 'All tokens wrapped in {} will be replaced by real data'),
(2, 3, 'en_US', 'Reset Password', '<p>Dear {name},</p>\r\n<p>\r\n    To reset Your Password follow this link : <a>{reset-link}</a>\r\n</p>', 'All tokens wrapped in {} will be replaced by real data'),
(3, 1, 'en_US', 'Email Confirmation', '<p>Dear {name},</p>\r\n<p>\r\n    Your confirmation key is : <a>{confirm-link}</a>\r\n</p>', 'All tokens wrapped in {} will be replaced by real data'),
(4, 4, 'en_US', 'Welcome to Mamgram <3', '<p>Dear {name},</p>\r\n<p>Thank you for choosing Mamgram.</p>\r\n<p>To verify your email address please click here, or copy and paste the following activation code in your\r\nsecure site account:{confirm-link}</p>\r\n\r\n<p>By activating your profile, you agree and accept Mamgram:</p>\r\n<ul>\r\n<li> Terms and Conditions</li>\r\n<li>  Privacy Policy</li>\r\n<li>  Risk Statement</li>\r\n</ul>\r\n<p>Our customer support team is available to assist you, for any queries you may have. Contact us by live chat,</p>\r\nby calling {phone} or via email {email}</p>\r\n<p>Sincerely, The Mamgram Team</p>', 'All tokens wrapped in {} will be replaced by real data');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `language_id` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name_ascii` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `language`, `country`, `name`, `name_ascii`, `status`) VALUES
('af-ZA', 'af', 'za', 'Afrikaans', 'Afrikaans', 0),
('ar-AR', 'ar', 'ar', '‏العربية‏', 'Arabic', 1),
('az-AZ', 'az', 'az', 'Azərbaycan dili', 'Azerbaijani', 0),
('be-BY', 'be', 'by', 'Беларуская', 'Belarusian', 0),
('bg-BG', 'bg', 'bg', 'Български', 'Bulgarian', 0),
('bn-IN', 'bn', 'in', 'বাংলা', 'Bengali', 0),
('bs-BA', 'bs', 'ba', 'Bosanski', 'Bosnian', 0),
('ca-ES', 'ca', 'es', 'Català', 'Catalan', 0),
('cs-CZ', 'cs', 'cz', 'Čeština', 'Czech', 0),
('cy-GB', 'cy', 'gb', 'Cymraeg', 'Welsh', 0),
('da-DK', 'da', 'dk', 'Dansk', 'Danish', 0),
('de-DE', 'de', 'de', 'Deutsch', 'German', 0),
('el-GR', 'el', 'gr', 'Ελληνικά', 'Greek', 0),
('en-GB', 'en', 'gb', 'English (UK)', 'English (UK)', 0),
('en-PI', 'en', 'pi', 'English (Pirate)', 'English (Pirate)', 0),
('en-UD', 'en', 'ud', 'English (Upside Down)', 'English (Upside Down)', 0),
('en-US', 'en', 'us', 'English (US)', 'English (US)', 1),
('eo-EO', 'eo', 'eo', 'Esperanto', 'Esperanto', 0),
('es-ES', 'es', 'es', 'Español (España)', 'Spanish (Spain)', 0),
('es-LA', 'es', 'la', 'Español', 'Spanish', 0),
('et-EE', 'et', 'ee', 'Eesti', 'Estonian', 0),
('eu-ES', 'eu', 'es', 'Euskara', 'Basque', 0),
('fa-IR', 'fa', 'ir', '‏فارسی‏', 'Persian', 0),
('fb-LT', 'fb', 'lt', 'Leet Speak', 'Leet Speak', 0),
('fi-FI', 'fi', 'fi', 'Suomi', 'Finnish', 0),
('fo-FO', 'fo', 'fo', 'Føroyskt', 'Faroese', 0),
('fr-CA', 'fr', 'ca', 'Français (Canada)', 'French (Canada)', 0),
('fr-FR', 'fr', 'fr', 'Français (France)', 'French (France)', 1),
('fy-NL', 'fy', 'nl', 'Frysk', 'Frisian', 0),
('ga-IE', 'ga', 'ie', 'Gaeilge', 'Irish', 0),
('gl-ES', 'gl', 'es', 'Galego', 'Galician', 0),
('he-IL', 'he', 'il', '‏עברית‏', 'Hebrew', 0),
('hi-IN', 'hi', 'in', 'हिन्दी', 'Hindi', 0),
('hr-HR', 'hr', 'hr', 'Hrvatski', 'Croatian', 0),
('hu-HU', 'hu', 'hu', 'Magyar', 'Hungarian', 0),
('hy-AM', 'hy', 'am', 'Հայերեն', 'Armenian', 0),
('id-ID', 'id', 'id', 'Bahasa Indonesia', 'Indonesian', 0),
('is-IS', 'is', 'is', 'Íslenska', 'Icelandic', 0),
('it-IT', 'it', 'it', 'Italiano', 'Italian', 0),
('ja-JP', 'ja', 'jp', '日本語', 'Japanese', 0),
('ka-GE', 'ka', 'ge', 'ქართული', 'Georgian', 0),
('km-KH', 'km', 'kh', 'ភាសាខ្មែរ', 'Khmer', 0),
('ko-KR', 'ko', 'kr', '한국어', 'Korean', 0),
('ku-TR', 'ku', 'tr', 'Kurdî', 'Kurdish', 0),
('la-VA', 'la', 'va', 'lingua latina', 'Latin', 0),
('lt-LT', 'lt', 'lt', 'Lietuvių', 'Lithuanian', 0),
('lv-LV', 'lv', 'lv', 'Latviešu', 'Latvian', 0),
('mk-MK', 'mk', 'mk', 'Македонски', 'Macedonian', 0),
('ml-IN', 'ml', 'in', 'മലയാളം', 'Malayalam', 0),
('ms-MY', 'ms', 'my', 'Bahasa Melayu', 'Malay', 0),
('nb-NO', 'nb', 'no', 'Norsk (bokmål)', 'Norwegian (bokmal)', 0),
('ne-NP', 'ne', 'np', 'नेपाली', 'Nepali', 0),
('nl-NL', 'nl', 'nl', 'Nederlands', 'Dutch', 0),
('nn-NO', 'nn', 'no', 'Norsk (nynorsk)', 'Norwegian (nynorsk)', 0),
('pa-IN', 'pa', 'in', 'ਪੰਜਾਬੀ', 'Punjabi', 0),
('pl-PL', 'pl', 'pl', 'Polski', 'Polish', 0),
('ps-AF', 'ps', 'af', '‏پښتو‏', 'Pashto', 0),
('pt-BR', 'pt', 'br', 'Português (Brasil)', 'Portuguese (Brazil)', 0),
('pt-PT', 'pt', 'pt', 'Português (Portugal)', 'Portuguese (Portugal)', 0),
('ro-RO', 'ro', 'ro', 'Română', 'Romanian', 0),
('ru-RU', 'ru', 'ru', 'Русский', 'Russian', 0),
('sk-SK', 'sk', 'sk', 'Slovenčina', 'Slovak', 0),
('sl-SI', 'sl', 'si', 'Slovenščina', 'Slovenian', 0),
('sq-AL', 'sq', 'al', 'Shqip', 'Albanian', 0),
('sr-RS', 'sr', 'rs', 'Српски', 'Serbian', 0),
('sv-SE', 'sv', 'se', 'Svenska', 'Swedish', 0),
('sw-KE', 'sw', 'ke', 'Kiswahili', 'Swahili', 0),
('ta-IN', 'ta', 'in', 'தமிழ்', 'Tamil', 0),
('te-IN', 'te', 'in', 'తెలుగు', 'Telugu', 0),
('th-TH', 'th', 'th', 'ภาษาไทย', 'Thai', 0),
('tl-PH', 'tl', 'ph', 'Filipino', 'Filipino', 0),
('tr-TR', 'tr', 'tr', 'Türkçe', 'Turkish', 0),
('uk-UA', 'uk', 'ua', 'Українська', 'Ukrainian', 0),
('vi-VN', 'vi', 'vn', 'Tiếng Việt', 'Vietnamese', 0),
('xx-XX', 'xx', 'xx', 'Fejlesztő', 'Developer', 0),
('zh-CN', 'zh', 'cn', '中文(简体)', 'Simplified Chinese (China)', 0),
('zh-HK', 'zh', 'hk', '中文(香港)', 'Traditional Chinese (Hong Kong)', 0),
('zh-TW', 'zh', 'tw', '中文(台灣)', 'Traditional Chinese (Taiwan)', 0);

-- --------------------------------------------------------

--
-- Table structure for table `language_source`
--

DROP TABLE IF EXISTS `language_source`;
CREATE TABLE IF NOT EXISTS `language_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language_source`
--

INSERT INTO `language_source` (`id`, `category`, `message`) VALUES
(1, 'array', 'apple'),
(2, 'mam', 'MamGram'),
(3, 'conf', 'Configuration'),
(4, 'user', 'Users'),
(5, 'word', 'Automate your trading'),
(6, 'follow', 'Follow Successful Signals and Strategies'),
(7, 'conf', 'mam mt4 manager password'),
(8, 'acc', 'Accounts\r\n'),
(9, 'traders', 'Leaderboard'),
(10, 'traders', 'Leaderboard'),
(11, 'home', 'Home');

-- --------------------------------------------------------

--
-- Table structure for table `language_translate`
--

DROP TABLE IF EXISTS `language_translate`;
CREATE TABLE IF NOT EXISTS `language_translate` (
  `id` int(11) NOT NULL,
  `language` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`language`),
  KEY `language_translate_idx_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language_translate`
--

INSERT INTO `language_translate` (`id`, `language`, `translation`) VALUES
(1, 'ar-AR', 'تفاح'),
(1, 'fr-FR', 'pomme'),
(2, 'ar-AR', 'مام'),
(2, 'fr-FR', 'mam-fr'),
(4, 'ar-AR', 'الزبائن'),
(4, 'fr-FR', 'client'),
(5, 'ar-AR', ' التداول بك'),
(5, 'fr-FR', 'automatiser votre trading'),
(6, 'ar-AR', 'اتبع'),
(7, 'ar-AR', 'الباسورد'),
(7, 'fr-FR', 'mot de passe'),
(8, 'ar-AR', 'حساب'),
(8, 'fr-FR', 'Compts'),
(9, 'ar-AR', 'المتداواون'),
(9, 'fr-FR', 'commerçants'),
(11, 'ar-AR', 'الصفحة الرئيسية'),
(11, 'fr-FR', 'Page Principale');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_time` double DEFAULT NULL,
  `prefix` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_log_level` (`level`),
  KEY `idx_log_category` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mam_accounts`
--

DROP TABLE IF EXISTS `mam_accounts`;
CREATE TABLE IF NOT EXISTS `mam_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` bigint(20) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `trades` int(11) NOT NULL DEFAULT 0,
  `balance` double NOT NULL DEFAULT 0,
  `margin` double NOT NULL DEFAULT 0,
  `equity` double NOT NULL DEFAULT 0,
  `leverage` int(11) NOT NULL DEFAULT 0,
  `platform` varchar(30) DEFAULT '',
  `margin_percentage` double NOT NULL DEFAULT 0,
  `account_type` varchar(50) DEFAULT 'slave',
  `currency` varchar(50) DEFAULT NULL,
  `is_live` tinyint(4) DEFAULT 0,
  `user_id` int(11) DEFAULT NULL,
  `brokers_id` int(11) DEFAULT NULL,
  `active_account` tinyint(4) NOT NULL DEFAULT 0,
  `account_status` varchar(50) NOT NULL DEFAULT 'not completed',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `user_id` (`user_id`),
  KEY `login` (`login`),
  KEY `brokers_id` (`brokers_id`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mam_accounts`
--

INSERT INTO `mam_accounts` (`id`, `login`, `name`, `trades`, `balance`, `margin`, `equity`, `leverage`, `platform`, `margin_percentage`, `account_type`, `currency`, `is_live`, `user_id`, `brokers_id`, `active_account`, `account_status`, `created_at`) VALUES
(1, 1111, 'DM_b0kcUjrs', 0, 10000, 0, 10000, 100, 'mt4', 0, 'slave', 'EUR', 0, 3, 2, 1, 'completed', '2020-04-30 09:33:46'),
(4, 303030, 'RL_BRPE2VbJ', 0, 10000, 0, 10000, 100, 'mt4', 0, 'slave', NULL, 1, 3, 2, 0, 'completed', '2020-04-30 09:33:46'),
(5, 607081, 'ice cream', 0, 10000, 0, 10000, 100, 'mt4', 0, 'master', 'EUR', 1, 4, 2, 0, 'completed', '2020-04-30 09:33:46'),
(7, 607081, 'lollipop', 0, 5000, 0, 5000, 100, 'mt4', 0, 'slave', 'GBP', 0, 4, 2, 0, 'completed', '2020-04-30 09:33:46'),
(8, 607081, 'oreo', 0, 10000, 0, 10000, 100, 'mt4', 0, 'master', 'USD', 1, 98, 2, 1, 'completed', '2020-04-30 09:33:46'),
(10, 607081, 'cheesecake', 0, 10000, 0, 10000, 100, 'mt4', 0, 'master', 'USD', 1, 98, 2, 0, 'completed', '2020-04-30 09:33:46'),
(11, 1111115791, 'fxgrow_one', 0, 10000, 0, 10000, 100, 'mt4', 0, 'slave', 'USD', 1, 37, 2, 0, 'completed', '2020-04-30 09:33:46'),
(146, 1111115791, 'fxgrow_two', 0, 10000, 0, 10000, 100, 'mt4', 0, 'slave', 'USD', 1, 37, 2, 1, 'completed', '2020-04-30 09:33:46'),
(181, NULL, 'DM_gDS97n1b', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 50, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(182, NULL, 'DM_pbm1WQjq', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 51, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(183, NULL, 'DM_Q7KL02OD', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 52, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(184, NULL, 'DM_E3Mbf1H0', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 53, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(185, 112233, 'bfjegikdlcha', 10, 10000, 0, 10000, 100, 'mt4', 0, 'slave', 'EUR', 0, 37, 2, 0, 'completed', '2020-04-30 09:33:46'),
(227, 101010, 'ljacdibekfgh', 10, 10000, 0, 10000, 100, 'mt4', 0, 'master', 'EUR', 1, 37, 2, 0, 'completed', '2020-04-30 09:33:46'),
(228, 101010, 'akehlcbjdfgi', 10, 10000, 0, 10000, 100, 'mt4', 0, 'master', 'EUR', 1, 37, 2, 0, 'completed', '2020-04-30 09:33:46'),
(229, NULL, 'DM_rILT3kb8', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 64, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(230, 607081, 'DM_3DVaWG7M', 0, 5000, 0, 0, 100, 'mt5', 0, 'slave', 'GBP', 0, 4, 2, 0, 'completed', '2020-04-30 09:33:46'),
(231, 607081, 'DM_pQscP1af', 0, 500, 0, 500, 300, 'mt5', 0, 'slave', 'EUR', 0, 4, 2, 0, 'completed', '2020-04-30 09:33:46'),
(232, NULL, 'DM_LfB3FwUd', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 67, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(233, NULL, 'DM_RJ6hK59s', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 68, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(234, NULL, 'DM_tUsw0heB', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 69, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(235, NULL, 'DM_faTW0I2D', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 70, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(236, NULL, 'DM_tMwpLrVA', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 71, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(237, NULL, 'DM_Q9sU7EPe', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 72, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(238, NULL, 'DM_M0c64L82', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 73, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(239, NULL, 'DM_WugoaAJE', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 75, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(240, NULL, 'DM_E2easbmd', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 76, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(241, NULL, 'DM_g4Q8Uzrb', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 77, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(242, NULL, 'DM_4z8WHmDK', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 78, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(243, NULL, 'DM_sFLcfAgV', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 79, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(244, NULL, 'DM_KfBRPs6c', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 80, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(245, NULL, 'DM_Fc6bmaMT', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 81, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(246, NULL, 'DM_EMsplKVA', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 82, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(247, NULL, 'DM_KRIC49bo', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 83, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(248, NULL, 'DM_n0E1W5v2', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 84, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(249, NULL, 'DM_kCFqSLeE', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 85, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(250, NULL, 'DM_wnDMsNAe', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 86, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(251, 607081, 'DM_KHv8LBn6', 0, 5000, 0, 0, 200, 'mt5', 0, 'slave', 'USD', 0, 4, 2, 1, 'completed', '2020-04-30 09:33:46'),
(254, 607081, 'RL_Jap6wERd', 0, 0, 0, 0, 0, '', 0, 'slave', 'USD', 1, 4, 1, 0, 'waiting confirmation', '2020-04-30 09:33:46'),
(255, NULL, 'DM_oIhN6Gtb', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 87, 2, 1, 'not completed', '2020-04-30 09:33:46'),
(256, NULL, 'DM_ad1hgjVl', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 93, 2, 1, 'not completed', '2020-05-05 10:20:06'),
(257, NULL, 'RL_khjz4ns5', 0, 0, 0, 0, 0, '', 0, 'slave', NULL, 1, 4, 2, 0, 'not completed', '2020-05-07 06:58:30'),
(258, NULL, 'DM_N1gkBIPf', 0, 0, 0, 0, 0, 'mt5', 0, 'slave', NULL, 0, 104, 2, 1, 'not completed', '2020-05-07 08:00:53'),
(259, 607081, 'DM_vpsbzRjC', 0, 500, 0, 500, 200, 'mt5', 0, 'slave', 'USD', 0, 106, 2, 1, 'completed', '2020-05-19 07:05:45'),
(260, NULL, 'RL_vmK2B084', 0, 0, 0, 0, 0, '', 0, 'slave', NULL, 1, 106, 3, 0, 'not completed', '2020-05-19 07:07:40');

--
-- Triggers `mam_accounts`
--
DROP TRIGGER IF EXISTS `update_peak`;
DELIMITER $$
CREATE TRIGGER `update_peak` AFTER UPDATE ON `mam_accounts` FOR EACH ROW BEGIN

declare tbalance double ;

if(new.active_account <>old.active_account) then 
select balance into tbalance from mam_accounts where id=NEW.id ;
if(new.active_account=1) then
update traders_info set minpeak=tbalance ,maxpeak=tbalance where trader_id=NEW.id ;
END IF;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mam_configuration`
--

DROP TABLE IF EXISTS `mam_configuration`;
CREATE TABLE IF NOT EXISTS `mam_configuration` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `conf_key` varchar(50) NOT NULL,
  `value` varchar(200) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`conf_key`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mam_configuration`
--

INSERT INTO `mam_configuration` (`id`, `conf_key`, `value`, `description`) VALUES
(1, 'mam_service_port', '6100', 'mam service port'),
(2, 'mam_service_host', 'localhost', ' mam server host'),
(3, 'mam_mt4_manager_login', '12', 'mam mt4 manager login'),
(4, 'mam_mt4_manager_password', 'abc123', 'mam mt4 manager password'),
(5, 'mam_mt4_server_ip', '185.97.161.11:443', 'mam mt4 server');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1551792571),
('m140506_102106_rbac_init', 1551792719),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1551792719),
('m180523_151638_rbac_updates_indexes_without_prefix', 1551792720),
('ymaker\\email\\templates\\migrations\\m171119_140800_create_email_template_entities', 1554360701),
('lajax\\translatemanager\\migrations\\namespaced\\m141002_030233_translate_manager_ns', 1554461803),
('m141002_030233_translate_manager', 1564402196),
('m140428_105041_db_translation', 1564569992),
('m141018_105939_create_table_upload', 1575385407),
('tigrov\\country\\migrations\\m170405_112954_init', 1586794232),
('backend\\modules\\log\\migrations\\m141106_185632_log_init', 1588677457),
('m150927_060316_cronjob_init', 1588677457);

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

DROP TABLE IF EXISTS `promotions`;
CREATE TABLE IF NOT EXISTS `promotions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) NOT NULL,
  `image_location` varchar(2083) DEFAULT NULL,
  `amount` double NOT NULL,
  `start_date` timestamp NULL DEFAULT current_timestamp(),
  `end_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bk_index` (`broker_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promotions`
--

INSERT INTO `promotions` (`id`, `broker_id`, `image_location`, `amount`, `start_date`, `end_date`) VALUES
(4, 2, 'aafx.png', 5000, '2020-01-31 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

DROP TABLE IF EXISTS `rates`;
CREATE TABLE IF NOT EXISTS `rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ib_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `master_account` int(11) DEFAULT NULL,
  `slave_account` int(11) NOT NULL,
  `rate` float DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `user_id` (`ib_id`),
  KEY `client_id` (`client_id`),
  KEY `client_id_2` (`client_id`),
  KEY `master_ac` (`master_account`),
  KEY `investment_ac` (`slave_account`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `ib_id`, `client_id`, `master_account`, `slave_account`, `rate`, `create_date`) VALUES
(36, 3, 16, 5, 100, 4, '2020-03-03 10:16:44'),
(37, 2, 16, 7, 100, 2, '2020-03-03 10:16:51');

--
-- Triggers `rates`
--
DROP TRIGGER IF EXISTS `after_rating`;
DELIMITER $$
CREATE TRIGGER `after_rating` AFTER INSERT ON `rates` FOR EACH ROW BEGIN
declare tid int(11);
declare trate int(5) ;
declare tavg double ;
SELECT master_account into tid FROM rates WHERE  id=New.id;
SELECT rate into trate FROM rates WHERE id=New.id;
SELECT avg(rate) into tavg FROM rates WHERE master_account=New.master_account;

if tid then
  update traders_info set rate=tavg  where trader_id=tid ;
end if;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

DROP TABLE IF EXISTS `relations`;
CREATE TABLE IF NOT EXISTS `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ib_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `master_account` int(11) NOT NULL,
  `slave_account` int(11) NOT NULL,
  `follow_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `unfollow_date` timestamp NULL DEFAULT NULL,
  `copy_type` varchar(50) DEFAULT NULL,
  `value` double DEFAULT NULL,
  `amount_following` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ib_id` (`ib_id`),
  KEY `client_id` (`client_id`),
  KEY `master_account` (`master_account`),
  KEY `slave_account` (`slave_account`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `ib_id`, `client_id`, `master_account`, `slave_account`, `follow_date`, `unfollow_date`, `copy_type`, `value`, `amount_following`) VALUES
(55, 2, 37, 10, 11, '2020-03-12 12:18:17', '2020-03-12 12:43:42', 'lot', 1, 10000),
(59, 2, 37, 10, 11, '2020-03-12 15:19:24', '2020-03-12 15:24:53', 'lot', 1, 10000),
(60, 2, 37, 10, 11, '2020-03-13 10:36:33', NULL, 'lot', 1, 10000),
(64, 98, 4, 5, 7, '2020-05-07 10:22:35', '2020-05-07 07:42:05', 'lot', 0.01, 5000),
(65, 98, 4, 5, 7, '2020-05-07 10:48:15', '2020-05-07 07:48:36', 'lot', 0.01, 5000),
(66, 98, 4, 4, 251, '2020-05-08 10:00:33', '2020-05-08 07:10:14', 'lot', 0.01, 5000),
(67, 98, 4, 8, 251, '2020-05-08 10:10:26', NULL, 'percentage', 20, 5000),
(68, 98, 106, 227, 259, '2020-05-19 07:16:20', '2020-05-19 04:17:05', 'percentage', 12, 500),
(69, 98, 106, 10, 259, '2020-05-19 07:17:20', NULL, 'lot', 0.05, 500);

-- --------------------------------------------------------

--
-- Table structure for table `traders_info`
--

DROP TABLE IF EXISTS `traders_info`;
CREATE TABLE IF NOT EXISTS `traders_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trader_id` int(11) NOT NULL,
  `trader_name` varchar(100) NOT NULL,
  `investors` int(11) NOT NULL DEFAULT 0,
  `profit` float NOT NULL DEFAULT 0,
  `roi` float NOT NULL DEFAULT 0,
  `country_code` varchar(4) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(2083) DEFAULT NULL,
  `is_live` tinyint(4) DEFAULT NULL,
  `trusting` int(11) DEFAULT NULL,
  `ea` int(11) DEFAULT NULL,
  `joined_at` timestamp NULL DEFAULT current_timestamp(),
  `left_at` int(11) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `equity` double DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `logged` tinyint(4) DEFAULT NULL,
  `strategy` varchar(500) DEFAULT NULL,
  `minpeak` float NOT NULL DEFAULT 0,
  `maxpeak` float NOT NULL DEFAULT 0,
  `maxddvalue` float NOT NULL DEFAULT 0,
  `ten_month_chart` varchar(500) DEFAULT NULL,
  `comission_received` float DEFAULT NULL,
  `comission_pending` float DEFAULT NULL,
  `open_positions` int(11) NOT NULL DEFAULT 0,
  `trades` int(11) NOT NULL DEFAULT 0,
  `avg_pips` double NOT NULL DEFAULT 0,
  `wining_trades` double NOT NULL DEFAULT 0,
  `total_profit_pips` double NOT NULL DEFAULT 0,
  `last_updated` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trader_id_2` (`trader_id`),
  KEY `trader_id` (`trader_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `traders_info`
--

INSERT INTO `traders_info` (`id`, `trader_id`, `trader_name`, `investors`, `profit`, `roi`, `country_code`, `country_name`, `profile_pic`, `is_live`, `trusting`, `ea`, `joined_at`, `left_at`, `currency`, `equity`, `rate`, `logged`, `strategy`, `minpeak`, `maxpeak`, `maxddvalue`, `ten_month_chart`, `comission_received`, `comission_pending`, `open_positions`, `trades`, `avg_pips`, `wining_trades`, `total_profit_pips`, `last_updated`, `user_id`) VALUES
(1, 5, 'Ricardo Federo', -1, 12550, 100.7, 'cn', 'lebanon', '300_5.jpg', 1, 1, 1, '2020-02-25 14:25:59', NULL, 'GBPUSD', NULL, 4, NULL, 'Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months', 10000, 10000, 10000, '{\"points\":[-58954.800000000025,-140392.30000000008,49555.30000000012,14.87,-8666,-6.3,18097.9,33719.7]}', NULL, NULL, 23, 587, 245.32, 68, 863, '2020-05-13 00:00:00', 98),
(3, 4, 'Hassan Harb', -1, 2669, -2.95, 'lb', 'cyprus', 'hasan.jpg', 1, 2, 0, '2020-02-25 14:48:16', NULL, 'USDCAD', NULL, 2, NULL, 'Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months', 10000, 10000, 32108.4, '{\"points\":[10, 20, 15, 30, 25, 40, 25, 30, 50]}', NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 98),
(4, 8, 'Stephanie Ghosn', 1, 78521, 100.7, 'us', 'united states', '100_12.jpg', 1, 1, 0, '2020-02-25 14:48:32', NULL, 'USDCAD', NULL, 5, NULL, 'Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months', 10000, 10000, 10000, '{\"points\":[-58954.800000000025,-140392.30000000008,49555.30000000012,14.87,-8666,-6.3,18097.9,33719.7]}', NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 98),
(5, 10, 'David Akua', 1, 66233, 100.7, 'kw', 'kuweit', '300_14.jpg', 0, 1, 1, '2020-02-25 14:48:58', NULL, 'USDCAD', NULL, 3, NULL, 'Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months', 10000, 10000, 0, '{\"points\":[-58954.800000000025,-140392.30000000008,49555.30000000012,14.87,-8666,-6.3,18097.9,33719.7]}', NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 98),
(6, 227, 'Claudin Marc', -1, 88954, 100, 'ci', 'lebanon', 'user5.jpg', 0, 2, 0, '2020-03-18 18:58:24', NULL, NULL, NULL, 2, NULL, 'Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months', 10000, 10000, 0, '{\"points\":[]}', NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 98),
(7, 228, 'Nadine Fakhro', 5, 125741, 100, 'lb', 'lebanon', 'designer.jpg', 1, 2, 1, '2020-03-18 19:00:32', NULL, 'EURUSD', 10000, 4, NULL, 'Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months Professional trader with successful strategies in the two previous months', 10000, 12312300, 12302300, '{\"points\":[]}', NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 98);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(150) NOT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  `unconfirmed_email` varchar(150) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `confirmation_key` varchar(200) DEFAULT NULL,
  `confirmed_at` timestamp NULL DEFAULT NULL,
  `phone` int(100) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'client',
  `status` varchar(11) NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text DEFAULT NULL,
  `registration_ip` varchar(20) DEFAULT NULL,
  `last_login_ip` varchar(20) DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `access_token_expired_at` int(11) DEFAULT NULL,
  `country_code` char(2) DEFAULT NULL,
  `image_location` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `country_code` (`country_code`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `auth_key`, `unconfirmed_email`, `password`, `password_reset_token`, `confirmation_key`, `confirmed_at`, `phone`, `type`, `status`, `created_at`, `updated_at`, `description`, `registration_ip`, `last_login_ip`, `last_login_at`, `access_token_expired_at`, `country_code`, `image_location`) VALUES
(1, 'nour', 'super123', 'nourhanm+super@fxgrow.com', NULL, NULL, 'e99a18c428cb38d5f260853678922e03', NULL, NULL, NULL, 89754455, 'client', 'active', '2020-02-07 11:23:42', NULL, '05222wqqww', NULL, '::1', '2020-02-27 08:15:35', 1582877735, 'LB', NULL),
(2, 'admin', 'admin', 'nourhanm+admin@fxgrow.com', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, 55555, 'client', 'active', '2020-02-07 11:23:42', NULL, NULL, NULL, '::1', '2020-02-27 08:15:16', 1582877716, 'LB', NULL),
(3, 'noura', 'noura', 'nourhanm+s@fxgrow.com', 'K6pqleNppGT3wwClOZ1baouL6aJKwbVc', NULL, '60ad53d1b31f23d49fa2a9b9a7103eea', NULL, '', '2020-02-25 14:10:33', 78787878, 'client', 'active', '2020-02-25 02:10:15', NULL, NULL, '::1', '::1', '2020-03-03 00:49:27', 1583326167, 'LB', NULL),
(4, 'Hassan A. Harb', 'HasFX', 'hharb@fxgrow.com', '4297f44b13955235245b2497399d7a93', NULL, 'e10adc3949ba59abbe56e057f20f883e', '45gRj7N1MeP5Dv0iLNXVaWXi8moKCP9B_1586613787', '', '2020-02-25 14:14:59', 70420844, 'client', 'active', '2020-02-25 02:14:36', '2020-04-21 20:36:09', NULL, '::1', '::1', '2020-05-07 05:05:07', 1588925107, 'LB', '100_12.jpg'),
(20, 'jhjhbdffd', 'dffdff', 'hharb+admin@fxgrow.com', 'LTwsjskJUxzJ4A72mIPahr_ae70Eo5cK', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'e088epQSlCuACRzsXcmqBup2m3ce3qa6_1583249613', '', '2020-03-03 14:05:11', 2147483647, 'admin', 'active', '2020-03-03 02:03:53', NULL, NULL, '::1', '::1', '2020-03-03 03:32:06', 1583335926, 'LB', NULL),
(32, 'haiddarr', 'haiddarr', 'sahaidadrr@fxgrow.com', 'MhdNXffq6dtbtmiFHI5T6OMcn0OaSCnB', NULL, '3d4bef235f4b12ed394b24eae880d8a7', NULL, 'd41d8cd98f00b204e9800998ecf8427ea7505f8fdbbdb3cc08a65c421bdc144a', NULL, 78824568, 'client', 'active', '2020-03-06 02:58:40', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(33, 'haiddarrr', 'haiddarrr', 'sahaidadrrr@fxgrow.com', '1GV5_34GYA-TtSgTxc03lNtSZEWPu4Tj', NULL, '3d4bef235f4b12ed394b24eae880d8a7', NULL, '', '2020-03-11 08:13:02', 78824568, 'client', 'active', '2020-03-11 08:12:48', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(34, 'haiddarrrr', 'haiddarrrr', 'haidar@fxgrow.com', '6NIn5m8hBBdZ7Rlmw-qpeCp-XGRJbus1', NULL, '3d4bef235f4b12ed394b24eae880d8a7', NULL, '', '2020-03-11 08:13:02', 78824568, 'client', 'active', '2020-03-11 08:14:55', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(37, 'Haidar', 'haidarssf', 'sahaidar@fxgrow.com', 'qWFfi_V4YPtJ4bUIj90N8K2v0elEkJci', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, '', '2020-03-11 10:47:01', 78824568, 'client', 'active', '2020-03-11 10:46:28', NULL, NULL, '127.0.0.1', '127.0.0.1', '2020-03-24 09:21:11', 1585128071, 'LB', NULL),
(46, 'Haidar', 'haidarssff', 'sahaidarr@fxgrow.com', 'KKc_mKOf-nmFIxz9gh9vkrkOtf87JgFD', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e7e7761b2d4c537b42007df4093f0427e', NULL, 78824568, 'client', 'active', '2020-03-12 02:13:22', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(47, 'Haidar', 'hasfsr', 'sahaidar+555f555@fxgrow.com', 'v9aZCrp8ST6P4H_Hr8cyEKvFW6wGQ6j2', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e9e5b783b1e2828c888aaf1a311105882', NULL, 78824568, 'client', 'active', '2020-03-16 07:58:14', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(48, 'Haidar', 'hasffsr', 'sahaidar+555f5f55@fxgrow.com', 'spijGPBgzTWZzRCi5XgVyXaZt4vuGX82', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e689d5c52f772fcab844704bed4abef36', NULL, 78824568, 'client', 'active', '2020-03-16 07:58:26', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(49, 'Haidar', 'hasfffsr', 'sahaidar+555ff5f55@fxgrow.com', '5XNzqFs0dpYeW7hXECUW3ZdEK4cRB1yw', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e1e2ed51c175529c40673c0cba6ae0693', NULL, 78824568, 'client', 'active', '2020-03-16 08:02:46', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(50, 'Haidar', 'hasffgfsr', 'sahaidar+555ff5fg55@fxgrow.com', 'h8h7np5U34GyaxWwjxA0HzmGZxfzHmUW', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e44dbb45112e73864a08f79fad0450e94', NULL, 78824568, 'client', 'active', '2020-03-17 08:55:07', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(51, 'Haidar', 'hasffgffsr', 'sahaidar+555ff5ffg55@fxgrow.com', 'SKZuFlmy0KAya8DNWFXfihiwzxNzYixl', NULL, '30822a851a5b1f59e2c6381f431d9093', NULL, 'd41d8cd98f00b204e9800998ecf8427ec3d301c076a69d8ed8d6df66871fee67', NULL, 78824568, 'client', 'active', '2020-03-17 08:57:17', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(52, 'Haidar', 'hasffgfgfsr', 'sahaidar+555gff5ffg55@fxgrow.com', 'HkHfikh-YFLaCo8VTecvesB1C5vTVqAe', NULL, '30822a851a5b1f59e2c6381f431d9093', NULL, 'd41d8cd98f00b204e9800998ecf8427e66eeb19f44404da29caac2751acec9c5', NULL, 78824568, 'client', 'active', '2020-03-17 09:52:59', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(53, 'Haidar', 'haass', 'hharb+55555@fxgrow.com', '5kfgNbi2BXQ22fnhTOFmPSHWb2iYUHy2', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, '', '2020-03-17 15:49:08', 78824568, 'client', 'active', '2020-03-17 03:48:47', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(54, 'Haidar', 'hasdss', 'sahaidasdr+55555@fxgrow.com', 'plXu2mPW5rU29PQKO23sOC6RjjfQlHYJ', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e8ac051d24035a1f2f062749d40f4ccfc', NULL, 78824568, '6', '13', '2020-03-26 23:37:12', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(58, 'Haidar', 'hasdddss', 'sahaidddasdr+55555@fxgrow.com', '5Uu1P2rTNoPjBiH55IyogRhupnjaPn_L', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e62687c6f923929fd7fd6523506990dec', NULL, 78824568, '6', '13', '2020-03-26 23:53:42', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(59, 'Haidar', 'hasdsddss', 'sahaisdddasdr+55555@fxgrow.com', '6J7Pw79f78DTXZZoH9SHMZL14MMrfldZ', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427ee2f25df7f6c29e84d6453ff89df2cd62', NULL, 78824568, '6', '13', '2020-03-26 23:55:02', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(60, 'Haidar', 'hasdxxsddss', 'sahaisxxdddasdr+55555@fxgrow.com', '07k5hvfFLLczfn3Kdk2fdPVAv4T0-Ab1', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427ec0a5d67cf068f92015f831acd13caa26', NULL, 78824568, '6', '13', '2020-03-26 23:56:23', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(61, 'Haidar', 'hasdxdxsddss', 'sahaisxxdddasddr+55555@fxgrow.com', 'NQZks2rlq0XigEo5lWVjUSEqqWvsnKC4', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e2f1942c008184657bbc04b3950d6022c', NULL, 78824568, '6', '13', '2020-03-26 23:56:52', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(62, 'Haidar', 'hasdssxdxsddss', 'sahaisxssxdddasddr+55555@fxgrow.com', 'TXvcNu6qWije25UU0Eeim6awLjSQodse', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e62b87a1d4893022962c22fd2bddcc4d4', NULL, 78824568, '6', '13', '2020-03-26 23:58:05', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(63, 'Haidar', 'hxasdssxdxsddss', 'sahaisxxssxdddasddr+55555@fxgrow.com', 'ggwS_ZRMqVuYVXrFivkC-Q08hU2vfUqT', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e91c6ae82b636e5585d7f4667c8825573', NULL, 78824568, '6', '13', '2020-03-26 23:58:38', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(64, 'Haidar', 'hxsasdssxdxsddss', 'sahasisxxssxdddasddr+55555@fxgrow.com', 'oPxpwcEr5dDtVotyIvu9Al0wA-KFnAkC', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e1f5054df55d1301b6ca60ad4a81e41b8', NULL, 78824568, '6', '13', '2020-03-27 00:00:32', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(65, 'Haidar', 'hxsasdssxdddxsddss', 'sahasisxxssxdddddasddr+55555@fxgrow.com', 'jdfr_OFU9jTS6R9hECMsXGpz1ObogPiO', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e8542793117072fd0057be075e298a840', NULL, 78824568, '6', '13', '2020-03-27 00:00:41', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(66, 'Haidar', 'hxssasdssxdddxsddss', 'sahasissxxssxdddddasddr+55555@fxgrow.com', 's2RtKnuaELg8eA0rTV3QDYDIFxWQZEgY', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e872103f29eb6b3601c35e7fbee8905d5', NULL, 78824568, '6', '13', '2020-03-27 00:03:49', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(67, 'Haidar', 'hxssasdsxxsxdddxsddss', 'sahasissxxxxssxdddddasddr+55555@fxgrow.com', 'TEhRpWqi4LJU6KB2QSLT9dKpv6mHqoAC', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427eb87195ee10bd8764ae25b86accf5df43', NULL, 78824568, '6', '13', '2020-03-27 00:04:04', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(68, 'Haidar', 'hxssssasdsxxsxdddxsddss', 'sahasissssxxxxssxdddddasddr+55555@fxgrow.com', 'pT3Woh8jQ2TnANpJAu7jPaA1WygZOIXA', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e6e1d6a757948ea5a76dffc707bba900e', NULL, 78824568, '6', '13', '2020-03-27 00:08:35', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(69, 'Haidar', 'hxsstressasds', 'hharb+fghfsbfddgs@fxgrow.com', 'ZfNLlqwWIFSvQg9ysDVhVCMleITRnoav', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, '', '2020-03-27 14:11:57', 78824568, '6', '3', '2020-03-27 00:09:31', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(70, 'Haidar', 'hxsstresssasds', 'hharb+fghfsbsfddgs@fxgrow.com', '6BDqxE5D6wC7fwA4P0vildGBHiTchBqR', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, '', '2020-03-27 14:17:50', 78824568, 'client', '3', '2020-03-27 00:17:06', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(71, 'Haidar', 'hxssxtresssasds', 'hharb+fgxhfsbsfddgs@fxgrow.com', 'FLkRZsK2_5pVxNups4W-m-89TyXbmQ6p', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, 'd41d8cd98f00b204e9800998ecf8427e4d9e7bbcf47ce8cdc98e3e6e626ac71a', NULL, 78824568, 'client', 'active', '2020-03-27 00:30:45', NULL, NULL, '127.0.0.1', NULL, NULL, NULL, 'LB', NULL),
(72, 'Haidar', 'hxssxtsresssasds', 'hharb+fgxshfsbsfddgs@fxgrow.com', 'MVd_ciI7Bg-kd6vR3oShzEDKzVvS1ri3', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, '', '2020-03-27 12:32:34', 78824568, 'client', 'active', '2020-03-27 00:32:11', NULL, NULL, '127.0.0.1', '127.0.0.1', '2020-03-27 00:33:02', 1585405982, 'LB', NULL),
(73, 'Haidar', 'hxssxtssresssasds', 'hharb+fgxsshfsbsfddgs@fxgrow.com', 'fqSWdknIPUFMAqzHCIHk4-fiKzxpgyD_', NULL, '7742dd227a50b53d93524b5a6299dc75', NULL, '', '2020-03-27 12:54:09', 78824568, 'client', 'active', '2020-03-27 00:53:26', NULL, NULL, '127.0.0.1', '127.0.0.1', '2020-03-30 08:51:45', 1585655505, 'LB', NULL),
(74, 'sdfdsf', 'sdfdsf', 'hharb+dsdjvbdj@fxgrow.com', 'ZZcyrzSldlZB5As2horDCtWAIKNt5YgW', NULL, '03431af2dd9e7baf69cab6e65b781de0', NULL, 'd41d8cd98f00b204e9800998ecf8427eed79592af04547e720b7f3b051c701c2', NULL, 453, 'client', 'active', '2020-04-08 00:35:57', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(75, 'sdfdsf', 'acdf', 'hharb+99787@fxgrow.com', 'sjU1TEBYkRxlDxHjObYn7HPzPJLVIk1i', NULL, 'ed0af9f0ebda3fc32124c3b5849dbd45', NULL, 'd41d8cd98f00b204e9800998ecf8427e2af92c21ca0044d0e15f286cac1e02de', NULL, 453, 'client', 'active', '2020-04-08 00:42:04', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(76, 'adcad', 'dsvdv', 'hharb+dsddddjvbdj@fxgrow.com', '25yV_WTbj8gg7qvKS_eDXhV3XrYMAg-S', NULL, 'dfc05738d31f79ab9d9726466a526648', NULL, 'd41d8cd98f00b204e9800998ecf8427e4eb7a8bfde9be55f6a626f7ea8043455', NULL, 453, 'client', 'active', '2020-04-08 00:43:20', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(77, 'sadasd', 'fwfe', 'hharb+mn@fxgrow.com', '7kIKUNWNmgrsD24sNUNK5EJEIxlsrHIG', NULL, '0ee7dddb3d3ae3b07681ec4a8975c1a8', NULL, 'd41d8cd98f00b204e9800998ecf8427e693173cc1676b9c20e5c2b4cea53adc8', NULL, 32343432, 'client', 'active', '2020-04-10 23:18:48', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(78, 'hasan code', 'code', 'hharb+code@fxgrow.com', 'Mgac49pt3HxMAYw74UDe1uAK_dJGMzkI', NULL, '67b5ecb25d44544904b7ff98e903a7bd', NULL, '', '2020-04-13 18:51:33', 71676989, 'client', 'active', '2020-04-13 06:48:14', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(79, 'adcad', 'fgfdg', 'hharb+997c7@fxgrow.com', '2VqOrlflFLRzuj8UpGaJAsP-ReKwTAJh', NULL, '9130668e0e029aa27e57c929d7c81fba', NULL, 'd41d8cd98f00b204e9800998ecf8427e2ab4dbea2bbf25c45abcdca9ca02b6c9', NULL, 71676989, 'client', 'active', '2020-04-13 06:53:17', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(80, 'sdfdsf', 'hasandd', 'hharb+mnj@fxgrow.com', 'M5xjlum38TDg8TEcMUn5AKO8MojaXOuU', NULL, '6ae625d653d9f99a2f254ae8133ab329', NULL, 'd41d8cd98f00b204e9800998ecf8427e27e4cb81b326de92f38ce0141c9a3327', NULL, 453, 'client', 'active', '2020-04-13 06:54:49', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(81, 'hasan codes', 'ssss', 'hharb+yet@fxgrow.com', 'SOLTAkkTEkm8F-CjmdVkPc2w-fmHf6x8', NULL, 'cae2d2bf34b23044e58e08a8687fd891', NULL, 'd41d8cd98f00b204e9800998ecf8427edd3c9ecc13e9e1523a1b34f624d952d1', NULL, 71676989, 'client', 'active', '2020-04-13 06:55:53', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(82, 'sdfdsf', 'sdsd', 'hharb+khsd@fxgrow.com', 'cZGaxJOevu98mEUYtpv5x_TqKU5T28Hi', NULL, 'cf48b86a3d897130b301737229c693b3', NULL, 'd41d8cd98f00b204e9800998ecf8427e99ab1a968ccc17d3258a9ddc63b001a9', NULL, 71676989, 'client', 'active', '2020-04-13 06:56:47', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(83, 'sdfdsf', 'asaff', 'hharb+dfhjsa@fxgrow.com', '0n0aT5zWTfjeTCWcaUJsNVXx0Rcw4Bes', NULL, '67b5ecb25d44544904b7ff98e903a7bd', NULL, 'd41d8cd98f00b204e9800998ecf8427e6dc03c8c412f694a74ee27f187f9806b', NULL, 453, 'client', 'active', '2020-04-21 06:35:29', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(84, 'asda', 'dff', 'hharb+oiuyt@fxgrow.com', '9Z8gm-4D1ECQQmunuVmbK1L6Rcq4C_Xi', NULL, '67b5ecb25d44544904b7ff98e903a7bd', NULL, 'd41d8cd98f00b204e9800998ecf8427ebeaf449faa59ad4f0f124f5cfb4eca66', NULL, 71676989, 'client', 'active', '2020-04-21 06:41:41', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(85, 'hasan hasan', 'hasdx', 'hharb+n@fxgrow.com', 'b-boUTqzMuXO11nSoV4KWUP3FJRnHakY', NULL, '67b5ecb25d44544904b7ff98e903a7bd', NULL, '', '2020-04-22 04:25:07', 71676989, 'client', 'active', '2020-04-22 04:23:14', NULL, NULL, '::1', '::1', '2020-04-22 04:25:18', 1587626718, 'LB', NULL),
(86, 'nadine', 'fakro', 'nfakhro@fxgrow.com', 'OyWn8ZPK1smkoEJOe9vuKj2Um5cFRafs', NULL, '67b5ecb25d44544904b7ff98e903a7bd', NULL, 'd41d8cd98f00b204e9800998ecf8427e59c9edf038d79c843104fa39105d0060', NULL, 90000000, 'client', 'active', '2020-04-27 04:29:30', NULL, NULL, '::1', NULL, NULL, NULL, 'LB', NULL),
(87, 'hassan', 'hasotr', 'hharb+123@fxgrow.com', 'Z8oA7NX_-zgxX9FYLgXmtK1Yr3QyjkhY', NULL, '4f8d4ef7996a41d8b4941ae047935312', NULL, 'd41d8cd98f00b204e9800998ecf8427e0b43653d6688d12f9214d5b4c6e7b740', NULL, 90000000, 'client', 'active', '2020-04-30 06:16:46', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(88, 'hassan', 'hasotr12', 'hharb+ib@fxgrow.com', 'cGOS2lwN-oQLS0nOlt5EBfZ_XL62JNHl', NULL, '67b5ecb25d44544904b7ff98e903a7bd', NULL, 'd41d8cd98f00b204e9800998ecf8427ed4d2b43687e2407c2bc937a82f7b2c32', NULL, 90000000, 'money manager', 'active', '2020-05-05 07:09:03', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(89, 'hassan', 'hasotrdd', 'hharb+ds@fxgrow.com', 'zDJCFyQiJ-Khz3ynfvo_Qs5jpOIWEidv', NULL, '9993849b8c059920ae14483ccf2c004b', NULL, 'd41d8cd98f00b204e9800998ecf8427e5060f8609929a16f5edafcc4da490f66', NULL, 90000000, 'money manager', 'active', '2020-05-05 07:10:52', NULL, NULL, '::1', NULL, NULL, NULL, 'AE', NULL),
(90, 'hassan', 'hasotree', 'hharb+dd@fxgrow.com', 'NgSrWcdOgXVfkps6ZzbAmt6OuTD8TiHz', NULL, 'd52bfa10b3d8a7793705418007132cdf', NULL, 'd41d8cd98f00b204e9800998ecf8427e64d8d917dd4fd6b0251f06b718c98abf', NULL, 90000000, 'money manager', 'active', '2020-05-05 07:12:08', NULL, NULL, '::1', NULL, NULL, NULL, 'AI', NULL),
(91, 'hassan', 'hasotreee', 'hharb+ibee@fxgrow.com', 'ibTu-raZTuKMerlqVSH7BxDlAGLYaFIu', NULL, '6c78063a798bc90cbdb0f0074968b0d1', NULL, 'd41d8cd98f00b204e9800998ecf8427e4aa4d863f981b5a56455511311a8db35', NULL, 90000000, 'money manager', 'active', '2020-05-05 07:14:12', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(92, 'hassan', 'hasotrdddd', 'hharb+ytr@fxgrow.com', 'b6QySLvWqJpPry-Rle4jK7OUSHJOfn0x', NULL, '20d6c3a4956f0e62a1c72db8b9ae4b8a', NULL, 'd41d8cd98f00b204e9800998ecf8427eb2ef24511ef987d811018406a4664122', NULL, 90000000, 'money manager', 'active', '2020-05-05 07:15:54', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(93, 'hassan', 'hasotrza', 'hharb+uetr@fxgrow.com', 'fitAOJhx5DbMMeJQz0Jc8P8Gxijpjxd5', NULL, '4f76d852f0a9f75f69a9e3b039ce45bd', NULL, 'd41d8cd98f00b204e9800998ecf8427ec060f39eb3074860241b3ffdf65939ef', NULL, 90000000, 'client', 'active', '2020-05-05 07:20:02', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(94, 'hassan', 'hasotr12xs', 'hharb+ds12aq@fxgrow.com', 'CFaLdB4XK9d4oE7RItqHSNOfD0MRjTg0', NULL, 'c71d60e78282b33278705ac89172310d', NULL, 'd41d8cd98f00b204e9800998ecf8427ef194ee8dd2e2706d69d00d6f45105134', NULL, 90000000, 'money manager', 'active', '2020-05-05 07:24:52', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(95, 'hassan', 'hasotrxsw', 'hharb+rfgh@fxgrow.com', '0d5TNxQVrO5FkgrbZ9JwdGQALwhz0GCz', NULL, 'a03740634fb12d26f9d6e39a20c52474', NULL, 'd41d8cd98f00b204e9800998ecf8427ec13f4106e8a78b12a29c5a578e4eba41', NULL, 90000000, 'money manager', 'active', '2020-05-05 07:28:03', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(96, 'hassan', 'hasotrwrf', 'hharb+uywetw6@fxgrow.com', 'agvKnvhNKFIeq_0LADsVL49kY-nUJOcT', NULL, '9d4139fe8fd33ef147514bfeb4d057a4', NULL, 'd41d8cd98f00b204e9800998ecf8427e52a62872df12623812ec73adbe9cc6a1', NULL, 90000000, 'money manager', 'active', '2020-05-05 07:29:52', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(97, 'hassan', 'hasotree234', 'hharb+9865@fxgrow.com', 'RKjMbR1aOxIsOiikTLkKDX5sPhcC79C3', NULL, 'a2cb62d1250e711808b3f2e81c629f70', NULL, 'd41d8cd98f00b204e9800998ecf8427e98263c5fe8fe8140f68e6e8df26c8ef3', NULL, 90000000, 'money manager', 'active', '2020-05-05 07:32:39', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(98, 'hassan ib', 'hasib', 'hharb+ibtest@fxgrow.com', 'lGvE813TaU0pSBNcmrW9ofuBrY76Y1VX', NULL, 'ebf0747447791d17c93d949885afbaa1', NULL, '', '2020-05-05 07:50:12', 90000000, 'money manager', 'active', '2020-05-05 07:49:27', NULL, NULL, '::1', '::1', '2020-05-05 07:52:18', 1588762338, 'LB', NULL),
(99, 'hassan', 'fakrodd', 'hharb+9ik@fxgrow.com', 'JW5M2WG6VuNpsENKsCCi_Y-nEjjiCJHE', NULL, '7a6696ad1a19a130605aa60aaeb7a269', NULL, 'd41d8cd98f00b204e9800998ecf8427e0756c8cbae9219039e14e8c0dc504d98', NULL, 90000000, 'client', 'active', '2020-05-07 04:52:45', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(100, 'hassan', 'hasotrww', 'hharb+skdj@fxgrow.com', 'Bm74wv7zz594Pv8uf-mMB48XT4WjpJgj', NULL, 'c4cae4c48c29071c29eee910527c9024', NULL, 'd41d8cd98f00b204e9800998ecf8427eb79166e22fbdd4a5d4ab597e21d4b647', NULL, 90000000, 'client', 'active', '2020-05-07 04:54:17', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(101, 'hassan', 'hasotrdx', 'hharb+trader@fxgrow.com', '9g86eJueHKwTxcgfs8xSwwD3UBYpsjLy', NULL, 'da4ecbfd20b14a3abb3b57301507a235', NULL, 'd41d8cd98f00b204e9800998ecf8427ee67df866130be986ae11e552d2fcd15f', NULL, 90000000, 'money manager', 'active', '2020-05-07 04:54:55', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(102, 'hassan', 'hasotrcxz', 'hharb+iddb@fxgrow.com', 'MvnQxYkfe0KnXzMlf22Uv6NCLoAsDYKx', NULL, '92bf08413d1250a3707983674ee551d4', NULL, 'd41d8cd98f00b204e9800998ecf8427e0b9fb84fc40e3f24e5cf2a117ed8e84d', NULL, 90000000, 'money manager', 'active', '2020-05-07 04:55:39', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(103, 'hassan', 'hasotrssd', 'hharb+trade@fxgrow.com', 'GxivjsJsdZbw7h9TUs2xL16gx1D_ADYv', NULL, 'd4885ec74538ed236dfd5b4ba20718a7', NULL, 'd41d8cd98f00b204e9800998ecf8427e828e5d95e31fc22c25345f3aa7274b88', NULL, 90000000, 'money manager', 'active', '2020-05-07 04:59:42', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(104, 'hassan', 'hasotrzaq', 'hharb+4mnv@fxgrow.com', 'K_Z3XP7M-JMLhoR30KYW1BsyWA2nhWqx', NULL, '2e1d80c7dab17f4a472b7ba8ab691c71', NULL, 'd41d8cd98f00b204e9800998ecf8427e107a35734796e00af7f815c6b057a334', NULL, 90000000, 'client', 'active', '2020-05-07 05:00:46', NULL, NULL, '::1', NULL, NULL, NULL, 'AD', NULL),
(105, 'Hassan Harb', 'hharb21', 'hharb+c1@fxgrow.com', '6QE_74mtkNpvvX9xZ8WH_JfGgOkLQ_rz', NULL, 'e9a10ed878c26db3233293357112a846', NULL, 'd41d8cd98f00b204e9800998ecf8427e73b9026d977913e41f7a5d9d563cfd22', NULL, 70000000, 'client', 'not active', '2020-05-18 09:02:21', NULL, '', '::1', NULL, NULL, NULL, '', ''),
(106, 'hasan hareb', 'hasfv', 'hharb+haso@fxgrow.com', 'Qp1rYI_1vVm2orj1RV3ZV6iQ_EI-SWBY', NULL, '67b5ecb25d44544904b7ff98e903a7bd', NULL, '', '2020-05-19 04:06:24', 90000000, 'client', 'active', '2020-05-19 04:05:40', NULL, NULL, '::1', '::1', '2020-05-19 04:14:58', 1589958898, 'AD', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_info`
--

DROP TABLE IF EXISTS `users_info`;
CREATE TABLE IF NOT EXISTS `users_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_info`
--

INSERT INTO `users_info` (`id`, `user_id`, `key`, `value`) VALUES
(1, 90, 'website', 'fx.co'),
(2, 90, 'country_name', 'lebanon'),
(3, 91, 'website', 'fx.co'),
(4, 91, 'country_name', 'leb'),
(5, 92, 'website', 'fx.co'),
(6, 92, 'country_name', 'leb'),
(7, 93, 'website', 'fx.com'),
(8, 93, 'country_name', 'lebanon'),
(9, 98, 'website', 'ssss'),
(10, 98, 'country_name', ''),
(11, 106, 'website', 'd'),
(12, 106, 'country_name', ''),
(13, 108, 'website', ''),
(14, 108, 'country_name', ''),
(15, 109, 'website', ''),
(16, 109, 'country_name', ''),
(17, 111, 'website', ''),
(18, 111, 'country_name', ''),
(19, 112, 'website', 'fx'),
(20, 112, 'country_name', ''),
(21, 118, 'website', ''),
(22, 118, 'country_name', ''),
(23, 119, 'website', ''),
(24, 119, 'country_name', ''),
(25, 120, 'website', 'ssssxs'),
(26, 120, 'country_name', 'ssdsdd'),
(27, 121, 'website', 'fxcv'),
(28, 121, 'country_name', 'europe'),
(29, 123, 'website', ''),
(30, 123, 'country_name', ''),
(31, 92, 'website', 'wen.com'),
(32, 95, 'website', 'wen.com'),
(33, 96, 'website', 'wen.com'),
(34, 97, 'website', 'wen.com'),
(35, 98, 'website', 'wen.com'),
(36, 103, 'website', 'wen.com');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `broker_details`
--
ALTER TABLE `broker_details`
  ADD CONSTRAINT `bk_index_idd` FOREIGN KEY (`broker_id`) REFERENCES `brokers` (`id`);

--
-- Constraints for table `codes`
--
ALTER TABLE `codes`
  ADD CONSTRAINT `CODE_TYPE_INDEX` FOREIGN KEY (`code_type_id`) REFERENCES `codes_type` (`id`);

--
-- Constraints for table `email_template_translation`
--
ALTER TABLE `email_template_translation`
  ADD CONSTRAINT `fk-email_template_translation-email_template` FOREIGN KEY (`templateId`) REFERENCES `email_template` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `language_translate`
--
ALTER TABLE `language_translate`
  ADD CONSTRAINT `language_translate_ibfk_1` FOREIGN KEY (`language`) REFERENCES `language` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `language_translate_ibfk_2` FOREIGN KEY (`id`) REFERENCES `language_source` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `relations`
--
ALTER TABLE `relations`
  ADD CONSTRAINT `master-accoun-fk` FOREIGN KEY (`master_account`) REFERENCES `mam_accounts` (`id`),
  ADD CONSTRAINT `slave-accoun-fk` FOREIGN KEY (`slave_account`) REFERENCES `mam_accounts` (`id`),
  ADD CONSTRAINT `users_fk_cl` FOREIGN KEY (`client_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_fk_ib` FOREIGN KEY (`ib_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
