<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\services;

use common\models\mamaccounts\MamAccounts;

/**
 * Description of TradeObject
 *
 * @author MSI
 */
class TradeObject {

    public $ticket;
    public $login;
    public $platform;
    public $broker;
    public $symbol;
    public $cmd;
    public $volume;
    public $price;
    public $sl;
    public $tp;

    function __construct($request) {
        if ($request['name']) {
            $account = MamAccounts::find()->joinWith("brokers")->where("name='" . $request['name']."'")->asArray()->one();

            $this->ticket = isset($request['ticket']) ? $request['ticket'] : 0;
            $this->login = isset($account['login']) ? $account['login'] : 0;
            $this->platform = isset($account['platform']) ? $account['platform'] : "";
            $this->broker = isset($account['brokers']['broker_name']) ? $account['brokers']['broker_name'] : "";
            $this->symbol = isset($request['symbol']) ? $request['symbol'] : "";
            $this->cmd = isset($request['cmd']) ? $request['cmd'] : 0;
            $this->volume = isset($request['volume']) ? $request['volume'] : 0;
            $this->price = isset($request['price']) ? $request['price'] : 0;
            $this->sl = isset($request['sl']) ? $request['sl'] : 0;
            $this->tp = isset($request['tp']) ? $request['tp'] : 0;
        }
    }
}
