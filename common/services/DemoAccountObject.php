<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\services;

use common\models\users\Users;

/**
 * Description of AccountObject
 *
 * @author MSI
 */
class DemoAccountObject {

    public $currency;
    public $balance;
    public $leverage;
    public $country;
    public $name;
    public $phone;

    function __construct($user_id, $demo_form) {
        if ($user_id) {
            $user = Users::findOne($user_id);
            if ($user) {
                $this->currency = $demo_form->currency;
                $this->balance = $demo_form->balance;
                $this->leverage = $demo_form->leverage;
                $this->country = $user['country']['name'];
                $this->name = $user['name'];
                $this->phone = $user['phone'];
            }
        }
    }

}
