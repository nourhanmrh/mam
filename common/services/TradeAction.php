<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\services;

use yii\helpers\ArrayHelper;
use common\interfaces\MessagesInterface;
use Socket\Raw;

/**
 * Description of MamAccountOperation
 *
 * @author user
 */
class TradeAction extends Socket implements MessagesInterface {
    const OPERATION="Trading";
    const OPEN_OPERATION = "OpenTrade";
    const MODIFY_OPERATION = "ModifyTrade";
    const CLOSE_OPERATION = "CloseTrade";
    
    const ACCOUNT_OPERATION_TYPE_INDEX = 1;
    const MAP_TRADE_INDEX = 2;

    public $trade_operation_type;
    public $trade_data;

    public function __construct($operation_type, $data_trade) {
        $this->trade_operation_type = $operation_type;
        $this->trade_data = json_encode($data_trade);
    }

    public function get_message_model() {
        $message = explode(";", $this->message);
        $this->trade_operation_type = $message[self::ACCOUNT_OPERATION_TYPE_INDEX];
        $this->trade_data = $message[self::MAP_TRADE_INDEX];
    }

    public function create_message() {
        $message = [];
        $message[0] = self::OPERATION;
        $message[self::ACCOUNT_OPERATION_TYPE_INDEX] = $this->trade_operation_type;
        $message[self::MAP_TRADE_INDEX] = $this->trade_data;

        return implode(";", $message);
    }

    public function send_message() {
        $this->message = $this->create_message();
        return parent::send_message();
    }

    public function get_activity_description($message) {
        
    }

}
