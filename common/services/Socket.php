<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\services;

use Socket\Raw;

/**
 * Description of Socket
 *
 * @author MSI
 */
class Socket {

    public $message;

    public function send_message() {
        $factory = new Raw\Factory();
        try {
            $socket = $factory->createClient(\Yii::$app->params['socket_address'] . ":" . \Yii::$app->params['socket_port']);
            $socket->write($this->message);
            $res = $socket->read(10000000);
            $socket->close();
            return $res;
        } catch (Raw\Exception $ex) {
            return false;
        }
        return false;
    }

}
