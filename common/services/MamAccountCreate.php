<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\services;

use yii\helpers\ArrayHelper;
use common\interfaces\MessagesInterface;
use Socket\Raw;

/**
 * Description of MamAccountOperation
 *
 * @author user
 */
class MamAccountCreate extends Socket implements MessagesInterface {

    const OPERATION = "MamAccountCreate";
    const ACCOUNT_DATA_INDEX = 1;

    public $account_data;

    public function __construct($data_account) {
        $this->account_data = json_encode($data_account);
    }

    public function get_message_model() {
        $message = explode(";", $this->message);
        $this->account_data = $message[self::ACCOUNT_DATA_INDEX];
    }

    public function create_message() {
        $message = [];
        $message[0] = self::OPERATION;
        $message[self::ACCOUNT_DATA_INDEX] = $this->account_data;

        return implode(";", $message);
    }

    public function send_message() {
        $this->message = $this->create_message();
        return parent::send_message();
    }

    public function get_activity_description($message) {
        
    }

}
