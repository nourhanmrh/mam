<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\services;

use yii\helpers\ArrayHelper;
use common\interfaces\MessagesInterface;
use Socket\Raw;

/**
 * Description of MamAccountOperation
 *
 * @author user
 */
class MamAccountAction extends Socket implements MessagesInterface {

    const OPERATION = "MamAccountAction";
    const ACCOUNT_OPERATION_TYPE_INDEX = 1;
    const MAP_DATA_INDEX = 2;
    const FOLLOW_TYPE = "follow";
    const UNFOLLOW_TYPE = "unfollow";
    const UPDATE_FOLLOW_TYPE = "updatefollow";

    public $account_operation_type;
    public $map_data;

    public function __construct($operation_type, $data_map) {
        $this->account_operation_type = $operation_type;
        $this->map_data = json_encode($data_map);
    }

    public function get_message_model() {
        $message = explode(";", $this->message);
        $this->account_operation_type = $message[self::ACCOUNT_OPERATION_TYPE_INDEX];
        $this->map_data = $message[self::MAP_DATA_INDEX];
    }

    public function create_message() {
        $message = [];
        $message[0] = self::OPERATION;
        $message[self::ACCOUNT_OPERATION_TYPE_INDEX] = $this->account_operation_type;
        $message[self::MAP_DATA_INDEX] = $this->map_data;

        return implode(";", $message);
    }

    public function send_message() {
        $this->message = $this->create_message();
        return parent::send_message();
    }

    public function get_activity_description($message) {
        
    }

}
