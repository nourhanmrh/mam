<?php

return [
    'language' => isset($_GET['lang']) ? $_GET['lang'] : "en_US",
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'sse' => [
            'class' => \odannyc\Yii2SSE\LibSSE::class
        ],
        'templateManager' => [
            'class' => \ymaker\email\templates\components\TemplateManager::class
        ],
        'geoip' => ['class' => 'lysenkobv\GeoIP\GeoIP'],
        'emailTemplates' => [
            'class' => bl\emailTemplates\components\TemplateManager::class
        ],
        'assetManager' => [
            'bundles' => [
                'kartik\form\ActiveFormAsset' => [
                    'bsDependencyEnabled' => false // do not load bootstrap assets for a specific asset bundle
                ],
            ],
        ],
        'image' => [
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD', //GD or Imagick
        ],
        'request' => [
        ],
        'user' => [
            'identityClass' => 'common\models\users\Users',
            'loginUrl' => ['/login'],
            'enableAutoLogin' => true,
            'enableSession' => true
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'en-US',
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => false,
                ],
            ]
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                $message = "";
                $yii_response = false;

                if ($response->format == 'html') {
                    return $response;
                }

                $responseData = $response->data;
                 //print_r($response);die;
                if (is_string($responseData) && json_decode($responseData)) {
                    $responseData = json_decode($responseData, true);
                }

                if ($response->content != '') {
                    $message = $response->content;
                }

                if (is_array($responseData)) {
                    if (array_key_exists("message", $responseData)) {
                        $message = $responseData['message'];
                    }
                } else if (is_string($responseData)) {
                    $message = $responseData;
                }


                if (is_array($responseData)) {
                    if (array_key_exists("code", $responseData)) {
                        $yii_response = true;
                    }
                }

                if ($response->statusCode >= 200 && $response->statusCode <= 299) {
                    $response->data = [
                        'success' => true,
                        'status' => $response->statusCode,
                        'message' => $message,
                        'data' => $yii_response ? "" : $responseData,
                        'errors' => ""
                    ];
                } else {

                    $response->data = [
                        'success' => false,
                        'status' => $response->statusCode,
                        'message' => $message,
                        'data' => "",
                        'errors' => $yii_response ? "" : $responseData,
                            // 'errors'=> json_encode($responseData),
                    ];
                }
                return $response;
            },
        ]
    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
            'downloadAction' => 'gridview/export/download'
        ]
    ],
];

