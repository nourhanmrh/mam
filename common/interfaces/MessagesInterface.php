<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace common\interfaces;
/**
 *
 * @author user
 */
interface MessagesInterface {
    
    public function create_message();
    public function get_message_model();
    public function send_message();
    public function get_activity_description($message);
}
