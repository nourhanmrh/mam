<?php

namespace common\helpers;

use yii;
use mdm\admin\components\DbManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuthManager
 *
 * @author user
 */
class AuthManager {

    const SUPER_ADMIN_ROLE = 'super admin';
    const ADMIN_ROLE = 'admin';
    const IB_ROLE = 'ib';
    const CLIENT_ROLE = 'client';
    const VIEW_GROUP_PERMISSION = 'copy_admin_view_group';
    const DELETE_ACCOUNTS_PERMISSION = 'copy_admin_delete_accounts';
    const ADD_ACCOUNTS_PERMISSION = 'copy_admin_add_accounts';
    const EDIT_COPYTYPE_PERMISSION = 'copy_client_edit_copytypes';
    const VIEW_RELATIONS_PERMISSION = 'copy_ib_view_relations';
    const DELETE_RELATIONS_PERMISSION = 'copy_ib_delete_relations';
    const EDIT_MASTER_ACCOUNT_PERMISSION = 'copy_ib_edit_master_account';
    const UPDATE_TRADES = 'copy_admin_update_trades';
    const DESTROY_TRADES = 'copy_admin_destroy_trades';
    const REFRESH_ACCOUNTS = 'copy_admin_refresh_accounts';
    const SHOW_ACCOUNTS_COLUMNS = 'copy_admin_columns';
    const SHOW_ACTIVE_ACCOUNTS = 'copy_ib_active_accounts';
    const EDIT_ACCOUNT_TYPE = 'copy_ib_edit_account_type';
    const SYNC_ACCOUNTS = 'copy_ib_sync_accounts';
    const ACCOUNTS_DEPOSIT = 'copy_admin_accounts_deposit';
    const USERS_ACCOUNTS = 'copy_ib_users_edits';

    public static function is_admin() {
        return Yii::$app->user->can(self::ADMIN_ROLE);
    }

    public static function is_ib() {
        return Yii::$app->user->can(self::IB_ROLE) && !self::is_admin();
    }

    public static function is_super_admin() {
        return Yii::$app->user->can(self::SUPER_ADMIN_ROLE);
    }

    public static function is_client() {
        return Yii::$app->user->can(self::CLIENT_ROLE);
    }

    public static function can_view_active_accounts() {
        return Yii::$app->user->can(self::SHOW_ACTIVE_ACCOUNTS);
    }

    public static function can_edit_account_type() {
        return Yii::$app->user->can(self::EDIT_ACCOUNT_TYPE);
    }

    public static function can_edit_accounts_deposit() {
        return Yii::$app->user->can(self::ACCOUNTS_DEPOSIT);
    }

    public static function can_edit_users_account() {
        return Yii::$app->user->can(self::USERS_ACCOUNTS);
    }

    public static function can_sync_accounts() {
        return Yii::$app->user->can(self::SYNC_ACCOUNTS);
    }

    public static function can_view_group() {
        return Yii::$app->user->can(self::VIEW_GROUP_PERMISSION);
    }

    public static function can_view_accounts_columns() {
        return Yii::$app->user->can(self::SHOW_ACCOUNTS_COLUMNS);
    }

    public static function can_delete_accounts() {
        return Yii::$app->user->can(self::DELETE_ACCOUNTS_PERMISSION);
    }

    public static function can_refresh_accounts() {
        return Yii::$app->user->can(self::REFRESH_ACCOUNTS);
    }

    public static function can_add_accounts() {
        return Yii::$app->user->can(self::ADD_ACCOUNTS_PERMISSION);
    }

    public static function can_edit_copy_types() {
        return Yii::$app->user->can(self::EDIT_COPYTYPE_PERMISSION);
    }

    public static function can_view_relations() {
        return Yii::$app->user->can(self::VIEW_RELATIONS_PERMISSION);
    }

    public static function can_delete_relations() {
        return Yii::$app->user->can(self::DELETE_RELATIONS_PERMISSION);
    }

    public static function can_edit_master_account() {
        return Yii::$app->user->can(self::EDIT_MASTER_ACCOUNT_PERMISSION);
    }

    public static function can_update_trades() {
        return Yii::$app->user->can(self::UPDATE_TRADES);
    }

    public static function can_destroy_trades() {
        return Yii::$app->user->can(self::DESTROY_TRADES);
    }

    public static function get_to_assign($assignment) {
        return (object) ['name' => $assignment];
    }

    public static function assign($id, $assignment) {
        $r = new DbManager;
        $r->init();
        $r->assign(self::get_to_assign($assignment), $id);
    }

    public static function revoke($role, $id) {
        $r = new DbManager;
        $r->init();
        $r->revoke(self::get_to_assign($role), $id);
    }

}
