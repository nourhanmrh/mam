<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\helpers;

use yii;
use common\models\Constants;

/**
 * Description of Notification
 *
 * @author user
 */
class Notification {

    public $type = '';
    public $icon = '';
    public $icon_type = 'class';
    public $message = '';
    public $url = '';
    public $delay = 7000;

    function __construct($message) {
        $this->message = $message;
    }

    public function success() {
        $this->type = Constants::NOTIFICATION_TYPE_SUCCESS;
        $this->icon = Constants::NOTIFICATION_ICON_CLASS_SUCCESS;
        $this->run();
    }

    public function info() {
        $this->type = Constants::NOTIFICATION_TYPE_INFO;
        $this->icon = Constants::NOTIFICATION_ICON_CLASS_INFO;
        $this->run();
    }

    public function warning() {
        $this->type = Constants::NOTIFICATION_TYPE_WARNING;
        $this->icon = Constants::NOTIFICATION_ICON_CLASS_WARNING;
        $this->run();
    }

    public function error() {
        $this->type = Constants::NOTIFICATION_TYPE_ERROR;
        $this->icon = Constants::NOTIFICATION_ICON_CLASS_ERROR;
        $this->run();
    }

    private function run() {
        \Yii::$app->session->set(
                'message', [
            'type' => $this->type,
            'icon' => $this->icon,
            'icon_type' => $this->icon_type,
            'title' => '',
            'message' => $this->message,
            'element' => 'body',
            'position' => 'fixed',
            'allow_dismiss' => '0',
            'newest_on_top' => '0',
            'showProgressbar' => '0',
            'url' => $this->url,
            'target' => '_blank',
            'placement_from' => 'top',
            'placement_align' => 'right',
            'offset_y' => 60,
            'delay' => 10000,
            'animate_enter' => 'animated fadeIn',
            'animate_exit' => 'animated fadeOut',
            'template' => ''
            . '<div data-notify="container" class="col-md-1 col-lg-1 col-xs-6 col-sm-3 alert alert-{0}" role="alert">'
            . '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>'
            . '<span data-notify="icon"></span>'
            . '<span data-notify="message">{2}</span>'
            . '<div class="progress" data-notify="progressbar">'
            . '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>'
            . '</div><a href="{3}" target="{4}" data-notify="url"></a></div>',
                ]
        );
    }

}
