<?php

namespace common\helpers;

use yii;

class Helper {

    public function filterRequestMapping($filter, $class) {

        if ($filter->filters) {
            foreach ($filter->filters as $key => $value) {
                if (array_key_exists('filters', (array) $value)) {
                    $this->filterRequestMapping($value, $class);
                } else {
                    $filter->filters[$key]->field = $class::$map[$filter->filters[$key]->field];
                }
            }
        }
    }

}
