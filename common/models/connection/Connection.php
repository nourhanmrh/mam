<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\models\connection;

use Yii;
use yii\base\Model;
use Socket\Raw;
use app\interfaces\MessagesInterface;

/**
 * Description of Connection
 *
 * @author user
 */
class Connection extends Model implements MessagesInterface {

    //put your code here
    const CONNECT_MANAGER = "ConnectManager";
    const PLATFORM_INDEX = 1;
    const MANAGER_LOGIN_INDEX = 2;
    const PASSWORD_INDEX = 3;
    const ADDRESS_INDEX = 4;

    public $platform;
    public $manager_login;
    public $password;
    public $address;
    public $message;

    public function rules() {
        return [
            [['platform', 'manager_login', 'password', 'address'], 'required']
        ];
    }

    public function create_message() {
        $messasge = [];
        $message[0] = self::CONNECT_MANAGER;
        $message[self::PLATFORM_INDEX] = $this->platform;
        $message[self::MANAGER_LOGIN_INDEX] = $this->manager_login;
        $message[self::PASSWORD_INDEX] = $this->password;
        $message[self::ADDRESS_INDEX] = $this->address;

        return implode(";", $message);
    }

    public function get_message_model() {
        $message = explode(";", $this->message);
        $this->platform = $message[self::PLATFORM_INDEX];
        $this->manager_login = $message[self::MANAGER_LOGIN_INDEX];
        $this->password = $message[self::PASSWORD_INDEX];
        $this->address = $message[self::ADDRESS_INDEX];
    }

    public function get_activity_description($message) {
        $this->message = $message;
        $this->get_message_model();
        return "Connect to manager " . $this->manager_login . " " . $this->platform . " to server " . $this->address;
    }

    public function connect() {
        $this->message = $this->create_message();
        $factory = new Raw\Factory();
        $socket = $factory->createClient(Yii::$app->params['socket_address'] . ":" . Yii::$app->params['socket_port']);
        $socket->write($this->message);
        $res = $socket->read(8192);
        $socket->close();
        return $res;
    }

    public function send_message() {
        
    }

}
