<?php

namespace common\models\usersinfo;

use Yii;

/**
 * This is the model class for table "users_info".
 *
 * @property int $id
 * @property int $user_id
 * @property string $key
 * @property string $value
 */
class UsersInfo extends \yii\db\ActiveRecord {

    public $country_name;
    public $website;
    public $array = ['website'];

    public static function tableName() {
        return 'users_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id', 'key', 'value'], 'required'],
            [['user_id'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

}
