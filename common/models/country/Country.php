<?php

namespace common\models\country;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "country".
 *
 * @property string $code
 * @property int $geoname_id
 * @property int $capital_geoname_id
 * @property string $language_code
 * @property string $currency_code
 * @property string $timezone_code
 * @property string $latitude
 * @property string $longitude
 * @property string $name
 * @property string $iso3
 * @property int $numcode
 * @property int $phonecode
 */
class Country extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['code', 'geoname_id', 'capital_geoname_id', 'language_code', 'currency_code', 'timezone_code', 'latitude', 'longitude', 'name'], 'required'],
            [['geoname_id', 'capital_geoname_id', 'numcode', 'phonecode'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['code'], 'string', 'max' => 2],
            [['language_code', 'currency_code', 'iso3'], 'string', 'max' => 3],
            [['timezone_code'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 100],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'code' => 'Code',
            'geoname_id' => 'Geoname ID',
            'capital_geoname_id' => 'Capital Geoname ID',
            'language_code' => 'Language Code',
            'currency_code' => 'Currency Code',
            'timezone_code' => 'Timezone Code',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'name' => 'Name',
            'iso3' => 'Iso3',
            'numcode' => 'Numcode',
            'phonecode' => 'Phonecode',
        ];
    }

    public function getNameForDropdown() {
        return $this->name . '(+' . $this->phonecode .')';
    }

    public static function countriesDropdown() {
        return ArrayHelper::map(static::find()->all(), 'code', 'nameForDropdown');
    }

    public function countriesForMobile(){
        return ArrayHelper::map(static::find()->all(), 'code' , 'name');
    }


}
