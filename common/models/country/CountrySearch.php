<?php

namespace common\models\country;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\country\Country;

/**
 * CountrySearch represents the model behind the search form of `common\models\country\Country`.
 */
class CountrySearch extends Country
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'language_code', 'currency_code', 'timezone_code', 'name', 'iso3'], 'safe'],
            [['geoname_id', 'capital_geoname_id', 'numcode', 'phonecode'], 'integer'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Country::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'geoname_id' => $this->geoname_id,
            'capital_geoname_id' => $this->capital_geoname_id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'numcode' => $this->numcode,
            'phonecode' => $this->phonecode,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'language_code', $this->language_code])
            ->andFilterWhere(['like', 'currency_code', $this->currency_code])
            ->andFilterWhere(['like', 'timezone_code', $this->timezone_code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'iso3', $this->iso3]);

        return $dataProvider;
    }
}
