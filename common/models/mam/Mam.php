<?php

namespace common\models\mam;

use Yii;
use yii\base\Model;

class Mam extends Model {

    public $id;
    public $conf_key;
    public $value;
    public $description;

    public static function tableName() {
        return 'mam_configuration';
    }

    public function rules() {
        return [
            [['conf_key', 'value'], 'required'],
            [['id'], 'integer'],
            [['description'], 'string', 'max' => 50]
        ];
    }

}
