<?php

namespace common\models\traders;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use common\models\rates\Rates;
use common\models\ReportServer\ReportServer;
use common\models\ReportServer\Mt5Reportserver;
use common\models\ReportServer\Mt4Reportserver;
use common\models\tradersinfo\TradersInfo;
use common\models\mamaccounts\MamAccounts;
use common\models\users\Users;
use common\models\relations\Relations;
use common\models\brokers\Brokers;
use common\models\codes\Codes;
use common\models\Constants;
use common\services\Response;
use common\services\MamAccountAction;

class Traders extends Model {

    public function get_ROI($id) {
        $roi = (new \yii\db\Query())->select('roi')->from('traders_info')->where(['trader_id' => $id])->one();
        return $roi['roi'];
    }

    public function getDrawdown($name, $time) {
        $acc_id = (new \yii\db\Query())->select('trader_id')->from('traders_info')->where(['trader_name' => $name])->one();
        $end_date = date('Y-m-d H:i:s');
        $start_date = $this->filterbydate($time);
        $data = (new \yii\db\Query())->select('*')->from('drawdown')->where(['between', 'time', $start_date, $end_date])
                        ->andWhere(['trader_id' => $acc_id['trader_id']])->all();
        return $data;
    }

    public function get_Equity($id) {
        $account_equity = (new \yii\db\Query())->select('equity')->from('traders_info')->where(['trader_id' => $id])->one();
        return $account_equity['equity'];
    }

    public function get_chart_ten_days($trader_id) {
        $data = new MamAccounts();
        $acc = MamAccounts::findOne(['id' => $trader_id]);
        $info = $data->getDataBase($acc['name']);

        $report_server_class = (new ReportServer($info['platform']))->getClass();
        $reportserver = new $report_server_class($info['database'], $info['login']);

        $result = $reportserver->getChartTenDays();
        $points_array = json_encode(array_values($result));
        $array = '{"points":' . $points_array . '}';
        $response = \Yii::$app->getResponse();
        $response->setStatusCode(200);
        return $array;
    }

//    public static function live_pending() {
//        $pending_acc = MamAccounts::find()->where(['user_id' => \yii::$app->user->id])->andWhere(['active_account' => 1])->andWhere(['is_live' => 1])->andWhere(['account_status' => Codes::ACCOUNT_NOT_COMPLETED])->one();
//        return $pending_acc;
//    }

    public function get_amount_following($master_id) {
        $account_equity = (new \yii\db\Query())->select('amount_following')->from('relations')->where(['master_account' => $master_id])->andWhere(['unfollow_date' => null])->sum('amount_following');
        if ($account_equity) {
            return $account_equity;
        } else {
            return 0;
        }
    }

    public function get_trader_info($id) {
        $inf = \Yii::$app->db->createCommand("SELECT mam_accounts.id,mam_accounts.platform as platform,login,name,balance,brokers.broker_name FROM mam_accounts inner join brokers on brokers.id=mam_accounts.brokers_id  where mam_accounts.id=" . $id)->queryOne();
        return $inf;
    }

    public function actionMonthProfit($trader_name) {
        $data = new MamAccounts();

        $info = $data->getDataBase($trader_name);

        if ($info['platform'] == 'mt4') {
            $mt4reportserver = new Mt4Reportserver($info['database'], $info['login']);
        } elseif ($info['platform'] == 'mt5') {
            $mt4reportserver = new Mt5Reportserver($info['database'], $info['login']);
        } else {
            return [];
        }
        $data = $mt4reportserver->calculateMonthProfit($trader_name);

        return round($data['sum'], 2);
    }

    public function actionTotalProfit($trader_id) {
        $data = new MamAccounts();
        $acc = MamAccounts::findOne(['id' => $trader_id]);
        $info = $data->getDataBase($acc['name']);

        $report_server_class = (new ReportServer($info['platform']))->getClass();
        $reportserver = new $report_server_class($info['database'], $info['login']);

        $data = $reportserver->calculateTotalProfit();

        return round($data['sum'], 2);
    }

    public function actionAverageProfit($trader_name) {
        $data = new MamAccounts();

        $info = $data->getDataBase($trader_name);

        $report_server_class = (new ReportServer($info['platform']))->getClass();
        $reportserver = new $report_server_class($info['database'], $info['login']);

        $data = $reportserver->calculateTotalAverage();

        return round($data['avg'], 2);
    }

    public function get_star_width($acc) {
        $w = TradersInfo::find()->where(['trader_id' => $acc])->one();
        if ($w['rate'] != NULL) {
            $result = $w['rate'] * 100 / 5;
            return $result;
        }
    }

    public function getFollowers($id) {
        $followers = (new \yii\db\Query())
                ->select(['*'])
                ->from('mam_accounts')
                ->join("INNER JOIN", Relations::tableName(), Relations::tableName() . '.slave_account =' . MamAccounts::tableName() . '.id')
                ->where([Relations::tableName() . '.master_account' => $id])
                ->all();

        return $followers;
    }

    public function get_followed_broker($acc) {

        $bk = (new \yii\db\Query())
                ->select([MamAccounts::tableName() . '.id', Brokers::tableName() . '.id', Brokers::tableName() . '.image_location'])
                ->from('mam_accounts')
                ->join("INNER JOIN", Brokers::tableName(), Brokers::tableName() . '.id =' . MamAccounts::tableName() . '.brokers_id')
                ->where([MamAccounts::tableName() . '.id' => $acc])
                ->one();
        return $bk;
    }

    public function get_Master_Name($id) {

        $master_name = (new \yii\db\Query())->select('trader_name')->from('traders_info')->where(['trader_id' => $id])->one();

        return $master_name['trader_name'];
    }

    public function get_Slave_Name($id) {

        $name = (new \yii\db\Query())->select('name')->from('mam_accounts')->where(['id' => $id])->one();

        return $name['name'];
    }

    public function display_strategy($id) {
        $strat = (new \yii\db\Query())->select('strategy')->from('traders_info')->where(['trader_id' => $id])->one();

        $strategy = substr($strat['strategy'], 0, 100);
        return $strategy;
    }

    public function has_voted() {
        if (Yii::$app->user->isGuest) {
            return 0;
        } else {
            $active = Traders::get_active();
            $count = (new \yii\db\Query())->select('count(*)')->from('rates')->where(['slave_account' => $active['id']])->andWhere(['client_id' => \yii::$app->user->id])->one();

            if ($count['count(*)'] == 0) {
                return 1;
            } elseif ($count['count(*)'] > 0) {
                return 0;
            }
        }
    }

    public function get_Investors_Profit($id) {
        $prof = (new \yii\db\Query())->select('profit')->from('traders_info')->where(['trader_id' => $id])->one();

        return $prof['profit'];
    }

    public function canvote($trader_id) {
        if (Yii::$app->user->isGuest) {
            return 0;
        } else {
            $acc = Traders::get_active();
            $chk = Relations::get_user_followed_traders($acc['id']);
            if ((in_array($trader_id, $chk)) == 1) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function get_Login($id, $plat, $brok) {
        $login = (new \yii\db\Query())
                ->select('login')
                ->from('mam_accounts')
                ->where(['id' => $id])
                ->andWhere(['platform' => $plat])
                ->andWhere(['brokers_id' => $brok])
                ->one();
        return $login['login'];
    }

    public function get_rates($acc) {
        if (Yii::$app->user->isGuest) {
            return null;
        } else {
            $active = Traders::get_active();
            $rates = Rates::find('rate')->where('master_account=' . $acc)->andWhere(['slave_account' => $active['id']])->one();
            if ($rates) {
                return $rates['rate'];
            } else {
                return null;
            }
        }
    }

    public function display_rate($acc) {
        $star_number = (new \yii\db\Query())->select(['*'])->from('traders_info')->where(['trader_id' => $acc])->one();

        return $star_number['rate'];
    }

    public static function get_logged_user() {
        if (Yii::$app->user->isGuest) {
            return 0;
        } else {

            $rows = (new \yii\db\Query())
                    ->select(['master_account'])
                    ->from('relations')
                    ->where(['client_id' => \Yii::$app->user->identity->id])
                    ->andWhere(['unfollow_date' => null])
                    ->all();
            foreach ($rows as $k => $v) {
                $array[] = $v['master_account'];
            }
            return $array;
        }
    }

    public static function is_logged() {
        return !Yii::$app->user->isGuest;
        $chk = Users::findIdentity(Yii::$app->user->id);
        if ($chk) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function getstats($list) {

        $data = Yii::$app->db->createCommand('select sum(comission_received) as com_rcv_sum,sum(comission_pending) as com_png_sum,avg(comission_received) as com_rcv_avg,avg(comission_pending) as com_png_avg,sum(roi)as roi_sum ,AVG(roi) as roi_avg,sum(profit) as total_profit , AVG(profit) as profit_avg from traders_info where id in' . $list)->queryAll();

        return $data;
    }

    public static function getAvgRoi($list) {

        $data = Yii::$app->db->createCommand('select AVG(roi) as roi_avg,sum(profit) as total_profit from traders_info where  roi!=0 and id in' . $list)->queryAll();

        return $data;
    }

    public static function get_followed_traders($trader_account_id) {
        if (!\Yii::$app->user->isGuest) {
            $active = Traders::get_active();
            $relation = (new \yii\db\Query())
                    ->select(['*'])
                    ->from('relations')
                    ->where(['client_id' => \Yii::$app->user->identity->id])
                    ->andWhere(['slave_account' => $active['id']])
                    ->andWhere(['master_account' => $trader_account_id])
                    ->andWhere(['unfollow_date' => null])
                    ->one();

            return $relation;
        } else {
            return "";
        }
    }

    public static function get_followed_trader_details($id) {
        $result = "";
        $user = new Users();
        $trader = new Traders();
        $traders_info = new TradersInfo();
        $current_date = strtotime(date('Y-m-d H:i:s'));

        $ib = (new \yii\db\Query())
                ->select([Relations::tableName() . '.id', Relations::tableName() . '.master_account', MamAccounts::tableName() . '.login', MamAccounts::tableName() . '.platform', MamAccounts::tableName() . '.brokers_id'])
                ->from('relations')
                ->join("INNER JOIN", MamAccounts::tableName(), MamAccounts::tableName() . '.id =' . Relations::tableName() . '.master_account')
                ->where([Relations::tableName() . '.slave_account' => $id])
                ->andWhere(['unfollow_date' => null])
                ->all();

        if ($ib) {
            $result = $traders_info->getTradersInfo([], [], $ib);
        }

//        foreach ($ib as $k => $v) {
//            $ib[$k]['name'] = $trader->get_Master_Name($v['master_account']);
//            $ib[$k]['profit'] = $trader->get_Investors_Profit($v['master_account']);
//            $ib[$k]['roi'] = $trader->get_ROI($v['master_account']);
//            $ib[$k]['is_active'] = $user->get_active($v['master_account']);
//            $ib[$k]['investors'] = $trader->getInvestors($v['master_account']);
//
//            // $ib[$k]['photo'] =   
//        }
        return $result;
    }

    public static function get_trader_details() {
        $user = new Users();
        $trader = new Traders();
        $ibs = (new \yii\db\Query())
                ->select([users::tableName() . '.id', users::tableName() . '.name', 'phone', 'username', 'email', MamAccounts::tableName() . '.active_account', Users::tableName() . '.description', MamAccounts::tableName() . '.login', MamAccounts::tableName() . '.active'])
                ->from('users')
                ->join("INNER JOIN", Codes::tableName(), Codes::tableName() . '.id =' . Users::tableName() . '.type')
                ->where([Codes::tableName() . '.code' => Codes::USER_IB])
                ->join("INNER JOIN", MamAccounts::tableName(), MamAccounts::tableName() . '.user_id =' . Users::tableName() . '.id')
                ->all();
        foreach ($ibs as $k => $v) {
            $ibs[$k]['name'] = $trader->get_Master_Name($v['login'], $v['name']);
            $ibs[$k]['profit'] = $trader->get_Investors_Profit($v['login']);
            $ibs[$k]['roi'] = $trader->get_ROI($v['login']);
            $ibs[$k]['is_active'] = $user->get_active($v['login']);
            $ibs[$k]['equity'] = $trader->get_equity($v['login']);
            $ibs[$k]['investors'] = $trader->get_Investors($v['id']);
            $ibs[$k]['chart'] = $trader->get_chart_values($v['login']);
            $ibs[$k]['width'] = $trader->get_rates($v['id']);
            $ibs[$k]['votes'] = $trader->get_rates($v['id']);
            $ibs[$k]['display_vote'] = $trader->display_rate($v['id']);
            $ibs[$k]['logged'] = $trader->is_logged();
            $ibs[$k]['canvote'] = 1;
            $ibs[$k]['status'] = $trader->user_status($v['login']);
        }
        return $ibs;
    }

    public static function get_active() {
        $acc = \yii::$app->session->get('active_account');
        
        if($acc){
             $active = MamAccounts::find()->joinWith("brokers")->where('user_id =' . \Yii::$app->user->getId())->andWhere(['name' => $acc])->asArray()->one();
        }else{
          
                  $active = MamAccounts::find()->joinWith("brokers")->where('user_id =' . \Yii::$app->user->getId())->andWhere(['active_account' => 1])->asArray()->one();

        }
        
        $data = new MamAccounts;
        if ($active['login']) {
            if (($active['platform'] != '')) {
                $info = $data->geBrokerDataBase($active['brokers']['broker_name'], $active['platform']);
                if ($info) {
                    $report_server_class = (new ReportServer($active['platform']))->getClass();
                    $reportserver = new $report_server_class($info['database'], $active['login']);
                    $active['balance'] = $reportserver->get_user_balance();
                    $active['equity'] = $reportserver->get_user_equity();
                    $active['currency'] = $reportserver->get_user_currency();
                }
            }
        }
        return $active;
    }

    public static function get_demo_pending($id) {
        $pending_acc = MamAccounts::find()->where(['user_id' => $id])->andWhere(['is_live' => 0])->andWhere(['account_status' => Codes::ACCOUNT_NOT_COMPLETED])->one();
        return $pending_acc;
    }

    public static function get_live_pending($id) {
        $pending_acc = MamAccounts::find()->where(['user_id' => $id])->andWhere(['is_live' => 1])->andWhere(['account_status' => Codes::ACCOUNT_NOT_COMPLETED])->one();
        return $pending_acc;
    }

    public function getInvestors($trader_id) {
        $i = Relations::find()->where(['master_account' => $trader_id])->andWhere(['unfollow_date' => null])->count();

        return $i;
    }

    public function removeTrader($post) {
        foreach ($post['slaves'] as $k => $v) {
            $relations = Relations::find()->where(['master_account' => $post['master_account'],
                        'slave_account' => $v, 'unfollow_date' => NULL])->one();
            if (!$relations) {
                return "Relation does not exist";
            }
            $relations->unfollow_date = date('Y-m-d H:i:s');
            $relations->save();
        }
    }

    public function updatefollow($relation_id, $copy, $value) {
        $slave_account = Traders::get_active();
        if ($slave_account['account_status'] == Constants::ACCOUNT_STATUS_COMPLETED) {
            $relation = Relations::findOne($relation_id);
            if ($relation) {
                $mam_account = new MamAccounts();
                $map = $mam_account->getMasterWithSlaveInfo($relation['master_account'], $relation['slave_account'], $copy, $value);
                try {
                    $mam_account_update = new MamAccountAction(MamAccountAction::UPDATE_FOLLOW_TYPE, $map);
                    $operation_callback = $mam_account_update->send_message();
                    $operation_response = new Response($operation_callback);

                    if ($operation_response->status == Response::RESPONSE_DONE) {
                        $relation->copy_type = $copy;
                        $relation->value = $value;
                        if ($relation->save()) {
                            return true;
                        }
                    }
                } catch (\Socket\Raw\Exception $ex) {
                    return false;
                }
            }
        }
        return false;
    }

    public function follow($trader_name, $account_name, $copy, $value) {
        $slave_account = Traders::get_active();
        if ($slave_account['name'] == $account_name) {
            if ($slave_account['account_status'] == Constants::ACCOUNT_STATUS_COMPLETED) {

                $trader = TradersInfo::find()->where(['trader_name' => $trader_name])->one();
                $invest = $this->getInvestors($trader['trader_id']);

                if ($trader && $slave_account) {
                    $relations = Relations::find()->where(['ib_id' => $trader->user_id,
                                'client_id' => \yii::$app->user->id, 'master_account' => $trader->trader_id,
                                'slave_account' => $slave_account['id'], 'unfollow_date' => NULL])->asArray()->all();

                    if (is_array($relations) && count($relations) > 0) {
                        return false;
                    }

                    $mam_account = new MamAccounts();
                    $map = $mam_account->getMasterWithSlaveInfo($trader['trader_id'], $slave_account['id'], $copy, $value);

                    try {
                        $mam_account_update = new MamAccountAction(MamAccountAction::FOLLOW_TYPE, $map);
                        $operation_callback = $mam_account_update->send_message();
                        $operation_response = new Response($operation_callback);

                        if ($operation_response->status == Response::RESPONSE_DONE) {
                            $model = new Relations();
                            $model->ib_id = $trader->user_id;
                            $model->client_id = \yii::$app->user->id;
                            $model->master_account = $trader->trader_id;
                            $model->slave_account = $slave_account['id'];
                            $model->copy_type = $copy;
                            $model->value = $value;
                            $model->amount_following = $slave_account['balance'];
                            if ($model->save()) {
                                TradersInfo::updateAll(['investors' => $invest + 1], ['=', 'id', $trader->id]);
                                return true;
                            } else {
                                print_r($model->errors);
                                die;
                            }
                        }
                    } catch (\Socket\Raw\Exception $ex) {
                        return false;
                    }
                }
            }
            return false;
        }
    }

    public function unfollow($trader_name, $account_name) {
        $mam_account = new MamAccounts();
        $trader = TradersInfo::find()->where(['trader_name' => $trader_name])->one();
        $slave_account = Traders::get_active();
        $invest = $this->getInvestors($trader['trader_id']);
        $map = $mam_account->getAllAccountsMap($trader['trader_id'], $slave_account['id']);
        if ($trader && $slave_account && $map) {
            try {
                $relations = Relations::find()->where(['ib_id' => $trader->user_id,
                            'client_id' => \yii::$app->user->id, 'master_account' => $trader->trader_id,
                            'slave_account' => $slave_account['id'], 'unfollow_date' => NULL])->one();

                if (!$relations) {
                    return false;
                }
                if ($slave_account['account_status'] == Constants::ACCOUNT_STATUS_COMPLETED) {
                    try {
                        $temp_map = $map[0];
                        $temp_map['slave'] = $temp_map['slave'][0];

                        $mam_account_update = new MamAccountAction(MamAccountAction::UNFOLLOW_TYPE, $temp_map);
                        $operation_callback = $mam_account_update->send_message();
                        $operation_response = new Response($operation_callback);
                        if ($operation_response->status == Response::RESPONSE_DONE) {
                            $relations->unfollow_date = date('Y-m-d H:i:s');
                            if ($relations->save()) {
                                TradersInfo::updateAll(['investors' => $invest - 1], ['=', 'id', $trader->id]);
                                return true;
                            }
                        }
                    } catch (\Socket\Raw\Exception $ex) {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (Exception $ex) {
                return false;
            }
        }
        return false;
    }

    public function user_status() {
        $active = (new \yii\db\Query())->select(['id', 'login', 'brokers_id', 'platform'])->from('mam_accounts')->where(['user_id' => \yii::$app->user->id])->andWhere(['active_account' => 1])->one();
        $a = [];

        if (!\Yii::$app->user->isGuest) {
            $masters_followed = (new \yii\db\Query())
                    ->select(['*'])
                    ->from('relations')
                    ->where(['client_id' => \Yii::$app->user->identity->id])
                    ->andWhere(['slave_account' => $active['id']])
                    ->andWhere(['unfollow_date' => null])
                    ->distinct()
                    ->all();
            return $masters_followed;
        } else {
            return $a;
        }
    }

    public function filterbydate($val) {

        $end = date('Y-m-d H:i:s');
        if ($val == 'oneD') {
            $start = date("Y-m-d H:i:s", strtotime('-1 days'));
        } else if ($val == 'threeD') {
            $start = date("Y-m-d H:i:s", strtotime('-3 days'));
        } else if ($val == 'week') {
            $start = date("Y-m-d H:i:s", strtotime('-7 days'));
        } else if ($val == 'oneM') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-1 months"));
        } else if ($val == 'threeM') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-3 months"));
        } else if ($val == 'sixM') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-6 months"));
        } else if ($val == 'year') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-1 year"));
        } else {

            return $two = date('Y-m-d H:i:s', strtotime('-7 days'));
            $start = date('Y-m-d H:i:s', strtotime('-7 days'));
        }
        return $start;
    }

}
