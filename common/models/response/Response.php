<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\models\response;

use Yii;
use yii\base\Model;

/**
 * Description of Response
 *
 * @author user
 */
class Response extends Model {

    const RESPONSE_DONE = 200;
    const RESPONSE_ERROR = 500;

    public static $responses = [
        self::RESPONSE_DONE => 'DONE',
        self::RESPONSE_ERROR => 'ERROR'
    ];
    public $status;
    public $message;
    public $data;
    public $errors;

    public function __construct($response = null) {
        if ($response) {
            $rep = json_decode($response);
            $this->status = $rep->status;
            $this->message = $rep->message;
            $this->data = $rep->data;
            $this->errors = $rep->errors;
        }
    }

    public function get_data() {
        if ($this->data) {
            return json_decode($this->data);
        }
        return "";
    }

    public function get_errors() {
        if ($this->errors) {
            return json_decode($this->errors);
        }
        return "";
    }

    public function check() {
        if ($this->status == self::RESPONSE_DONE)
            return true;
        return false;
    }

}
