<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property int $id
 * @property int $user_id
 * @property string $message
 * @property string $type
 * @property int $status
 * @property string $reply_message
 * @property string $created_at
 *
 * @property Users $user
 */
class Activity extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'activity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id', 'message', 'type', 'created_at'], 'required'],
            [['user_id', 'status', 'created_at'], 'integer'],
            [['message', 'reply_message'], 'string'],
            [['created_at'], 'safe'],
            [['type'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'message' => 'Message',
            'type' => 'Type',
            'status' => 'Status',
            'reply_message' => 'Reply Message',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

}
