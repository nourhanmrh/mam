<?php

namespace common\models\codes;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\codes\CodesType;

/**
 * This is the model class for table "codes".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property int $code_type_id
 * @property int $is_active
 *
 * @property CodesType $codeType
 */
class Codes extends \yii\db\ActiveRecord {

    const CODE_ACTIVE = 1;
    const CODE_NON_ACTIVE = 0;
    const IS_ENABLED = 1;
    const NOT_ENABLED = 0;
    const ENABLE_ARRAY = [0 => 'NO', 1 => 'YES'];
    const ENABLE_ARRAY_STATUS = [3 => 'Verified', 4 => 'Deleted'];
    const ENABLE_ARRAY_TYPE = [1 => 'admin', 2 => 'ib', 10 => 'SuperAdmin', 6 => 'client'];
    const USER_ADMIN = "admin";
    const USER_IB = "ib";
    const USER_CLIENT = "client";
    const USER_VERIFIED = "verified";
    const USER_INACTIVE = "Deleted";
    const User_Deleted = 4;
    const IS_ACTIVE = 1;
    const IS_NOT_ACTIVE = 0;
    const ACCOUNT_COMPLETED = "completed";
    const ACCOUNT_NOT_COMPLETED = "not completed";
    const ACCOUNT_DEACTIVATED = "deactivated";

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'codes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['code', 'name', 'code_type_id'], 'required'],
            [['description'], 'string'],
            [['code_type_id', 'is_active'], 'integer'],
            [['code', 'name'], 'string', 'max' => 50],
            [['code_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CodesType::className(), 'targetAttribute' => ['code_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'description' => 'Description',
            'code_type_id' => 'Code Type ID',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeType() {
        return $this->hasOne(CodesType::className(), ['id' => 'code_type_id']);
    }

    public static function getCodesListByTypes($type) {
        $id = CodesType::find()->where(['code' => $type])->one()->id;
        $codes = ArrayHelper::map(static::find()->where(['code_type_id' => $id])->all(), 'id', 'name');
        return $codes;
    }

    public static function getCodesListByTypesAdmin($type) {
        $id = CodesType::find()->where(['code' => $type])->one()->id;
        $codes = ArrayHelper::map(static::find()->where(['code_type_id' => $id])->andWhere(['code' => ['ib', 'client']])->all(), 'id', 'name');
        return $codes;
    }

    public static function getCodesListByTypesIb($type) {
        $id = CodesType::find()->where(['code' => $type])->one()->id;
        $codes = ArrayHelper::map(static::find()->where(['code_type_id' => $id])->andWhere(['code' => 'client'])->all(), 'id', 'name');
        return $codes;
    }

//    public static function getCodesListByIB($type) {
//        $id = CodesType::find()->where(['code' => $type])->one()->id;
//        $codes = ArrayHelper::map(static::find()->where(['code_type_id' => $id])->andWhere(['code' => 'ib'])->all(), 'id', 'name');
//        return $codes;
//    }


    public static function getCodesListByStatus($status) {
        //status has id 2 in table codetype
        $id = CodesType::find()->where(['code' => $status])->one()->id;

        $codes = ArrayHelper::map(static::find()->where(['code_type_id' => $id])->all(), 'id', 'name');
        //return the array values of status
        return $codes;
    }

    public static function getCodesIdFromTypes($type) {
        return static::find()->where(['code' => $type])->one()->id;
    }

    public static function getCodesFromDropdown() {

        return ArrayHelper::map(static::find()->joinWith("codeType code")->where('code.code="U_TY"')->all(), 'id', 'name');
    }

    public static function getStatusFromDropdown() {

        return ArrayHelper::map(static::find()->joinWith("codeType code")->where('code.code="U_ST"')->all(), 'id', 'name');
    }
    
    public static function getDropdownCodesByCodeType($code_type){
        return ArrayHelper::map(static::find()->joinWith("codeType code")->where('code.code="'.$code_type.'"')->all(), 'code', 'name');
    }

}
