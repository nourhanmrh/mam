<?php

namespace common\models\codes;

use Yii;

/**
 * This is the model class for table "codes_type".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $description
 *
 * @property Codes[] $codes
 */
class CodesType extends \yii\db\ActiveRecord {

    const USER_TYPE = "U_TY";
    const USER_STATUS = "U_ST";
    const ACCOUNT_STATUS = "A_ST";
    const BUILDER = "BU";
    const CONNECTIONS_TYPES = "CO_TY";
    const EXECUTION_MODE = "EX_MO";
    const FEED_MODE = "FE_MO";
    const USER_IB = "ib";
   

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'codes_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['code', 'name'], 'required'],
            [['description'], 'string'],
            [['code'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodes() {
        return $this->hasMany(Codes::className(), ['code_type_id' => 'id']);
    }

}
