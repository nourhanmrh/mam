<?php

namespace common\models\codes;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\codes\Codes;

/**
 * CodesSearch represents the model behind the search form of `app\models\Codes`.
 */
class CodesSearch extends Codes {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'code_type_id', 'is_active'], 'integer'],
            [['code', 'name', 'description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Codes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'code_type_id' => $this->code_type_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
                ->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

}
