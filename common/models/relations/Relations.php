<?php

namespace common\models\relations;

use Yii;
use common\models\mamaccounts\MamAccountsQuery;
use common\models\mamaccounts\MamAccounts;
use common\models\users\Users;

/**
 * This is the model class for table "relations".
 *
 * @property int $id
 * @property int $ib_id
 * @property int $client_id
 * @property int $master_account
 * @property int $slave_account
 * @property string $follow_date
 * @property string|null $unfollow_date
 * @property string|null $copy_type
 * @property float|null $value
 * @property float $amount_following
 *
 * @property Users $client
 * @property Users $ib
 * @property MamAccounts $masterAccount
 * @property MamAccounts $slaveAccount
 */
class Relations extends \yii\db\ActiveRecord {

    const ACTION_FOLLOW = "follow";
    const ACTION_UNFOLLOW = "unfollow";

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'relations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['ib_id', 'client_id', 'master_account', 'slave_account', 'amount_following'], 'required'],
            [['ib_id', 'client_id', 'master_account', 'slave_account'], 'integer'],
            [['follow_date', 'unfollow_date'], 'safe'],
            [['value', 'amount_following'], 'number'],
            [['copy_type'], 'string', 'max' => 50],
            [['master_account'], 'exist', 'skipOnError' => true, 'targetClass' => MamAccounts::className(), 'targetAttribute' => ['master_account' => 'id']],
            [['slave_account'], 'exist', 'skipOnError' => true, 'targetClass' => MamAccounts::className(), 'targetAttribute' => ['slave_account' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['ib_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['ib_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'ib_id' => 'Ib ID',
            'client_id' => 'Client ID',
            'master_account' => 'Master Account',
            'slave_account' => 'Slave Account',
            'follow_date' => 'Follow Date',
            'unfollow_date' => 'Unfollow Date',
            'copy_type' => 'Copy Type',
            'value' => 'Value',
            'amount_following' => 'Amount Following',
        ];
    }

    /**
     * Gets query for [[Client]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getClient() {
        return $this->hasOne(Users::className(), ['id' => 'client_id']);
    }

    /**
     * Gets query for [[Ib]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getIb() {
        return $this->hasOne(Users::className(), ['id' => 'ib_id']);
    }

    /**
     * Gets query for [[MasterAccount]].
     *
     * @return \yii\db\ActiveQuery|MamAccountsQuery
     */
    public function getMasterAccount() {
        return $this->hasOne(MamAccounts::className(), ['id' => 'master_account']);
    }

    /**
     * Gets query for [[SlaveAccount]].
     *
     * @return \yii\db\ActiveQuery|MamAccountsQuery
     */
    public function getSlaveAccount() {
        return $this->hasOne(MamAccounts::className(), ['id' => 'slave_account']);
    }

    /**
     * {@inheritdoc}
     * @return MamAccountsQuery the active query used by this AR class.
     */
    public static function find() {
        return new MamAccountsQuery(get_called_class());
    }

    public static function getIBName() {
        $query = new Query;
        $query->select(Users::tableName() . '.id,' . Users::tableName() . '.name AS text')
                ->from(Users::tableName())
                ->join("INNER JOIN", Relations::tableName(), Relations::tableName() . '.ib_id =' . Users::tableName() . '.id')
                ->distinct();
        //    ->where([Relations::tableName() . '.ib_id' => Users::tableName() . '.id']);


        $command = $query->createCommand();
        $data = $command->queryAll();
        foreach ($data as $d) {
            $result[] = $d['text'];
        }return $result;
    }

    public static function getClientName() {

        $query = new yii\db\Query();
        $cname = $query->select(['name'])
                ->from('users')
                ->where(['id' => 'client_id'])
                ->all();

        return $cname;
    }
     public function get_amount_following($list) {
       $amt = Yii::$app->db->createCommand('select sum(amount_following)as amount from relations where master_account in' . $list)->queryAll();
        return $amt;
     }
    

    public function relations_map() {
        $result = [];
        $ibs = Relations::find()->joinWith('ib ibs')->joinWith('client clients');

        if (AuthManager::is_ib()) {
            $ibs = $ibs->where(['ib_id' => Yii::$app->user->id]);
        }

        $ibs = $ibs->asArray()->all();

        foreach ($ibs as $ib) {
            $result[$ib['ib']['name']][] = ['name' => $ib['client']['name'], 'id' => $ib['id']];
        }

        return $result;
    }

    public static function get_clients_without_relations() {
        $query = new Query;
        $query->select([users::tableName() . '.id', users::tableName() . '.name', MamAccounts::tableName() . '.id', MamAccounts::tableName() . '.login', Brokers::tableName() . '.broker_name', MamAccounts::tableName() . '.platform'])
                ->from(Users::tableName())
                ->join("INNER JOIN", MamAccounts::tableName(), MamAccounts::tableName() . '.user_id =' . Users::tableName() . '.id')
                ->join("INNER JOIN", Codes::tableName(), Codes::tableName() . '.id =' . Users::tableName() . '.type')
                ->where([Codes::tableName() . '.code' => Codes::USER_CLIENT])
                ->join("INNER JOIN", Brokers::tableName(), Brokers::tableName() . '.id =' . MamAccounts::tableName() . '.brokers_id')
                ->andFilterWhere(['NOT IN', MamAccounts::tableName() . '.id', (new Query())->select('slave_account')->from('relations')->where(['unfollow_date' => null])]);

        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);

        return $out['results'];
    }

    public static function get_user_followed_traders($id) {
        $q = Relations::find()->where(['client_id' => \yii::$app->user->id])->andWhere(['slave_account' => $id])->andWhere(['unfollow_date' => null])->all();
        $array = [];
        if ($q) {
            foreach ($q as $k => $v) {
                $array[] = $v['master_account'];
            }
        }

        return $array;
    }

    public static function relationsMapping($relations) {
        return array_map(function($relation) {
            return array(
                "Id" => $relation['id'],
                "CopyType" => $relation['copy_type'],
                "Value" => $relation['value']
            );
        },$relations);
    }

}
