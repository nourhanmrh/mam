<?php

namespace common\models\mamtrades;

use Yii;

/**
 * This is the model class for table "mam_trades".
 *
 * @property int $id
 * @property string $order
 * @property string $login
 * @property string $symbol
 * @property string $comment
 * @property string $command
 * @property double $volume
 * @property int $open_time
 * @property int $close_time
 * @property double $open_price
 * @property double $sl
 * @property double $tp
 * @property double $commission
 * @property double $commission_agent
 * @property double $swap
 * @property double $profit
 * @property int $state
 * @property string $account_type
 */
class MamTrades extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'mam_trades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['order', 'login', 'symbol', 'command', 'volume', 'open_time', 'close_time', 'open_price', 'sl', 'tp', 'commission', 'commission_agent', 'swap', 'profit', 'state'], 'required'],
            [['order', 'login', 'open_time', 'close_time', 'state'], 'integer'],
            [['volume', 'open_price', 'sl', 'tp', 'commission', 'commission_agent', 'swap', 'profit'], 'number'],
            [['symbol'], 'string', 'max' => 100],
            [['comment'], 'string', 'max' => 250],
            [['command'], 'string', 'max' => 25],
            [['account_type'], 'string', 'max' => 50],
            [['order'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'order' => 'Order',
            'login' => 'Login',
            'symbol' => 'Symbol',
            'comment' => 'Comment',
            'command' => 'Command',
            'volume' => 'Volume',
            'open_time' => 'Open Time',
            'close_time' => 'Close Time',
            'open_price' => 'Open Price',
            'sl' => 'Sl',
            'tp' => 'Tp',
            'commission' => 'Commission',
            'commission_agent' => 'Commission Agent',
            'swap' => 'Swap',
            'profit' => 'Profit',
            'state' => 'State',
            'account_type' => 'Account Type',
        ];
    }

}
