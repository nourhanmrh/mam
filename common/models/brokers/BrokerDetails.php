<?php

namespace common\models\brokers;

use Yii;

class BrokerDetails extends \yii\db\ActiveRecord {

    public $name;
    public $location;
    public $currency;
    public $array = ['name', 'location', 'currency'];

    public static function tableName() {
        return 'broker_details';
    }

    public function rules() {
        return [
            [['broker_id', 'key', 'value'], 'required'],
            [['broker_id'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 128],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

    /**
     * Gets query for [[Broker]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBroker() {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }

    public function getAvailableDetails() {

        return $array;
    }

}
