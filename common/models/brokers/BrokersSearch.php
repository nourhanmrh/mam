<?php

namespace common\models\brokers;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\brokers\Brokers;

/**
 * BrokersSearch represents the model behind the search form of `app\models\Brokers`.
 */
class BrokersSearch extends Brokers {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'min_deposit'], 'integer'],
            [['broker_name', 'platforms', 'regulated_in', 'supp_currencies', 'commissions', 'website'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Brokers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'min_deposit' => $this->min_deposit,
           
        ]);
       
        $query->andFilterWhere(['like', 'platforms', $this->platforms])
                ->andFilterWhere(['like', 'broker_name', $this->broker_name])
                ->andFilterWhere(['like', 'commissions', $this->commissions])
                ->andFilterWhere(['like', 'regulated_in', $this->regulated_in])
                ->andFilterWhere(['like', 'supp_currencies', $this->supp_currencies])
                ->andFilterWhere(['like', 'website', $this->website]);
       $dataProvider->sort->attributes['login'] = [
        'asc' => ['login' => SORT_ASC],
        'desc' => ['login' => SORT_DESC],
        ];
       
        return $dataProvider;
    }

}
