<?php

namespace common\models\brokers;

use Yii;
use common\models\mamaccounts\MamAccountsQuery;
use common\models\mamaccounts\MamAccounts;

/**
 * This is the model class for table "brokers".
 *
 * @property int $id
 * @property string|null $image_location
 * @property string $broker_name
 * @property string $platforms
 * @property string $regulated_in
 * @property int $min_deposit
 * @property string $supp_currencies
 * @property float $commissions
 * @property int $is_paid
 * @property float|null $amount_paid
 * @property string|null $website
 *
 * @property BrokerDetails[] $brokerDetails
 * @property MamAccounts[] $mamAccounts
 * @property Promotions[] $promotions
 */
class Brokers extends \yii\db\ActiveRecord {

    public $file;
    private $datasource;
    private $columns = array(
        'id', 'broker_name', 'image_location', 'platforms', 'regulated_in', 'min_deposit','is_live',
        'supp_currencies', 'commissions', 'is_paid', 'amount_paid', 'website', 'max_leverage', 'description', 'status'
    );
    public static $map = [
        'Id' => 'id',
        'BrokerName' => 'broker_name',
        'Image' => 'image_location',
        'Platforms' => 'platforms',
        'ReguledIn' => 'regulated_in',
        'MinDeposit' => 'min_deposit',
        'SupportedCurrencies' => 'supp_currencies',
        'Commission' => 'commissions',
        'Website' => 'website',
        'MaxLeverage' => 'max_leverage',
        'Description' => 'description',
        'Status' => 'status',
        'AmountPaid' => 'amount_paid'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'brokers';
    }

    public function __construct() {
        $this->datasource = new \DataSourceResult(Yii::$app->db->dsn . ";charset=" . Yii::$app->db->charset, Yii::$app->db->username, Yii::$app->db->password);
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['broker_name', 'platforms', 'regulated_in', 'min_deposit', 'supp_currencies', 'commissions'], 'required'],
            [['min_deposit', 'is_paid', 'max_leverage'], 'integer'],
            [['commissions', 'amount_paid'], 'number'],
            [['image_location'], 'string', 'max' => 2083],
            [['broker_name', 'platforms', 'regulated_in', 'supp_currencies'], 'string', 'max' => 100],
            [['website','description','status'], 'string', 'max' => 300],
            [['broker_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'image_location' => 'Image Location',
            'broker_name' => 'Broker Name',
            'platforms' => 'Platforms',
            'regulated_in' => 'Regulated In',
            'min_deposit' => 'Min Deposit',
            'supp_currencies' => 'Supp Currencies',
            'commissions' => 'Commissions',
            'is_paid' => 'Is Paid',
            'amount_paid' => 'Amount Paid',
            'website' => 'Website',
            'max_leverage' => 'Max Leverage',
            'description' => 'Description',
            'status' => 'Status'
        ];
    }

    
    public function getBrokerDetails() {
        return $this->hasMany(BrokerDetails::className(), ['broker_id' => 'id']);
    }

    /**
     * Gets query for [[MamAccounts]].
     *
     * @return \yii\db\ActiveQuery|MamAccountsQuery
     */
    public function getMamAccounts() {
        return $this->hasMany(MamAccounts::className(), ['brokers_id' => 'id']);
    }

    /**
     * Gets query for [[Promotions]].
     *
     * @return \yii\db\ActiveQuery|PromotionsQuery
     */
    public function getPromotions() {
        return $this->hasMany(Promotions::className(), ['broker_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return MamAccountsQuery the active query used by this AR class.
     */
    public static function find() {
        return new MamAccountsQuery(get_called_class());
    }

    public function findImagePath($id, $size) {
        return Yii::$app->params['ImageBrokerPath'] . "?id=" . $id . "&size=" . $size;
    }

  

    public static function get_brokers_details() {
        $brokers = new Brokers();
        $bk = (new \yii\db\Query())
                ->select('*')
                ->from('brokers')
                ->all();

        foreach ($bk as $k => $v) {
            $bk[$k]['id'] = $v['id'];
            $bk[$k]['image_location'] = $v['image_location'];
            $bk[$k]['name'] = $v['broker_name'];
            $bk[$k]['platforms'] = $v['platforms'];
            $bk[$k]['regulated_in'] = $v['regulated_in'];
            $bk[$k]['min_deposit'] = $v['min_deposit'];
            $bk[$k]['supp_currencies'] = $v['supp_currencies'];
            $bk[$k]['commissions'] = $v['commissions'];
            $bk[$k]['website'] = $v['website'];
        }
        return $bk;
    }

    public static function get_sponsor_details() {
        $brokers = new Brokers();
        $max_amount = (new \yii\db\Query())->from('brokers')->max('amount_paid');

        $sp = (new \yii\db\Query())
                ->select('*')
                ->from('brokers')
                ->where(['amount_paid' => $max_amount])
                ->one();

        return $sp;
    }

    public static function get_followed_brokers_details($acc, $broker) {
        $broker_followed = (new \yii\db\Query())
                ->select('*')
                ->from('brokers')
                ->where([Brokers::tableName() . '.id' => $broker])
                ->all();

        foreach ($broker_followed as $k => $v) {
            $broker_followed[$k]['pic'] = $v['image_location'];
            $broker_followed[$k]['broker_name'] = $v['broker_name'];
            $broker_followed[$k]['platforms'] = $v['platforms'];
            $broker_followed[$k]['regulated_in'] = $v['regulated_in'];
            $broker_followed[$k]['min_deposit'] = $v['min_deposit'];
            $broker_followed[$k]['supp_currencies'] = $v['supp_currencies'];
            $broker_followed[$k]['commissions'] = $v['commissions'];
        }
        return $broker_followed;
    }

    public static function get_broker($id) {
        $broker_name = (new \yii\db\Query())
                ->select('broker_name')
                ->from('brokers')
                ->where(['id' => $id])
                ->one();
        return $broker_name['broker_name'];
    }

    public static function getBrokerId($name) {
        $brokerId = (new \yii\db\Query())
                ->select('id')
                ->from('brokers')
                ->where(['broker_name' => $name])
                ->one();
        return $brokerId['id'];
    }

    public function getBrokersByKendo($request) {
        $res = $this->datasource->read($this->tablename(), $this->columns, $request);
        $res['data'] = $this->brokerMapping($res['data']);
        return $res;
    }

    public static function brokerMapping($brokers) {
        return array_map(function($broker) {
            return array(
                'Id' => $broker['id'],
                'BrokerName' => $broker['broker_name'],
                'Image' => Yii::$app->params['frontendURL'] . Yii::$app->params['ImageBrokerPath'] . "?id=" . $broker['id'] . "&size=SM",
                'Platforms' => implode(",",json_decode($broker['platforms'])),
                'ReguledIn' => $broker['regulated_in'],
                'MinDeposit' => $broker['min_deposit'],
                'SupportedCurrencies' => implode(",",json_decode($broker['supp_currencies'])),
                'Commission' => $broker['commissions'],
                'Website' => $broker['website'],
                'MaxLeverage' => $broker['max_leverage'],
                'Description' => $broker['description'],
                'Status' => $broker['status'],
                //'AmountPaid' => $broker['amount_paid']
            );
        }, $brokers);
    }
}
