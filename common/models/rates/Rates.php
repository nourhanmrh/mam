<?php

namespace common\models\rates;

use Yii;

/**
 * This is the model class for table "rates".
 *
 * @property int $id
 * @property int $ib_id
 * @property int $client_id
 * @property int|null $master_account
 * @property int $slave_account
 * @property int|null $rate
 * @property string $create_date
 */
class Rates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ib_id', 'client_id', 'slave_account'], 'required'],
            [['ib_id', 'client_id', 'master_account', 'slave_account', 'rate'], 'integer'],
            [['create_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ib_id' => 'Ib ID',
            'client_id' => 'Client ID',
            'master_account' => 'Master Account',
            'slave_account' => 'Slave Account',
            'rate' => 'Rate',
            'create_date' => 'Create Date',
        ];
    }

    /**
     * {@inheritdoc}
     * @return RatesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RatesQuery(get_called_class());
    }

    public function setRating($userId,$activeLogin,$post)
    {   
        $master = (new \yii\db\Query())
                ->select('id,user_id')
                ->from('mam_accounts')
                ->where(['name' => $post['master']])
                ->one();

        $this->ib_id = $master['user_id'];
        $this->client_id = $userId;
        $this->master_account = $master['id'];
        $this->slave_account = $activeLogin;
        $this->rate = $post['rating'];
        $this->save();
        return 'done';
    }
}
