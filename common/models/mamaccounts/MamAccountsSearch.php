<?php

namespace common\models\mamaccounts;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\mamaccounts\MamAccounts;
use app\helpers\AuthManager;
use common\models\users\Users;
use common\models\brokers\Brokers;

/**
 * MamAccountsSearch represents the model behind the search form of `app\models\MamAccounts`.
 */
class MamAccountsSearch extends MamAccounts {

  
    public function rules() {
        return [
            [['id', 'login', 'trades', 'balance', 'margin', 'equity', 'leverage', 'margin_percentage'], 'integer'],
            [['name', 'platform', 'account_type', 'currency', 'is_live', 'active_account', 'account_status', 'broker_name', 'user_name'], 'safe'],
        ];
    }

 
    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {   
        $query = MamAccounts::find()
                ->joinWith("brokers brokers")
                ->joinWith("user users");
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
      
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'trades' => $this->trades,
            'balance' => $this->balance,
            'margin' => $this->margin,
            'equity' => $this->equity,
            'leverage' => $this->leverage,
            'margin_percentage' => $this->margin_percentage,
            'account_status'=>$this->account_status,
            'account_type'=>$this->account_type
        ]);

        $query->andFilterWhere(['like', MamAccounts::tableName() . '.name', $this->name])
                ->andFilterWhere(['like', 'login', $this->login])
                ->andFilterWhere(['like', 'platform', $this->platform])
                ->andFilterWhere(['like', 'currency', $this->currency])
                ->andFilterWhere(['like', 'is_live', $this->is_live])
                ->andFilterWhere(['like', 'users.username', $this->user_name])
                ->andFilterWhere(['like', 'brokers.broker_name', $this->broker_name])
                ->andFilterWhere(['like', 'active_account', $this->active_account]);
       

        $query->orderBy(['id' => SORT_DESC])->all();
        return $dataProvider;
    }

}
