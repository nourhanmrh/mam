<?php

namespace  common\models\mamaccounts;

/**
 * This is the ActiveQuery class for [[MamAccounts]].
 *
 * @see MamAccounts
 */
class MamAccountsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MamAccounts[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MamAccounts|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
