<?php

namespace common\models\mamaccounts;

use Yii;
use yii\db\Query;
use common\models\brokers\Brokers;
use common\models\codes\Codes;
use common\models\users\Users;
use common\models\brokers\Brokes;
use common\models\tradersinfo\TradersInfo;
use common\models\relations\Relations;
use common\models\Constants;
use common\services\DemoAccountObject;
use common\services\MamAccountCreate;
use common\services\MamAccountCheckPassword;
use common\services\Response;
use common\models\forms\ConfirmationForm;
use common\models\ReportServer\ReportServer;
use common\models\ReportServer\Mt5Reportserver;

/**
 * This is the model class for table "mam_accounts".
 *
 * @property int $id
 * @property int|null $login
 * @property string $name
 * @property int $trades
 * @property float $balance
 * @property float $margin
 * @property float $equity
 * @property int $leverage
 * @property string $platform
 * @property float $margin_percentage
 * @property string|null $account_type
 * @property string|null $currency
 * @property int|null $is_live
 * @property int|null $user_id
 * @property int|null $brokers_id
 * @property int|null $active_account
 * @property string|null $account_status
 */
class MamAccounts extends \yii\db\ActiveRecord {

    public $broker_name;
    public $user_name;
    public static $map = [
        "Name" => "name",
        "Trades" => "trades",
        "Balance" => "balance",
        "Margin" => "margin",
        "Equity" => "equity",
        "Leverage" => "leverage",
        "Platform" => "platform",
        "Margin Percentage" => "margin_percentage",
        "Broker Id" => "brokers_id",
        "Currency" => "currency",
        "Live" => "is_live",
        "Account Status" => "account_status"
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'mam_accounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['login', 'trades', 'leverage', 'is_live', 'user_id', 'brokers_id', 'active_account'], 'integer'],
            [['name'], 'required'],
            [['balance', 'margin', 'equity', 'margin_percentage'], 'number'],
            [['name'], 'string', 'max' => 100],
            [['platform'], 'string', 'max' => 30],
            [['account_type', 'currency', 'account_status'], 'string', 'max' => 50],
            [['name'], 'unique'],
            [['trades', 'balance', 'margin', 'equity', 'leverage', 'margin_percentage', 'active_account'], 'default', 'value' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'name' => 'Name',
            'trades' => 'Trades',
            'balance' => 'Balance',
            'margin' => 'Margin',
            'equity' => 'Equity',
            'leverage' => 'Leverage',
            'platform' => 'Platform',
            'margin_percentage' => 'Margin Percentage',
            'account_type' => 'Account Type',
            'currency' => 'Currency',
            'is_live' => 'Is Live',
            'user_id' => 'User ID',
            'brokers_id' => 'Brokers ID',
            'active_account' => 'Active Account',
            'account_status' => 'Account Status',
        ];
    }

    public function getBrokers() {
        return $this->hasOne(Brokers::className(), ['id' => 'brokers_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[Rates]].
     *
     * @return \yii\db\ActiveQuery|RatesQuery
     */
    public function getRates() {
        return $this->hasMany(Rates::className(), ['master_account' => 'id']);
    }

    /**
     * Gets query for [[Rates0]].
     *
     * @return \yii\db\ActiveQuery|RatesQuery
     */
    public function getRates0() {
        return $this->hasMany(Rates::className(), ['slave_account' => 'id']);
    }

    /**
     * Gets query for [[Relations]].
     *
     * @return \yii\db\ActiveQuery|MamAccountsQuery
     */
    public function getRelations() {
        return $this->hasMany(Relations::className(), ['master_account' => 'id']);
    }

    /**
     * Gets query for [[Relations0]].
     *
     * @return \yii\db\ActiveQuery|MamAccountsQuery
     */
    public function getRelations0() {
        return $this->hasMany(Relations::className(), ['slave_account' => 'id']);
    }

    public static function find() {
        return new MamAccountsQuery(get_called_class());
    }

    public function getBroker() {
        return $this->hasOne(Brokers::className(), ['id' => 'brokers_id']);
    }

    public function getFollowing($request) {
        $query = new Query;
        $query->select('*')
                ->from(MamAccounts::tableName())
                ->join("INNER JOIN", Relations::tableName(), MamAccounts::tableName() . '.id =' . Relations::tableName() . '.slave_account')
                ->where(['ib_id' => $request->ib_id])->andWhere(['master_account' => $request->trader_id])->andWhere(['unfollow_date' => null]);



        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);
        return $out['results'];
    }

    public function getAllSlaveBrokers($id, $platform, $broker, $slave_id = null) {
        $slav = new Query;
        $slav->select('*')
                ->from(MamAccounts::tableName())
                ->join("INNER JOIN", Relations::tableName(), MamAccounts::tableName() . '.id =' . Relations::tableName() . '.slave_account')
                ->where([Relations::tableName() . '.master_account' => $id]);
        if ($slave_id) {
            $slav->andWhere([Relations::tableName() . '.slave_account' => $slave_id]);
        }
        $slav->andWhere(['unfollow_date' => null])
                ->join("INNER JOIN", Brokers::tableName(), Brokers::tableName() . '.id =' . MamAccounts::tableName() . '.brokers_id')
                ->all();
        $command = $slav->createCommand();
        $data = $command->queryAll();
        $res = [];
        foreach ($data as $k => $v) {
            $res[$k]['login'] = $v['login'];
            $res[$k]['name'] = $v['name'];
            $res[$k]['platform'] = $v['platform'];
            $res[$k]['broker'] = Brokers::findOne(['id' => $v['brokers_id']])->broker_name;
            $res[$k]['account_type'] = $v['account_type'];
            $res[$k]['copy_type'] = $v['copy_type'];
            $res[$k]['value'] = $v['value'];
            $res[$k]['active'] = $v['active_account'];
            $res[$k]['master_platform'] = $platform;
            $res[$k]['master_broker'] = Brokers::findOne(['id' => $broker])->broker_name;
        }



        return $res;
    }

    public function getSlave($id, $slave_id, $platform, $broker_name) {
        $slav = new Query;
        $slav->select('*')
                ->from(MamAccounts::tableName())
                ->join("INNER JOIN", Relations::tableName(), MamAccounts::tableName() . '.id =' . Relations::tableName() . '.slave_account')
                ->where([Relations::tableName() . '.master_account' => $id])
                ->andWhere(['slave_account' => $slave_id])
                ->andWhere(['unfollow_date' => null])
                ->join("INNER JOIN", Brokers::tableName(), Brokers::tableName() . '.id =' . MamAccounts::tableName() . '.brokers_id')
                ->one();
        $command = $slav->createCommand();
        $data = $command->queryOne();
        $res = [];
        $t = MamAccounts::find()->select(['login'])->where(['id' => $data['master_account']])->one();
        $res['login'] = $data['login'];
        $res['name'] = $data['name'];
        $res['platform'] = $data['platform'];
        $res['broker'] = Brokers::findOne(['id' => $data['brokers_id']])->broker_name;
        $res['account_type'] = $data['account_type'];
        $res['copy_type'] = $data['copy_type'];
        $res['value'] = $data['value'];
        $res['active'] = $data['active_account'];
        $res['master_platform'] = $platform;
        $res['master_broker'] = $broker_name;
        $res['master_account'] = $t['login'];
        return $res;
    }

    public function getAllMastersForMap($master_id = null) {
        $map = Array();
        $master = new Query;
        $master->select([MamAccounts::tableName() . '.id', MamAccounts::tableName() . '.login', MamAccounts::tableName() . '.platform', MamAccounts::tableName() . '.brokers_id', Brokers::tableName() . '.broker_name'])
                ->from(Relations::tableName());
        if ($master_id) {
            $master->where(Relations::tableName() . ".master_account=" . $master_id);
        }
        $master->join("INNER JOIN", MamAccounts::tableName(), MamAccounts::tableName() . '.id =' . Relations::tableName() . '.master_account')
                ->join("INNER JOIN", Brokers::tableName(), Brokers::tableName() . '.id =' . MamAccounts::tableName() . '.brokers_id')
                ->groupBy('master_account')->distinct()->all();
        $command = $master->createCommand();
        $data = $command->queryAll();

        return $data;
    }

    public function getAllAccountsMap($master_id = null, $slave_id = null) {
        $result = Array();
        $array = Array();
        $masters = $this->getAllMastersForMap($master_id);
        foreach ($masters as $acc) {
            $slaves = $this->getAllSlaveBrokers($acc['id'], $acc['platform'], $acc['brokers_id'], $slave_id);
            $m = $slaves;
            if (array_key_exists($acc['broker_name'], $result)) {
                $result['brokers'] = $acc['broker_name'];
                if (array_key_exists($acc['platform'], $result)) {
                    $result['platforms'] = $acc['platform'];
                    $result['master'] = $acc['login'];
                    $result['slave'] = $slaves;
                }
            }
            $result['broker'] = $acc['broker_name'];
            $result['platform'] = $acc['platform'];
            $result['master'] = $acc['login'];
            $result['slave'] = $slaves;
            $array[] = $result;
        }
        return $array;
    }

    public function getMasterWithSlaveInfo($master_id, $slave_id, $copy_type, $value) {
        $result = [];
        $master = self::findOne($master_id);
        $slave = self::findOne($slave_id);
        if ($master && $slave) {
            $slv = [];
            $slv['login'] = $slave['login'];
            $slv['name'] = $slave['name'];
            $slv['platform'] = $slave['platform'];
            $slv['broker'] = Brokers::findOne(['id' => $slave['brokers_id']])->broker_name;
            $slv['account_type'] = $slave['account_type'];
            $slv['copy_type'] = $copy_type;
            $slv['value'] = $value;
            $slv['active'] = $slave['active_account'];
            $slv['master_platform'] = $master['platform'];
            $slv['master_broker'] = Brokers::findOne(['id' => $master['brokers_id']])->broker_name;

            $result['broker'] = $slv['master_broker'];
            $result['platform'] = $slv['master_platform'];
            $result['master'] = $master['login'];
            $result['slave'] = $slv;

            return $result;
        }
        return false;
    }

    public static function getMaster() {
        return self::find()->select([MamAccounts::tableName() . '.login as value', MamAccounts::tableName() . '.login as text'])->where(['account_type' => "master"])->asArray()->all();
    }

    public static function getOwn() {
        $query = new yii\db\Query();
        $re = $query->select(['login'])
                ->from('mam_accounts')
                ->where(['account_type' => 'master'])
                ->andWhere(['user_id' => \Yii::$app->user->id])
                ->all();

        return $re;
    }

    public static function getOwnMaster() {
        $query = new yii\db\Query();
        $re = $query->select(['id'])
                ->from('mam_accounts')
                ->where(['account_type' => 'master'])
                ->all();

        return $re;
    }

    public function CrendentialsCreation($name, $login) {
        if ($name) {
            $acc = MamAccounts::find()->where(['name' => $name])->one();
            $acc->login = $login;
            $acc->account_status = Codes::ACCOUNT_COMPLETED;
            $acc->save();
        } else {
            MamAccounts::updateAll(['active_account' => 0], 'user_id =' . \yii::$app->user->id);
            $model_acc = new MamAccounts();
            $model_acc->name = 'RL_' . $model_acc->createAccountName();
            $model_acc->user_id = yii::$app->user->id;
            $model_acc->is_live = 1;
            $model_acc->brokers_id = $id;
            $model_acc->active_account = 1;
            $model_acc->account_status = Codes::ACCOUNT_NOT_COMPLETED;
            $model_acc->save();
        }
    }

    public function createLive($id, $bk_id) {
        $model = new MamAccounts();
        $u = new Users();
        $user = $u->getUserById($id);
        $model->name = 'RL_' . $model->createAccountName();
        $model->account_type = ($user['type'] == Constants::USER_TYPE_MONEY_MANAGER) ? 'master' : 'slave';
        $model->user_id = $id;
        $model->is_live = 1;
        $model->brokers_id = $bk_id;
        MamAccounts::updateAll(['active_account' => 0], ['user_id' => $model->user_id]);
        $model->active_account = 1;

        $model->account_status = Constants::ACCOUNT_STATUS_NOT_COMPETED;
        if ($model->save()) {
            \yii::$app->session->set('active_account', $model->name);
            $n = new \common\models\forms\ConfirmationForm();
            $n->sendAccountCreation($model, \yii::$app->user->id);
            return $model->name;
        }
    }

    public function createDemo($id) {
        $u = new Users();
        $user = $u->getUserById($id);

        $model = new MamAccounts();

        $model->name = 'DM_' . $model->createAccountName();

        MamAccounts::updateAll(['active_account' => 0], ['user_id' => \yii::$app->user->id]);
        $model->active_account = 1;

        $model->account_type = ($user['type'] == Constants::USER_TYPE_MONEY_MANAGER) ? 'master' : 'slave';

        $model->user_id = $id;

        $broker = Brokers::find()->where("broker_name='" . \Yii::$app->params['DefaultDemoBrokerName'] . "'")->one();
        if ($broker) {
            $model->brokers_id = $broker['id'];
        } else {
            return false;
        }

        $model->is_live = 0;

        $model->platform = 'mt5';

        $model->account_status = Codes::ACCOUNT_NOT_COMPLETED;

        if ($model->save()) {
            \yii::$app->session->set('active_account', $model->name);
        }

        return $model;
    }

    public function filters($request, $result) {

        if (AuthManager::is_ib()) {
            $master_accounts = MamAccounts::find()->select("login")->where("user_id =" . \Yii::$app->user->id)->andWhere(['account_type' => 'master'])->andWhere(['active_account' => 1])->asArray()->all();
            $m_accounts = [];
            foreach ($master_accounts as $master) {
                $m_accounts[] = $master['login'];
            }
            $ib_accounts = MamAccounts::find()->select(MamAccounts::tableName() . ".login")
                            ->join('LEFT JOIN', Relations::tableName(), MamAccounts::tableName() . ".user_id = " . Relations::tableName() . ".client_id")
                            ->where(Relations::tableName() . ".ib_id =" . \Yii::$app->user->id)
                            ->andWhere(MamAccounts::tableName() . ".deposit = 1")
                            ->orWhere([MamAccounts::tableName() . ".user_id" => \Yii::$app->user->id])
                            ->orWhere([MamAccounts::tableName() . ".master_account" => $m_accounts, MamAccounts::tableName() . ".user_id" => null])
                            ->asArray()->all();


            $filters_array = [];
            if (count($ib_accounts) > 0) {
                foreach ($ib_accounts as $account) {
                    $filters_array[] = $result->ObjectsFilter('login', 'eq', $account['login']);
                }
                $master_filter[] = $result->addExternalFilterToRequest($filters_array, 'or');
                $request = $result->addFilter('and', $master_filter, $request);
            } else {
                $filters_array[] = $result->ObjectsFilter('login', 'eq', -1);
                $request = $result->addFilter('and', $filters_array, $request);
            }
        }

        if (AuthManager::is_client()) {
            $clients_accounts = MamAccounts::find()->select("login")
                            ->where("user_id =" . \Yii::$app->user->id)->andWhere(['account_type' => 'slave'])
                            ->andWhere(['active_account' => 1])->asArray()->all();

            $filters_array = [];
            if (count($clients_accounts) > 0) {
                foreach ($clients_accounts as $account) {
                    $filters_array[] = $result->ObjectsFilter('login', 'eq', $account['login']);
                }
                $master_filter[] = $result->addExternalFilterToRequest($filters_array, 'or');
                $request = $result->addFilter('and', $master_filter, $request);
            } else {
                $filters_array[] = $result->ObjectsFilter('login', 'eq', -1);
                $request = $result->addFilter('and', $filters_array, $request);
            }
        }

        $res = $result->read('mam_trades', array(
            'id', '`order`', 'login', 'symbol', 'comment', 'command', 'volume', 'open_time', 'close_time', 'open_price',
            'sl', 'tp', 'commission', 'commission_agent', 'swap', 'profit', 'state', 'account_type'), $request);

        $data = json_encode($res);

        return $data;
    }

    public function getOwnNotCompleteAccountByName($name, $user_id) {
        return MamAccounts::find()->where(['name' => $name, 'user_id' => $user_id, 'account_status' => Constants::ACCOUNT_STATUS_NOT_COMPETED])->joinWith("brokers")->one();
    }

    public function isActive() {
        return $this->active_account;
    }

    public function CheckAccountPasswordRequest($account) {
        try {
            $mam_account_check_password = new MamAccountCheckPassword($account);
            $operation_callback = $mam_account_check_password->send_message();
            $operation_response = new Response($operation_callback);
            if ($operation_response->status == Response::RESPONSE_DONE) {
                return true;
            }
        } catch (Exception $ex) {
            return false;
        }
        return false;
    }

    public function completeLiveAccount($post) {
        if ($this->CheckAccountPasswordRequest($post)) {
            $acc = MamAccounts::find()
                    ->joinWith('brokers')
                    ->where(['name' => $post['name']])
                    ->one();
            $acc->login = $post['login'];
            if (array_key_exists("platform", $post)) {
                $acc->platform = $post['platform'];
            } else {
                $acc->platform = 'mt5';
            }
            $acc->account_status = Constants::ACCOUNT_STATUS_WAITING_CONFIRMATION;
            if ($acc->is_live) {
                $acc->name = "RL_" . $acc['brokers']->broker_name . "_" . $acc->login;
            } else {
                $acc->name = "DM_" . $acc['brokers']->broker_name . "_" . $acc->login;
            }
            if ($acc->save()) {
            \yii::$app->session->set('active_account', $model->name);

                $n = new ConfirmationForm();

                $n->sendAccountCreation($acc, \yii::$app->user->id);

                return true;
            } else {
                print_r($acc->errors);
                die;
            }
        }
        return false;
    }

    public function CreateDemoAccountRequest($user_id, $account) {
        $demo_object = new DemoAccountObject($user_id, $account);
        try {
            $mam_account_create = new MamAccountCreate($demo_object);
            $operation_callback = $mam_account_create->send_message();
            $operation_response = new Response($operation_callback);
            if ($operation_response->status == Response::RESPONSE_DONE) {
                return $operation_response->data;
            }
        } catch (Exception $ex) {
            return 0;
        }
        return 0;
    }

    public function completeAccount($account) {
        if (($login = $this->CreateDemoAccountRequest(\Yii::$app->user->id, $account)) > 0) {
            $acc = self::find()
                    ->joinWith('brokers')
                    ->where(['name' => $account->name, 'account_status' => Constants::ACCOUNT_STATUS_NOT_COMPETED])
                    ->one();

            $acc->balance = $account->balance;
            $acc->equity = $account->balance;
            $acc->currency = $account->currency;
            $acc->leverage = $account->leverage;
            $acc->account_status = Constants::ACCOUNT_STATUS_COMPLETED;
            $acc->login = $login;
//            if ($acc->is_live) {
//                $acc->name = "RL_" . $acc['brokers']['broker_name'] . "_" . $acc->login;
//            } else {
//                $acc->name = "DM_" . $acc['brokers']['broker_name'] . "_" . $acc->login;
//            }
            if ($acc->save()) {
                $n = new ConfirmationForm();
                $n->sendAccountCreation($acc, \yii::$app->user->id);
                return true;
            } else {
                print_r($acc->errors());
                die;
            }
        }

        return false;
    }

    public function createDemoAccount($account) {
        if (($login = $this->CreateDemoAccountRequest(\Yii::$app->user->id, $account)) > 0) {
            $new_demo_account = $this->createDemo(\yii::$app->user->id);
            $new_demo_account->balance = $account->balance;
            $new_demo_account->equity = $account->balance;
            $new_demo_account->currency = $account->currency;
            $new_demo_account->leverage = $account->leverage;
            $new_demo_account->account_status = Constants::ACCOUNT_STATUS_COMPLETED;
            $new_demo_account->login = $login;
            if ($new_demo_account->save()) {
                $n = new \common\models\forms\ConfirmationForm();
                $n->sendAccountCreation($new_demo_account, \yii::$app->user->id);
                return true;
            } else {
                print_r($new_demo_account->errors);
                die;
            }
        }
        return false;
    }

    public function createLiveAccount() {
        $this->name = 'RL_' . $this->createAccountName();
        $this->user_id = yii::$app->user->id;
        $this->is_live = 1;
        MamAccounts::updateAll(['active_account' => 0], ['user_id' => $this->user_id]);
        $this->active_account = 1;
        if ($this->save(false)) {
            \yii::$app->session->set('active_account', $model->name);
            return true;
        }
    }

    public function getLiveAccountRequests() {
        return MamAccounts::find()
                        ->where(['user_id' => \yii::$app->user->id])
                        ->andWhere(['is_live' => 1])
                        ->andWhere(['account_status' => Constants::ACCOUNT_STATUS_NOT_COMPETED])
                        ->one();
    }

    public function getDataBase($name) {
        $account = MamAccounts::find()
                ->joinWith('broker')
                ->where(['name' => $name])
                ->asArray()
                ->one();

        $database = ($account['platform'] . "_" . $account['broker']['broker_name']);
        return array('database' => $database, 'login' => $account['login'], 'platform' => $account['platform']);
    }

    public function getTraderDatabase($name) {
        $account = TradersInfo::find()
                        ->where(['trader_name' => $name])
                        ->joinWith('mamAccount')
                        ->asArray()->one();

        $account = $account['mamAccount'];
        if ($account['brokers_id']) {
            $broker = Brokers::findOne($account['brokers_id']);
            $database = ($account['platform'] . "_" . $broker['broker_name']);
            return array('database' => $database, 'login' => $account['login'], 'platform' => $account['platform']);
        }
    }

    public function geBrokerDataBase($broker, $platform) {
        $database = ($platform . "_" . $broker);
        return array('database' => $database, 'platform' => $platform);
    }

    public function createAccountName() {
        $permitted_chars = '0123456789abcdefghejklmnopqrstuvwzABCDEFGHIJKLMNOPQRSTUVW';
        $log = substr(str_shuffle($permitted_chars), 0, 8);

        return $log;
    }

    public function getActiveAccountInfo($user) {
        $active = MamAccounts::find()
                ->select(self::tableName() . '.id,login,name,trades,balance,margin,equity,leverage,platform,margin_percentage,brokers_id,currency,' . MamAccounts::tableName() . '.is_live,account_status,active_account')
                ->joinWith('broker')
                ->where(['active_account' => Constants::IS_ACTIVE])
                ->andwhere(['user_id' => $user['id']])
                // ->andwhere(['account_type' => 'slave'])
                ->asArray()
                ->one();
        $data = new MamAccounts;
        if ($active['login']) {
            if (($active['platform'] != '')) {
                $info = $data->geBrokerDataBase($active['broker']['broker_name'], $active['platform']);
                if ($info) {
                    $report_server_class = (new ReportServer($active['platform']))->getClass();
                    $reportserver = new $report_server_class($info['database'], $active['login']);
                    $active['balance'] = $reportserver->get_user_balance();
                    $active['equity'] = $reportserver->get_user_equity();
                    $active['currency'] = $reportserver->get_user_currency();
                    $active['trades'] = $reportserver->getPositionsTrades();
                }
            }
        }

        return $active;
    }

    public function getActiveAccountByUserId($id) {
        $info = MamAccounts::find()
                ->select('login,name,trades,balance,margin,equity,account_type,leverage,platform,margin_percentage,brokers_id,currency,is_live,account_status')
                ->where(['active_account' => Constants::IS_ACTIVE])
                ->andwhere(['user_id' => $id])
                ->asArray()
                ->one();

        return $info;
    }

    public function getFollowedTraders($user) {
        $traders = (new \yii\db\Query())
                ->select('*')
                ->from('relations')
                ->where(['client_id' => $user['id']])
                ->all();

        $master = (new \yii\db\Query())
                ->select('*')
                ->from('mam_accounts')
                ->where(['user_id' => $traders[0]['ib_id']])
                ->andwhere(['account_type' => 'master'])
                ->all();

        return $master;
    }

    public function getAllAccounts($user) {
        $accounts = self::find()
                ->joinWith('broker')
                ->where('user_id=' . $user['id'])
                ->asArray()
                ->all();

        return $accounts;
    }

    public function getAllAccountNames($user) {
        $accounts = (new \yii\db\Query())
                ->select('name')
                ->from('mam_accounts')
                ->where(['user_id' => $user['id']])
                ->all();

        return $accounts;
    }

    public function getAccountByNameAndUser($name, $user_id) {
        $account = self::find()
                ->where('user_id=' . $user_id)
                ->andWhere(['name' => $name])
                ->asArray()
                ->one();

        return $account;
    }

    public function getAccountStatus($trader_id) {
        $account = self::find()
                ->select('account_status')
                ->where('id=' . $trader_id)
                ->asArray()
                ->one();

        return $account['account_status'];
    }

    public function switchAccount($user, $name) {

        MamAccounts::updateAll(['active_account' => 0], ['like', 'user_id', $user['id']]);

        MamAccounts::updateAll(['active_account' => 1], ['like', 'name', $name['name']]);

        return true;
    }

    public function demo($post) {
        if (isset($post['name']) && $post['name'] != "") {
            return $this->completeAccount((object) $post);
        } else {
            return $this->createDemoAccount((object) $post);
        }
    }

    public static function profileMapping($profile) {
        return array_map(function($info) {
            return array(
                "Name" => $info['name'],
                "Trades" => (int) $info['trades'],
                "Balance" => $info['balance'],
                "Margin" => $info['margin'],
                "Equity" => $info['equity'],
                "Leverage" => $info['leverage'],
                "Platform" => $info['platform'],
                "MarginPercentage" => $info['margin_percentage'],
                "Broker" => is_array($info['broker']) ? Brokers::brokerMapping(array($info['broker'])) : "",
                "Currency" => $info['currency'],
                "Live" => (int) $info['is_live'],
                "AccountStatus" => $info['account_status'],
                "IsActive" => (int) $info['active_account']
            );
        }, $profile);
    }

}
