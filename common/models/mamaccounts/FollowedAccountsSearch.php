<?php

namespace common\models\mamaccounts;

use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\mamaccounts\FollowedAccountsSearch;
use app\helpers\AuthManager;
use common\models\users\Users;
use common\models\brokers\Brokers;

class FollowedAccountsSearch extends MamAccounts {

    public $slaves;

    public function rules() {
        return [
            [['id', 'login', 'trades', 'balance', 'margin', 'equity', 'leverage', 'margin_percentage'], 'integer'],
            [['name', 'login', 'trades', 'balance', 'platform', 'account_type', 'currency', 'is_live', 'active_account', 'account_status'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params, $slaves) {
        $query = MamAccounts::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andWhere(['in', MamAccounts::tableName() . '.id', $this->slaves]);

        $query->andFilterWhere([
            'login' => $this->login,
            'trades' => $this->trades,
            'balance' => $this->balance,
            'margin' => $this->margin,
            'equity' => $this->equity,
            'leverage' => $this->leverage,
            'margin_percentage' => $this->margin_percentage,
            'account_status' => $this->account_status,
        ]);

        $query->andFilterWhere(['like', MamAccounts::tableName() . '.name', $this->name])
                ->andFilterWhere(['like', 'platform', $this->platform])
                ->andFilterWhere(['like', 'currency', $this->currency])
                ->andFilterWhere(['like', 'account_type', $this->account_type]);

        $query->all();

        return $dataProvider;
    }

}
