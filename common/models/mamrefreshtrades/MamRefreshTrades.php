<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\models\mamrefreshtrades;

use Yii;
use yii\base\Model;
use Socket\Raw;
use app\interfaces\MessagesInterface;
use yii\helpers\ArrayHelper;
use common\models\mamconfiguration\MamConfiguration;

/**
 * Description of MamRefreshTrades
 *
 * @author user
 */
class MamRefreshTrades implements MessagesInterface {

    const REFRESH_TRADES = "RefreshTrades";
    const PLATFORM_INDEX = 1;
    const MANAGER_LOGIN_INDEX = 2;
    const ORDERS_INDEX = 3;

    public $platform;
    public $manager_login;
    public $orders;
    public $message;
    private $configuration;

    public function __construct($platform) {
        $this->platform = $platform;
        $this->configuration = ArrayHelper::map(MamConfiguration::find()
                                ->select('conf_key, value')
                                ->asArray()
                                ->all(), 'conf_key', 'value');
    }

    public function connect() {
        $this->connection = new Connection();
        $this->connection->platform = $this->platform;
        $this->connection->manager_login = $this->configuration['mam_' . $this->platform . '_manager_login'];
        $this->connection->password = $this->configuration['mam_' . $this->platform . '_manager_password'];
        $this->connection->address = $this->configuration['mam_' . $this->platform . '_server_ip'];
        $res = $this->connection->connect();

        return $res;
    }

    public function create_message() {
        $messasge = [];
        $message[0] = self::REFRESH_TRADES;
        $message[self::PLATFORM_INDEX] = $this->platform;
        $message[self::MANAGER_LOGIN_INDEX] = $this->configuration['mam_' . $this->platform . '_manager_login'];
        $message[self::ORDERS_INDEX] = $this->orders;

        return implode(";", $message);
    }

    public function get_activity_description($message) {
        
    }

    public function get_message_model() {
        $message = explode(";", $this->message);
        $this->platform = $message[self::PLATFORM_INDEX];
        $this->manager_login = $message[self::MANAGER_LOGIN_INDEX];
        $this->orders = $message[self::ORDERS_INDEX];
    }

    public function send_message() {
        $this->message = $this->create_message();
        $factory = new Raw\Factory();
        $socket = $factory->createClient($this->configuration['mam_service_host'] . ":" . $this->configuration['mam_service_port']);
        $socket->write($this->message);
        $res = $socket->read(10000000);
        $socket->close();
        return $res;
    }

}
