<?php
/**
 * Created by PhpStorm.
 * User: Баранов Владимир <phpnt@yandex.ru>
 * Date: 18.08.2018
 * Time: 19:47
 */

namespace common\models;

class Constants
{
    // Codes Type
    const CODE_TYPE_COUNTRIES = 'CTS';
    
    const IS_ACTIVE = 1;
    const NOT_ACTIVE = 0;
    
    // Users Type
    const USER_TYPE_CLIENT = "client";
    const USER_TYPE_MONEY_MANAGER = "money manager";
    const USER_TYPE_ADMIN = "admin";
    const USER_TYPE_SALES = "sales";
    
    const USERS_FRONTEND_TYPES = [self::USER_TYPE_CLIENT,self::USER_TYPE_MONEY_MANAGER];
    const USERS_BACKEND_TYPES = [self::USER_TYPE_ADMIN,self::USER_TYPE_SALES];
    
    // Users Status
    const USER_STATUS_ACTIVE = 'active';
    const USER_STATUS_NOT_ACTIVE = 'not active';
    
    // Account status
    const ACCOUNT_STATUS_COMPLETED = 'completed';
    const ACCOUNT_STATUS_NOT_COMPETED = 'not completed';
    const ACCOUNT_STATUS_WAITING_CONFIRMATION = 'waiting confirmation';
    const ACCOUNT_STATUS_DELETED = 'deleted';
    
    // Account type
    const ACCOUNT_TYPE_MASTER = 'master';
    const ACCOUNT_TYPE_SLAVE = 'slave';
    
    // Account identity
    const ACCOUNT_DEMO = 'demo';
    const ACCOUNT_LIVE = 'live';
    
    // Notification types
    const NOTIFICATION_TYPE_SUCCESS = 'success';
    const NOTIFICATION_TYPE_ERROR = 'danger';
    const NOTIFICATION_TYPE_WARNING = 'warning';
    const NOTIFICATION_TYPE_INFO = 'info';
    
    // Notification icons class
    const NOTIFICATION_ICON_CLASS_SUCCESS = 'glyphicon glyphicon-ok-sign';
    const NOTIFICATION_ICON_CLASS_ERROR = 'glyphicon glyphicon-exclamation-sign';
    const NOTIFICATION_ICON_CLASS_WARNING = 'glyphicon glyphicon-exclamation-sign';
    const NOTIFICATION_ICON_CLASS_INFO = 'glyphicon glyphicon-info-sign';
    
    // Image Size
    const IMAGE_SIZE_XTRASMALL = "XS";
    const IMAGE_SIZE_SMALL = "SM";
    const IMAGE_SIZE_LARGE = "L";
    const IMAGE_SIZE_XTRALARGE = "XL";
}