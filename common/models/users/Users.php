<?php

namespace common\models\users;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\db\Expression;
use common\models\codes\Codes;
use common\models\mamaccounts\MamAccounts;
use common\models\relations\Relations;
use yii\base\Security;
use common\helpers\AuthManager;
use yii\helpers\ArrayHelper;
use Firebase\JWT\JWT;
use yii\rbac\Permission;
use yii\web\IdentityInterface;
use yii\web\Request as WebRequest;
use yii\behaviors\TimestampBehavior;
use common\models\Constants;
use common\models\country\Country;

class Users extends \yii\db\ActiveRecord implements IdentityInterface {

    public $file;
    protected static $decodedToken;
    public $access_token;
    Public $permissions;
    public $image;

    // public $file;


    public static function tableName() {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'username', 'email', 'phone', 'status', 'type', 'created_at'], 'required'],
            [['password'], 'required', 'on' => 'create'],
            [['description', 'type', 'status', 'email', 'country_code', 'image_location'], 'string'],
            [
                'password',
                'match',
                'pattern' => '/^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/',
                'message' => Yii::t('app', 'Your password is not complex'), 'on' => 'create'
            ],
            // [['updated_at'], 'date'],
            [['name', 'image'], 'string', 'max' => 50],
            [['country_code'], 'string', 'max' => 5],
            [['username', 'password'], 'string', 'max' => 200],
            [['auth_key', 'password_reset_token'], 'string', 'max' => 255],
            [['registration_ip', 'last_login_ip', 'phone'], 'string', 'max' => 20],
            [['email'], 'unique'],
            ['email', 'email']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'confirmation_key' => 'Confirmation Key',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'unconfirmed_email' => 'Unconfirmed Email',
            'password' => 'Password',
            'password_reset_token' => 'Password Reset Token',
            'confirmed_at' => 'Confirmed At',
            'phone' => 'Phone',
            'type' => 'Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'description' => 'Description',
            'registration_ip' => 'Registration Ip',
            'last_login_ip' => 'Last Login Ip',
            'last_login_at' => 'Last Login At',
            'access_token_expired_at' => 'Access Token Expired At',
        ];
    }

    public function getCountry() {
        return $this->hasOne(Country::className(), ['code' => 'country_code']);
    }

    protected static function getSecretKey() {
        return Yii::$app->params['jwtSecretCode'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMamAccounts() {
        return $this->hasMany(MamAccounts::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelations() {
        return $this->hasMany(Relations::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelations0() {
        return $this->hasMany(Relations::className(), ['ib_id' => 'id']);
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function getId() {
        return $this->getPrimaryKey();
    }

    public static function findByUsername($username) {
        return static::findOne(['username' => $username]);
    }

    public static function findByEmail($email, $users_type) {
        $user = static::find()
                ->where('email="' . $email . '"')
                ->andWhere(['status' => Constants::USER_STATUS_ACTIVE])
                ->andWhere(['type' => $users_type]);

        return $user->one();
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public function findIdentityWithCountry($id) {
        return static::find()->where('id=' . $id)->joinWith('country')->one();
    }

    public function validatePassword($password) {
        return $this->password === md5($password);
    }

    public function notConfirmed() {
        return $this->confirmed_at == null;
    }

    public function validateStatus() {
        return Codes::USER_VERIFIED === $this->status0->code;
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        $secret = static::getSecretKey();


        try {

            $decoded = JWT::decode($token, $secret, [static::getAlgo()]);
        } catch (\Exception $e) {

            return false;
        }
        static::$decodedToken = (array) $decoded;
        // If there's no jti param - exception
        if (!isset(static::$decodedToken['jti'])) {
            return false;
        }

        $id = static::$decodedToken['jti'];

        return static::findByJTI($id);
    }

    public function findImagePath($size) {
        return Yii::$app->params['ImageUserPath'] . "?id=" . $this->id . "&size=" . $size;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
                    'password_reset_token' => $token
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public static function getPass() {
        $security = new Security();
        $full = $security->generateRandomString(6);
        return substr($full, 0, 6);
    }

    public static function getUsers() {
        $users = self::find()->select([Users::tableName() . '.id as value', Users::tableName() . '.name as text']);
        if (AuthManager::is_ib()) {
            $clients = ArrayHelper::map(Relations::find()->where('ib_id = ' . Yii::$app->user->id)->asArray()->all(), 'client_id', 'ib_id');
            $users = $users->where('id = ' . Yii::$app->user->id)->orWhere(['id' => array_keys($clients)]);
        }
        $users = $users->asArray()->all();
        return $users;
    }

    public static function getClients() {
        $users = self::find()->select([Users::tableName() . '.id as value', Users::tableName() . '.name as text']);
        if (AuthManager::is_ib()) {
            $clients = ArrayHelper::map(Relations::find()->where('ib_id = ' . Yii::$app->user->id)->asArray()->all(), 'client_id', 'ib_id');
            $users = $users->where(['id' => array_keys($clients)]);
        }
        $users = $users->asArray()->all();
        return $users;
    }

    public static function getUserById($id) {
        $user = self::find()->select('*')->where(['id' => $id])->one();
        return $user;
    }

    public static function get_Admins_Users() {
        $query_client = new Query;
        $query_client = Users::find()
                ->select(Users::tableName() . '.id,' . Users::tableName() . '.name,' . Users::tableName() . '.type')
                ->join("INNER JOIN", Codes::tableName(), Codes::tableName() . '.id =' . Users::tableName() . '.type')
                ->where([Codes::tableName() . '.code' => Codes::USER_CLIENT]);
        $query_ib = new Query;
        $query_ib = Users::find()
                ->select(Users::tableName() . '.id,' . Users::tableName() . '.name,' . Users::tableName() . '.type')
                ->join("INNER JOIN", Codes::tableName(), Codes::tableName() . '.id =' . Users::tableName() . '.type')
                ->where([Codes::tableName() . '.code' => Codes::USER_IB]);

        $command_client = $query_client->createCommand();
        $command_ib = $query_ib->createCommand();
        $data_client = $command_client->queryAll();
        $data_ib = $command_ib->queryAll();
        $out['results_clients'] = array_values($data_client);
        $out['results_ib'] = array_values($data_ib);

        return $out;
    }

    public static function get_ib_details() {
        $user = new Users();
        $trader = new Traders();
        $ibs = (new \yii\db\Query())
                ->select([users::tableName() . '.name', 'phone', 'username', 'email', MamAccounts::tableName() . '.id', MamAccounts::tableName() . '.active_account', Users::tableName() . '.description', MamAccounts::tableName() . '.login', MamAccounts::tableName() . '.active', MamAccounts::tableName() . '.platform', MamAccounts::tableName() . '.brokers_id'])
                ->from('users')
                ->join("INNER JOIN", Codes::tableName(), Codes::tableName() . '.id =' . Users::tableName() . '.type')
                ->where([Codes::tableName() . '.code' => Codes::USER_IB])
                ->join("INNER JOIN", MamAccounts::tableName(), MamAccounts::tableName() . '.user_id =' . Users::tableName() . '.id')
                ->all();

        foreach ($ibs as $k => $v) {
            $ibs[$k]['id'] = $v['id'];
            $ibs[$k]['login'] = $v['login'];
            //$ibs[$k]['name'] = $trader->get_Master_Name($v['login'], $v['platform'], $v['brokers_id']);
            $ibs[$k]['name'] = $trader->get_Master_Name($v['id']);
            $ibs[$k]['profit'] = $trader->get_Investors_Profit($v['id']);
            $ibs[$k]['roi'] = $trader->get_ROI($v['id']);
            $ibs[$k]['is_active'] = $user->get_active($v['login']);
            $ibs[$k]['amount_following'] = $trader->get_amount_following($v['id']);
            $ibs[$k]['investors'] = $trader->get_Investors($v['id']);
            $ibs[$k]['chart'] = $trader->get_chart_values($v['login']);
            $ibs[$k]['width'] = $trader->get_rates($v['login']);
            $ibs[$k]['votes'] = $trader->get_rates($v['login']);
            $ibs[$k]['display_vote'] = $trader->display_rate($v['login']);
            $ibs[$k]['logged'] = $trader->is_logged();
            $ibs[$k]['canvote'] = 1;
            $ibs[$k]['platform'] = $v['platform'];
            $ibs[$k]['broker_name'] = Brokers::get_broker($v['brokers_id']);

            $x = $trader->user_status($v['login']);
            if (in_array($v['id'], array_column($x, 'master_account'))) {
                $ibs[$k]['status'] = 'unfollow';
            } else {
                $ibs[$k]['status'] = 'follow';
            }
        }


        return $ibs;
    }

    public static function get_active($login) {
        if ($login == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function get_profile_details($login) {
        $user = new Users();
        $brokers = new Brokers();
        $infos = (new \yii\db\Query())
                ->select([users::tableName() . '.id', users::tableName() . '.name', MamAccounts::tableName() . '.login', MamAccounts::tableName() . '.equity', MamAccounts::tableName() . '.brokers_id', MamAccounts::tableName() . '.balance', 'platform', 'currency'])
                ->from('users')
                ->join("INNER JOIN", MamAccounts::tableName(), MamAccounts::tableName() . '.user_id =' . Users::tableName() . '.id')
                ->where(['login' => $login])
                ->andWhere(['user_id' => \yii::$app->user->id])
                ->all();

        foreach ($infos as $k => $v) {

            $infos[$k]['equity'] = $v['equity'];
            $infos[$k]['balance'] = $v['balance'];
            $infos[$k]['currency'] = $v['currency'];
            $infos[$k]['accounts'] = $user->get_accounts($v['id']);
            $infos[$k]['platform'] = $v['platform'];
            $infos[$k]['broker'] = $brokers->get_broker($v['brokers_id']);
        }

        return $infos;
    }

    public function generateAccessTokenAfterUpdatingClientInfo($forceRegenerate = false) {
        // update client login, ip
        $this->last_login_ip = Yii::$app->request->getUserIP();
        $this->last_login_at = date("Y-m-d h:i:s");

        // check time is expired or not
        if ($forceRegenerate == true || $this->access_token_expired_at == null || (time() > $this->access_token_expired_at)) {
            // generate access token
            $this->generateAccessToken();
        }
        $this->save(false);
        return true;
    }

    public static function get_accounts($id) {
        $acc = (new \yii\db\Query())->select(['login'])->from('mam_accounts')->where(['user_id' => $id])->all();
        return $acc;
    }

    public static function get_ib_cli($id) {
        $query = new Query;
        $query->select(Users::tableName() . '.id,' . Users::tableName() . '.name AS text')
                ->from(Users::tableName())
                ->join("INNER JOIN", Codes::tableName(), Codes::tableName() . '.id =' . Users::tableName() . '.type')
                ->where([Codes::tableName() . '.code' => Codes::USER_CLIENT])->all();
        $query->andFilterWhere(['IN', Users::tableName() . '.id', (new Query())->select('client_id')->from('relations')->where(['ib_id' => $id])]);

        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);
        return $out['results'];
    }

    public static function get_ib_in_relations() {
        $query = new Query;
        $query->select(Users::tableName() . '.id,' . Users::tableName() . '.name AS text')
                ->from(Users::tableName())
                ->join("INNER JOIN", Codes::tableName(), Codes::tableName() . '.id =' . Users::tableName() . '.type')
                ->where([Codes::tableName() . '.code' => Codes::USER_IB])
                ->andFilterWhere(['IN', Users::tableName() . '.id', (new Query())->select('ib_id')->from('relations')]);

        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);
        return $out['results'];
    }

    public static function get_client_by_ib($id) {
        $users = self::find()->select([Users::tableName() . '.id as value', Users::tableName() . '.name as text']);

        $clients = ArrayHelper::map(Relations::find()->where(['ib_id' => $id])->asArray()->all(), 'client_id', 'ib_id');
        $users = $users->where(['id' => array_keys($clients)]);

        $users = $users->asArray()->all();
        return $users;
    }

    public static function get_trader_details() {
        $user = new Users();
        $trader = new Traders();
        $ibs = (new \yii\db\Query())
                ->select([users::tableName() . '.id', users::tableName() . '.name', 'phone', 'username', 'email', MamAccounts::tableName() . '.active_account', Users::tableName() . '.description', MamAccounts::tableName() . '.login', MamAccounts::tableName() . '.active'])
                ->from('users')
                ->join("INNER JOIN", Codes::tableName(), Codes::tableName() . '.id =' . Users::tableName() . '.type')
                ->where([Codes::tableName() . '.code' => Codes::USER_IB])
                ->join("INNER JOIN", MamAccounts::tableName(), MamAccounts::tableName() . '.user_id =' . Users::tableName() . '.id')
                ->all();
        foreach ($ibs as $k => $v) {
            $ibs[$k]['name'] = $trader->get_Master_Name($v['login'], $v['name']);
            $ibs[$k]['profit'] = $trader->get_Investors_Profit($v['login']);
            $ibs[$k]['roi'] = $trader->get_ROI($v['login']);
            $ibs[$k]['is_active'] = $user->get_active($v['login']);
            $ibs[$k]['equity'] = $trader->get_equity($v['login']);
            $ibs[$k]['investors'] = $trader->get_Investors($v['id']);
            $ibs[$k]['chart'] = $trader->get_chart_values($v['login']);
            $ibs[$k]['width'] = $trader->get_rates($v['id']);
            $ibs[$k]['votes'] = $trader->get_rates($v['id']);
            $ibs[$k]['display_vote'] = $trader->display_rate($v['id']);
            $ibs[$k]['logged'] = $trader->is_logged();
            $ibs[$k]['canvote'] = 1;
            $ibs[$k]['status'] = $trader->user_status($v['login']);
        }
        return $ibs;
    }

    public static function get_followed_trader_details($active_login) {
        $user = new Users();
        $trader = new Traders();
        $current_date = strtotime(date('Y-m-d H:i:s'));
        $ibs = (new \yii\db\Query())
                ->select([Relations::tableName() . '.id', Relations::tableName() . '.master_account'])
                ->from('relations')
                ->where([Relations::tableName() . '.slave_account' => $active_login])
                ->andWhere(['unfollow_date' => null])
                ->all();

        foreach ($ibs as $k => $v) {

            $ibs[$k]['login'] = $v['master_account'];
            $ibs[$k]['name'] = $trader->get_Master_Name($v['master_account']);
            $ibs[$k]['profit'] = $trader->get_Investors_Profit($v['master_account']);
            $ibs[$k]['roi'] = $trader->get_ROI($v['master_account']);
            $ibs[$k]['is_active'] = $user->get_active($v['master_account']);
            $ibs[$k]['equity'] = $trader->get_equity($v['master_account']);
            $ibs[$k]['investors'] = $trader->get_Investors($v['id']);
        }
        return $ibs;
    }

    public function generateAccessToken() {
        // generate access token
//        $this->access_token = Yii::$app->security->generateRandomString();
        $tokens = $this->getJWT();
        $this->access_token = $tokens[0];   // Token
        $this->access_token_expired_at = $tokens[1]['exp']; // Expire
    }

    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public static function getAlgo() {
        return 'HS256';
    }

    public static function findByJTI($id) {
        /** @var User $user */
        $user = static::find()->where(['=', 'id', $id])
                        ->andWhere(['>', 'access_token_expired_at',
                            date('Y-m-d H:i:s')])->one();
        // print_r($user);die;
        return $user;
    }

    /**
     * Confirm Email address
     *      Not implemented Yet
     *
     * @return bool whether the email is confirmed o not
     */
    public function confirmEmail() {
        if ($this->unconfirmed_email != '') {
            $this->email = $this->unconfirmed_email;
        }
        $this->registration_ip = Yii::$app->request->userIP;
        $this->save(false);
        $this->touch('confirmed_at');

        return true;
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {

        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    public function setPassword($password) {
        $this->password = md5($password);
    }

    /**
     * Validate email
     *
     * @param $attribute
     * @param $params
     */
    public function validateEmail($attribute, $params) {
        // get post type - POST or PUT
        $request = Yii::$app->request;

        // if POST, mode is create
        if ($request->isPost) {
            // check username is already taken

            $existingUser = Users::find()
                    ->where(['email' => $this->$attribute])
                    ->count();

            if ($existingUser > 0) {
                $this->addError($attribute, Yii::t('app', 'The email has already been taken.'));
            }
        } elseif ($request->isPut) {
            // get current user
            $user = Users::findIdentityWithoutValidation($this->id);

            if ($user == null) {
                $this->addError($attribute, Yii::t('app', 'The system cannot find requested user.'));
            } else {
                // check username is already taken except own username
                $existingUser = Users::find()
                        ->where(['=', 'email', $this->$attribute])
                        ->andWhere(['!=', 'id', $this->id])
                        ->count();
                if ($existingUser > 0) {
                    $this->addError($attribute, Yii::t('app', 'The email has already been taken.'));
                }
            }
        } else {
            // unknown request
            $this->addError($attribute, Yii::t('app', 'Unknown request'));
        }
    }

    public static function findIdentityWithoutValidation($id) {
        $user = static::findOne(['id' => $id]);

        return $user;
    }

    public function validateUsername($attribute, $params) {
        // get post type - POST or PUT
        $request = Yii::$app->request;

        // if POST, mode is create
        if ($request->isPost) {
            // check username is already taken

            $existingUser = Users::find()
                    ->where(['username' => $this->$attribute])
                    ->count();
            if ($existingUser > 0) {
                $this->addError($attribute, Yii::t('app', 'The username has already been taken.'));
            }
        } elseif ($request->isPut) {
            // get current user
            $user = Users::findIdentityWithoutValidation($this->id);
            if ($user == null) {
                $this->addError($attribute, Yii::t('app', 'The system cannot find requested user.'));
            } else {
                // check username is already taken except own username
                $existingUser = Users::find()
                        ->where(['=', 'username', $this->$attribute])
                        ->andWhere(['!=', 'id', $this->id])
                        ->count();
                if ($existingUser > 0) {
                    $this->addError($attribute, Yii::t('app', 'The username has already been taken.'));
                }
            }
        } else {
            // unknown request
            $this->addError($attribute, Yii::t('app', 'Unknown request'));
        }
    }

    /**
     * Validate permissions array
     *
     * @param $attribute
     * @param $params
     */
    public function validatePermissions($attribute, $params) {
        if (!empty($this->$attribute)) {
            $authManager = Yii::$app->authManager;
            // Get existing permissions
            $existingPermissions = $authManager->getPermissions();

            // Loop attributes
            foreach ($this->$attribute as $permissionKey => $permission) {
                // Validate attributes in the array
                if (array_key_exists('name', $permission) === false ||
                        array_key_exists('description', $permission) === false ||
                        array_key_exists('checked', $permission) === false) {
                    $this->addError($attribute, Yii::t('app', 'The permission is not valid format.'));
                } elseif (isset($existingPermissions[$permission['name']]) == false) {
                    // Validate name
                    $this->addError(
                            $attribute, Yii::t(
                                    'app', 'The permission name \'' . $permission['name'] . '\' is not valid.'
                            )
                    );
                } elseif (is_bool($permission['checked']) === false) {
                    // Validate checked
                    $this->addError($attribute, Yii::t('app', 'The permission checked \'' . $permission['checked'] . '\' is not valid.'
                            )
                    );
                }
            }
        }
    }

    /**
     * Validate whether password is submitted or not
     *  Only required to submit the password on creation
     *
     * @param $attribute
     * @param $params
     */
    public function validatePasswordSubmit($attribute, $params) {
        // get post type - POST or PUT
        $request = Yii::$app->request;

        // if POST, mode is create
        if ($request->isPost) {
            if ($this->$attribute == '') {
                $this->addError($attribute, Yii::t('app', 'The password is required.'));
            }
        } elseif ($request->isPut) {
            // No action required
        }
    }

    /**
     * Encodes model data to create custom JWT with model.id set in it
     * @return array encoded JWT
     */
    public function getJWT() {
        // Collect all the data
        $secret = static::getSecretKey();
        $currentTime = time();
        $expire = $currentTime + Yii::$app->params['token_age'];
        $request = Yii::$app->request;
        $hostInfo = '';
        // There is also a \yii\console\Request that doesn't have this property
        if ($request instanceof WebRequest) {
            $hostInfo = $request->hostInfo;
        }

        // Merge token with presets not to miss any params in custom
        // configuration
        $token = array_merge([
            'iat' => $currentTime,
            // Issued at: timestamp of token issuing.
            'iss' => $hostInfo,
            // Issuer: A string containing the name or identifier of the issuer application. Can be a domain name and can be used to discard tokens from other applications.
            'aud' => $hostInfo,
            'nbf' => $currentTime,
            // Not Before: Timestamp of when the token should start being considered valid. Should be equal to or greater than iat. In this case, the token will begin to be valid 10 seconds
            'exp' => $expire,
            // Expire: Timestamp of when the token should cease to be valid. Should be greater than iat and nbf. In this case, the token will expire 60 seconds after being issued.
            'data' => [
                'username' => $this->username,
                'lastLoginAt' => $this->last_login_at,
            ]
                ], static::getHeaderToken());
        // Set up id
        $token['jti'] = $this->getJTI();    // JSON Token ID: A unique string, could be used to validate a token, but goes against not having a centralized issuer authority.
        return [JWT::encode($token, $secret, static::getAlgo()), $token];
    }

    protected static function getHeaderToken() {
        return [];
    }

    /**
     * @return bool Whether the user is confirmed or not.
     */
    public function getIsConfirmed() {
        return $this->confirmed_at != null;
    }

    /**
     * Returns some 'id' to encode to token. By default is current model id.
     * If you override this method, be sure that findByJTI is updated too
     * @return integer any unique integer identifier of user
     */
    public function getJTI() {
        return $this->getId();
    }

    public function getTraderInfo() {
        
    }

    public function setConfirmationKey() {
        $hash_username = md5($this->username);

        $hash_authkey = md5($this->auth_key);

        $confirmation_key = $hash_username . $hash_authkey;

        $this->confirmation_key = $confirmation_key;
    }

}
