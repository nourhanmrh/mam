<?php

namespace common\models\users;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\users\Users;
use app\helpers\AuthManager;

/**
 * UsersSearch represents the model behind the search form of `app\models\Users`.
 */
class UsersSearch extends Users {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'phone'], 'integer'],
            [['name', 'username', 'email', 'auth_key', 'password_reset_token', 'confirmation_key', 'last_login_ip', 'last_login_at', 'password', 'created_at', 'updated_at', 'confirmed_at','type', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Users::find();

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
            return $dataProvider;
        }

// grid filtering conditions

        try {
            $query->andFilterWhere([
                'id' => $this->id,
                'phone' => $this->phone,
                'type' => $this->type,
                'status' => $this->status,
                'last_login_ip' => $this->last_login_ip,
                'password_reset_token' => $this->password_reset_token,
            ]);

            $query->andFilterWhere(['like', 'name', $this->name])
                    ->andFilterWhere(['like', 'username', $this->username])
                    ->andFilterWhere(['like', 'email', $this->email])
                    ->andFilterWhere(['like', 'auth_key', $this->auth_key])
                    ->andFilterWhere(['like', 'last_login_ip', $this->last_login_ip])
                    ->andFilterWhere(['like', 'confirmation_key', $this->confirmation_key])
                    ->andFilterWhere(['like', 'confirmed_at', $this->confirmed_at])
                    ->andFilterWhere(['like', Users::tableName() . '.password', $this->password]);

            if ($this->created_at) {
                $created_at = $this->created_at;
                $first = date_create($created_at . " 00:00:00");
                $last = date_create($created_at . " 23:59:59");
                $from = date_timestamp_get($first);
                $to = date_timestamp_get($last);
                $query->andFilterWhere(['>', Users::tableName() . ".created_at", $from])
                        ->andFilterWhere(['<', Users::tableName() . ".created_at", $to]);
            }

            if ($this->updated_at) {
                $updated_at = $this->updated_at;
                $first = date_create($updated_at . " 00:00:00");
                $last = date_create($updated_at . " 23:59:59");
                $from = date_timestamp_get($first);
                $to = date_timestamp_get($last);
                $query->andFilterWhere(['>', Users::tableName() . ".updated_at", $from])
                        ->andFilterWhere(['<', Users::tableName() . ".updated_at", $to]);
            }

            if ($this->last_login_at) {
                $last_login_at = $this->last_login_at;
                $first = date_create($last_login_at . "00:00:00");
                $last = date_create($last_login_at . "23:59:59");
                $from = date_timestamp_get($first);
                $to = date_timestamp_get($last);
                $query->andFilterWhere(['>', Users::tableName() . ".last_login_at", $from])
                        ->andFilterWhere(['<', Users::tableName() . ".last_login_at", $to]);
            }

        }// db related exceptions
        catch (\yii\db\Exception $exception) {
            Yii::error($exception->getMessage());
            throw new \yii\web\HttpException(405, 'Error saving model');

            // any exception throwin by yii
        } catch (\yii\base\Exception $exception) {
            Yii::error($exception->getMessage());
            throw new \yii\web\HttpException(404, 'Error saving model' . $exception->getMessage());

            // any php exception
        } catch (\Exception $exception) {
            Yii::error($exception->getMessage());
            throw new \yii\web\HttpException(405, 'Error saving model');
        }

        $query->orderBy(['id' => SORT_DESC])->all();
        return $dataProvider;
    }

}
