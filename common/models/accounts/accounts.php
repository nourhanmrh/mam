<?php

namespace common\models\accounts;

use Yii;
use yii\base\Model;

class Accounts extends Model {

    public $id;
    public $login;
    public $groups;
    public $parent;

    public static function tableName() {
        return 'accounts';
    }

    public function rules() {
        return [
            [['id', 'login'], 'required'],
            [['id'], 'integer'],
            [['$groups'], 'string', 'max' => 50]
        ];
    }

}
