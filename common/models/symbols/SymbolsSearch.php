<?php

namespace common\models\symbols;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\symbols\Symbols;

/**
 * BrokersSearch represents the model behind the search form of `app\models\Brokers`.
 */
class SymbolsSearch extends Symbols {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id'], 'integer'],
            [['symbol_name', 'symbol_value', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Symbols::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'symbol_name', $this->symbol_name])
              ->andFilterWhere(['like', 'symbol_value', $this->symbol_value]);


        return $dataProvider;
    }

}
