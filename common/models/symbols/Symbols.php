<?php

namespace common\models\symbols;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "symbols".
 *
 * @property int $id
 * @property string $symbol_name
 * @property string $symbol_value
 * @property int $status
 */
class Symbols extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'symbols';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['symbol_name', 'symbol_value', 'status'], 'required'],
            [['id', 'status'], 'integer'],
            [['symbol_name', 'symbol_value'], 'string', 'max' => 10],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'symbol_name' => 'Symbol Name',
            'symbol_value' => 'Symbol Value',
            'status' => 'Status',
        ];
    }

        public function symbolsForMobile(){
        return ArrayHelper::map(static::find()->all(), 'symbol_name' , 'symbol_value');
    }
}
