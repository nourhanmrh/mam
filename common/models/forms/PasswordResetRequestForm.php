<?php

namespace common\models\forms;

use Yii;
use yii\base\Model;
use common\models\users\Users;
use yii\helpers\Url;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model {

    public $email;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'exist',
                'targetClass' => '\common\models\users\Users',
                'message' => Yii::t('app', 'There is no user with this email address.')
            ],
        ];
    }

    public function sendPasswordResetEmail() { 
        /* @var $user User */
        $user = Users::findOne([
                    'email' => $this->email,
        ]);
      
        if (!$user) {
            return false;
        }
       
        if (!Users::isPasswordResetTokenValid($user['password_reset_token'])) {   
            $user->generatePasswordResetToken();       
            if ($user->save(false)) {
        
            }
        }else{                 
            $conf=new ConfirmationForm();
            $conf->sendResetPwdEmail($user);
          return true;
        }

        
    }

}
