<?php

namespace common\models\forms;

use Yii;
use yii\base\Model;
use common\models\users\Users;
use yii\helpers\Url;
use common\models\Constants;

/**
 * Confirmation form
 */
class ConfirmationForm extends Model {

    public $email;

    public function rules() {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    public function sendRegistrationEmail($user) {

        $confirmation_key = $user->confirmation_key;

        $confirmURL = \Yii::$app->params['confirmURL'] . $confirmation_key;

        $template_main = Yii::$app->get('templateManager')->getTemplate('main-page');
        if ($user->type == Constants::USER_TYPE_MONEY_MANAGER) {
            $template = Yii::$app->get('templateManager')->getTemplate('verify-trader');
        } else if ($user->type == Constants::USER_TYPE_CLIENT) {
            $template = Yii::$app->get('templateManager')->getTemplate('verify');
        }

        $template->parseBody([
            'name' => $user['name'],
            'confirm-link' => $confirmURL,
            'phone' => '+35725211707 ',
            'email' => 'support@fxgrow.com',
        ]);
        $template_main->parseBody(['body' => $template->body]);
        return Yii::$app->get('mailer')
                        ->compose()
                        ->setFrom('noreply@fxgrow.com')
                        ->setTo($user['email'])
                        ->setSubject($template->subject)
                        ->setHtmlBody($template_main->body)
                        ->send();
    }

    public function sendPasswordChanged($user) {

        $template_main = Yii::$app->get('templateManager')->getTemplate('main-page');

        $template = Yii::$app->get('templateManager')->getTemplate('password-changed');

        $template->parseBody([
            'name' => $user['name'],
        ]);

        $template_main->parseBody([
            'body' => $template->body
        ]);


        $email = Yii::$app->get('mailer')
                ->compose()
                ->setFrom('info@fxgrow.com')
                ->setTo($user->email)
                ->setSubject($template->subject)
                ->setHtmlBody($template_main->body)
                ->send();
    }

    public function sendResetPwdEmail($user) {       
        $resetURL = \Yii::$app->params['PasswordResetURLWeb'] . "?token=" . $user->password_reset_token;

        $template_main = Yii::$app->get('templateManager')->getTemplate('main-page');

        $template = Yii::$app->get('templateManager')->getTemplate('reset-password');

        $template->parseBody([
            'name' => $user['name'],
            'reset-link' => $resetURL,
        ]);

        $template_main->parseBody([
            'body' => $template->body
        ]);


        $email = Yii::$app->get('mailer')
                ->compose()
                ->setFrom('info@fxgrow.com')
                ->setTo($user->email)
                ->setSubject($template->subject)
                ->setHtmlBody($template_main->body)
                ->send();
    }

    public function sendAccountCreation($account, $id) {

        $u = new Users();
        $user = $u->getUserById($id);
        $template_main = Yii::$app->get('templateManager')->getTemplate('main-page');
        if ($account['is_live'] == 0) {
            $template = Yii::$app->get('templateManager')->getTemplate('create-demo');
        } else if ($account['is_live'] == 1) {
            if ($account['account_status'] == Constants::ACCOUNT_STATUS_NOT_COMPETED) {
                $template = Yii::$app->get('templateManager')->getTemplate('create-live');
            } else if ($account['account_status'] == Constants::ACCOUNT_STATUS_WAITING_CONFIRMATION) {
                $template = Yii::$app->get('templateManager')->getTemplate('waiting-confirmation');
            } else if ($account['account_status'] == Constants::ACCOUNT_STATUS_COMPLETED) {
                $template = Yii::$app->get('templateManager')->getTemplate('account-confirmed');
            }
        }
        $template->parseBody([
            'name' => $user['name'],
            'account_name' => $account['name'],
            'login' => $account['login']
        ]);

        $template_main->parseBody([
            'body' => $template->body
        ]);


        $email = Yii::$app->get('mailer')
                ->compose()
                ->setFrom('info@fxgrow.com')
                ->setTo($user->email)
                ->setSubject($template->subject)
                ->setHtmlBody($template_main->body)
                ->send();
    }

}
