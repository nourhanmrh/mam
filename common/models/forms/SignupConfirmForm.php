<?php

namespace common\models\forms;

use yii\base\Model;

class SignupConfirmForm extends Model
{
    public $confirmation_key;

    private $_user = false;

    public function confirm()
    {
        if ($this->validate()) {
            $this->getUserByID();

            // fill confirmed_at
            $this->_user->confirmEmail();

            // generate access token
            $this->_user->generateAccessTokenAfterUpdatingClientInfo(true);

            // Send confirmation email
            $this->sendSignupSuccessEmail();

            return true;
        }
        return false;
    }

    public function getUserByID()
    {
        if ($this->_user === false) {
            $this->_user = Users::findOne($this->id);
        }

        return $this->_user;
    }

    public function sendSignupSuccessEmail()
    {
       // $loginURL = \Yii::$app->params['servicesURL'] . '#/login';

        $template_initial = Yii::$app->get('templateManager')->getTemplate('initial');
        

         $template = Yii::$app->get('templateManager')->getTemplate('signupConfirm');
                  
                    $template->parseSubject([
                        'subject' => $this->name,
                    ]);
                    $template->parseBody([
                        'name' => $this->name,
                        'username' => $this->username,
                        'password' => $this
                    ]);

                    $template_initial->parseBody([
                        'body' => $template->body
                        ]);
                    $template_initial->parseSubject([
                        'subject' => $template->subject,
                    ]);


                    Yii::$app->get('mailer')
                            ->compose()
                            ->setFrom('no-reply@fxgrow.com')
                            ->setTo($this->email)
                            ->setSubject($template_initial->subject)
                            ->setHtmlBody($template_initial->body)
                            ->send();
                    

        return $email;
    }


    /**
     * Return User object
     *
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }
}
