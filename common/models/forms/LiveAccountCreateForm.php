<?php

namespace common\models\forms;

use Yii;
use yii\base\Model;

class LiveAccountCreateForm extends Model {

    public $name;
    public $login;
    public $password;
    public $platform;
    public $broker;

    public function rules() {
        return [
            [['password', 'name', 'login','platform'], 'required'],
            [['password', 'name','platform','broker'], 'string'],
            [['login'], 'number']
        ];
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('app', "Name"),
            'login' => Yii::t('app', "Login"),
            'password' => Yii::t('app', "Investor Password"),
            'platform' => Yii::t('app', "Platform")
        ];
    }

}
