<?php

namespace common\models\forms;

use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\Cookie;
use common\models\users\Users;

class TraderRegistrationForm extends Model {

    public $name;
    public $username;
    public $email;
    public $phone;
    public $password;
    public $website;
    public $country_code;
    public $status;
    public $country;
    private $_user = false;

    public function rules() {
        return [
            ['name', 'required'],
            ['username', 'trim'],
            [
                'username',
                'unique',
                'targetClass' => Users::className(),
                'message' => Yii::t('app', 'This username has already been taken.')
            ],
            ['username', 'required'],
            ['password', 'trim'],
            ['username', 'string', 'length' => [3, 25]],
            [
                'password',
                'match',
                'pattern' => '/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
                'message' => Yii::t(
                        'app', 'Your password is not complex'
                )
            ],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => Users::className(),
                'message' => Yii::t('app', 'This email address has already been taken.')
            ],
            ['email', 'string', 'max' => 255],
            ['phone', 'required'],
            ['phone', 'integer'],
            ['password', 'required']
        ];
    }

    public function Tradersignup() {
        if ($this->validate()) {
            $user = new Users();
            $user->name = $this->name;
            $user->generateAuthKey();
            $user->setConfirmationkey();
            $user->username = strtolower($this->username);
            $user->email = $this->email;
            $user->phone = $this->phone;
            $user->setPassword($this->password);
            $user->type = 'money manager';
            $user->status = 'active';
            $user->created_at = date("Y-m-d h:i:s");
            $user->registration_ip = Yii::$app->request->userIP;
            if ($user->save(false)) {
                $this->_user = $user;
                return true;
            }
            return false;
        } else {
            print_r($this->error);
        }

        return false;
    }

    

    public function getUser() {
        return $this->_user;
    }

}
