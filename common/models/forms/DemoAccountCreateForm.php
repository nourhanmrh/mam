<?php

namespace common\models\forms;

use Yii;
use yii\base\Model;

class DemoAccountCreateForm extends Model {

    public $name;
    public $currency;
    public $balance;
    public $leverage;

    public function rules() {
        return [
            [['balance', 'leverage', 'currency'], 'required'],
            [['name'], 'string']
        ];
    }

    public function attributeLabels() {
        return [
            'balance' => Yii::t('app',"Balance"),
            'leverage' => Yii::t('app',"Leverage"),
            'currency' => Yii::t('app',"Base Currency"),
            'name' => Yii::t('app',"Name")
        ];
    }

}
