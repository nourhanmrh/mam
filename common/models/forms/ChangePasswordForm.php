<?php

namespace common\models\forms;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;
use common\models\users\Users;
use common\models\forms\ConfirmationForm;
class ChangePasswordForm extends Model {

    public $id;
    public $old_password;
    public $password;
    public $confirm_password;
    private $_user;

    function rules() {
        return [
            [['old_password', 'password', 'confirm_password'], 'required'],
            [
                'password',
                'match',
                'pattern' => '/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])[0-9A-Za-z@_#$%-\/]{8,}$/',
                'message' => Yii::t(
                        'app', 'Your password is not complex'
                )
            ],
            ['old_password', 'confirm_old_password'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function __construct($id, $config = []) {
        $this->_user = Users::findIdentity($id);
        if (!$this->_user) {
            throw new InvalidParamException('Unable to find user!');
        }
        $this->id = $this->_user->id;
        parent::__construct($config);
    }

    public function confirm_old_password($attribute, $params) {
        $password_md5 = md5($this->old_password);
        if ($this->_user->password != $password_md5)
            $this->addError($attribute, 'Old password is incorrect.');
    }

    function changePassword() {
        $user = $this->_user;
        $this->_user->password = md5($this->password);
        $this->_user->updated_at = date('Y-m-d H:i:s');
        if ($this->_user->save(false)){
              $conf=new ConfirmationForm();
            $conf->sendPasswordChanged($user);  
          return true;
        }
            
    }

}

?>