<?php

namespace common\models\drawdown;

use Yii;
use common\models\drawdown\Drawdown;
use common\models\tradersinfo\TradersInfo;

/**
 * This is the model class for table "drawdown".
 *
 * @property int $id
 * @property int $trader_id
 * @property double $drawdown
 * @property string $time
 */
class Drawdown extends \yii\db\ActiveRecord
{
    public $trader_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'drawdown';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trader_id', 'drawdown'], 'required'],
            [['trader_id'], 'integer'],
            [['drawdown'], 'number'],
            [['time','drawdown'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trader_id' => 'Trader ID',
            'drawdown' => 'Drawdown',
            'time' => 'Time',
        ];
    }

        public function getTradersinfo() {
            return $this->hasOne(TradersInfo::className(), ['id' => 'trader_id']);
    }
}
