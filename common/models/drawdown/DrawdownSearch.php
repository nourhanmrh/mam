<?php

namespace common\models\drawdown;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\helpers\AuthManager;
use common\models\drawdown\Drawdown;
use common\models\tradersinfo\TradersInfo;

/**
 * BrokersSearch represents the model behind the search form of `app\models\Brokers`.
 */
class DrawdownSearch extends Drawdown {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id'], 'integer'],
            [['trader_id', 'drawdown', 'time', 'trader_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Drawdown::find()
                ->joinWith("tradersinfo tradersinfo")
                ->orderBy('trader_name');

        // add conditions that should always apply here
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'drawdown' => $this->drawdown
        ]);


        $query->andFilterWhere(['like', 'time', $this->time])
                ->andFilterWhere(['like', 'tradersinfo.trader_name', $this->trader_name]);
       
    
        return $dataProvider;
    }

}
