<?php

namespace common\models\currency;

use Yii;


class Currency extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return 'currency';
    }

    
    public function rules()
    {
        return [
            [['code', 'status'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['code'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'status' => 'Status',
        ];
    }
}
