<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\models\ReportServer;

use yii;
use common\models\ReportServer\ReportServerInterface;
use common\models\ReportServer\Mt4Reportserver;
use common\models\ReportServer\Mt5Reportserver;

/**
 * Description of ReportServer
 *
 * @author user
 */
class ReportServer {

    const MT4_PLATFORM = "mt4";
    const MT5_PLATFORM = "mt5";

    private $platform;

    function __construct($platform) {
        $this->platform = $platform;
    }

    function getClass() {
        if ($this->platform == self::MT4_PLATFORM) {
            return Mt4Reportserver::className();
        } else if ($this->platform == self::MT5_PLATFORM) {
            return Mt5Reportserver::className();
        }
    }

}
