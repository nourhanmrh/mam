<?php

namespace common\models\ReportServer;

interface ReportServerInterface {

    public function getData($dateone, $datetwo);

    public function getChartTenDays();

    public function getKendoTrades($post);

    public function getChartCustom($time);

    public function calculatePips($result);

    public function calculateMonthProfit($trader_name);

    public function calculateTotalProfit($trader_name);

    public function filterbydate($result);

    public function getTrades($request);

    public function getSymbolsData($time);

    public function getTradesForActive($login);

    public function getSymbols();
    
    public function getAccountTrades($login);
    
    public function openTrade($request);
    
    public function modifyTrade($request);
    
    public function closeTrade($request);

    //function tradesMap();
}
