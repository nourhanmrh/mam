<?php

namespace common\models\ReportServer;

use Yii;
use yii\base\Model;
use yii\db\Expression;
use common\models\ReportServer\ReportServerInterface;
use common\services\TradeObject;
use common\services\TradeAction;
use common\services\Response;
use Socket\Raw;

class Mt5Reportserver extends Model implements ReportServerInterface {

    const POSITIONS_TABLE_NAME = "mt5_positions";
    const DEALS_TABLE_NAME = "mt5_deals";

    private $db;
    private $db_object;
    public $result;
    private $login;
    private $platform;
    private $broker;
    public static $map = [
        'Ticket' => 'Deal',
        //'Login' => 'LOGIN',
        'Symbol' => 'Symbol',
        'Digits' => 'Digits',
        'Type' => 'Action',
        'Volume' => 'Volume',
        'OpenTime' => 'Time',
        'OpenPrice' => 'Price',
        'CloseTime' => 'Time',
        'ClosePrice' => 'Price',
        'Profit' => 'Profit',
        'Pips' => 'Pip'
    ];
    private $deal_table_columns = array(
        'Deal',
        'Login',
        'Symbol',
        'Action',
        'Entry',
        'PositionID', 'Time', 'Price',
        'Digits', 'Profit', 'Volume', 'ApiData', 'PriceSL', 'PriceTP', 'Comment');
    private $deals_table_columns = array('Deal', 'Timestamp', 'ExternalID', 'Login', 'Dealer', '`Order`', 'Action', 'Entry', 'Reason', 'Digits', 'DigitsCurrency', 'ContractSize', 'Time', 'TimeMsc', 'Symbol', 'Price', 'VolumeExt', 'Profit', 'Storage', 'Commission', 'RateProfit', 'RateMargin', 'ExpertID', 'PositionID', 'Comment', 'ProfitRaw', 'PricePosition', 'PriceSL', 'PriceTP', 'VolumeClosedExt', 'TickValue', 'TickSize', 'Flags', 'Gateway', 'PriceGateway', 'ModifyFlags', 'Volume', 'VolumeClosed', 'ApiData');
    private $positions_table_columns = array(
        'Position',
        'Login',
        'Symbol',
        'Action',
        'TimeCreate',
        'PriceOpen',
        'Digits', 'Profit ', 'Volume',
        'PriceCurrent', 'PriceSL', 'PriceTP', 'Comment');

    public function __construct($dbname, $login) {
        try {
            $this->db = $dbname;
            $this->login = $login;
            $this->db_object = Yii::$app->{$this->db};
            $this->result = new \DataSourceResult($this->db_object->dsn . ";charset=" . $this->db_object->charset, $this->db_object->username, $this->db_object->password);
        } catch (Exception $e) {
            echo $e->getMessage();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function calculateMonthProfit($trader_name) {
        $start_date = $this->filterbydate('oneM');
        $end_date = date('Y-m-d H:i:s');
        $data = \Yii::$app->{$this->db}->createCommand("SELECT sum(profit) as sum FROM " . self::DEALS_TABLE_NAME . " Where (action < 2) AND (LOGIN = $this->login)AND (Entry = 1) AND (TIME BETWEEN '$start_date' AND '$end_date') ")->queryOne();
        return $data;
    }

    public function calculateTotalProfit($trader_name = null, $login = null) {
        $end_date = date('Y-m-d H:i:s');
        $login = ($login == null) ? ($this->login) : $login;
        $data = \Yii::$app->{$this->db}->createCommand("SELECT sum(profit) as sum FROM " . self::DEALS_TABLE_NAME . " Where (action < 2) AND (LOGIN = $login)AND (Entry = 1) AND (TIME < '$end_date') ")->queryOne();
        return $data;
    }

    public function calculateTotalAverage() {
        $end_date = date('Y-m-d H:i:s');
        $data = \Yii::$app->{$this->db}->createCommand("SELECT avg(profit) as avg FROM " . self::DEALS_TABLE_NAME . " Where (action < 2) AND (LOGIN = $this->login)AND (Entry = 1) AND (TIME < '$end_date') ")->queryOne();
        return $data;
    }

    public function get_yesterday_profit($from, $to) {
        $sql_yesterday_profit = \Yii::$app->{$this->db}->createCommand("SELECT sum(profit) as profit FROM " . self::DEALS_TABLE_NAME . " where login=" . $this->login . " and (action<2) AND (Entry = 1) AND (TIME BETWEEN '$from' AND '$to')")->queryAll();
        return $sql_yesterday_profit;
    }

    public function get_threemonths_profit($from, $to) {
        $sql_threemonths_profit = \Yii::$app->{$this->db}->createCommand("SELECT sum(profit) as profit FROM " . self::DEALS_TABLE_NAME . " where login=" . $this->login . " and (action<2) and  (Entry = 1) AND (TIME BETWEEN '$from' AND '$to') ")->queryAll();
        return $sql_threemonths_profit;
    }

    public function getMinTime($login = null) {
        $login = ($login == null) ? ($this->login) : $login;
        $data = \Yii::$app->{$this->db}->createCommand("SELECT min(Time) as time FROM " . self::DEALS_TABLE_NAME . " Where LOGIN = " . $this->login . " and (action<2) and  (Entry = 1) ")->queryAll();
        return $data;
    }

    public function getTradesNumber() {
        $data = \Yii::$app->{$this->db}->createCommand("SELECT count(*) as count FROM " . self::POSITIONS_TABLE_NAME . " Where LOGIN = " . $this->login)->queryAll();
        //  print_r($data);die; 
        return $data;
    }

    public function getPositionsTrades() {
        $data = \Yii::$app->{$this->db}->createCommand("SELECT count(*) as count FROM " . self::POSITIONS_TABLE_NAME . " Where LOGIN = " . $this->login . " and (action<2) ")->queryOne();
        return $data['count'];
    }

    public function getOverallTrades($request) {
        $res = $this->result->read(self::DEALS_TABLE_NAME, $this->deal_table_columns, $request);

        $res['data'] = $this->tradesMap($res['data']);
        $data = json_encode($res);
        return $res;
    }

    public function get_user_equity() {

        $equity = \Yii::$app->{$this->db}->createCommand("SELECT Equity as equity FROM mt5_accounts where login=" . $this->login)->queryOne();

        return $equity['equity'];
    }

    public function get_user_currency() {
        $currency = \Yii::$app->{$this->db}->createCommand("SELECT Currency as currency FROM mt5_users inner join mt5_groups on mt5_groups.Group=mt5_users.Group  where mt5_users.login=" . $this->login)->queryOne();
        return $currency['currency'];
    }

    public function get_user_balance() {

        $balance = \Yii::$app->{$this->db}->createCommand("SELECT Balance as balance FROM mt5_accounts where login=" . $this->login)->queryOne();
        return $balance['balance'];
    }

    public function get_user_marginpercentage($login) {
        if ($login) {
            $perc = \Yii::$app->{$this->db}->createCommand("SELECT MarginFree FROM mt5_accounts where login=" . $login)->queryOne();
            return $perc['MarginFree'];
        }
    }

    public function getTrades($request) {
        $start_date = $this->filterbydate($request->time);
        if (array_key_exists('type', $request)) {
            if ($request->type == "positions") {
                if ($this->login) {
                    $filters_array[] = $this->result->ObjectsFilter('Login', 'eq', $this->login);
                    $request = $this->result->addFilter('and', $filters_array, $request);
                }

                $positions = $this->result->read(self::POSITIONS_TABLE_NAME, $this->positions_table_columns, $request);
                foreach ($positions['data'] as $key => $position) {
                    if ($position['Action'] == 0) {
                        $positions['data'][$key]['Pips'] = round(($position['PriceCurrent'] - $position['PriceOpen']) * pow(10, ($position['Digits']) - 1), 1);
                    } elseif ($position['Action'] == 1) {
                        $positions['data'][$key]['Pips'] = round(($position['PriceOpen'] - $position['PriceCurrent']) * pow(10, ($position['Digits']) - 1), 1);
                    }
                }
                $positions['data'] = $this->positionsMap($positions['data']);
                return json_encode($positions);
            } else {
                $request->take = $request->take * 2;
                $request->skip = $request->skip * 2;
//                $filters_array[] = $this->result->ObjectsFilter('Time', "gt", $start_date);
                if ($this->login) {
                    $filters_array[] = $this->result->ObjectsFilter('Login', 'eq', $this->login);
                }
                $filters_array[] = $this->result->ObjectsFilter('Action', 'lt', 2);
                $filters_array[] = $this->result->ObjectsFilter('Symbol', 'neq', '');
                $filters_array[] = $this->result->ObjectsFilter('Time', 'isnotnull', null);
                $request = $this->result->addFilter('and', $filters_array, $request);
                $res = $this->result->read(self::DEALS_TABLE_NAME, $this->deal_table_columns, $request);
                $dat = $this->getJoinedDeals($res['data']);
                $data['total'] = count($dat);
                $data['data'] = $this->tradesMap($dat);
                return json_encode($data);
            }
        }
    }

    public function getJoinedDeals($deals) {
        $trades = [];
        $data = [];
        foreach ($deals as $d) {
            if ($d['Entry'] == 0) {
                $trades[$d['PositionID']]["open"] = $d;
            } elseif ($d['Entry'] == 1) {
                $trades[$d['PositionID']]["close"][] = $d;
            }
        }

        foreach ($trades as $key => $tr) {
            if (!array_key_exists("open", $tr) || !array_key_exists("close", $tr)) {
                unset($trades[$key]);
            }
        }

        foreach ($trades as $key => $value) {
            $p = [];
            $close = 0;
            $profit = 0;
            $sl = 0;
            $tp = 0;
            $volume = 0;
            $p['PositionID'] = $value['open']['PositionID'];
            $p['Login'] = $value['open']['Login'];
            $p['Action'] = $value['open']['Action'];
            $p['Digits'] = $value['open']['Digits'];
            $p['Symbol'] = $value['open']['Symbol'];
            $p['OpenTime'] = $value['open']['Time'];
            $p['CloseTime'] = $value['close'][count($value['close']) - 1]['Time'];
            foreach ($value['close'] as $v) {
                $close = $close + $v['Price'];
            }
            $p['OpenPrice'] = $value['open']['Price'];
            $p['ClosePrice'] = $close / count($value['close']);
            foreach ($value['close'] as $v) {
                $volume = $volume + $v['Volume'];
            }
            $p['Volume'] = $volume / 10000;
            foreach ($value['close'] as $v) {
                $profit = $profit + $v['Profit'];
            }
            $p['Profit'] = $profit;
            foreach ($value['close'] as $v) {
                $sl = $sl + $v['PriceSL'];
            }
            $p['StopLoss'] = $sl / count($value['close']);
            foreach ($value['close'] as $v) {
                $tp = $tp + $v['PriceTP'];
            }
            $p['TakeProfit'] = $tp / count($value['close']);
            $p['Comment'] = $value['close'][count($value['close']) - 1]['Comment'];
            if ($value['open']['Action'] == 0) {
                $p['Pips'] = round(($p['ClosePrice'] - $p['OpenPrice']) * pow(10, ($p['Digits']) - 1), 1);
            } elseif ($value['open']['Action'] == 1) {
                $p['Pips'] = round(($p['OpenPrice'] - $p['ClosePrice']) * pow(10, ($p['Digits']) - 1), 1);
            }
            $data[] = $p;
        }
        return $data;
    }

    public function getData($dateone, $datetwo) {
        $two = $datetwo;
        $one = $dateone;
        $data = \Yii::$app->{$this->db}->createCommand("SELECT * FROM " . self::DEALS_TABLE_NAME . " Where (Action < 2) AND (Time  > '$dateone') AND (LOGIN = $this->login) ORDER BY Time ASC")->queryAll();
        return $data;
    }

    public function getAccountTrades($login) {
        $data = \Yii::$app->{$this->db}->createCommand('SELECT * FROM mt5_users INNER JOIN mt5_groups as grp ON mt5_users.Group=grp.Group where Login=' . $login)->queryAll();
        if ($data) {
            $data = $this->AccountsMap($data);
            return $data;
        }
        return false;
    }

    private function AccountsMap($account) {
        return array_map(function($acc) {
            return array(
                'Login' => $acc['Login'],
                'Balance' => $acc['Balance'],
                'Currency' => $acc['Currency'],
            );
        }, $account);
    }

    public function getAllTrades($dateone) {
        $data = \Yii::$app->{$this->db}->createCommand("SELECT * FROM " . self::DEALS_TABLE_NAME . " Where (Action < 2) AND (Time  > '$dateone') AND (LOGIN = $this->login) ORDER BY PositionID ASC ")->queryAll();
        return $data;
    }

    public function getChart10Records() {
        $data = \Yii::$app->{$this->db}->createCommand("SELECT * FROM " . self::DEALS_TABLE_NAME . " Where (Action < 2) AND  (LOGIN = $this->login) ORDER BY Time DESC LIMIT 10 ")->queryAll();
        return $data;
    }

    function getSymbolsData($time) {
        $start_date = $this->filterbydate($time);
        $end_date = date('Y-m-d H:i:s', strtotime("now -3 GMT"));
        $data = \Yii::$app->{$this->db}->createCommand("SELECT SYMBOL,count(*) as count FROM " . self::DEALS_TABLE_NAME . " Where (action < 2) AND (LOGIN = $this->login) AND (time BETWEEN '$start_date' AND '$end_date') GROUP BY SYMBOL ")->queryAll();
        return $data;
    }

    public function getChartTenDays() {
        $end = date('Y-m-d H:i:s');
        $start = date('Y-m-d H:i:s', strtotime('-10 days'));
        $data = $this->getAllTrades($start);
        $p = [];
        $final = [];
        $result = array();
        foreach ($data as $d) {
            $result[$d['PositionID']][] = $d;
        }
        foreach ($result as $res) {
            if (count($res) == 2) {
                $p['open_time'] = $res[0]['Time'];
                $p['close_time'] = $res[1]['Time'];
                $p['open_price'] = $res[0]['Price'];
                $p['close_price'] = $res[1]['Price'];
                if ($res['Action'] = 1) {
                    $res['PIP'] = round(($p['close_price'] - $p['open_price']) * pow(10, ($res[0]['Digits']) - 1), 1);
                } elseif ($res['Action'] = 0) {
                    $res['PIP'] = round(($p['open_price'] - $p['close_price']) * pow(10, ($res[0]['Digits']) - 1), 1);
                }
                $day = substr($p['close_time'], 8, 2);
                if (array_key_exists($day, $final)) {
                    $final[$day] = $final[$day] + $res['PIP'];
                } else {
                    $final[$day] = $res['PIP'];
                }
            }
        }
        return $final;
    }

    public function getKendoTrades($request) {
        header('Content-Type: application/json');
        $res = $this->result->read(self::DEALS_TABLE_NAME, $this->deals_table_columns, $request);
        $res = $this->result->read('mt5_deals', $this->deals_table_columns, $request);

        return json_encode($res);
    }

    public function filterbydate($val) {
        $start = 0;
        $end = date('Y-m-d H:i:s', strtotime("now -3 GMT"));
        if ($val == 'oneD') {
            $start = date("Y-m-d H:i:s", strtotime('-1 days -3 GMT'));
        } else if ($val == 'threeD') {
            $start = date("Y-m-d H:i:s", strtotime('-3 days -3 GMT'));
        } else if ($val == 'week') {
            $start = date("Y-m-d H:i:s", strtotime('-7 days -3 GMT'));
        } else if ($val == 'oneM') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-1 months"));
        } else if ($val == 'threeM') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-3 months"));
        } else if ($val == 'sixM') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-6 months"));
        } else if ($val == 'year') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-1 year"));
        } else {
            return $two = date('Y-m-d H:i:s', strtotime('-7 days'));
            $start = date('Y-m-d H:i:s', strtotime('-7 days'));
        }
        return $start;
    }

    public function calculatePips($result) {
        return 1;
    }

    public function getChartCustom($time) {
        $start_date = $this->filterbydate($time);
        $end_date = date('Y-m-d H:i:s');
        $data = $this->getData($start_date, $end_date);
        $p = [];
        $deals = $this->getJoinedDeals($data);

        foreach ($deals as $trade) {
            $day = substr($trade['CloseTime'], 0, 10);
            if (array_key_exists($day, $p)) {
                $p[$day] = $p[$day] + $trade['Pips'];
            } else {
                $p[$day] = $trade['Pips'];
            }
        }
        ksort($p);
        return $p;
    }

    function getTradesForActive($login) {
        
    }

    function getSymbols() {
        $data = \Yii::$app->{$this->db}->createCommand("SELECT DISTINCT Symbol FROM " . self::DEALS_TABLE_NAME . " Where (Login = $this->login)")->queryAll();
        return array_column($data, 'Symbol');
    }

    public function tradesMap($trades) {
        return array_map(function($trade) {
            return array(
                'Ticket' => $trade['PositionID'],
                'Currency' => $trade['Symbol'],
                'Digits' => $trade['Digits'],
                'Type' => $trade['Action'] == 0 ? "buy" : "sell",
                'Volume' => $trade['Volume'],
                'OpenTime' => $trade['OpenTime'],
                'OpenPrice' => $trade['OpenPrice'],
                'CloseTime' => $trade['CloseTime'],
                'ClosePrice' => $trade['ClosePrice'],
                'Profit' => $trade['Profit'],
                'StopLoss' => $trade['StopLoss'],
                'TakeProfit' => $trade['TakeProfit'],
                'Comment' => $trade['Comment'],
                'Swap' => array_key_exists("Swap", $trade) ? $trade['Swap'] : "",
                'Taxes' => array_key_exists("Taxes", $trade) ? $trade['Taxes'] : "",
                'Magic' => array_key_exists("Magic", $trade) ? $trade['Magic'] : "",
                'Pips' => array_key_exists("Pips", $trade) ? $trade['Pips'] : "",
            );
        }, $trades);
    }

    public function positionsMap($trades) {
        return array_map(function($trade) {
            return array(
                'Ticket' => $trade['Position'],
                'Currency' => $trade['Symbol'],
                'Digits' => $trade['Digits'],
                'Type' => $trade['Action'] == 0 ? "buy" : "sell",
                'Volume' => $trade['Volume'] / 10000,
                'OpenTime' => $trade['TimeCreate'],
                'OpenPrice' => $trade['PriceOpen'],
                'CloseTime' => null,
                'ClosePrice' => $trade['PriceCurrent'],
                'Profit' => $trade['Profit'],
                'StopLoss' => $trade['PriceSL'],
                'TakeProfit' => $trade['PriceTP'],
                'Comment' => $trade['Comment'],
                'Swap' => array_key_exists("Swap", $trade) ? $trade['Swap'] : "",
                'Taxes' => array_key_exists("Taxes", $trade) ? $trade['Taxes'] : "",
                'Magic' => array_key_exists("Magic", $trade) ? $trade['Magic'] : "",
                'Pips' => array_key_exists("Pips", $trade) ? $trade['Pips'] : "",
            );
        }, $trades);
    }

    public function openTrade($request) {
        $trade = new TradeObject($request);
        $trade_action = new TradeAction(TradeAction::OPEN_OPERATION, $trade);
        try {
            $trade_callback = $trade_action->send_message();
            $trade_response = new Response($trade_callback);
            if ($trade_response->status == Response::RESPONSE_DONE) {
                if (isset($trade_response->data)) {
                    $data = $trade_response->get_data();

                    return $this->positionsMap([(array) $data]);
                }
            }
        } catch (Raw\Exception $ex) {
            return false;
        }
        return false;
    }

    public function closeTrade($request) {
        $trade = new TradeObject($request);
        $trade_action = new TradeAction(TradeAction::CLOSE_OPERATION, $trade);
        try {
            $trade_callback = $trade_action->send_message();
            $trade_response = new Response($trade_callback);
            if ($trade_response->status == Response::RESPONSE_DONE) {
                if (isset($trade_response->data)) {
                    $data = $trade_response->get_data();
                    return $this->tradesMap([(array) $data]);
                }
            }
        } catch (Raw\Exception $ex) {
            return false;
        }
        return false;
    }

    public function modifyTrade($request) {
        $trade = new TradeObject($request);
        $trade_action = new TradeAction(TradeAction::MODIFY_OPERATION, $trade);
        try {
            $trade_callback = $trade_action->send_message();
            $trade_response = new Response($trade_callback);
            if ($trade_response->status == Response::RESPONSE_DONE) {
                if (isset($trade_response->data)) {
                    $data = $trade_response->get_data();
                    return $this->tradesMap([(array) $data]);
                }
            }
        } catch (Raw\Exception $ex) {
            return false;
        }
        return false;
    }

}
