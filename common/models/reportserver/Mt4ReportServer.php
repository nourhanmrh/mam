<?php

namespace common\models\ReportServer;

use Yii;
use yii\base\Model;
use yii\db\Expression;
use common\models\ReportServer\ReportServerInterface;
use common\models\traderinfo\TradersInfo;
use common\models\mamtrades\MamTrades;

class Mt4Reportserver extends Model implements ReportServerInterface {

    private $db;
    private $db_object;
    private $result;
    private $login;
    private $platform;
    private $broker;
    private $columns = array('TICKET', 'LOGIN', 'SYMBOL', 'DIGITS', 'CMD', 'VOLUME', 'OPEN_TIME', 'OPEN_PRICE', 'CLOSE_TIME', 'CLOSE_PRICE', 'PROFIT', 'SL', 'TP', 'SWAPS', 'TAXES', 'COMMENT', 'MAGIC');
    public static $map = [
        'Ticket' => 'TICKET',
        'Login' => 'LOGIN',
        'Symbol' => 'SYMBOL',
        'Digits' => 'DIGITS',
        'Type' => 'CMD',
        'Volume' => 'VOLUME',
        'OpenTime' => 'OPEN_TIME',
        'OpenPrice' => 'OPEN_PRICE',
        'CloseTime' => 'CLOSE_TIME',
        'ClosePrice' => 'CLOSE_PRICE',
        'Profit' => 'PROFIT',
        'Pips' => 'PIP',
        'StopLoss' => 'SL',
        'TakeProfit' => 'TP',
        'Swap' => 'SWAPS',
        'Taxes' => 'TAXES',
        'Comment' => 'COMMENT',
        'Magic' => 'MAGIC'
    ];

    public function Mt4Reportserver() {
        
    }

    public function getTradesNumber() {
        $data = \Yii::$app->{$this->db}->createCommand("SELECT count(*) as count FROM mt4_trades Where  (LOGIN = $this->login)  ")->queryAll();
        return $data;
    }

    public function __construct($dbname, $login) {
        try {

            $this->db = $dbname;

            $this->login = $login;

            $this->db_object = Yii::$app->{$this->db};

            $this->result = new \DataSourceResult($this->db_object->dsn . ";charset=" . $this->db_object->charset, $this->db_object->username, $this->db_object->password);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getData($start_date, $end_date) {

        $start = $start_date;

        $end = $end_date;

        $data = \Yii::$app->{$this->db}->createCommand("SELECT * FROM mt4_trades Where (CMD < 2) AND (CLOSE_TIME BETWEEN '$start' AND '$end') AND (LOGIN = $this->login) ORDER BY CLOSE_TIME ASC")->queryAll();

        return $data;
    }

    public function getAccountTrades($login) {
        $data = \Yii::$app->{$this->db}->createCommand("SELECT * FROM mt4_users Where (LOGIN = $login) ")->queryAll();
        if ($data) {
            $data = $this->AccountsMap($data);
            return $data;
        }
    }

    private function AccountsMap($account) {
        return array_map(function($acc) {
            return array(
                'Login' => $acc['LOGIN'],
                'Balance' => $acc['BALANCE'],
                'Currency' => $acc['CURRENCY'],
            );
        }, $account);
    }

    public function get_yesterday_profit($from, $to) {
        $sql_yesterday_profit = \Yii::$app->{$this->db}->createCommand("SELECT sum(profit) as profit FROM mt4_trades where login=" . $this->login . " and cmd<2 and close_time between  '" . $from . "' and '" . $to . "' ")->queryAll();
        return $sql_yesterday_profit;
    }

    public function get_threemonths_profit($from, $to) {
        $sql_threemonths_profit = \Yii::$app->{$this->db}->createCommand("SELECT sum(profit) as profit FROM mt4_trades where login=" . $login . " and cmd<2 and close_time between  '" . $from . "' and '" . $to . "' ")->queryAll();
        return $sql_threemonths_profit;
    }

    public function get_user_equity() {

        $equity = \Yii::$app->{$this->db}->createCommand("SELECT equity FROM mt4_users where login=" . $this->login)->queryOne();
        return $equity['equity'];
    }

    public function get_user_balance() {

        $balance = \Yii::$app->{$this->db}->createCommand("SELECT balance FROM mt4_users where login=" . $this->login)->queryOne();
        return $balance['balance'];
    }

    public function get_user_marginpercentage($login) {
        if ($login) {
            $perc = \Yii::$app->{$this->db}->createCommand("SELECT MARGIN_FREE as margin FROM mt4_users where login=" . $login)->queryOne();
            return $perc['margin'];
        }
    }

    public function getOverallTrades($request) {
        $result = $this->result;
        $res = $result->read('mt4_trades', $this->columns, $request);
        foreach ($res['data'] as $key => $trade) {

            if ($trade['CMD'] = 1) {

                $res['data'][$key]['PIP'] = round(($trade['CLOSE_PRICE'] - $trade['OPEN_PRICE']) * pow(10, ($trade['DIGITS']) - 1), 1);
            } elseif ($trade['CMD'] = 0) {

                $res['data'][$key]['PIP'] = round(($trade['OPEN_PRICE'] - $trade['CLOSE_PRICE']) * pow(10, ($trade['DIGITS']) - 1), 1);
            }
        }
        $res['data'] = $this->tradesMap($res['data']);
        $data = json_encode($res);
        return $res;
    }

    public function getChartTenDays() {
        $end = date('Y-m-d H:i:s');
        $start = date('Y-m-d H:i:s', strtotime('-10 days'));
        $data = $this->getData($start, $end);
        $p = [];
        foreach ($data as $trade) {
            if ($trade['CMD'] = 1) {
                $trade['PIP'] = round(($trade['CLOSE_PRICE'] - $trade['OPEN_PRICE']) * pow(10, ($trade['DIGITS']) - 1), 2);
            } elseif ($trade['CMD'] = 0) {
                $trade['PIP'] = round(($trade['OPEN_PRICE'] - $trade['CLOSE_PRICE']) * pow(10, ($trade['DIGITS']) - 1), 2);
            }
            $month = substr($trade['CLOSE_TIME'], 8, 2);
            if (array_key_exists($month, $p)) {
                $p[$month] = $p[$month] + $trade['PIP'];
            } else {
                $p[$month] = $trade['PIP'];
            }
        }
        return $p;
    }

    public function getKendoTrades($request) {
        header('Content-Type: application/json');
        $res = $this->result->read('mt4_trades', $this->columns, $request);

        $res['data'] = $this->tradesMap($res['data']);
        $data = json_encode($res);
        return $data;
    }

    public function getChartCustom($time) {
        $start_date = $this->filterbydate($time);

        $end_date = date('Y-m-d H:i:s');

        $data = $this->getData($start_date, $end_date);
        $p = [];
        foreach ($data as $trade) {
            if ($trade['CMD'] = 1) {
                $trade['PIP'] = round(($trade['CLOSE_PRICE'] - $trade['OPEN_PRICE']) * pow(10, ($trade['DIGITS']) - 1), 1);
            } elseif ($trade['CMD'] = 0) {
                $trade['PIP'] = round(($trade['OPEN_PRICE'] - $trade['CLOSE_PRICE']) * pow(10, ($trade['DIGITS']) - 1), 1);
            }
            $day = substr($trade['CLOSE_TIME'], 0, 10);
            if (array_key_exists($day, $p)) {
                $p[$day] = $p[$day] + $trade['PIP'];
            } else {
                $p[$day] = $trade['PIP'];
            }
        }


        return $p;
    }

    public function calculatePips($result) {
        
    }

    public function calculateMonthProfit($trader_name) {
        $start_date = $this->filterbydate('oneM');
        $end_date = date('Y-m-d H:i:s');
        $data = \Yii::$app->{$this->db}->createCommand("SELECT sum(profit) as sum FROM mt4_trades Where (CMD < 2) AND (LOGIN = $this->login) AND (CLOSE_TIME BETWEEN '$start_date' AND '$end_date') ")->queryOne();

        return $data;
    }

    public function calculateTotalProfit($trader_name = null, $login = null) {
        $end_date = date('Y-m-d H:i:s');
        $data = \Yii::$app->{$this->db}->createCommand("SELECT sum(profit) as sum FROM mt4_trades Where (CMD < 2) AND (LOGIN = $this->login) AND (CLOSE_TIME != '1970-01-01 00:00:00') ")->queryOne();
        return $data;
    }

    public function calculateTotalAverage($trader_name) {
        $end_date = date('Y-m-d H:i:s');
        $data = \Yii::$app->{$this->db}->createCommand("SELECT avg(profit) as avg FROM mt4_trades Where (CMD < 2) AND (LOGIN = $this->login) AND (CLOSE_TIME < '$end_date') ")->queryOne();
        return $data;
    }

    public function filterbydate($val) {
        $start = 0;
        $end = date('Y-m-d H:i:s');
        if ($val == 'oneD') {
            $start = date("Y-m-d H:i:s", strtotime('-1 days'));
        } else if ($val == 'threeD') {
            $start = date("Y-m-d H:i:s", strtotime('-3 days'));
        } else if ($val == 'week') {
            $start = date("Y-m-d H:i:s", strtotime('-7 days'));
        } else if ($val == 'oneM') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-1 months"));
        } else if ($val == 'threeM') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-3 months"));
        } else if ($val == 'sixM') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-6 months"));
        } else if ($val == 'year') {
            $start = date('Y-m-d H:i:s', strtotime($end . "-1 year"));
        } else {
            return $two = date('Y-m-d H:i:s', strtotime('-7 days'));
            $start = date('Y-m-d H:i:s', strtotime('-7 days'));
        }
        return $start;
    }

    public function getTrades($request) {        
        $result = $this->result;
        $start_date = $this->filterbydate($request->time);
        $filters_array = [];
        $filters_array[] = $result->ObjectsFilter('VOLUME', "neq", "0");
        $filters_array[] = $result->ObjectsFilter('CLOSE_TIME', "isnotnull", null);
        $filters_array[] = $result->ObjectsFilter('SYMBOL', "neq", "");
        $filters_array[] = $result->ObjectsFilter('OPEN_TIME', "isnotnull", null);
        $filters_array[] = $result->ObjectsFilter('OPEN_PRICE', "isnotnull", null);
        if (array_key_exists('type', (array) $request)) {
            if ($request->type == "history") {
                $filters_array[] = $result->ObjectsFilter('CLOSE_TIME', "gt", $start_date);
            } else if ($request->type == "positions") {
                $filters_array[] = $result->ObjectsFilter('CLOSE_TIME', "eq", "1970-01-01 00:00:00");
            }
        } else {
            $request->type = "history";
            $filters_array[] = $result->ObjectsFilter('CLOSE_TIME', "gt", $start_date);
        }
        if ($this->login) {
            $filters_array[] = $this->result->ObjectsFilter('LOGIN', 'eq', $this->login);
        }
        $request = $result->addFilter('and', $filters_array, $request);
        // print_r($request);die;
        $res = $result->read('mt4_trades', $this->columns, $request);
        foreach ($res['data'] as $key => $trade) {

            if ($trade['CMD'] = 1) {

                $res['data'][$key]['PIP'] = round(($trade['CLOSE_PRICE'] - $trade['OPEN_PRICE']) * pow(10, ($trade['DIGITS']) - 1), 1);
            } elseif ($trade['CMD'] = 0) {

                $res['data'][$key]['PIP'] = round(($trade['OPEN_PRICE'] - $trade['CLOSE_PRICE']) * pow(10, ($trade['DIGITS']) - 1), 1);
            }
        }

        $res['data'] = $this->tradesMap($res['data']);
        $data = json_encode($res);
        return $data;
    }

    public function getPositionsTrades() {
        return 1;
    }

    public function getMinTime($login = null) {
        return 1;
    }

    function getSymbolsData($time) {
        $start_date = $this->filterbydate($time);

        $end_date = date('Y-m-d H:i:s');
        $array = [];

        $data = \Yii::$app->{$this->db}->createCommand("SELECT SYMBOL,count(*) as count FROM mt4_trades Where (CMD < 2) AND (LOGIN = $this->login) AND (CLOSE_TIME BETWEEN '$start_date' AND '$end_date') GROUP BY SYMBOL ")->queryAll();
        return $data;
    }

    function getTradesForActive($login) {
        $data = \Yii::$app->{$this->db}->createCommand("SELECT * FROM mt4_trades Where (LOGIN = $this->login) ORDER BY CLOSE_TIME ASC")->queryAll();

        return $data;
    }

    function getSymbols() {

        $data = \Yii::$app->{$this->db}->createCommand("SELECT DISTINCT SYMBOL FROM mt4_trades Where (LOGIN = $this->login)")->queryAll();

        return array_column($data, 'SYMBOL');
    }

    public function tradesMap($trades) {
        return array_map(function($trade) {
            return array(
                'Ticket' => $trade['TICKET'],
                //'Login' => $trade['LOGIN'],
                'Symbol' => $trade['SYMBOL'],
                'Digits' => $trade['DIGITS'],
                'Type' => $trade['CMD'] == 0 ? "buy" : "sell",
                'Volume' => $trade['VOLUME'] / 100,
                'OpenTime' => $trade['OPEN_TIME'],
                'OpenPrice' => $trade['OPEN_PRICE'],
                'CloseTime' => $trade['CLOSE_TIME'] == "1970-01-01 00:00:00" ? null : $trade['CLOSE_TIME'],
                'ClosePrice' => $trade['CLOSE_PRICE'],
                'Profit' => $trade['PROFIT'],
                'StopLoss' => $trade['SL'],
                'TakeProfit' => $trade['TP'],
                'Swap' => $trade['SWAPS'],
                'Taxes' => $trade['TAXES'],
                'Comment' => $trade['COMMENT'],
                'Magic' => $trade['MAGIC'],
                'Pips' => $trade['PIP'],
            );
        }, $trades);
    }

    public function openTrade($request) {
        
    }

    public function closeTrade($request) {
        
    }

    public function modifyTrade($request) {
        
    }

}
