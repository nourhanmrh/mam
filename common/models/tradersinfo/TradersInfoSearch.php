<?php

namespace common\models\tradersinfo;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tradersinfo\TradersInfo;


class TradersInfoSearch extends TradersInfo
{
  
    public function rules()
    {
        return [
            [['id', 'trader_id', 'user_id', 'investors', 'is_live', 'trusting', 'ea', 'left_at', 'logged'], 'integer'],
            [['trader_name','name', 'country_code', 'country_name', 'profile_pic', 'joined_at', 'currency', 'strategy', 'ten_month_chart'], 'safe'],
            [['profit', 'roi', 'equity', 'rate', 'minpeak', 'maxpeak', 'maxddvalue', 'comission_received', 'comission_pending'], 'number'],
        ];
    }


    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = TradersInfo::find()
                ->joinWith("mamAccount mamAccount")
                ->joinWith("user user");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'trader_id' => $this->trader_id,
            'user_id' => $this->user_id,
            'investors' => $this->investors,
            'profit' => $this->profit,
            'roi' => $this->roi,
            'is_live' => $this->is_live,
            'trusting' => $this->trusting,
            'ea' => $this->ea,
            'joined_at' => $this->joined_at,
            'left_at' => $this->left_at,
            'equity' => $this->equity,
            'rate' => $this->rate,
            'logged' => $this->logged,
            'minpeak' => $this->minpeak,
            'maxpeak' => $this->maxpeak,
            'maxddvalue' => $this->maxddvalue,
            'comission_received' => $this->comission_received,
            'comission_pending' => $this->comission_pending,
        ]);

        $query->andFilterWhere(['like', 'trader_name', $this->trader_name])
            ->andFilterWhere(['like', 'country_code', $this->country_code])
            ->andFilterWhere(['like', 'country_name', $this->country_name])
            ->andFilterWhere(['like', 'profile_pic', $this->profile_pic])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'strategy', $this->strategy])
            ->andFilterWhere(['like', 'ten_month_chart', $this->ten_month_chart]);

        return $dataProvider;
    }
}
