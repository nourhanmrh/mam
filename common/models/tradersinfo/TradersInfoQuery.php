<?php

namespace common\models\tradersinfo;

/**
 * This is the ActiveQuery class for [[TradersInfo]].
 *
 * @see TradersInfo
 */
class TradersInfoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TradersInfo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TradersInfo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
