<?php

namespace common\models\tradersinfo;

use Yii;
use common\models\traders\Traders;
use common\models\mamaccounts\MamAccounts;
use common\models\tradersinfo\TradersInfoQuery;
use common\models\ReportServer\ReportServer;
use common\models\drawdown\Drawdown;
use common\models\relations\Relations;
use common\models\users\Users;
use common\models\ReportServer\Mt5Reportserver;

class TradersInfo extends \yii\db\ActiveRecord {

    const TYPE_RATE = 'rate';
    const TYPE_PROFIT = 'profit';

    public $file;
    public static $map = [
        "Id" => 'id',
        "TraderName" => 'trader_name',
        "Roi" => 'roi',
        "CountryCode" => 'country_code',
        "CountryName" => 'country_name',
        "Picture" => 'profile_pic',
        "ExpertAdvisor" => 'ea',
        "Live" => 'is_live',
        "Profit" => 'profit',
        "Investors" => 'investors',
        "Symbols" => 'currency',
        "AverrageRate" => 'rate',
        "SparklinePoints" => 'chart',
        "FollowStatusText" => 'status',
        "CanVote" => 'canvote',
        "BrokerPic" => 'broker',
        "ActiveAccountVotes" => 'votes',
        "Strategy" => 'strategy',
        "AmountFollowing" => 'amount_following',
        "ReceivedCommission" => 'comission_received',
        "PendingCommission" => 'comission_pending',
    ];

    public static function tableName() {
        return 'traders_info';
    }

    private $datasource;
    private $columns = array(
        'id', 'trader_id', 'trader_name', 'roi', 'user_id', 'country_code', 'ten_month_chart',
        'trusting', 'strategy', 'profile_pic', 'ea', 'is_live', 'profit', 'investors', 'currency',
        'country_name', 'logged', 'rate', 'comission_received', 'comission_pending', 'open_positions',
        'trades', 'avg_pips', 'wining_trades', 'total_profit_pips', 'last_updated', 'maxddvalue'
    );

    public function __construct() {
        $this->datasource = new \DataSourceResult(Yii::$app->db->dsn . ";charset=" . Yii::$app->db->charset, Yii::$app->db->username, Yii::$app->db->password);
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['trader_id', 'trader_name'], 'required'],
            [['trader_id', 'investors', 'is_live', 'trusting', 'ea', 'left_at', 'logged', 'open_positions', 'trades', 'user_id'], 'integer'],
            [['profit', 'roi', 'equity', 'rate', 'minpeak', 'maxpeak', 'maxddvalue', 'comission_received', 'comission_pending', 'avg_pips', 'wining_trades', 'total_profit_pips'], 'number'],
            [['joined_at', 'last_updated'], 'safe'],
            [['trader_name'], 'string', 'max' => 100],
            [['country_code'], 'string', 'max' => 4],
            [['country_name', 'currency'], 'string', 'max' => 255],
            [['profile_pic'], 'string', 'max' => 2083],
            [['strategy', 'ten_month_chart'], 'string', 'max' => 500],
            [['trader_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'trader_id' => 'Trader ID',
            'trader_name' => 'Trader Name',
            'investors' => 'Investors',
            'profit' => 'Profit',
            'roi' => 'Roi',
            'country_code' => 'Country Code',
            'country_name' => 'Country Name',
            'profile_pic' => 'Profile Pic',
            'is_live' => 'Is Live',
            'trusting' => 'Trusting',
            'ea' => 'Ea',
            'joined_at' => 'Joined At',
            'left_at' => 'Left At',
            'platform' => 'Platform',
            'currency' => 'Currency',
            'equity' => 'Equity',
            'rate' => 'Rate',
            'logged' => 'Logged',
            'strategy' => 'Strategy',
            'minpeak' => 'Minpeak',
            'maxpeak' => 'Maxpeak',
            'maxddvalue' => 'Maxddvalue',
        ];
    }

    public static function find() {
        return new TradersInfoQuery(get_called_class());
    }

    public function getMamAccount() {
        return $this->hasOne(MamAccounts::className(), ['id' => 'trader_id']);
    }

    public function getTraderInfoByName($trader_name) {
        return self::find()->where("trader_name='" . $trader_name . "'")->asArray()->one();
    }

    public function getUser() {

        return $this->hasOne(Users::className(), ['id' => 'id']);
    }

    public function get_chart_values($name) {
        $item = [];
        $reportserver = [];
        $account = new MamAccounts();
        $info = $account->getDataBase($name);       
        $report_server_class = new ReportServer($info['platform']);
        $reportserver = new $report_server_class($info['database'], $info['login']);
        $data = $reportserver->getChartTenMonth();
        $item = [];
        if (is_array($data) && count($data) > 0) {
            foreach ($data as $key => $val) {
                $res['profit'] = $val;
                $item[] = $res['profit'];
            }
        }
        return $item;
    }

    public function getTradersInfo($request, $result, $condition = null) {
        if ($condition) {
            $filter_request = [];
            foreach ($condition as $id => $client) {
                $filter_request[] = $this->datasource->ObjectsFilter('trader_id', 'eq', $client['master_account']);
            }
            $request = $this->datasource->addFilter('or', $filter_request, (object) $request);
        }
        $res = $this->datasource->read($this->tablename(), $this->columns, $request);
        $n = new MamAccounts();
        $data = array();
        foreach ($res['data'] as $k => $val) {
            $res['data'][$k]['account_status'] = $n->getAccountStatus($val['trader_id']);
            if ($res['data'][$k]['account_status'] == \common\models\Constants::ACCOUNT_STATUS_COMPLETED) {
                $data[] = $res['data'][$k];
            }
        }
       
        $res['data'] = $this->completeTradersInfo($data);
        return $res;
    }

    public function getDrawdown() {
        return $this->hasMany(Drawdown::className(), ['trader_id' => 'id']);
    }

    public function getTradersStatistics($request, $result) {
        $res = $this->datasource->read($this->tablename(), $this->columns, $request);
        $idz = array();
        if ($res['data']) {
            foreach ($res['data'] as $key => $value) {
                $idz[] = $value['id'];
                $tidz[] = $value['trader_id'];
                $trader_name = $value['trader_name'];
            }
            $list_trader = implode(',', $tidz);
            $list = implode(',', $idz);
            return $this->completeTradersStatistics($list, $trader_name, $list_trader);
        }
    }

    public function completeTradersStatistics($list, $trader_name, $list_trader) {
        $trader = new Traders();
        $rls = new Relations();
        $a = "[" . $list . "]";
        $b = "[" . $list_trader . "]";
        $count = count(json_decode($a));
        $total_profit = $this->getTotalProfit(json_decode($b));     
        $rs = $rls->get_amount_following("(" . $list_trader . ")");
        $stats = $trader->getAvgRoi("(" . $list . ")");
        $comm = $trader->getstats("(" . $list . ")");
        if ($rs) {
            $amount = $rs[0]['amount'];
        } else {
            $amount = 0;
        }
        $traders['avg_rcom'] = round($comm[0]['com_rcv_avg'], 2);
        $traders['avg_pcom'] = round($comm[0]['com_png_avg'], 2);
        $traders['sum_rcom'] = round($comm[0]['com_rcv_sum'], 2);
        $traders['sum_pcom'] = round($comm[0]['com_png_sum'], 2);
        //$traders['profit_avg'] = round($comm[0]['profit_avg'], 2);
        $traders['profit_avg'] = round(($total_profit / $count),2);
        $traders['roi_avg'] = round($stats[0]['roi_avg'], 2);
        $traders['profit_sum'] = $total_profit;
        $traders['total_amount'] = $amount;
        $x[] = $traders;
        return $x;
    }

    public function completeTradersInfo($traders) {
        $active_account = '';
        $trader = new Traders();
        $mam_accounts = new MamAccounts();
        if (!Yii::$app->user->isGuest) {
            $active_account = $mam_accounts->getActiveAccountByUserId(\Yii::$app->user->id);         
        }
   

        foreach ($traders as $key => $value) { 
            $master_acc = $traders[$key]['trader_id'];
            $traders[$key]['log'] = $trader->is_logged();
            $traders[$key]['active_account_status'] = $active_account ? $active_account['account_status'] : "";
            $traders[$key]['active_account_name'] = $active_account ? $active_account['name'] : "";
            $traders[$key]['active_account_type'] = $active_account ? $active_account['account_type'] : "";
            $relation_details = $trader->get_followed_traders($traders[$key]['trader_id']);
            $traders[$key]['status'] = $relation_details ? Yii::t("app", "Unfollow") : Yii::t("app", "Follow");
            $traders[$key]['canvote'] = $trader->canvote($master_acc);
            $traders[$key]['width'] = $trader->get_star_width($master_acc);
            $broker = $trader->get_followed_broker($traders[$key]['trader_id']);
            $traders[$key]['broker'] = Yii::$app->params['frontendURL'] . Yii::$app->params['ImageBrokerPath'] . "?id=" . $broker['id'] . "&size=SM";
            $traders[$key]['profile_pic'] = Yii::$app->params['frontendURL'] . Yii::$app->params['ImageTraderPath'] . "?id=" . $traders[$key]['id'] . "&size=SM";
            $traders[$key]['has_voted'] = $trader->has_voted();
            $traders[$key]['votes'] = $trader->get_rates($master_acc);
            $traders[$key]['display_vote'] = $trader->display_rate($master_acc);
            $traders[$key]['amount_following'] = $trader->get_amount_following($traders[$key]['trader_id']);
            $traders[$key]['profit'] = $trader->actionTotalProfit($value['trader_id']);
            $traders[$key]['comission_received'] = $value['comission_received'];
            $traders[$key]['comission_pending'] = $value['comission_pending'];
            $traders[$key]['following_details'] = $relation_details;
            $traders[$key]['ten_month_chart'] = $trader->get_chart_ten_days($value['trader_id']);

            unset($traders[$key]['trader_id']);
        }
        return $this->tradersInfoMap($traders);
    }

    public function tradersInfoMap($tradersInfo) {
        return array_map(function($info) {
            $array['points'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            $st_chart = (object) $array;
            $x = (count(json_decode($info['ten_month_chart'])->points) != 0) ? (json_decode($info['ten_month_chart'])->points) : (json_decode(json_encode($st_chart))->points);
            return array(
                "Id" => (int) $info['id'],
                "IsLogged" => $info['log'],
                "TraderName" => $info['trader_name'],
                "Roi" => (double) $info['roi'],
                "CountryCode" => $info['country_code'],
                "CountryName" => $info['country_name'],
                "Picture" => $info['profile_pic'] ? $info['profile_pic'] : "",
                "ExpertAdvisor" => (int) $info['ea'] == NULL ? 0 : (int) $info['ea'],
                "Live" => (int) $info['is_live'],
                "Profit" => (double) $info['profit'],
                "Investors" => (int) $info['investors'],
                "Currency" => $info['currency'] ? $info['currency'] : "",
                "AverrageRate" => (double) $info['rate'],
                "SparklinePoints" => $x,
                "FollowStatusText" => $info['status'],
                "CanVote" => (int) $info['canvote'],
                "BrokerPic" => $info['broker'],
                "ActiveAccountVotes" => $info['votes'] == NULL ? 0 : $info['votes'],
                "Strategy" => $info['strategy'] ? $info['strategy'] : "",
                "AmountFollowing" => (int) $info['amount_following'],
                "Trusted" => $info['trusting'],
                "ActiveAccountStatus" => $info['active_account_status'],
                "ActiveAccountName" => $info['active_account_name'],
                "ActiveAccountType" => $info['active_account_type'],
                "ReceivedCommission" => (float) $info['comission_received'],
                "PendingCommission" => (float) $info['comission_pending'],
                "OpenPositions" => (int) $info['open_positions'],
                "Trades" => (int) $info['trades'],
                "AvgPips" => (float) $info['avg_pips'],
                "WiningTrades" => (float) $info['wining_trades'],
                "MaxDrawdown" => (float) $info['maxddvalue'],
                "TotalProfitPips" => (float) $info['total_profit_pips'],
                "LastUpdated" => $info['last_updated'],
                "FollowingDetailsLabel" => Yii::t("app", "Edit Configuration"),
                "FollowingDetails" => $info['following_details'] ? Relations::relationsMapping([$info['following_details']])[0] : null,
            );
        }, $tradersInfo);
        die;
    }

    public function getRS($trader_id) {
        $data = new MamAccounts();
        $data = MamAccounts::find()->where(['id' => $trader_id])->one();     
        $info = $data->getDataBase($data['name']);   
        $report_server_class = (new ReportServer($data['platform']))->getClass();
        $reportserver = new $report_server_class($info['database'], $data['login']);
        return $reportserver;
    }

    public function getTradesNumber($trader_id) {
        $reportserver = $this->getRS($trader_id);
        $acc= MamAccounts::findOne(['id'=>$trader_id]);        
        $reques = [];
        $request = (object) $reques;
        $request->time = '';
        $request->type = 'history';
        $request->take = 20;
        $request->skip = 0;
        $request->pagesize = 20;
        $request->page = 1;
        $request->name = '';   
        $no_of_trades = json_decode($reportserver->getTrades($request));       
        return $no_of_trades->total;
    }

    public function getChartTenmonth($trader_id) {
        $reportserver = $this->getRS($trader_id);
        $chart_points = $reportserver->getChartTenMonth();
        $points_array = json_encode(array_values($chart_points));
        $array = '{"points":' . $points_array . '}';
        return $array;
    }

    public function getTotalProfit($list) {    
        $sum = 0;
        foreach ($list as $k => $trader_id) { 
          $reportserver = $this->getRS($trader_id);     
            $data = MamAccounts::find()->where(['id' => $trader_id])->one();
            $total_profit = $reportserver->calculateTotalProfit($data['name'], $data['login']);
            
            $sum += $total_profit['sum'];   
        }   
  return round($sum, 2);
    }

    public function getTotalPositions($trader_id) {
        $reportserver = $this->getRS($trader_id);
        $opened_positions = $reportserver->getPositionsTrades();      
        return $opened_positions;
    }

    public function getAvgPips($trader_id) {
        $reportserver = $this->getRS($trader_id);
        $avg_pips = $reportserver->getMinTime();
        if ($avg_pips) {
            $profit_by_pips = $this->getTotalProfitByPips($trader_id);
            $current = date('Y-m-d H:i:s');
            $time = $avg_pips[0]['time'];
            $now = new \DateTime($current);
            $from = new \DateTime($time);
            $diff = $now->diff($from);
            $interval = $diff->d;
            if ($interval) {
                $avg = $profit_by_pips['sum'] / $interval;
                return round($avg, 2);
            } else {
                return 0;
            }
        }
    }

    public function getTotalProfitByPips($trader_id) {    
        $data = new MamAccounts();
        $data = MamAccounts::find()->where(['id' => $trader_id])->one();
        $info = $data->getDataBase($data['name']);
        $report_server_class = (new ReportServer($data['platform']))->getClass();
        $reportserver = new $report_server_class($info['database'], $data['login']);
        $reques = [];
        $request = (object) $reques;
        $request->time = '';
        $request->type = '';
        $request->take = 20;
        $request->skip = 0;
        $request->pagesize = 20;
        $request->page = 1;
        $request->name = '';
        $request->filter = $reportserver->result->ObjectsFilter('Profit', 'gte', 0);
        $winn_trades = json_decode($reportserver->getTrades($request));
        $sum = 0;
        foreach ($winn_trades->data as $t) {
            $sum += $t->Pips;
        }
        $array = array();
        $array['sum'] = $sum;
        $array['winning_trades'] = $winn_trades->total;
        return $array;
    }

}
