<?php

namespace common\sse;

use odannyc\Yii2SSE\SSEBase;
use common\models\mamaccounts\MamAccounts;
use common\models\ReportServer\ReportServer;

class TraderTradesHandler extends SSEBase {

    public $trader_name;
    public $tickets;

    function __construct($traderName, $tickets = null) {
        $this->trader_name = $traderName;
        if ($tickets) {
            $this->tickets = json_decode($tickets);
        }
    }

    public function check() {
        return true;
    }

    public function update() {
        $data = new MamAccounts();

        $request = [];
        $request['type'] = 'positions';
        $request['time'] = "";
        $request = (object) $request;

        $info = $data->getTraderDatabase($this->trader_name);
        if (!$info['login']) {
            $info = $data->getDataBase($this->trader_name);
        }

        $report_server_class = (new ReportServer($info['platform']))->getClass();
        $reportserver = new $report_server_class($info['database'], $info['login']);
        if ($this->tickets) {
            foreach ($this->tickets as $ticket) {
                $filters_array[] = $reportserver->result->ObjectsFilter('Position', 'eq', $ticket);
            }
            $request = $reportserver->result->addFilter('or', $filters_array, $request);
        }
        $equity = $reportserver->get_user_equity();
        $data = $reportserver->getTrades($request);
        $data = (array) json_decode($data);
        $result = [];
        if ($data['data']) {
            foreach ($data['data'] as $d) {
                $result[$d->Ticket] = ['profit' => $d->Profit, 'pips' => $d->Pips, 'equity' => $equity];
            }
        }

        return json_encode($result);
    }

}
