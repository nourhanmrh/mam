<?php

namespace common\controllers;

use lajax\translatemanager\helpers\Language;

class AppController extends \yii\web\Controller
{

 	public function init() {
        // Language::registerAssets();
        parent::init();
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

}
