<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\tradersinfoTradersInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="traders-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'trader_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'trader_name') ?>

    <?= $form->field($model, 'investors') ?>

    <?php // echo $form->field($model, 'profit') ?>

    <?php // echo $form->field($model, 'roi') ?>

    <?php // echo $form->field($model, 'country_code') ?>

    <?php // echo $form->field($model, 'country_name') ?>

    <?php // echo $form->field($model, 'profile_pic') ?>

    <?php // echo $form->field($model, 'is_live') ?>

    <?php // echo $form->field($model, 'trusting') ?>

    <?php // echo $form->field($model, 'ea') ?>

    <?php // echo $form->field($model, 'joined_at') ?>

    <?php // echo $form->field($model, 'left_at') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'equity') ?>

    <?php // echo $form->field($model, 'rate') ?>

    <?php // echo $form->field($model, 'logged') ?>

    <?php // echo $form->field($model, 'strategy') ?>

    <?php // echo $form->field($model, 'minpeak') ?>

    <?php // echo $form->field($model, 'maxpeak') ?>

    <?php // echo $form->field($model, 'maxddvalue') ?>

    <?php // echo $form->field($model, 'ten_month_chart') ?>

    <?php // echo $form->field($model, 'comission_received') ?>

    <?php // echo $form->field($model, 'comission_pending') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
