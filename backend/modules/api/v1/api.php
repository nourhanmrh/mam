<?php

namespace backend\modules\api\v1;

/**
 * API module definition class
 */
class api extends \yii\base\Module
{

    
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\api\v1\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
