<?php

namespace backend\modules\api\v1\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Application;
use yii\web\NotFoundHttpException;
use yii\db\Query;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use common\models\helpers\AuthManager;
use yii\helpers\ArrayHelper;
use common\models\accounts\Accounts;
use common\models\users\Users;
use common\models\mamaccounts\MamAccounts;
use common\models\codes\Codes;
use common\models\mamtrades\MamTrades;
use common\models\mamupdatetrades\MamUpdateTrades;
use common\models\response\Response;
use common\models\mamrefreshtrades\MamRefreshTrades;
use common\models\relations\Relations;
use common\models\mamclosetrades\MamCloseTrades;
use common\models\mamtradesinfo\MamTradeInfo;
use common\models\mam\Mam;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use common\filters\auth\HttpBearerAuth;
use common\models\ReportServer\ReportServer;

class TradesController extends Controller {

    public $modelClass = 'app\models\MamTrades';

    public function __construct($id, $module, $config = []) {
        parent::__construct($id, $module, $config);
    }

    public function actions() {
        return [];
    }

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
            ],
        ];


        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['get', 'post'],
                'History' => ['get', 'post']
            ],
        ];

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = [
            'index',
            'history'
        ];

        // setup access
        // $behaviors['access'] = [
        //     'class' => AccessControl::className(),
        //     'only' => ['index', 'view', 'create', 'update', 'delete','profile'], //only be applied to
        //     'rules' => [
        //         [
        //             'allow' => true,
        //             'actions' => ['index', 'view', 'create', 'update', 'delete'],
        //             'roles' => ['admin', 'manageUsers'],
        //         ],
        //         [
        //             'allow' => true,
        //             'actions' => ['profile, login, signup'],
        //             'roles' => ['*']
        //         ]
        //     ],
        // ];

        return $behaviors;
    }

    public function actionOpenTrade() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $MamAccount = new MamAccounts();

        $user = $user = Users::findIdentity(\Yii::$app->user->getId());

        $active = $MamAccount->getActiveAccountInfo($user);

        $request = Yii::$app->request->post();

        $response = \Yii::$app->getResponse();

        if ($request['name'] == $active['name']) {
            $info = $MamAccount->getDataBase($request['name']);

            $report_server_class = (new ReportServer($info['platform']))->getClass();
            $reportserver = new $report_server_class($info['database'], $info['login']);

            $action = $reportserver->openTrade($request);
            if ($action) {
                $response->setStatusCode(200);
                $response->content = "Open Trade Success";
                return $action;
                 
            } else {
                $response->setStatusCode(500);
                $response->content = "Open Trade Failed";
            }

           
        } else {
            $response->setStatusCode(500);
            $response->content = "Open Trade Failed";
        }

        return $response;
    }

    public function actionModifyTrade() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $MamAccount = new MamAccounts();

        $user = $user = Users::findIdentity(\Yii::$app->user->getId());

        $active = $MamAccount->getActiveAccountInfo($user);

        $request = Yii::$app->request->post();

        $response = \Yii::$app->getResponse();

        if ($request['name'] == $active['name']) {
            $info = $MamAccount->getDataBase($request['name']);
            $report_server_class = (new ReportServer($info['platform']))->getClass();
            $reportserver = new $report_server_class($info['database'], $info['login']);

            $action = $reportserver->modifyTrade($request);
            if ($action) {
                $response->setStatusCode(200);
                $response->content = "Modify Trade Success";
            } else {
                $response->setStatusCode(500);
                $response->content = "Modify Trade Failed";
            }

            return $action;
        } else {
            $response->setStatusCode(500);
            $response->content = "Modify Trade Failed";
        }

        return $response;
    }

    public function actionCloseTrade() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $MamAccount = new MamAccounts();

        $user = $user = Users::findIdentity(\Yii::$app->user->getId());

        $active = $MamAccount->getActiveAccountInfo($user);

        $request = Yii::$app->request->post();

        $response = \Yii::$app->getResponse();

        if ($request['name'] == $active['name']) {
            $info = $MamAccount->getDataBase($request['name']);
            $report_server_class = (new ReportServer($info['platform']))->getClass();
            $reportserver = new $report_server_class($info['database'], $info['login']);

            $action = $reportserver->closeTrade($request);
            if ($action) {
                $response->setStatusCode(200);
                $response->content = "Close Trade Success";
            } else {
                $response->setStatusCode(500);
                $response->content = "Close Trade Failed";
            }

            return $action;
        } else {
            $response->setStatusCode(500);
            $response->content = "Close Trade Failed";
        }

        return $response;
    }

    public function getType($cmd) {

        if ($cmd == "0") {
            return "buy";
        } elseif ($cmd == "1") {
            return "sell";
        }
    }

}
