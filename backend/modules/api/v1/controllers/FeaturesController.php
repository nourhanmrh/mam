<?php

namespace backend\modules\api\v1\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;
use yii\web\Controller;
use common\models\users\Users;

class FeaturesController extends Controller {

    public function actionIndex() {
        $user = Users::findIdentity(\Yii::$app->user->getId());

        if ($user) {

            $result = array(
                array(
                    'feature' => 'TradersFilter',
                    'description' => 'Filter traders in leaderboard',
                    'allowed' => 'yes'
                ),
                array(
                    'feature' => 'TraderAnalytics',
                    'description' => 'Analytics for trader in leaderboard',
                    'allowed' => 'no'
                ),
                array(
                    'feature' => 'TraderFollow',
                    'description' => 'Follow traders in leaderboard',
                    'allowed' => 'yes'
                ),
                array(
                    'feature' => 'TraderTrades',
                    'description' => 'Show trader trades in leaderboard',
                    'allowed' => 'yes'
                ),
                array(
                    'feature' => 'HistoryFilter',
                    'description' => 'history filter in profile',
                    'allowed' => 'yes'
                ),
                array(
                    'feature' => 'OpenTrades',
                    'description' => 'Open trades manualy',
                    'allowed' => 'yes'
                ),
                array(
                    'feature' => 'CloseTrades',
                    'description' => 'Close trades manualy',
                    'allowed' => 'yes'
                ),
                array(
                    'feature' => 'ModofyTrades',
                    'description' => 'Modify trades manualy',
                    'allowed' => 'yes'
                ),
            );

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(200);

            $responseData = 'true';

            $features = json_encode($result);

            return $features;
        } else {

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(500);

            $responseData = 'false';

            // Validation error
            throw new NotFoundHttpException('Object not found');
        }
    }

}
