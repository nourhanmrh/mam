<?php

namespace backend\modules\api\v1\controllers;

use yii\web\Controller;
use yii\rest\ActiveController;

/**
 * Default controller for the `API` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return 'Api';
    }
}
