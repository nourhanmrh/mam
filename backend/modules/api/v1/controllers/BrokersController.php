<?php

namespace backend\modules\api\v1\controllers;

use yii;
use yii\helpers\Url;
use common\helpers\Helper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use common\models\brokers\Brokers;
use common\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class BrokersController extends ActiveController {

    public $modelClass = 'app\models\Brokers';

    public function __construct($id, $module, $config = []) {
        parent::__construct($id, $module, $config);
    }

    public function actions() {
        return [];
    }

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
            ],
        ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['get']
            ],
        ];

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['index'
        ];

        return $behaviors;
    }

    public function actionAll() {
        $model = new Brokers();

        if (!Yii::$app->request->post()) {
            throw new HttpException(422, json_encode("Empty post Request"));
        }

        $request = json_decode(json_encode(Yii::$app->request->post()));
        $request->skip = ($request->page - 1) * $request->take;

        $helper = new Helper();

        if (array_key_exists('filter', (array) $request)) {
            $helper->filterRequestMapping($request->filter, Brokers::className());
        }

        $sort = [];
        $sort['field'] = 'AmountPaid';
        $sort['dir'] = 'desc';
        $request->sort = [(object) $sort];

        if (array_key_exists('sort', (array) $request)) {
            foreach ($request->sort as $key => $value) {
                $request->sort[$key]->field = Brokers::$map[$request->sort[$key]->field];
            }
        }

        $brokers = $model->getBrokersByKendo($request);

        if (!$brokers) {
            throw new HttpException(422, json_encode("Brokers Not Available at this Moment"));
        }

        $response = \Yii::$app->getResponse();
        $response->setStatusCode(200);
        $response->content = "Brokers successfuly retrieved";

        return $brokers;
    }

    public function actionIndex() {
        $model = new Brokers();

        $brokers = $model->get_brokers_details();

        if (!$brokers) {
            throw new HttpException(422, json_encode("Brokers Not Available at this Moment"));
        }

        $response = \Yii::$app->getResponse();
        $response->setStatusCode(200);
        $response->content = "Brokers successfuly retrieved";

        return $model->brokerMapping($brokers);
    }

    public function actionOptions($id = null) {
        return 'ok';
    }

}
