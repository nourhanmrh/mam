<?php

namespace backend\modules\api\v1\controllers;

use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use common\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use common\models\country\Country;
use common\models\symbols\Symbols;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class ConfigurationController extends ActiveController {

    public $modelClass = 'app\models\MamAccounts';

    public function __construct($id, $module, $config = []) {
        parent::__construct($id, $module, $config);
    }

    public function actions() {
        return [];
    }

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className()
            ],
        ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'country' => ['get'],
                'symbol' => ['get'],
                'sort' => ['get']
            ],
        ];

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['index'];

        return $behaviors;
    }

    public function actionCountry() {
        $model = new Country();

        $countries = $model->countriesForMobile();

        $response = \Yii::$app->getResponse();

        $response->setStatusCode(200);

        $responseData = 'true';

        $response->content = "Data Retrieved";

        $data = json_encode($countries);

        return $data;
    }

    public function actionSymbol() {
        $model = new Symbols();

        $symbols = $model->symbolsForMobile();

        $response = \Yii::$app->getResponse();

        $response->setStatusCode(200);

        $responseData = 'true';

        $response->content = 'Data Retrieved';

        $data = json_encode($symbols);

        return $data;
    }

    public function actionSort() {
        $data = array(
            'Roi' => 'Roi',
            'Investors' => 'Investors',
            'AverrageRate' => 'Rate',
            'AmountFollowing' => 'Amount Following'
        );

        $response = \Yii::$app->getResponse();

        $response->setStatusCode(200);

        $responseData = 'true';

        $response->content = "Data Retrieved";

        $sort = json_encode($data);

        return $sort;
    }

    public function actionAll(){

        $data = [
            "countries" => json_decode($this->actionCountry()) ,

            "symbols" => json_decode($this->actionSymbol()),

            "currencies" => [
                'USD' => 'USD',
                'EUR' => 'EUR',
                'GBP' => 'GBP'
            ],

            "leverages" => [
                '100' => '1:100',
                '200' => '1:200',
                '300' => '1:300',
                '400' => '1:400'
            ],

            "balance_list" => [
                '1000' => '1000',
                '5000' => '5000',
                '10000' => '10000',
                '20000' => '20000'
            ],

            "prices_dns" => ['url' => \Yii::$app->params['prices_dns'],
                             'port' => \Yii::$app->params['prices_port']],

            "lod_link" => ['url' => \Yii::$app->params['frontendURL'] . "/pdf/lod.pdf"],

            /*"platforms" => [
                'mt4' => 'MT4',
                'mt5' => 'MT5',
            ],*/
            "sort" => json_decode($this->actionSort())
        ];
        
        $response = \Yii::$app->getResponse();

        $response->setStatusCode(200);

        $responseData = 'true';

        $response->content = "Configuration data retrieved";
        
        return json_encode($data);
    }

}
