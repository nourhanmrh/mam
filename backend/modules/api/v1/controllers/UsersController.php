<?php

namespace backend\modules\api\v1\controllers;

use Yii;
use yii\web\Session;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;
use yii\rest\ActiveController;
use common\models\users\Users;
use common\models\demo\Demo;
use common\models\forms\UsersEditForm;
use common\models\mamaccounts\MamAccounts;
use common\models\userssearch\UsersSearch;
use common\models\forms\ConfirmationForm;
use common\models\forms\LoginForm;
use common\models\forms\SignupForm;
use common\models\forms\SignupConfirmForm;
use common\models\forms\PasswordResetForm;
use common\models\forms\PasswordResetRequestForm;
use common\models\forms\PasswordResetTokenVerificationForm;
use common\models\Constants;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use common\filters\auth\HttpBearerAuth;
use yii\db\Query;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\base\ErrorException;

class UsersController extends ActiveController {

    public $modelClass = 'common\models\users\Users';

    public function __construct($id, $module, $config = []) {
        parent::__construct($id, $module, $config);
    }

    public function actions() {
        return [];
    }

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
            ],
        ];


        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['get'],
                'view' => ['get'],
                'create' => ['post'],
                'update' => ['put'],
                'delete' => ['delete'],
                'login' => ['post'],
                'profile' => ['get', 'post'],
                'reset' => ['post']
            ],
        ];

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = [
            'options',
            'login',
            'signup',
            'confirm',
            'reset',
            'reset-password',
            'password-reset-request'
        ];

        // setup access
        // $behaviors['access'] = [
        //     'class' => AccessControl::className(),
        //     'only' => ['index', 'view', 'create', 'update', 'delete','profile'], //only be applied to
        //     'rules' => [
        //         [
        //             'allow' => true,
        //             'actions' => ['index', 'view', 'create', 'update', 'delete'],
        //             'roles' => ['admin', 'manageUsers'],
        //         ],
        //         [
        //             'allow' => true,
        //             'actions' => ['profile, login, signup'],
        //             'roles' => ['*']
        //         ]
        //     ],
        // ];

        return $behaviors;
    }

    public function actionIndex() {
        $search = new UsersSearch();
        $search->load(\Yii::$app->request->get());
        if (!$search->validate()) {
            throw new BadRequestHttpException(
            'Invalid parameters: ' . json_encode($search->getErrors())
            );
        }

        return $search->getDataProvider();
    }

    Public Function actionLogin() {
        $model = new LoginForm();
        $model->types = Constants::USERS_FRONTEND_TYPES;
        $response = \Yii::$app->getResponse();
        if ($model->load(Yii::$app->request->post()) && $model->login()) { 
            $user = $model->getUser();
            $user->generateAccessTokenAfterUpdatingClientInfo(true);

            $response->setStatusCode(202);
            $id = implode(',', array_values($user->getPrimaryKey(true)));
            $response->content = "Login is done";

            $account = new MamAccounts(); 
            $usr = Users::findIdentity(\Yii::$app->user->getId());          
            $active_account = $account->getActiveAccountInfo($usr);

            $responseData = [
                'id' => (int) $id,
                'token_expiration' => Yii::$app->params['token_age'],
                'access_token' => $user->access_token,
                'active_account' => $account->profileMapping([$active_account])
            ];

            return $responseData;
        } else {
            $response->setStatusCode(400);
            $response->content = "Login is failed";
            $responseData = $model->errors;

            return $responseData;
            // Validation error
            //throw new HttpException(422, json_encode($model->errors));
        }
    }

    /**
     * Process user sign-up
     *
     * @return string
     * @throws HttpException
     */
    public function actionSignup() {
        $model = new SignupForm();

        $confirm = new ConfirmationForm();

        $model->load(Yii::$app->request->post());

        $response = \Yii::$app->getResponse();

        $model->type = "";

        if ($model->validate() && $model->signup()) {

            $user = $model->getUser();
            $user->type = Constants::USER_TYPE_CLIENT;

            if ($model->getUser()) {

                $id = $model->getUser()->id;

                $demo = new MamAccounts();

                $r = $demo->createDemo($id);

                if ($r) {
                    $confirm->sendRegistrationEmail($user);
                }
            }

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(201);

            $response->content = 'User Created, Check your email to confirm your account';

            return $response;
        } else {

            $response->setStatusCode(400);

            $response->content = 'This email it not registered';

            return $model->errors;
        }
    }

    public function actionPasswordResetRequest() {

        $model = new PasswordResetRequestForm();

        $model->load(Yii::$app->request->post());

        $response = \Yii::$app->getResponse();

        if ($model->validate() && $model->sendPasswordResetEmail()) {

            $response->setStatusCode(200);

            $response->content = 'Check you email to reset your password';
        } else {

            // // Validation error
            // throw new HttpException(422, json_encode($model->errors));
            $response->setStatusCode(400);
            $response->content = 'This email doesnt exist';
        }

        return $response;
    }

    // /**
    //  * Process password reset
    //  *
    //  * @return string
    //  * @throws HttpException
    //  */
    // public function actionPasswordReset()
    // {
    //     $model = new PasswordResetForm();
    //     $model->load(Yii::$app->request->post());
    //     if ($model->validate() && $model->resetPassword()) {
    //         $response = \Yii::$app->getResponse();
    //         $response->setStatusCode(200);
    //         $responseData = 'true';
    //         return $responseData;
    //     } else {
    //         // Validation error
    //         throw new HttpException(422, json_encode($model->errors));
    //     }
    // }

    /**
      //  * Verify password reset token
      //  *
      //  * @return string
      //  * @throws HttpException
      // */
    // public function actionPasswordResetTokenVerification() {
    //     $model = new PasswordResetTokenVerificationForm();
    //     $model->load(Yii::$app->request->post());
    //     if ($model->validate() && $model->validate()) {
    //         $response = \Yii::$app->getResponse();
    //         $response->setStatusCode(200);
    //         $responseData = 'true';
    //         return $responseData;
    //     } else {
    //         // Validation error
    //         throw new HttpException(422, json_encode($model->errors));
    //     }
    // }

    public function actionConfirm($key) {

        $query = Users::find()->where("confirmation_key ='" . $key . "'")->one();

        if ($query) {
            $x = Users::findOne($query['id']);
            $x->confirmed_at = date('Y-m-d H:i:s');
            $x->status = 3;
            $x->confirmation_key = '';
            if (!$x->save()) {
                print_r($x->errors);
            }
        }
    }

    public function actionReset() {
        $model = new PasswordResetRequestForm();
        $x = Yii::$app->getRequest()->getCookies()->getValue('verification');
        if ($x == 'pending') {
            $status = 'pending';
            return $this->redirect('login');
        } else {
            $status = 'completed';

            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate() && $model->sendPasswordResetEmail()) {
                    $response = \Yii::$app->getResponse();
                    $response->setStatusCode(200);
                }
            }
        }
        return $this->render('reset', ['model' => $model, 'status' => $status]);
    }

    public function actionProfile() {
        $user = Users::find()->select('name,username,email,phone,description')->where('id = ' . \Yii::$app->user->getId())->asArray()->one();
        $response = \Yii::$app->getResponse();
        $response->setStatusCode(200);
        $response->content = "User profile";
        return $user;
    }

    // $model = new SignupConfirmForm();
    // $model->load(Yii::$app->request->get('key'));
    // if ($model->validate() && $model->confirm()) {
    //     $response = \Yii::$app->getResponse();
    //     $response->setStatusCode(200);
    //     $user = $model->getUser();
    //     $responseData = [
    //         'id' => (int) $id,
    //         'token expiration' => Yii::$app->params['token_age'],
    //         'access_token' => $user->access_token,
    //     ];
    //     return $responseData;
    // } else {
    //     // Validation error
    //     throw new HttpException(422, json_encode($model->errors));
    // }
    // }
}
