<?php

namespace backend\modules\api\v1\controllers;

use Yii;
use common\models\mamaccounts\MamAccounts;
use common\models\users\Users;
use common\models\traders\Traders;
use common\models\brokers\Brokers;
use common\models\rates\Rates;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\helpers\Url;
use yii\web\Session;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use common\models\ReportServer\ReportServer;
use common\helpers\Helper;

class ProfileController extends ActiveController {

    public $modelClass = 'app\models\MamAccounts';

    public function __construct($id, $module, $config = []) {
        parent::__construct($id, $module, $config);
    }

    public function actions() {
        return [];
    }

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
            ],
        ];


        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'profile' => ['post']
            ],
        ];

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = [
            'live'
        ];

        // setup access
        // $behaviors['access'] = [
        //     'class' => AccessControl::className(),
        //     'only' => ['index', 'view', 'create', 'update', 'delete','profile'], //only be applied to
        //     'rules' => [
        //         [
        //             'allow' => true,
        //             'actions' => ['index', 'view', 'create', 'update', 'delete'],
        //             'roles' => ['admin', 'manageUsers'],
        //         ],
        //         [
        //             'allow' => true,
        //             'actions' => ['profile, login, signup'],
        //             'roles' => ['*']
        //         ]
        //     ],
        // ];

        return $behaviors;
    }

    public function actionProfile() {
        $post = Yii::$app->request->post();

        $user = Users::findIdentity(\Yii::$app->user->getId());

        $account = new MamAccounts();

        $active_account = $account->getActiveAccountInfo($user);


        $response = \Yii::$app->getResponse();

        if ($post['name'] == $active_account['name']) {

            $response->setStatusCode(200);

            $result = $account->profileMapping(array($active_account));

            $response->content = 'User Profile Retrieved';

//         $result = $active_account;

            return $result;
        } else {

            $response->setStatusCode(400);

            $response->content = 'Something Went Wrong';
        }
    }

    public function actionFollowedTraders() {
        $post = Yii::$app->request->post();

        $active = Traders::get_active();

        $response = \Yii::$app->getResponse();

        if ($post['name'] == $active['name']) {

            $res = Traders::get_followed_trader_details($active['id']);

            if ($res) {

                foreach ($res['data'] as $key => $value) {
                    $res['data'][$key]['CountryCode'] = strtolower($res['data'][$key]['CountryCode']);
                    $res['data'][$key]['FollowStatusText'] = strtolower($res['data'][$key]['FollowStatusText']);
                    // $res['data'][$key]['Symbols'] = "GBPUSD";
//$res['data'][$key]['Profit'] = 34225;
//$res['data'][$key]['Investors'] = 6;
                    $res['data'][$key]['AverrageRate'] = 2;
//$res['data'][$key]['AmountFollowing'] = 4000;
//$res['data'][$key]['BrokerPic'] = 'http://newmam.equitick.com/logo/logo.png';
//$res['data'][$key]['Picture'] = 'http://newmam.equitick.com/logo/logo.png';
                    //unset($res['data'][$key]['IsLogged']);
                    //unset($res['data'][$key]['Trusted']);
                    //unset($res['data'][$key]['ActiveAccountStatus']);
                    //unset($res['data'][$key]['ActiveAccountName']);
                    //unset($res['data'][$key]['ActiveAccountType']);
                    //unset($res['data'][$key]['ReceivedCommission']);
                    //unset($res['data'][$key]['PendingCommission']);
                    //unset($res['data'][$key]['OpenPositions']);
                    //unset($res['data'][$key]['AvgPips']);
                    //unset($res['data'][$key]['WiningTrades']);
                    //unset($res['data'][$key]['MaxDrawdown']);
                    //unset($res['data'][$key]['TotalProfitPips']);
                    //unset($res['data'][$key]['LastUpdated']);
                    //unset($res['data'][$key]['FollowingDetailsLabel']);
                    //unset($res['data'][$key]['FollowingDetails']);
                    //unset($res['data'][$key]['Trades']);
                }

                $result = json_encode($res);

                $response->setStatusCode(200);

                $response->content = 'Followed Traders Retrieved';

                return $result;
            } else {
                $response->setStatusCode(200);

                $response->content = 'NO Followed Traders';

                return [];
            }
        } else {
            $response->setStatusCode(400);
            $response->content = 'Something Went Wrong';
        }
    }

    public function actionTrades() {
        if (!Yii::$app->request->post()) {
            throw new HttpException(422, json_encode("Empty post Request"));
        }

        $request = json_decode(json_encode(Yii::$app->request->post()));

        $request->skip = ($request->page - 1) * $request->take;

        if (!$request->name) {

            throw new HttpException(422, json_encode("No Name Provided"));
        }

        if (!$request->type) {

            throw new HttpException(422, json_encode("No type Provided"));
        }

        if (!$request->time) {

            throw new HttpException(422, json_encode("No time Provided"));
        }

        $helper = new Helper();

        $data = new MamAccounts();
        $info = $data->getDataBase($request->name);
        if ($info['login']) {
            $report_server_class = (new ReportServer($info['platform']))->getClass();
            $reportserver = new $report_server_class($info['database'], $info['login']);

            if (array_key_exists('filter', (array) $request)) {
                $helper->filterRequestMapping($request->filter, $report_server_class);
            }

            if (array_key_exists('sort', (array) $request)) {
                foreach ($request->sort as $key => $value) {
                    $request->sort[$key]->field = $report_server_class::$map[$request->sort[$key]->field];
                }
            }

            $res = $reportserver->getTrades($request);

            return $res;
        }
        return [];
    }

    public function actionSwitchAccount() {

        $model = new MamAccounts();

        $user = Users::findIdentity(\Yii::$app->user->getId());

        $name = Yii::$app->request->get();

        $account = $model->getAccountByNameAndUser($name, $user['id']);

        if ($user && $account) {

            $accounts = new MamAccounts();

            $data = $accounts->switchAccount($user, $name);

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(200);

            $response->content = 'Select Account Success';

            $result = json_encode($data);

            return $result;
        } else {

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(500);

            $responseData = 'false';

            throw new NotFoundHttpException('Switch Account Failed');
        }
    }

    public function actionAllAccounts() {
        $user = Users::findIdentity(\Yii::$app->user->getId());

        if ($user) {

            $accounts = new MamAccounts();

            $data = $accounts->getAllAccounts($user);

            $data = $accounts->profileMapping($data);

            foreach ($data as $k => $v) {
                $data[$k]['Currency'] = null;
            }

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(200);

            $responseData = 'true';

            $result = json_encode($data);

            return $result;
        } else {

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(500);

            $responseData = 'false';

            // Validation error
            throw new NotFoundHttpException('Object not found');
        }
    }

    public function actionCreateDemo() {
        $user = Users::findIdentity(\Yii::$app->user->getId());
        $post = Yii::$app->request->post();
        $account = new MamAccounts();
        $active = $account->getActiveAccountInfo($user);
        $response = \Yii::$app->getResponse();

        if ($post['name']) {
            if ($active['name'] == $post['name']) {
                if (!$active['is_live']) {
                    if ($active['account_status'] == \common\models\codes\Codes::ACCOUNT_NOT_COMPLETED) {
                        $r = $account->demo($post);
                        if ($r) {
                            $response->setStatusCode(200);
                            $response->content = "Complete Demo Account Success";
                        } else {
                            $response->setStatusCode(400);
                            $response->content = "Complete Demo Account Failed";
                        }
                    } else {
                        $response->setStatusCode(400);
                        $response->content = "The Account is completed";
                    }
                } else {
                    $response->setStatusCode(400);
                    $response->content = "The Account is not demo";
                }
            } else {
                $response->setStatusCode(400);
                $response->content = "The Account Is Not Active";
            }
        } else {
            $r = $account->demo($post);
            if ($r) {
                $response->setStatusCode(200);
                $response->content = "Create Demo Account Success";
            } else {
                $response->setStatusCode(400);
                $response->content = "Create Demo Account Failed";
            }
        }


        return $response;
    }

    public function actionLiveRequest() {
        $user = Users::findIdentity(\Yii::$app->user->getId());

        $broker = Yii::$app->request->post();

        $broker_Id = new Brokers();

        $brokerId = $broker_Id->getBrokerId($broker['broker']);

        $account = new MamAccounts();

        $live = $account->createLive($user['id'], $brokerId);

        if ($live) {

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(200);

            $response->content = "Live Account request sent";
        } else {

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(400);

            $response->errors = "Account has not created";
        }
    }

    public function actionCompleteLive() {
        $response = \Yii::$app->getResponse();
        $user = Users::findIdentity(\Yii::$app->user->getId());

        $post = Yii::$app->request->post();

        if (!$post['name']) {
            $response->setStatusCode(400);
            $response->content = "Name Not Exist";
        } else {
            $account = new MamAccounts();
            $active = $account->getActiveAccountInfo($user);

            $post['broker'] = $active['broker']['broker_name'];
            if ($active['name'] == $post['name']) {
                if ($active['account_status'] == \common\models\codes\Codes::ACCOUNT_NOT_COMPLETED) {

                    $account->completeLiveAccount($post);
                    $response->setStatusCode(200);
                    $response->content = "Live Account Success";
                    return $account->profileMapping([$active]);
                } else {
                    $response->setStatusCode(400);
                    $response->content = "The Account Waiting Confirmation";
                }
            } else {
                $response->setStatusCode(400);
                $response->content = "The Account Is Not Active";
            }
        }
        return $response;
    }

    public function actionCreateNonCompleteAccounts($type) {
        $response = \Yii::$app->getResponse();
        $account = new MamAccounts();
        if ($type == "live") {
            $account->createLive(\Yii::$app->user->getId(), 2);
            $response->content = "Create live account success";
        } elseif ($type == "demo") {
            $account->createDemo(\Yii::$app->user->getId());
            $response->content = "Create demo account success";
        } else {
            $response->setStatusCode(200);
            $response->content = "Type Not Exist";
        }
        return $response;
    }

    public function actionSymbol() {
        $user = Users::findIdentity(\Yii::$app->user->getId());

        $account = new MamAccounts();

        $name = Yii::$app->request->post('name');

        $info = $account->getDataBase($name);

        $report_server_class = (new ReportServer($info['platform']))->getClass();
        $reportserver = new $report_server_class($info['database'], $info['login']);

        $response = \Yii::$app->getResponse();

        $response->setStatusCode(200);

        $response->content = "Symbols Retrieved";

        return $reportserver->getSymbols();
    }

    public function actionRating() {
        $user = Users::findIdentity(\Yii::$app->user->getId());

        $account = new MamAccounts();

        $active = $account->getActiveAccountInfo($user);

        $post = Yii::$app->request->post();

        $model = new Rates();

        $rate = $model->setRating($user['id'], $active['login'], $post);

        if ($rate == 'done') {

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(200);

            $response->content = "Rating succesfull";

            return $rate;
        } else {

            $response = \Yii::$app->getResponse();

            $response->setStatusCode(400);

            $response->content = "Rating failed";
        }
    }

}
