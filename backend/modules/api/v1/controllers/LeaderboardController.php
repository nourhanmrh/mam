<?php

namespace backend\modules\api\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use common\models\accounts\Accounts;
use common\models\users\Users;
use common\models\mamaccounts\MamAccounts;
use common\models\codes\Codes;
use common\models\mamtrades\MamTrades;
use common\models\mamupdatetrades\MamUpdateTrades;
use common\models\response\Response;
use common\models\mamrefreshtrades\MamRefreshTrades;
use common\models\relations\Relations;
use common\models\mamclosetrades\MamCloseTrades;
use common\models\mamtradeinfo\MamTradeInfo;
use common\models\traders\Traders;
use common\models\tradersinfo\TradersInfo;
use common\models\rates\Rates;
use common\models\reportserver\ReportServer;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use common\filters\auth\HttpBearerAuth;
use yii\data\ActiveDataProvider;
use common\helpers\Helper;
use common\services\MamAccountAction;

class LeaderboardController extends ActiveController {

    public $modelClass = 'app\models\MamAccounts';

    public function __construct($id, $module, $config = []) {
        parent::__construct($id, $module, $config);
    }

    public function actions() {
        return [];
    }

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
            ],
        ];


        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['post', 'get'],
                'positions' => ['post', 'get'],
                'chart' => ['get'],
                'charttenmonth' => ['get'],
                'chartcustom' => ['get', 'post'],
                'traders-info' => ['post', 'get'],
                'relation' => ['post'],
                'toptraders' => ['get'],
                'createtrader' => ['post']
            ]
        ];

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = [
            'create-trader'
        ];

        // setup access
        // $behaviors['access'] = [
        //     'class' => AccessControl::className(),
        //     'only' => ['index', 'view', 'create', 'update', 'delete', 'profile'], //only be applied to
        //     'rules' => [
        //         [    
        //             'allow' => true,
        //             'actions' => ['index', 'view', 'create', 'update', 'delete'],
        //             'roles' => ['admin', 'manageUsers'],
        //         ],
        //         [
        //             'allow' => true,
        //             'actions' => ['profile, login, signup'],
        //             'roles' => ['*']
        //         ]
        //     ],
        // ];

        return $behaviors;
    }

    public function actionTrades() {
        if (!Yii::$app->request->post()) {
            throw new HttpException(422, json_encode("Empty post Request"));
        }

        $request = json_decode(json_encode(Yii::$app->request->post()));

        $request->skip = ($request->page - 1) * $request->take;

        if (!$request->name) {
            throw new HttpException(422, json_encode("No Name Provided"));
        }

        if (!$request->type) {
            throw new HttpException(422, json_encode("No type Provided"));
        }

        if (!$request->time) {
            throw new HttpException(422, json_encode("No time Provided"));
        }
        $data = new MamAccounts();

        $info = $data->getTraderDataBase($request->name);
        if ($info) {
            $report_server_class = (new ReportServer($info['platform']))->getClass();

            $helper = new Helper();

            if (array_key_exists('filter', (array) $request)) {
                $helper->filterRequestMapping($request->filter, $report_server_class);
            }

            if (array_key_exists('sort', (array) $request)) {
                foreach ($request->sort as $key => $value) {
                    $request->sort[$key]->field = $report_server_class::$map[$request->sort[$key]->field];
                }
            }

            $reportserver = new $report_server_class($info['database'], $info['login']);

            $res = $reportserver->getTrades($request);


            $response = \Yii::$app->getResponse();
            $response->setStatusCode(200);
            $response->content = "Trades retrieved";

            return $res;
        }
        return [];
    }

    public function actionChartCustom($time, $name) {

        $data = new MamAccounts();

        $info = $data->getTraderDataBase($name);

        $report_server_class = (new ReportServer($info['platform']))->getClass();
        $reportserver = new $report_server_class($info['database'], $info['login']);

        $response = \Yii::$app->getResponse();
        $response->setStatusCode(200);
        $response->content = "Chart Custom";

        return $reportserver->getChartCustom($time);
    }

    public function actionTradersInfo() {
        if (!Yii::$app->request->post()) {
            throw new HttpException(422, json_encode("Empty post Request"));
        }

        $post = Yii::$app->request->post();

        $request = json_decode(json_encode(Yii::$app->request->post()));

        $request->skip = ($request->page - 1) * $request->take;

        $tradersinfo = new TradersInfo();

        $helper = new Helper();

        if (array_key_exists('filter', (array) $request)) {
            $helper->filterRequestMapping($request->filter, TradersInfo::className());
        }

        if (array_key_exists('sort', (array) $request)) {
            foreach ($request->sort as $key => $value) {
                $request->sort[$key]->field = TradersInfo::$map[$request->sort[$key]->field];
            }
        }

        $res = $tradersinfo->getTradersInfo($request, $post);

        foreach ($res['data'] as $key => $value) {
            $res['data'][$key]['CountryCode'] = strtolower($res['data'][$key]['CountryCode']);
            $res['data'][$key]['FollowStatusText'] = strtolower($res['data'][$key]['FollowStatusText']);
            // $res['data'][$key]['Symbols'] = "GBPUSD";
            //$res['data'][$key]['Profit'] = 34225;
            //$res['data'][$key]['Investors'] = 6;
            $res['data'][$key]['AverrageRate'] = 2;
            //$res['data'][$key]['AmountFollowing'] = 4000;
            //$res['data'][$key]['BrokerPic'] = 'http://newmam.equitick.com/logo/logo.png';
            //$res['data'][$key]['Picture'] = 'http://newmam.equitick.com/logo/logo.png';
            //unset($res['data'][$key]['IsLogged']);
            //unset($res['data'][$key]['Trusted']);
            //unset($res['data'][$key]['ActiveAccountStatus']);
            //unset($res['data'][$key]['ActiveAccountName']);
            //unset($res['data'][$key]['ActiveAccountType']);
            //unset($res['data'][$key]['ReceivedCommission']);
            //unset($res['data'][$key]['PendingCommission']);
            //unset($res['data'][$key]['OpenPositions']);
            //unset($res['data'][$key]['AvgPips']);
            //unset($res['data'][$key]['WiningTrades']);
            //unset($res['data'][$key]['MaxDrawdown']);
            //unset($res['data'][$key]['TotalProfitPips']);
            //unset($res['data'][$key]['LastUpdated']);
            //unset($res['data'][$key]['FollowingDetailsLabel']);
            //unset($res['data'][$key]['FollowingDetails']);
            //unset($res['data'][$key]['Trades']);
        }

        $response = \Yii::$app->getResponse();

        $response->content = 'Traders information';

        return $res;
    }

    public function actionRelation() {
        $user = Users::findIdentity(\Yii::$app->user->getId());

        $active = Traders::get_active();

        if (!Yii::$app->request->post()) {
            throw new HttpException(422, json_encode("Empty post Request"));
        }

        $post = Yii::$app->request->post();

        $trader = new Traders();

        $result = "";
        $type = "";

        if ($post['action'] == Relations::ACTION_FOLLOW && $active['name'] == $post['slave_name']) {
            $type = MamAccountAction::FOLLOW_TYPE; 
            $result = $trader->follow($post['trader_name'], $post['slave_name'], $post['copy_type'], $post['value']);
        } elseif ($post['action'] == Relations::ACTION_UNFOLLOW) {
            $type = MamAccountAction::UNFOLLOW_TYPE;
            $result = $trader->unfollow($post['trader_name'], $post['slave_name']);
        }

        $response = \Yii::$app->getResponse();

        if ($result) {
            $response->setStatusCode(201);
            $response->content = 'Success ' . $type;
        } else {
            $response->setStatusCode(500);
            $response->content = $type . " Operation Failed";
        }

        return $response;
    }

    public function actionTopTraders() {
        if (!Yii::$app->request->get()) {
            throw new HttpException(422, json_encode("Empty get Request"));
        }

        $get = Yii::$app->request->get();

        $data = $data->getTradersByRatingOrProfit($get['type']);

        $result = json_encode($data);

        return $result;
    }

    public function actionCreateTrader() {
        $str = 'abcdefghijkl';

        for ($i = 1; $i < 50; $i++) {
            $mam_account = new MamAccounts();
            $mam_account->name = "Trader " . $i;
            $mam_account->login = 607081;
            $mam_account->trades = rand(10, 100);
            $mam_account->balance = rand(10000, 100000);
            $mam_account->margin = 0;
            $mam_account->equity = rand(10000, 100000);
            $mam_account->leverage = 100;
            $mam_account->platform = "mt4";
            $mam_account->margin_percentage = rand(1000, 2000);
            $mam_account->account_type = "master";
            $mam_account->currency = "EUR";
            $mam_account->is_live = rand(0, 1);
            $mam_account->user_id = 3;
            $mam_account->brokers_id = 2;
            $mam_account->active_account = 0;
            $mam_account->account_status = "completed";
            if ($mam_account->save()) {
                $trader_info = new TradersInfo();
                $trader_info->trader_id = $mam_account->id;
                $trader_info->trader_name = $mam_account->name = "Trader " . $i;
                $trader_info->investors = rand(100, 4000);
                $trader_info->profit = rand(20000, 200000);
                $trader_info->roi = rand(1, 5);
                $trader_info->country_code = "lb";
                $trader_info->country_name = "lebanon";
                $trader_info->profile_pic = "323.jpg";
                $trader_info->is_live = rand(0, 1);
                $trader_info->trusting = null;
                $trader_info->ea = rand(0, 1);
                $trader_info->joined_at = date('Y-m-d H:i:s');
                $trader_info->left_at = null;
                $trader_info->currency = "EURUSD";
                $trader_info->equity = 10000;
                $trader_info->rate = rand(1, 5);
                $trader_info->logged = null;
                $trader_info->strategy = null;
                $trader_info->minpeak = 15555;
                $trader_info->maxpeak = 12312313;
                $trader_info->maxddvalue = 23132;
                $trader_info->save();
            } else {
                print_r($mam_account->errors);
                die;
            }
        }
    }

}
