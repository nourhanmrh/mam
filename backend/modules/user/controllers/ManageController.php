<?php

namespace backend\modules\user\controllers;

use yii;
use common\models\users\UsersSearch;
use common\models\country\Country;
use common\models\forms\PasswordResetRequestForm;
use common\models\forms\ConfirmationForm;
use kartik\grid\EditableColumnAction;
use yii\web\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use common\models\users\Users;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class ManageController extends \yii\web\Controller {

    public function actionIndex() {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Users();

        $model->generateAuthKey();

        $model->setConfirmationkey();

        $model->created_at = date("Y-m-d h:i:s");

        $model->registration_ip = Yii::$app->request->userIP;

        $users = Yii::$app->request->post('Users');

        if ($model->load(Yii::$app->request->post())) {
            $model->password = md5($model->password);
            $model->file = UploadedFile::getInstance($model, 'image_location');
            if ($model->file) {
                $model->image_location = $model->file->name;
                $model->save();
                $x = FileHelper::createDirectory(Yii::getAlias("@common/uploads/users/" . $model->id));
                $model->file->saveAs("@common/uploads/users/" . $model->id . "/" . $model->image_location);
            } else {
                print_r($model->errors);
                die;
            }
            if ($model->save()) {
                return $this->redirect('index');
            } else {
                print_r($model->errors);
                die;
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->password = "";
        $post = Yii::$app->request->post('Users');
        $img_old = $model->image_location;

        if ($model->load(Yii::$app->request->post())) {
            $model->password = md5($model->password);
            $model->file = UploadedFile::getInstance($model, 'image_location');
            if ($model->file) {
                $model->image_location = $model->file->name;
                $model->save();
                $x = FileHelper::createDirectory(Yii::getAlias("@common/uploads/users/" . $model->id));
                $model->file->saveAs("@common/uploads/users/" . $model->id . "/" . $model->image_location);
            } else {
                $model->image_location = $img_old;
            }
            if ($model->save()) {
                
            } else {
                print_r($model->errors);
            }
            return $this->render('update', ['model' => $model]);
        }

        return $this->render('update', ['model' => $model]);
    }

    protected function findModel($id) {

        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPasswordReset() { 

        $key = Yii::$app->request->post('id');

        foreach ($key as $id) {

            $user = Users::findOne($id);

            $model = new PasswordResetRequestForm();

            $model->email = $user['email'];
      
            $model->sendPasswordResetEmail();
        }
    }

    public function actionConfirmationEmail() {

        $key = Yii::$app->request->post('id');

        foreach ($key as $id) {

            $user = Users::findOne($id);

            $model = new ConfirmationForm();

            $model->sendRegistrationEmail($user);
        }
    }

    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findUserModel($id),
        ]);
    }

    protected function findUserModel($id) {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCountryList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q) && $q != "") {
            $query = new Query;
            $query->select([Country::tableName() . '.code AS id', Country::tableName() . '.name'])
                    ->from('country')
                    ->where(['like', Country::tableName() . '.code', $q])
                    ->orWhere(['like', Country::tableName() . '.name', $q])
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['code AS id' => $code, 'text' => Country::find($code)->name];
        } elseif (is_null($q) || $q == "") {
            $query = new Query;
            $query->select([Country::tableName() . '.code AS id', Country::tableName() . '.name'])
                    ->from('country')
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }

        foreach ($out['results'] as $key => $value) {
            $out['results'][$key]['text'] = $value['id'] . " - " . $value['name'];
            unset($out['results'][$key]['name']);
        }
        return $out;
    }

    public function actions() {
        return ArrayHelper::merge(parent::actions(), [
                    'edit' => [
                        'class' => EditableColumnAction::className(),
                        'modelClass' => Users::className(),
                        'outputValue' => function ($model, $attribute, $key, $index) {
                            return $model->$attribute;
                        },
                        'outputMessage' => function($model, $attribute, $key, $index) {
                            return '';
                        },
                        'showModelErrors' => true,
                        'errorOptions' => ['header' => '']
                    ],
        ]);
    }

}
