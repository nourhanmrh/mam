<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use common\models\Constants;
use kartik\widgets\FileInput;
use kartik\select2\Select2;
use yii\web\JsExpression;

?>
<style>label{
        color: #5d5d5d;
        font-size: 15px;
        }
</style>

    <div class="user-form">

    <div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">

        <div class="col-lg-2"></div>

        <div class="col-md-8">

             <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <div class="col-lg-6">
                <?= $form->field($model, 'name') ?>
            </div>
             <div class="col-lg-6">
                <?=
                $form->field($model, 'image_location')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                        'showUpload' => true,
                        'initialPreview' => [
                            Html::img(yii::$app->params['frontendURL'] . yii::$app->params['ImageUserPath'] . '?id=' . $model->id . '&size=SM'),
                        ],
                        'initialCaption' => $model->image_location,
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'username') ?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'email')->input('email') ?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'phone') ?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'type')->dropdownList(['client' => Constants::USER_TYPE_CLIENT, 'money manager' => Constants::USER_TYPE_MONEY_MANAGER, 'admin' => Constants::USER_TYPE_ADMIN, 'sales' => Constants::USER_TYPE_SALES], ['prompt' => 'Select Type..']); ?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'status')->dropdownList(['active' => Constants::USER_STATUS_ACTIVE, 'not active' => Constants::USER_STATUS_NOT_ACTIVE], ['prompt' => 'Select Status..']);?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'description') ?>
            </div>

                <div class="col-lg-6">
                    <?php
                    echo $form->field($model, 'country_code')->widget(Select2::classname(), [
                        'language' => 'en',
                        'maintainOrder' => true,
                        'options' => ['placeholder' => 'Search for a country ...', 'class' => 'kt-form'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::to(['country-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                'processResults' => new JsExpression('function(data) {return  data.data }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(result) {  return result.text; }'),
                            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                        ]
                    ]);
                    ?>
                </div>


            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>

</div>
