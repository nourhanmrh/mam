<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\editable\Editable;
use common\models\Constants;

$type = array('Client' => Constants::USER_TYPE_CLIENT, 'Money Manager' => Constants::USER_TYPE_MONEY_MANAGER, 'admin' => Constants::USER_TYPE_ADMIN, 'sales' => Constants::USER_TYPE_SALES);

$status = array('Active' => Constants::USER_STATUS_ACTIVE, 'Not Active' => Constants::USER_STATUS_NOT_ACTIVE);
?>

<?php \yii\widgets\Pjax::begin(['id' => 'users']); ?>
<div class="grid-scroll">

    <?=
    GridView::widget([
        'pjax' => true,
        'id' => 'users-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'perfectScrollbar' => true,
        'autoXlFormat' => true,
        'striped' => true,
        'hover' => true,
        'responsive' => true,
        'persistResize' => true,
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'heading' => 'Users Report',
            'before' => '<div><button onclick="resendConfirmation()">Resend Confirmation Email</button><button onclick="resendPasswordReset()">Resend Password Reset Email</button></div>'
                    ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'kartik\grid\CheckboxColumn',
            ],
            [
                'class' => '\kartik\grid\ActionColumn',
                'dropdown' => true
            ],
            [
                'attribute' => 'image_location',
                'label' => 'Image',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'format' => ['image'],
                'value' => function($data) {
                    return yii::$app->params['frontendURL'] . yii::$app->params['ImageUserPath'] . '?id=' . $data->id . '&size=SM';
                }
            ],
            [
                'attribute' => 'name',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'username',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'email',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'attribute' => 'auth_key',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:300px;'],
            ],
            [
                'attribute' => 'password_reset_token',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:100px;'],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'phone',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]]
            ],
            [
                'attribute' => 'confirmation_key',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:250px;'],
            ],
            [
                'attribute' => 'last_login_ip',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'filter' => $type,
                'attribute' => 'type',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']],
                    'inputType' => Editable::INPUT_DROPDOWN_LIST,
                    'data' => $type,
                ],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'filter' => $status,
                'attribute' => 'status',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:100px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']],
                    'inputType' => Editable::INPUT_DROPDOWN_LIST,
                    'data' => $status,
                ]
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'confirmed_at',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'label' => 'Confirmed At',
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['confirmed_at']]],
                'contentOptions' => ['style' => 'min-width:200px;'],
                'value' => function($model) {
                    return $model->confirmed_at;
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'confirmed_at',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'locale' => [
                            'format' => 'yyy-m-d H:i:s',
                            'datetimezone' => 'Asia/Beirut',
                        ],
                    ],
                ]),
            ],
            [
                'attribute' => 'last_login_at',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'label' => 'Last Login At',
                'contentOptions' => ['style' => 'min-width:200px;'],
                'value' => function($model) {
                    return $model->last_login_at;
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'last_login_at',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'locale' => [
                            'format' => 'Y-m-d H:i:s',
                            'datetimezone' => 'Asia/Beirut',
                        ],
                    ],
                ]),
            ],
            [
                'attribute' => 'created_at',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'label' => 'Created Date',
                'contentOptions' => ['style' => 'min-width:200px;'],
                'value' => function($model) {
                    return $model->created_at;
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'locale' => [
                            'format' => 'Y-m-d H:i:s',
                            'datetimezone' => 'Asia/Beirut',
                        ],
                    ],
                ]),
            ],
            [
                'attribute' => 'updated_at',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'label' => 'Updated Date',
                'contentOptions' => ['style' => 'min-width:200px;'],
                'value' => function($model) {
                    return $model->updated_at;
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'updated_at',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'locale' => [
                            'format' => 'yyyy-mm-dd',
                            'datetimezone' => 'Asia/Beirut',
                        ],
                    ],
                ]),
            ],
        ],
    ]);
    ?>
</div>
<?php \yii\widgets\Pjax::end(); ?>

<script>
    function resendConfirmation() {
        var keys = $('#users-grid-view').yiiGridView('getSelectedRows');

        $.ajax({
            url: "/user/manage/confirmation-email",
            method: "POST",
            data: {id: keys},
            success: function (data) {
                $.pjax.reload({container: '#users'});
                alert("Confirmation Email has been sent");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error sending confirmation email');
                // $('#result').html('<p>status code: ' + jqXHR.status + '</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>' + jqXHR.responseText + '</div>');
            },
        })
    }
    function resendPasswordReset() {
        var keys = $('#users-grid-view').yiiGridView('getSelectedRows');
        $.ajax({
            url: "/user/manage/password-reset",
            method: "POST",
            data: {id: keys},
            success: function (data) {
                alert('Password Reset Email has been sent');
                $.pjax.reload({container: '#users'});
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert('error sending reset password email');
            }
        })
    }
</script>