<?php

use yii\helpers\Html;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="users-update">
    <div class="m-container m-content col-12">
        <h1><?= Html::encode($this->title) ?></h1>
        <?=
        $this->render('_form', [
                    'model' => $model
           
        ])
        ?>
    </div>
</div>
