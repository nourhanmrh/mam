<?php

use yii\helpers\Html;


$this->title = Yii::t('app', 'Create Brokers');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brokers-create">
    <div class="m-content m-container col-12">
        <?=
        $this->render('_form', [
            'model' => $model
        ])
        ?>
    </div>
</div>
