<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
     .kt-login__btn-primary {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
        border-radius: 5px !important;
    }
</style>
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" >
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img src="/images/logo.png">  	
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">Sign In To Admin</h3>
                        </div>
                        
                        <?php $form = ActiveForm::begin(['id' => 'login-form',
                            'options' => ['class' => 'kt-form']]); ?>
                        
                                <?=
            $form->field($model, 'email')->textInput([
                'autofocus' => true,
                'class' => 'form-control',
                "placeholder" => $model->getAttributeLabel('email')
            ])
            ->label(false)
    ?>
                        
                                <?=
            $form->field($model, 'password')->passwordInput([
                'class' => 'form-control',
                "placeholder" => $model->getAttributeLabel('password')
            ])
            ->label(false)
    ?>

                            <div class="kt-login__actions">
                                        <?= Html::submitButton('Login', ['class' => 'btn btn-brand btn-elevate kt-login__btn-primary', 'name' => 'login-button']) ?>
                                <!--<button id="kt_login_signin_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Sign In</button>-->
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>	
            </div>
        </div>
    </div>	
</div>

