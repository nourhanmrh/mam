<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Brokers */

$this->title = Yii::t('app', 'Create Brokers');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brokers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brokers-create">
    <div class="m-content m-container col-12">
        <?=
        $this->render('_form', [
            'model' => $model,'model_details' => $model_details,'array'=>$array,'data'=>[]
        ])
        ?>
    </div>
</div>
