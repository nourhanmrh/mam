<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\editable\Editable;
use yii\helpers\ArrayHelper;
use common\models\currency\Currency;
?>
<?php \yii\widgets\Pjax::begin(['id' => 'brokers']); ?>
<div class="grid-scroll">

    <?=
    GridView::widget([
        'pjax' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'perfectScrollbar' => true,
        'autoXlFormat' => true,
        'striped' => true,
        'hover' => true,
        'responsive' => true,
        'persistResize' => true,
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'heading' => 'Brokers Report',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => '\kartik\grid\ActionColumn',
                'dropdown' => true
            ],
            [
                'attribute' => 'broker_name',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'image_location',
                'label' => 'Image',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'format' => ['image'],
                'value' => function($data) {
                    return yii::$app->params['frontendURL'] . yii::$app->params['ImageBrokerPath'] . '?id=' . $data->id . '&size=SM';
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'platforms',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
                'filter' => array("mt4" => "mt4", "mt5" => "mt5"),
                'value' => function($data) {
                    $json = json_decode($data->platforms);
                    return implode(",", $json);
                }
            ],
            [
                'attribute' => 'regulated_in',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'min_deposit',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'attribute' => 'supp_currencies',
                'label' => 'Supported Currencies',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'filter' => ArrayHelper::map(Currency::find()->asArray()->all(), 'code', 'code'),
                'value' => function($data) {
                    $json_c = json_decode($data->supp_currencies);
                    return implode(",", $json_c);
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'commissions',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'attribute' => 'website',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a($data->website,$data->website);
                }
            ],

        ],
    ]);
    ?>

</div>
<?php \yii\widgets\Pjax::end(); ?>
