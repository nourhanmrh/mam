<?php

use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\brokers\Brokers;
use common\models\currency\Currency;
use kartik\widgets\FileInput;
?>
<style>label{
        color: #5d5d5d;
        font-size: 15px;}
btn btn-default btn-secondary fileinput-upload fileinput-upload-button{
display:none}</style>
<div class="brokers-form">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <div class="col-lg-2"></div>
        <div class="col-md-8">
            <div class="kt-portlet">
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                <div class="col-lg-6">
                    <?= $form->field($model, 'broker_name')->textInput() ?>
                </div>
                 <div class="col-lg-6">
              
                      <?=
                    $form->field($model, 'image_location')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
//                            'previewFileType' => 'image',
                            'showUpload' => true,
                            'initialPreview' => [
                                  Html::img(yii::$app->params['frontendURL'] . yii::$app->params['ImageBrokerPath'] . '?id=' . $model->id . '&size=SM'),
                            ],
                            'initialCaption' => $model->image_location,
                        ],
                    ]);
                    ?>
                 </div>
                <div class="col-lg-6">
                    <div class="control-label"style="color: #5d5d5d!important;font-weight: 700;font-size: 15px;">Platform</div>
                    <?php
                    $p['mt4'] = 'mt4';
                    $p['mt5'] = 'mt5';
                    $pltfm_selected = ArrayHelper::map(Brokers::find()->where(['id' => $model->id])->all(), 'platforms', 'platforms');
                    if ($pltfm_selected) {
                        $a = array_values($pltfm_selected);
                        $pltfm_selected = json_decode($a[0]);
                    } else {
                        $pltfm_selected = [];
                    }

                    echo Select2::widget([
                        'name' => 'platforms',
                        'data' => $p,
                        'value' => $pltfm_selected ? array_values($pltfm_selected) : [],
                        'options' => [
                            'placeholder' => 'Select platform ...',
                            'multiple' => true
                        ],
                    ]);
                    ?>

                </div>
                <div class="col-lg-6">
                     <div class="control-label" style="color: #5d5d5d!important;font-weight: 700;font-size: 15px;">Currency</div>
                    <?php
                    $data = ArrayHelper::map(Currency::find()->all(), 'code', 'code');
                    $curr_selected = ArrayHelper::map(Brokers::find()->where(['id' => $model->id])->all(), 'supp_currencies', 'supp_currencies');
                    if ($curr_selected) {
                        $b = array_values($curr_selected);
                        $curr_selected = json_decode($b[0]);
                    } else {
                        $curr_selected = [];
                    }

                    echo Select2::widget([
                        'name' => 'supp_currencies',
                        'data' => $data,
                        'value' => $curr_selected ? (array_values($curr_selected)) : [],
                        'options' => [
                            'placeholder' => 'Select currency ...',
                            'multiple' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'regulated_in')->textInput() ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'min_deposit')->textInput() ?>
                </div>

                <div class="col-lg-6">
                    <?= $form->field($model, 'commissions')->textInput() ?>
                </div>
                <div class="col-lg-6">
                    <?php
                    echo $form->field($model, 'is_paid')->dropDownList([
                        0 => 'no',
                        1 => 'yes',
                    ]);
                    ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'amount_paid')->textInput() ?>
                </div>

                <div class="col-lg-6">
                    <?= $form->field($model, 'website')->textInput() ?>
                </div>
                
            </div>

            <?php
            if (isset($array)) {
                foreach ($array as $key => $val) {
                    $f = \common\models\brokers\BrokerDetails::find()->where(['broker_id' => $model->id])->andWhere(['key' => $val])->one();
                    ?>
                    <div class="col-lg-6">  <?php echo $val ?>

                        <?=
                        Html::activeInput('text', $model_details, $val, [
                            'class' => 'form-control ',
                            'placeholder' => $f['value'],
                        ])
                        ?> 
                    </div>
                    <?php
                }
            }
            ?>
            <div class="col-lg-6" style="margin-top:30px">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
