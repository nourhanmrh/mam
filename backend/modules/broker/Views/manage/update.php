<?php

use yii\helpers\Html;
$this->params['breadcrumbs'][] = ['label' => 'Brokers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->broker_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="brokers-update">
    <div class="m-container m-content col-12">
    

        <?=
        $this->render('_form', [
                    'model' => $model,'array' => $array, 'model_details' => $model_details,'data'=>$data
           
        ])
        ?>
    </div>
</div>
