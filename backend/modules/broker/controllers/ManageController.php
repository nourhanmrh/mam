<?php

namespace backend\modules\broker\controllers;

use yii;
use common\models\brokers\BrokersSearch;
use common\models\brokers\Brokers;
use common\models\users\Users;
use kartik\grid\EditableColumnAction;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\db\Query;
use common\models\currency\Currency;
use common\models\brokers\BrokerDetails;

class ManageController extends \yii\web\Controller {

    public function actionIndex() {
        $searchModel = new BrokersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Brokers();
        $model_details = new BrokerDetails();
        $platform_types = [];
        $manager = Yii::$app->authManager;
        $broker_details = Yii::$app->request->post('BrokerDetails');
        $brokers = Yii::$app->request->post('Brokers');
        if ($model->load(Yii::$app->request->post())) {
            $model->platforms = json_encode(Yii::$app->request->post('platforms'));
            $model->supp_currencies = json_encode(Yii::$app->request->post('supp_currencies'));
            $model->file = UploadedFile::getInstance($model, 'image_location');
            if ($model->file) {
                $model->image_location = $model->file->name;
                $model->save();
                $x = FileHelper::createDirectory(Yii::getAlias("@common/uploads/brokers/" . $model->id));
                $model->file->saveAs("@common/uploads/brokers/" . $model->id . "/" . $model->image_location);
            } else {
                print_r($model->errors);
                die;
            }
            if ($model->save()) {
                if ($broker_details) {
                    foreach ($broker_details as $key => $val) {
                        $model_details = new BrokerDetails();
                        $model_details->broker_id = $model->id;
                        $model_details->key = $key;
                        $model_details->value = $val;
                        $model_details->save(false);
                    }
                }
                return $this->redirect('index');
            } else {
                print_r($model->errors);
                die;
            }
        }
        return $this->render('create', ['model' => $model, 'model_details' => $model_details, 'array' => $model_details->array]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model_details = new BrokerDetails();
        $post = Yii::$app->request->post('Brokers');
        $broker_details = Yii::$app->request->post('BrokerDetails');
        $data = ArrayHelper::map(Currency::find()->all(), 'id', 'code');
        $img_old = $model->image_location;

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'image_location');
            if ($model->file) {
                $model->image_location = $model->file->name;
                $model->save();
                $x = FileHelper::createDirectory(Yii::getAlias("@common/uploads/brokers/" . $model->id));

                $model->file->saveAs("@common/uploads/brokers/" . $model->id . "/" . $model->image_location);
            } else {
                $model->image_location = $img_old;
            }
            $model->platforms = json_encode(Yii::$app->request->post('platforms'));
            $model->supp_currencies = json_encode(Yii::$app->request->post('supp_currencies'));
            $model->save();
            if ($broker_details) {
                foreach ($broker_details as $key => $val) {
                    $details_fields = $this->findBrokerDetails($id);
                    if (in_array($key, $details_fields)) {
                        $m = BrokerDetails::find()->where(['broker_id' => $id])->andWhere(['key' => $key])->one();
                        $m['value'] = $val;
                        $m->save();
                    } else {
                        $model_details = new BrokerDetails();
                        $model_details->broker_id = $model->id;
                        $model_details->key = $key;
                        $model_details->value = $val;
                        $model_details->save(false);
                    }
                }
            }
            return $this->render('update', ['model' => $model, 'model_details' => $model_details, 'array' => $model_details->array, 'data' => $data
            ]);
        }

        return $this->render('update', ['model' => $model, 'model_details' => $model_details, 'array' => $model_details->array, 'data' => $data
        ]);
    }

    protected function findModel($id) {
        if (($model = Brokers::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function findBrokerDetails($id) {
        $d = BrokerDetails::find()->select('key,value')->where(['broker_id' => $id])->all();
        $details = [];
        foreach ($d as $k => $v) {
            $details[] = $v['key'];
        }

        return $details;
    }

    public function actionCurrencyList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q) && $q != "") {
            $query = new Query;
            $query->select([Currency::tableName() . '.id', Currency::tableName() . '.code'])
                    ->from('currency')
                    ->andWhere(['like', Currency::tableName() . '.code', $q])
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Currency::find($id)->code];
        } elseif (is_null($q) || $q == "") {
            $query = new Query;
            $query->select([Currency::tableName() . '.id', Currency::tableName() . '.code'])
                    ->from('currency')
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        foreach ($out['results'] as $key => $value) {
            $out['results'][$key]['text'] = $value['code'];
            unset($out['results'][$key]['code']);
        }

        return $out;
    }

    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findBrokerModel($id),
        ]);
    }

    protected function findBrokerModel($id) {
        if (($model = Brokers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actions() {
        return ArrayHelper::merge(parent::actions(), [
                    'edit' => [
                        'class' => EditableColumnAction::className(),
                        'modelClass' => Brokers::className(),
                        'outputValue' => function ($model, $attribute, $key, $index) {
                            return $model->$attribute;
                        },
                        'outputMessage' => function($model, $attribute, $key, $index) {
                            return '';
                        },
                        'showModelErrors' => true,
                        'errorOptions' => ['header' => '']
                    ],
        ]);
    }

}
