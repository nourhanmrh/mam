<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\editable\Editable;

?>

 <?php \yii\widgets\Pjax::begin(['id' => 'drawdown']); ?>
                            <div class="grid-scroll">

                            	<?=
                                GridView::widget([
                                    'pjax' => true,
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                                    'perfectScrollbar' => true,
                                    'autoXlFormat' => true,
                                    'striped' => true,
                                    'hover'=>true,
                                    'responsive'=>true,
                                    'persistResize' => true,
                                    'panel' => [
                                        'type' => GridView::TYPE_SUCCESS,
                                        'heading' => 'drawdown Report',
                                    ],
                                    'columns' => [
                                    	['class' => 'yii\grid\SerialColumn'],

                                        [
                                            'attribute' => 'trader_name',
                                            'label' => 'Trader Namee',
                                            'vAlign' => 'middle',
                                            'hAlign' => 'center',
                                            'contentOptions' => ['style' => 'min-width:150px;'],
                                            'value' => function($data) {    
                                            $name= \common\models\tradersinfo\TradersInfo::findOne(['trader_id'=>$data['trader_id']]);
                                                return $name['trader_name'];
                                            }
                                        ],

                                        [
                                            'class' => 'kartik\grid\EditableColumn',
                                            'attribute' => 'drawdown',
                                            'vAlign' => 'middle',
                                            'hAlign' => 'center',
                                            'contentOptions' => ['style' => 'min-width:150px;'],
                                            'editableOptions' => [
                                                'asPopover' => false,
                                                'formOptions' => ['action' => ['drawdown']]],
                                        ],

                                        [
                                            'attribute' => 'time',
                                            'vAlign' => 'middle',
                                            'hAlign' => 'center',
                                            'contentOptions' => ['style' => 'min-width:150px;'],
                                        ],

                                ],
                                ]);
                                ?>
                               
                            </div>
                            <?php \yii\widgets\Pjax::end(); ?>