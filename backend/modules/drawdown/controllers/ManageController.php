<?php

namespace backend\modules\drawdown\controllers;

use yii;
use common\models\drawdown\DrawdownSearch;
use common\models\drawdown\Drawdown;
use kartik\grid\EditableColumnAction;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

class ManageController extends \yii\web\Controller {

    public function actionIndex() {
        $searchModel = new DrawdownSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    public function actions() {
        return ArrayHelper::merge(parent::actions(), [
                    'drawdown' => [
                        'class' => EditableColumnAction::className(),
                        'modelClass' => Drawdown::className(),
                        'outputValue' => function ($model, $attribute, $key, $index) {
                            return $model->$attribute;
                        },
                        'outputMessage' => function($model, $attribute, $key, $index) {
                            return '';
                        },
                        'showModelErrors' => true,
                        'errorOptions' => ['header' => '']
                    ],
        ]);
    }


}
