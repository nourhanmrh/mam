<?php

namespace backend\modules\reporting\controllers;

use yii;
use common\models\mamaccounts\MamAccountsSearch;
use common\models\mamaccounts\MamAccounts;
use common\models\tradersinfo\TradersInfo;
use common\models\users\Users;
use common\models\brokers\Brokers;
use common\models\country\Country;
use kartik\grid\EditableColumnAction;
use common\models\ReportServer\ReportServer;
use common\models\ReportServer\Mt4Reportserver;
use common\models\ReportServer\Mt5Reportserver;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\db\Query;

class ReportingController extends \yii\web\Controller {

    public function actionReports() {
        $model = new MamAccounts();

        return $this->render('reports', ['model' => $model]);
    }

    public function actionGetStatistics() {      
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = json_decode($_POST['models']);
        $data = new MamAccounts;
        $info = $data->geBrokerDataBase($request->broker_name, $request->platform);
        $report_server_class = (new ReportServer($request->platform))->getClass();
        $reportserver = new $report_server_class($info['database'], Null);
        return $reportserver->getKendoTrades($request);
    }

    public function actionReport() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = \yii::$app->request->post();
        if (($post['broker']) && ($post['platform'])) {
            $broker_name = Brokers::find()->select('broker_name')->where(['id' => $post['broker']])->one();
            if ($post['platform'] == 'mt4') {
                $columns = array( 'TICKET', 'LOGIN', 'SYMBOL', 'DIGITS', 'CMD', 'VOLUME', 'OPEN_TIME', 'OPEN_PRICE', 'SL', 'TP', 'CLOSE_TIME', 'EXPIRATION', 'REASON', 'CONV_RATE1', 'CONV_RATE2', 'COMMISSION', 'COMMISSION_AGENT', 'SWAPS', 'CLOSE_PRICE', 'PROFIT', 'TAXES', 'COMMENT', 'INTERNAL_ID', 'MARGIN_RATE', 'TIMESTAMP', 'MAGIC', 'GW_VOLUME', 'GW_OPEN_PRICE', 'GW_CLOSE_PRICE', 'MODIFY_TIME' );
                return $this->renderAjax('_mt4', ['broker_name' => $broker_name['broker_name'],'mt4_array'=>$columns]);
            } else if ($post['platform'] == 'mt5') {
                $deals_table_columns = array('Deal', 'Login', 'Dealer', 'order', 'Action', 'Entry', 'Reason', 'Digits', 'DigitsCurrency', 'ContractSize', 'Time', 'TimeMsc', 'Symbol', 'Price', 'VolumeExt', 'Profit', 'Storage', 'Commission', 'RateProfit', 'RateMargin', 'ExpertID', 'PositionID', 'Comment', 'ProfitRaw', 'PricePosition', 'PriceSL', 'PriceTP', 'VolumeClosedExt', 'TickValue', 'TickSize', 'Flags', 'Gateway', 'PriceGateway', 'ModifyFlags', 'Volume', 'VolumeClosed', 'ApiData');
                return $this->renderAjax('_mt5', ['broker_name' => $broker_name['broker_name'], 'mt5_array' => $deals_table_columns]);
            }
        }
    }

    public function actionBsList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q) && $q != "") {
            $query = new Query;
            $query->select([Brokers::tableName() . '.id', Brokers::tableName() . '.broker_name'])
                    ->from('brokers')
                    ->andWhere(['like', Brokers::tableName() . '.broker_name', $q])
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Brokers::find($id)->broker_name];
        } elseif (is_null($q) || $q == "") {
            $query = new Query;
            $query->select([Brokers::tableName() . '.id', Brokers::tableName() . '.broker_name'])
                    ->from('brokers')
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        foreach ($out['results'] as $key => $value) {
            $out['results'][$key]['text'] = $value['id'] . " - " . $value['broker_name'];
            unset($out['results'][$key]['broker_name']);
        }

        return $out;
    }

}

?>
