
<?php

$transport = new \Kendo\Data\DataSourceTransport();

$read = new \Kendo\Data\DataSourceTransportRead();

$read->url('/get-statistics')
        ->dataType('json')
        ->type('POST');

$transport->read($read)
        ->parameterMap('function (data, type) {data.platform="mt4",data.broker_name ="' . $broker_name . '";return {models: kendo.stringify(data)};}');

$model = new \Kendo\Data\DataSourceSchemaModel();

$idField = new \Kendo\Data\DataSourceSchemaModelField('TICKET');
$idField->type('number');


$model->addField($idField);
$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data.data')
        ->model($model)
        ->total('data.total');
$pageable = new Kendo\UI\GridPageable();
$pageable->refresh(true)
        ->pageSizes(true)
        ->alwaysVisible(true)
        ->previousNext(true)
        ->buttonCount(5);
$dataSource = new \Kendo\Data\DataSource();

$dataSource->transport($transport)
        ->pageSize(30)
        ->serverPaging(true)
        ->serverSorting(true)
        ->schema($schema);

$mt_trades = new \Kendo\UI\Grid('mt_trades');
foreach ($mt4_array as $k => $v) {   
    $id = new \Kendo\UI\GridColumn();
    $id->field($v)->title($v);    
  
}
$mt_trades
        ->dataSource($dataSource)
        ->sortable(true)
        ->pageable($pageable)
        ->attr('style', 'height:550px');
echo $mt_trades->render();
?>
