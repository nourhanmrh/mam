
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
?>

<div class="reports-index">
    <div class="m-content m-container col-12">
        <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" >
            <div class="row">
                <div class="kt-portlet">
                    <div class="row" >

                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                        <div class="col-lg-9">

                            <?php
                            echo $form->field($model, 'brokers_id')->widget(Select2::classname(), [
                                'data' => [],
                                'language' => 'en',
                                'maintainOrder' => true,
                                'options' => ['id' => 'broker_select', 'placeholder' => 'Search for a broker ...', 'class' => 'kt-form'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::to(['bs-list']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                        'processResults' => new JsExpression('function(data) {return  data.data }'),
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(result) {  return result.text; }'),
                                    'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                                ]
                            ]);
                            ?>
                        </div>
                        <div class="col-lg-3">
                            <?php echo $form->field($model, 'platform')->radioList(['mt4' => 'mt4', 'mt5' => 'mt5']); ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                    <div class="row"> 
                        <div id="gr"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        $('#broker_select').on("change", function (e) {
            myFun();
        });

        $('input[type=radio]').change(function () {
            myFun();
        });
        function myFun() {
            var broker= $('#broker_select').val();
            var platform=$("input[name='MamAccounts[platform]']:checked").val();
            if((broker!=null)&&(platform!=null)){
                 $.ajax({
                url: '/report',
                type: 'post',
                data: {broker:broker, platform: platform},
                success: function (response) {
                    $('#gr').html(response.data);
                 },
                error: function (error) {

                }
            });
            }
           
           
   
        }


    });
</script>
