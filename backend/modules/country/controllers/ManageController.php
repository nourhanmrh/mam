<?php

namespace backend\modules\country\controllers;

use yii;
use common\models\country\Country;
use common\models\country\CountrySearch;
use common\models\users\Users;
use kartik\grid\EditableColumnAction;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

class ManageController extends \yii\web\Controller {

    public function actions() {
        return ArrayHelper::merge(parent::actions(), [
                    'edit' => [
                        'class' => EditableColumnAction::className(),
                        'modelClass' => Country::className(),
                        'outputValue' => function ($model, $attribute, $key, $index) {
                            return $model->$attribute;
                        },
                        'outputMessage' => function($model, $attribute, $key, $index) {
                            return '';
                        },
                        'showModelErrors' => true,
                        'errorOptions' => ['header' => '']
                    ],
        ]);
    }

    public function actionIndex() {
        $searchModel = new CountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id) {
        if (($model = Country::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

?>