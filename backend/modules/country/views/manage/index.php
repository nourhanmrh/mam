<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\editable\Editable;
?>

<?php \yii\widgets\Pjax::begin(['id' => 'brokers']); ?>
<div class="grid-scroll">

    <?=
    GridView::widget([
        'pjax' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'perfectScrollbar' => true,
        'autoXlFormat' => true,
        'striped' => true,
        'hover' => true,
        'responsive' => true,
        'persistResize' => true,
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'heading' => 'Brokers Report',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'code',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'geoname_id',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'capital_geoname_id',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'latitude',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'longitude',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'language_code',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'currency_code',
                'label' => 'Supported Currencies',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'name',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'timezone_code',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'iso3',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'phonecode',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'numcode',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'class' => '\kartik\grid\ActionColumn',
                'dropdown' => true,
                'template' => '{view}{delete}',
            ],
        ],
    ]);
    ?>

</div>
<?php \yii\widgets\Pjax::end(); ?>