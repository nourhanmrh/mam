<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Codes;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View';
\yii\web\YiiAsset::register($this);
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" >
        <!--begin::Portlet-->
        <div class="row">
            <div class="col-md-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <?= print_r($model->name) ?>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body" id="viewuser">
                        <?=
                        DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'code',
                                'geoname_id',
                                'capital_geoname_id',
                                'language_code',
                                'currency_code',
                                'timezone_code',
                                'latitude',
                                'longitude',
                                'name',
                                'phonecode',
                                'iso3',
                                'numcode',
                            ],
                        ])
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


