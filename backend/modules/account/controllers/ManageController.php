<?php

namespace backend\modules\account\controllers;

use yii;
use common\models\mamaccounts\MamAccountsSearch;
use common\models\mamaccounts\MamAccounts;
use common\models\tradersinfo\TradersInfo;
use common\models\users\Users;
use common\models\brokers\Brokers;
use common\models\country\Country;
use kartik\grid\EditableColumnAction;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\db\Query;
use common\models\Constants;
use common\models\ReportServer\Mt4Reportserver;
use common\models\ReportServer\Mt5Reportserver;
use common\models\ReportServer\ReportServer;
use common\models\tradersinfo\TradersInfoSearch;
use common\models\mamaccounts\FollowedAccountsSearch;
use common\models\relations\Relations;
use common\models\traders\Traders;
use common\models\forms\ConfirmationForm;

class ManageController extends \yii\web\Controller {

    public function actionIndex() {
        $searchModel = new MamAccountsSearch();
        if (Yii::$app->request->queryParams) {
            if (isset(Yii::$app->request->queryParams['account_status'])) {
                if (Yii::$app->request->queryParams['account_status'] == Constants::ACCOUNT_STATUS_WAITING_CONFIRMATION) {
                    $dataProvider = $searchModel->search(['MamAccountsSearch' => ['account_status' => Constants::ACCOUNT_STATUS_WAITING_CONFIRMATION]]);
                }
            } else if (isset(Yii::$app->request->queryParams['account_type'])) {

                if (Yii::$app->request->queryParams['account_type'] == Constants::ACCOUNT_TYPE_MASTER)
                    $dataProvider = $searchModel->search(['MamAccountsSearch' => ['account_type' => Constants::ACCOUNT_TYPE_MASTER]]);
            } else {
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            }
        } else {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    public function actionTraderinfo($id) {
        $searchModel = new FollowedAccountsSearch();
        $slaves = [];
        $slave = Relations::find()->select(['slave_account'])->where(['master_account' => $id])->andWhere(['unfollow_date' => null])->asArray()->all();
        foreach ($slave as $k => $v) {
            $slaves[$k] = $v['slave_account'];
        }
        $searchModel->slaves = $slaves;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $slaves);
        return $this->render('traderinfo', ['model' => $this->findTraderModel($id), 'searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    public function actionAlltraders() {
        $searchModel = new TradersInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('alltraders', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider,]);
    }

    public function actionFollowers($id) {
        $trader_id = TradersInfo::find()->where("id=$id")->one();
        return $this->render('followers', ['trader_id' => $trader_id['trader_id'], 'ib_id' => $trader_id['user_id']]);
    }

    public function actionGetClients() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = json_decode($_POST['models']);
        $model = new MamAccounts();
        return $model->getFollowing($request);
    }

    public function actionCreate() {
        $model = new MamAccounts();
        $model->trades = 0;
        $v = $model->createAccountName();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->is_live == 1) {
                $model->name = 'RL_' . $v;
            } else {
                $model->name = 'RL_' . $v;
            }
            if ($model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
                return $this->redirect(['/account/manage/index']);
            }
        }
        return $this->render('create', ['model' => $model, 'broker' => [], 'user' => []]);
    }

    public function actionConfirm() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $res = Yii::$app->request->post('keys');
        $response = \Yii::$app->getResponse();
        $data = new MamAccounts();
        foreach ($res as $k) {
            $acc = MamAccounts::find()->select('*')->joinWith("brokers brokers")->where([MamAccounts::tableName() . '.id' => $k])->one();
            $database = ($acc['platform'] . "_" . $acc['broker_name']);
            if ($acc['platform'] == 'mt4') {

                $mt4reportserver = new Mt4Reportserver($database, $acc['login']);
            } else {
                $mt4reportserver = new Mt5Reportserver($database, $acc['login']);
            }
            $result = $mt4reportserver->getAccountTrades($acc['login']);
            if ($result) {
                $update = MamAccounts::updateAll(['balance' => $result[0]['Balance'], 'currency' => $result[0]['Currency'], 'account_status' => Constants::ACCOUNT_STATUS_COMPLETED], ['login' => $result[0]['Login']]);
                if ($update) {
                    $acz = MamAccounts::find()->select('*')->joinWith("brokers brokers")->where([MamAccounts::tableName() . '.id' => $k])->one();
                    $n = new ConfirmationForm();
                    $n->sendAccountCreation($acz, $acz['user_id']);
                    $response->setStatusCode(200);
                } else {
                    $response->setStatusCode(500);
                }
            } else {
                $response->setStatusCode(400);
            }
        }
    }

    public function actionRemove() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Traders();
//        $account_name = [];
//
//        $trader_name = MamAccounts::find()->select(['name'])->where(['id' => Yii::$app->request->post('master_account')])->one();
//        foreach ($res as $k => $v) {
//            $slave_name = MamAccounts::find()->select(['name'])->where(['id' => $v])->one();
//            $account_name[]=$slave_name['name'];
//        }
        return $model->removeTrader(Yii::$app->request->post());
    }

    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findAccountModel($id),
        ]);
    }

    protected function findAccountModel($id) {
        if (($model = MamAccounts::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findTraderModel($id) {
        if (($model = TradersInfo::findOne(['trader_id' => $id])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDetails($id) {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        $img_old = $model->profile_pic;
        if ($model->load(Yii::$app->request->post())) {
            $country = Country::find()->where(['code' => $post['TradersInfo']['country_code']])->one();
            $model->country_name = $country['name'];
            $model->file = UploadedFile::getInstance($model, 'profile_pic');
            if ($model->file) {
                $model->profile_pic = $model->file->name;
                if ($model->save()) {
                    $x = FileHelper::createDirectory(Yii::getAlias("@common/uploads/traders/" . $model->id));
                    $model->file->saveAs("@common/uploads/traders/" . $model->id . "/" . $model->profile_pic);
                } else {
                    print_r($model->errors);
                    die;
                }
            } else {
                $model->profile_pic = $img_old;
            }

            if ($model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            }
        }
        return $this->render('details', ['model' => $model, 'country' => [], 'user' => []]);
    }

    protected function findModel($id) {
        if (($model = TradersInfo::find()->where(['trader_id' => $id])->one()) !== null) {
            return $model;
        }
    }

    public function actionBrokersList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q) && $q != "") {
            $query = new Query;
            $query->select([Brokers::tableName() . '.id', Brokers::tableName() . '.broker_name'])
                    ->from('brokers')
                    ->andWhere(['like', Brokers::tableName() . '.broker_name', $q])
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Brokers::find($id)->broker_name];
        } elseif (is_null($q) || $q == "") {
            $query = new Query;
            $query->select([Brokers::tableName() . '.id', Brokers::tableName() . '.broker_name'])
                    ->from('brokers')
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        foreach ($out['results'] as $key => $value) {
            $out['results'][$key]['text'] = $value['id'] . " - " . $value['broker_name'];
            unset($out['results'][$key]['broker_name']);
        }

        return $out;
    }

    public function actionCountryList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q) && $q != "") {
            $query = new Query;
            $query->select([Country::tableName() . '.code AS id', Country::tableName() . '.name'])
                    ->from('country')
                    ->where(['like', Country::tableName() . '.code', $q])
                    ->orWhere(['like', Country::tableName() . '.name', $q])
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['code AS id' => $code, 'text' => Country::find($code)->name];
        } elseif (is_null($q) || $q == "") {
            $query = new Query;
            $query->select([Country::tableName() . '.code AS id', Country::tableName() . '.name'])
                    ->from('country')
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }

        foreach ($out['results'] as $key => $value) {
            $out['results'][$key]['text'] = $value['id'] . " - " . $value['name'];
            unset($out['results'][$key]['name']);
        }
        return $out;
    }

    public function actionUsersList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q) && $q != "") {
            $query = new Query;
            $query->select([Users::tableName() . '.id', Users::tableName() . '.name'])
                    ->from('users')
                    ->andWhere(['like', Users::tableName() . '.name', $q])
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Users::find($id)->broker_name];
        } elseif (is_null($q) || $q == "") {
            $query = new Query;
            $query->select([Users::tableName() . '.id', Users::tableName() . '.name'])
                    ->from('users')
                    ->all();
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }

        foreach ($out['results'] as $key => $value) {
            $out['results'][$key]['text'] = $value['id'] . " - " . $value['name'];

            unset($out['results'][$key]['name']);
        }

        return $out;
    }

    public function actions() {
        return ArrayHelper::merge(parent::actions(), [
                    'name' => [
                        'class' => EditableColumnAction::className(),
                        'modelClass' => MamAccounts::className(),
                        'outputValue' => function ($model, $attribute, $key, $index) {
                            return $model->$attribute;
                        },
                        'outputMessage' => function($model, $attribute, $key, $index) {
                            return '';
                        },
                        'showModelErrors' => true,
                        'errorOptions' => ['header' => '']
                    ],
                    'account_type' => [
                        'class' => EditableColumnAction::className(),
                        'modelClass' => MamAccounts::className(),
                        'outputValue' => function ($model, $attribute, $key, $index) {
                            return $model->$attribute;
                        },
                        'outputMessage' => function($model, $attribute, $key, $index) {
                            return '';
                        },
                        'showModelErrors' => true,
                        'errorOptions' => ['header' => '']
                    ],
                    'account_status' => [
                        'class' => EditableColumnAction::className(),
                        'modelClass' => MamAccounts::className(),
                        'outputValue' => function ($model, $attribute, $key, $index) {
                            return $model->$attribute;
                        },
                        'outputMessage' => function($model, $attribute, $key, $index) {
                            return '';
                        },
                        'showModelErrors' => true,
                        'errorOptions' => ['header' => '']
                    ],
        ]);
    }

}
