<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\editable\Editable;
use common\models\mamaccounts\MamAccounts;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>

<?php \yii\widgets\Pjax::begin(['id' => 'accounts']); ?>
<div class="grid-scroll">
    <div class="row" style="margin-bottom: 30px">
        <div class="col-lg-1"><button class="btn btn-danger" onclick="Confirm()">Confirm</button></div>

    </div>
</div>
<div class="transactions dashboard-grid__section">
    <?=
    GridView::widget([
        'pjax' => true,
        'id' => 'accounts-grid-view',
        'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'perfectScrollbar' => true,
        'options' => ['class' => 'transactions__table-container dashboard-grid__table-container _scrollbar'],
        'tableOptions' => ['class' => 'transactions__table resizable'],
        'autoXlFormat' => true,
        'striped' => true,
        'hover' => true,
        'responsive' => true,
        'persistResize' => true,
//    'panel' => [
//        'type' => GridView::TYPE_SUCCESS,
//        'heading' => 'Accounts Report',
//    ],
        'columns' => [
//        ['class' => 'yii\grid\SerialColumn'],
            // [
            //     'attribute' => 'id',
            //     'vAlign' => 'middle',
            //     'hAlign' => 'center',
            //     'contentOptions' => ['style' => 'min-width:150px;']
            // ],

            [
                'class' => 'kartik\grid\CheckboxColumn',
//                'headerOptions' => ['class' => 'kartik-sheet-style table__menu-column table__column_fixed'],
//                'contentOptions' => ['class' => 'table__menu-column '],
//            'cssClass' => 'transaction__menu context__menu',
                'pageSummary' => '<small>(amounts in $)</small>',
                'pageSummaryOptions' => ['colspan' => 3, 'data-colspan-dir' => 'rtl']
            ],
            [
                'attribute' => 'login',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'name',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['name']]],
            ],
            [
                'attribute' => 'trades',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'balance',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
//            [
//                'attribute' => 'margin',
//                'vAlign' => 'middle',
//                'hAlign' => 'center',
//                'contentOptions' => ['style' => 'min-width:150px;']
//            ],
//            [
//                'attribute' => 'equity',
//                'vAlign' => 'middle',
//                'hAlign' => 'center',
//                'contentOptions' => ['style' => 'min-width:150px;']
//            ],
//            [
//                'attribute' => 'leverage',
//                'vAlign' => 'middle',
//                'hAlign' => 'center',
//                'contentOptions' => ['style' => 'min-width:150px;']
//            ],
            [
                'attribute' => 'platform',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
//            [
//                'attribute' => 'margin_percentage',
//                'vAlign' => 'middle',
//                'hAlign' => 'center',
//                'contentOptions' => ['style' => 'min-width:150px;']
//            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'account_type',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['account_type']]],
            ],
            [
                'attribute' => 'currency',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'is_live',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'username',
                'label' => 'User Name',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;'],
                'value' => function($data) {
                    return $data->user['name'];
                }
            ],
            [
                'attribute' => 'broker_name',
                'label' => 'Broker',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;'],
                'value' => function($data) {
                    return $data->brokers['broker_name'];
                }
            ],
            [
                'attribute' => 'active_account',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;'],
                'filter' => array(0 => "no", 1 => "yes"),
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'account_status',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['account_status']]],
                'filter' => array('completed' => "completed", 'not completed' => "not completed", 'waiting confirmation' => "waiting confirmation"),
            ],
//        [
//            'class' => 'yii\grid\ActionColumn',
//            'template' => '{link}',
//            'buttons' => [
//                'link' => function ($url, $model, $key) {
//                    if ($model->account_type == 'master') {
//                        return Html::a('Show Info', '/account/manage/details?id=' . $key);
//                    }
//                }
//            ],
//        ],
//        [
//            'class' => 'yii\grid\ActionColumn',
//            'template' => '{link}',
//            'buttons' => [
//                'link' => function ($url, $model, $key) {
//                    if ($model->account_type == 'master') {
//                        return Html::a('View Trader', '/account/manage/traderinfo?id=' . $key);
//                    }
//                }
//            ],
//        ],
        ],
    ]);
    ?>

</div>

<?php \yii\widgets\Pjax::end(); ?>

<script>

    function Confirm() {
        Loader.show("#accounts-grid-view");
        var keys = $('#accounts-grid-view').yiiGridView('getSelectedRows');

        $.ajax({
            url: '<?= Url::to(['confirm']) ?>',
            type: 'post',
            data: {keys: keys},
            success: function (response) {
                Loader.stop("#accounts-grid-view");
                $.pjax.reload({container: '#accounts'});
                Notify.show("done", "success");
            },
            error: function (error) {
                Loader.stop("#accounts-grid-view");
                Notify.show("Error", "danger");
            }
        });



    }

</script>
<style>
    .table-bordered th, .table-bordered td {
        border:0!important;
    }
    table{text-align: center}
</style>