<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use common\models\currency\Currency;
?>

<div class="container"  id="brokers_form">

    <div class="kt-portlet card">
        <div class="kt-portlet__body" style="padding:20px">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="row">
                <div class="col-lg-6">   <?= $form->field($model, 'login') ?></div>
                <div class="col-lg-6">                    
                    <div class="control-label"style="color: #5d5d5d!important;font-weight: 700;font-size: 15px;">Platform</div>

                    <?php
                    $p['mt4'] = 'mt4';
                    $p['mt5'] = 'mt5';
                    echo Select2::widget([
                        'name' => 'platforms',
                        'data' => $p,
                        'value' => [],
                        'options' => [
                            'placeholder' => 'Select platform ...',
                           
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">  
                    <?php
                    echo $form->field($model, 'brokers_id')->widget(Select2::classname(), [
                        'data' => $broker,
                        'language' => 'en',
                        'maintainOrder' => true,
                        'options' => ['placeholder' => 'Search for a broker ...', 'class' => 'kt-form'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::to(['brokers-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                'processResults' => new JsExpression('function(data) {return  data.data }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(result) {  return result.text; }'),
                            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-lg-6">   
                    <?php
                    echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                        'data' => $user,
                        'language' => 'en',
                        'maintainOrder' => true,
                        'options' => ['placeholder' => 'Search for a user ...', 'class' => 'kt-form'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::to(['users-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                'processResults' => new JsExpression('function(data) {return  data.data }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(result) {  return result.text; }'),
                            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                        ]
                    ]);
                    ?>
                </div>
            </div>
            <div class="row">
                 <div class="col-lg-6">   <?= $form->field($model, 'trades') ?></div>
                <div class="col-lg-6"> 
                      <div class="control-label" style="color: #5d5d5d!important;font-weight: 700;font-size: 15px;">Currency</div>
                    <?php
                    $data = ArrayHelper::map(Currency::find()->all(), 'code', 'code');
                    

                    echo Select2::widget([
                        'name' => 'supp_currencies',
                        'data' => $data,
                        'value' =>  [],
                        'options' => [
                            'placeholder' => 'Select currency ...',
                           
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">  <?= $form->field($model, 'balance')->textInput(['type' => 'number'])?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'equity')->textInput(['type' => 'number']) ?></div>
            </div>
            
            <div class="row">
                <div class="col-lg-6">  <?= $form->field($model, 'margin')->textInput(['type' => 'number']) ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'margin_percentage')->textInput(['type' => 'number']) ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6">  <?= $form->field($model, 'is_live')->radioList([0 => 'Demo', 1 => 'Live']); ?></div>
                <div class="col-lg-6">   <?= $form->field($model, 'leverage')->textInput(['type' => 'number']) ?></div>
            </div>

            <div class="form-group">
<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

<?php ActiveForm::end(); ?>

        </div>

    </div>
</div>
