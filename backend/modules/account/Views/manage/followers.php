<?php
$transport = new \Kendo\Data\DataSourceTransport();

$read = new \Kendo\Data\DataSourceTransportRead();

$read->url('/account/manage/get-clients')
        ->dataType('json')
        ->type('POST');


$transport->read($read)
        ->parameterMap('function (data, type) {  data.trader_id ="' . $trader_id . '",data.ib_id="' . $ib_id . '";return {models: kendo.stringify(data)};}');
$model = new \Kendo\Data\DataSourceSchemaModel();

$id_field = new \Kendo\Data\DataSourceSchemaModelField('trader_id');
$id_field->type('number');
$phone_field = new \Kendo\Data\DataSourceSchemaModelField('LastName');
$phone_field->type('number');

$photo_field = new \Kendo\Data\DataSourceSchemaModelField('Photo');
$photo_field->type('string');
$model->addField($id_field);
$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data')
        ->errors(new \Kendo\JavaScriptFunction("function(data){if(data.data.errors){kendo.alert(data.data.errors)}}"))
        ->model($model);


$dataSource = new \Kendo\Data\DataSource();

$dataSource->transport($transport)
        ->pageSize(5)
        ->schema($schema)
        ->serverPaging(true)
        ->serverFiltering(true)
        ->serverSorting(true);
$listview = new \Kendo\UI\ListView('listview');
$listview->dataSource($dataSource)->pageable(false)->templateId('row-template');

echo $listview->render();
?>
 <script id="row-template" type="text/x-kendo-template" >
    
  <div class="col-lg-3 text-left" style="padding-top: 10px;margin: 0px 30px;background: white;height:400px;">
	   <div class="row">
	   <div class="col-lg-6"><p style="font-weight:bold">Login:</p> #:login #</div>
	     <div class="col-lg-6"><p style="font-weight:bold">Name:</p> #:name #</div>
	   </div>
	    <div class="row">
	   <div class="col-lg-6"><p style="font-weight:bold"> Platform:</p> #:platform #</div>
	     <div class="col-lg-6"><p style="font-weight:bold">Created at:</p> #:created_at #</div>
	   </div>
	    
	    <div class="row">
	   <div class="col-lg-6"><p style="font-weight:bold">Copy Type:</p> #:copy_type #</div>
	     <div class="col-lg-6"><p style="font-weight:bold">Value:</p> #:value #</div>
	   </div>
	   </div>
    </script>