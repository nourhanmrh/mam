<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\widgets\FileInput;
?>
<style>
    .btn.btn-default.btn-secondary.fileinput-upload.fileinput-upload-button{
        display:none
    }

</style>
<div class="container"  id="brokers_from" >

    <div class="kt-portlet card">
        <div class="kt-portlet__body" style="padding:20px">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="row">
                <div class="col-lg-6">

                    <?=
                    $form->field($model, 'profile_pic')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'showUpload' => true,
                            'initialPreview' => [
                                Html::img(yii::$app->params['frontendURL'] . yii::$app->params['ImageTraderPath'] . '?id=' . $model->id . '&size=SM'),
                            ],
                            'initialCaption' => $model->profile_pic,
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">  <?= $form->field($model, 'trader_name') ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'profit') ?></div>
            </div>
            <div class="row">

                <div class="col-lg-6">   
                    <?php
                    echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                        //   'data' => $user,
                        'language' => 'en',
                        'maintainOrder' => true,
                        'options' => ['placeholder' => 'Search for a user ...', 'class' => 'kt-form'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::to(['users-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                'processResults' => new JsExpression('function(data) {return  data.data }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(result) {  return result.text; }'),
                            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                        ]
                    ]);
                    ?>
                </div>

                <div class="col-lg-6">   
                    <?php
                    echo $form->field($model, 'country_code')->widget(Select2::classname(), [
                        //   'data' => $country,
                        'language' => 'en',
                        'maintainOrder' => true,
                        'options' => ['placeholder' => 'Search for country ...', 'class' => 'kt-form'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::to(['country-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                'processResults' => new JsExpression('function(data) {return  data.data }'),
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(result) {  return result.text; }'),
                            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
                        ]
                    ]);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">   <?= $form->field($model, 'investors') ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'rate') ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6">   <?= $form->field($model, 'roi') ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'profit') ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6">   <?= $form->field($model, 'strategy') ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'equity') ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6">   <?= $form->field($model, 'comission_pending') ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'comission_received') ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6">  <?= $form->field($model, 'trusting') ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'ea') ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6">   <?= $form->field($model, 'currency') ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'is_live')->radioList([0 => 'Demo', 1 => 'Live']); ?></div>
            </div>


            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        $('#tradersinfo-country_code').prop('disabled', false);
        $('#tradersinfo-country_code').select2('enable');

    });
</script>