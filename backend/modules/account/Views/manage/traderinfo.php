
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Codes;
use kartik\grid\GridView;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => 'accounts', 'url' => ['traderinfo']];
$this->params['breadcrumbs'][] = 'traderinfo';
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="users-view" class="col-lg-3">
        <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" >
            <div class="row">
                <div class="col-md-12">
                    <div class="kt-portlet">

                        <div class="kt-portlet__body" id="viewuser">
                            <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'id',
                                    'trader_id',
                                    'user_id',
                                    'trader_name',
                                    'investors',
                                    'profit',
                                    'roi',
                                    'country_code',
                                    'country_name',
                                    'profile_pic',
                                    'is_live',
                                    'trusting',
                                    'ea',
                                    'joined_at',
                                    'left_at',
                                    'currency',
                                    'equity',
                                    'rate',
                                    'logged',
                                    'strategy',
                                    'minpeak',
                                    'maxpeak',
                                    'maxddvalue',
                                    'ten_month_chart',
                                    'comission_received',
                                    'comission_pending'
                                ],
                            ])
                            ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="grid-scroll">
            <div class="row" style="margin-bottom: 30px">
                <div class="col-lg-1"><button class="btn btn-danger" onclick="remove('<?=$model['trader_id']?>')">Remove follower</button></div>

            </div>
        </div>
        <?php \yii\widgets\Pjax::begin(['id' => 'traders']); ?>
        <?=
        GridView::widget([
            'pjax' => true,
            'id' => 'traders-grid-view',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'perfectScrollbar' => true,
            'autoXlFormat' => true,
            'striped' => true,
            'hover' => true,
            'responsive' => true,
            'persistResize' => true,
            'panel' => [
                'type' => GridView::TYPE_SUCCESS,
                'heading' => 'Accounts Report',
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'kartik\grid\CheckboxColumn',
                    'headerOptions' => ['class' => 'kartik-sheet-style'],
                    'pageSummary' => '<small>(amounts in $)</small>',
                    'pageSummaryOptions' => ['colspan' => 3, 'data-colspan-dir' => 'rtl']
                ],
                [
                    'attribute' => 'id',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;']
                ],
                [
                    'attribute' => 'login',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;']
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'name',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;'],
                    'editableOptions' => [
                        'asPopover' => false,
                        'formOptions' => ['action' => ['name']]],
                ],
                [
                    'attribute' => 'trades',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;']
                ],
                [
                    'attribute' => 'balance',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;']
                ],
                [
                    'attribute' => 'platform',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;']
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'account_type',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;'],
                    'editableOptions' => [
                        'asPopover' => false,
                        'formOptions' => ['action' => ['account_type']]],
                ],
                [
                    'attribute' => 'currency',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;']
                ],
                [
                    'class' => 'kartik\grid\BooleanColumn',
                    'attribute' => 'is_live',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;']
                ],
                [
                    'attribute' => 'active_account',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;'],
                    'filter' => array(0 => "no", 1 => "yes"),
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'account_status',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'min-width:150px;'],
                    'editableOptions' => [
                        'asPopover' => false,
                        'formOptions' => ['action' => ['account_status']]],
                    'filter' => array('completed' => "completed", 'not completed' => "not completed", 'waiting confirmation' => "waiting confirmation"),
                ],
            ],
        ]);
        ?>

<?php \yii\widgets\Pjax::end(); ?>


    </div>

</div>

<script>

    function remove($trader_id) {
        var slaves = $('#traders-grid-view').yiiGridView('getSelectedRows');
        $.ajax({
            url: '<?= Url::to(['remove']) ?>',
            type: 'post',
            data: {slaves: slaves,master_account:$trader_id},
            success: function (response) {
                $.pjax.reload({container: '#traders'});
            },
            error: function (error) {
                kendo.alert("ERROR")
            }
        });
    }
</script>