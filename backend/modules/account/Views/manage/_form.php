<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
?>

<div class="container"  id="brokers_from" >

    <div class="kt-portlet card">
        <div class="kt-portlet__body" style="padding:20px">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
           
           
            <div class="row">
                <div class="col-lg-6">   <?= $form->field($model, 'login') ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'platform') ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6">   <?= $form->field($model, 'account_type') ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'currency') ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6">  <?= $form->field($model, 'balance')->textInput([
                                 'type' => 'number'
                            ]) ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'equity') ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6">   <?= $form->field($model, 'trades') ?></div>
                <div class="col-lg-6">   <?= $form->field($model, 'leverage') ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6">  <?= $form->field($model, 'margin') ?></div>
                <div class="col-lg-6">  <?= $form->field($model, 'margin_percentage') ?></div>
            </div>

            

            <div class="row">
                <div class="col-lg-6">  <?= $form->field($model, 'is_live')->radioList([0 => 'Demo', 1 => 'Live']); ?></div>

            </div>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>
