<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\editable\Editable;
use common\models\mamaccounts\MamAccounts;
use common\models\users\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>

<?php \yii\widgets\Pjax::begin(['id' => 'accounts']); ?>
<div class="transactions dashboard-grid__section">
    <?=
    GridView::widget([
        'pjax' => true,
        'id' => 'tradersinfo-grid-view',
        'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'perfectScrollbar' => true,
        'options' => ['class' => 'transactions__table-container dashboard-grid__table-container _scrollbar'],
        // 'tableOptions' => ['class' => 'transactions__table'],
        'autoXlFormat' => true,
        'striped' => true,
        'hover' => true,
        'responsive' => true,
        'persistResize' => true,
   // 'panel' => [
   //     'type' => GridView::TYPE_SUCCESS,
   //     'heading' => 'Accounts Report',
   // ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => 'Account Name',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;'],
                'value' => function($data) {
                    return $data->mamAccount['name'];
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'trader_name',
                'vAlign' => 'middle',
                'hAlign' => 'center',
           // 'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                    'formOptions' => ['action' => ['edit']]],
            ],
            [
                'attribute' => 'investors',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                // 'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'profit',
                'vAlign' => 'middle',
                'hAlign' => 'center',
           // 'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'roi',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'country_name',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'class' => '\kartik\grid\BooleanColumn',
                'attribute' => 'is_live',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'trusting',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'class' => '\kartik\grid\BooleanColumn',
                'attribute' => 'ea',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'joined_at',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'left_at',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'currency',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'equity',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'rate',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'logged',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
           //  [
           //      'attribute' => 'strategy',
           //      'vAlign' => 'middle',
           //      'hAlign' => 'center',
           //      'contentOptions' => ['style' => 'min-width:450px;']
           //  ],
            [
                'attribute' => 'minpeak',
                'label' => 'Min Peak',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'maxpeak',
                'label' => 'Max Peak',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'maxddvalue',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'comission_received',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'comission_pending',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'open_positions',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'trades',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'avg_pips',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'name',
                'label' => 'User',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//            'contentOptions' => ['style' => 'min-width:150px;'],
                'value' => function($data) {
                    return $data->user['name'];
                }
            ],


        ],
    ])
    ?>

</div>

<?php \yii\widgets\Pjax::end(); ?>

<style>
    .table-bordered th, .table-bordered td {
        border:0!important;
    }
    table{text-align: center}
    .transactions .transactions__table-container{
    overflow-x: hidden;
    }
</style>
