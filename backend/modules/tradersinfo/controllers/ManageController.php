<?php

namespace backend\modules\tradersinfo\controllers;

use yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\db\Query;
use kartik\grid\EditableColumnAction;
use common\models\mamaccounts\MamAccounts;
use common\models\tradersinfo\TradersInfo;
use common\models\tradersinfo\TradersInfoSearch;


class ManageController extends \yii\web\Controller {

    public function actionIndex() {
        $searchModel = new TradersInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actions() {
        return ArrayHelper::merge(parent::actions(), [
                    'edit' => [
                        'class' => EditableColumnAction::className(),
                        'modelClass' => MamAccounts::className(),
                        'outputValue' => function ($model, $attribute, $key, $index) {
                            return $model->$attribute;
                        },
                        'outputMessage' => function($model, $attribute, $key, $index) {
                            return '';
                        },
                        'showModelErrors' => true,
                        'errorOptions' => ['header' => '']
                    ],

        ]);
    }

}
