<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\editable\Editable;
?>

<?php \yii\widgets\Pjax::begin(['id' => 'symbols']); ?>
<div class="grid-scroll">
    <?=
    GridView::widget([
        'pjax' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'perfectScrollbar' => true,
        'autoXlFormat' => true,
        'striped' => true,
        'hover' => true,
        'responsive' => true,
        'persistResize' => true,
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'heading' => 'Symbols',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => '\kartik\grid\ActionColumn',
                'dropdown' => true
            ],
            [
                'attribute' => 'symbol_name',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'attribute' => 'symbol_value',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'contentOptions' => ['style' => 'min-width:150px;']
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'status',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'editableOptions' => [
                    'asPopover' => false,
                ],
                    'value' => function($data) {                        
                  return ($data->status==0)? 'not active':'active';
                }
            ],
        ],
    ]);
    ?>
</div>
    <?php \yii\widgets\Pjax::end(); ?>