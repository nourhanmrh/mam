<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<style>label{
        color: #5d5d5d;
        font-size: 15px;}</style>

    <div class="symbol-form">

    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

        <div class="col-lg-2"></div>

        <div class="col-md-8">

            <?php $form = ActiveForm::begin(); ?>

            <div class="col-lg-6">
                <?= $form->field($model, 'symbol_name') ?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'symbol_value') ?>
            </div>

            <div class="col-lg-12">
                <?= $form->field($model, 'status')->dropdownList([0 => 'not active', 1 => 'active'],['prompt'=>'Select Status..']); ?>
            </div>

            <div class="col-lg-12">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>

</div>

