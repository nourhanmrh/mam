<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BrokerDetails */

$this->title = 'Create Symbol';
$this->params['breadcrumbs'][] = ['label' => 'Symbol', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Symbol-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 
    ]) ?>

</div>
