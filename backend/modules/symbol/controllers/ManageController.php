<?php

namespace backend\modules\symbol\controllers;

use yii;
use kartik\grid\EditableColumnAction;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use common\models\symbols\Symbols;
use common\models\symbols\SymbolsSearch;

class ManageController extends \yii\web\Controller {

    public function actionIndex() {
        $searchModel = new SymbolsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post('Symbols');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['/symbol/manage/index']);
            } else {
                print_r($model->errors);
            }
            return $this->render('update', ['model' => $model]);
        }

        return $this->render('update', ['model' => $model]);
    }

    protected function findModel($id) {

        if (($model = Symbols::findOne($id)) !== null) {
            return $model;
        }
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        return $this->render('view', ['model' => $model]);
    }

    public function actionCreate() {
        $model = new Symbols();

        $users = Yii::$app->request->post('Symbols');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['/symbol/manage/index']);
            } else {
                print_r($model->errors);
                die;
            }
        }
        return $this->render('create', ['model' => $model]);
    }

}
