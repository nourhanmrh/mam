<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace backend\assets;

use yii;
use yii\web\AssetBundle;
/**
 * Description of FrontendAsset
 *
 * @author MSI
 */
class FrontendAsset extends AssetBundle{
    public $basePath = '@webroot';
    public $baseUrl = '@frontend/web';
    
    public $js = [


    'js/app.js',


];
}
