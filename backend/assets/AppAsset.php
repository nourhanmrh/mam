<?php

namespace backend\assets;

use yii;
use yii\web\AssetBundle;

class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/metronic/perfect-scrollbar.css',
        'css/metronic/tether.css',
        'css/line-awesome/css/line-awesome.css',
        'css/metronic-font/css/styles.css',
        'css/flaticon/css/flaticon.css',
        'css/metronic/styles.css',
        'css/metronic/style.bundle.css',
        '/css/kendo/kendo.common.min.css',
        '/css/kendo/kendo.material.min.css',
        '/css/kendo/kendo.material.min.css',
        'css/kendo/kendo.mobile.all.min.css',
        'css/dashboard.css'
    ];
    public $js = [
        'js/metronic/popper.js',
        'js/metronic/bootstrap.min.js',
        'js/metronic/js.cookie.js',
        'js/metronic/moment.min.js',
        'js/metronic/tooltip.min.js',
        'js/bootstrap-notify.min.js',
        'js/metronic/perfect-scrollbar.js',
        'js/metronic/wNumb.js',
        'js/metronic/bootstrap.min.js',
        'js/metronic/scripts.bundle.js',
        'js/metronic/switchbut.js',
        'js/metronic/datatables.bundle.js',
        'js/kendo/kendo.all.min.js',
//        'js/kendo/jquery.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
