<?php

$params = array_merge(
        require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'translatemanager'],
    'modules' => [
        'main' => [
            'class' => 'backend\modules\main\Module',
        ],
        'user' => [
            'class' => 'backend\modules\user\Module',
        ],
        'symbol' => [
            'class' => 'backend\modules\symbol\Module',
        ],
        'broker' => [
            'class' => 'backend\modules\broker\Module',
        ],
        'reporting' => [
            'class' => 'backend\modules\reporting\Module',
        ],
        'drawdown' => [
            'class' => 'backend\modules\drawdown\Module',
        ],
        'country' => [
            'class' => 'backend\modules\country\Module',
        ],
        'account' => [
            'class' => 'backend\modules\account\Module',
        ],
        'api-v1' => [
            'class' => 'backend\modules\api\v1\api',
        ],
        'log' => [
            'class' => 'backend\modules\log\Module',
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
        ],
        'tradersinfo' => [
            'class' => 'backend\modules\tradersinfo\Module',
        ],
        'email-templates' => [
            'class' => \ymaker\email\templates\Module::class,
            'languageProvider' => [
                'class' => \motion\i18n\ConfigLanguageProvider::class,
                'languages' => [
                    [
                        'locale' => 'en-US',
                        'label' => 'English',
                    ]
                ],
                'defaultLanguage' => [
                    'locale' => 'en-US',
                    'label' => 'English',
                ],
            ],
        ],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            'root' => '@app', // The root directory of the project scan.
// 'scanRootParentDirectory' => true, // Whether scan the defined `root` parent directory, or the folder itself.
// IMPORTANT: for detailed instructions read the chapter about root configuration.
            'layout' => 'language', // Name of the used layout. If using own layout use 'null'.
            'roles' => ['@'], // For setting access levels to the translating interface.
            'tmpDir' => '@runtime', // Writable directory for the client-side temporary language files.
// IMPORTANT: must be identical for all applications (the AssetsManager serves the JavaScript files containing language elements from this directory).
            'phpTranslators' => ['::t'], // list of the php function for translating messages.
            'jsTranslators' => ['lajax.t'], // list of the js function for translating messages.
            'patterns' => ['*.js', '*.php'], // list of file extensions that contain language elements.
            'scanTimeLimit' => null, // increase to prevent "Maximum execution time" errors, if null the default max_execution_time will be used
            'searchEmptyCommand' => '!', // the search string to enter in the 'Translation' search field to find not yet translated items, set to null to disable this feature
            'defaultExportStatus' => 1, // the default selection of languages to export, set to 0 to select all languages by default
            'defaultExportFormat' => 'json', // the default format for export, can be 'json' or 'xml'
            'tables' => [// Properties of individual tables
                [
                    'connection' => 'db', // connection identifier
                    'table' => '{{%language}}', // table name
                    'columns' => ['name', 'name_ascii'], // names of multilingual fields
                    'category' => 'database-table-name', // the category is the database table name
                ]
            ],
        ],
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Component'
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'templateManager' => [
            'class' => \ymaker\email\templates\components\TemplateManager::class,
        ],
        'emailTemplates' => [
            'class' => bl\emailTemplates\components\TemplateManager::class
        ],
        'session' => [
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning', 'info', 'trace', 'profile'],
                    'categories' => ['application*', 'yii\db\*', 'api'],
                    'logFile' => '@runtime/logs/' . date('Y') . '/' . date('m') . '/' . date('d') . '/app-back.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'categories' => ['user'],
                    'logFile' => '@runtime/logs/' . date('Y') . '/' . date('m') . '/' . date('d') . '/user-back.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 20,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info', 'trace'],
                    'except' => ['application'],
                    'maskVars' => ['_SERVER', '_GET', '_SESSION', '_COOKIE'],
                    'categories' => ['main'],
                    'logFile' => '@runtime/logs/' . date('Y') . '/' . date('m') . '/' . date('d') . '/main-back.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info', 'trace', 'profile'],
                    'except' => ['application'],
                    'maskVars' => ['_SERVER', '_GET', '_SESSION', '_COOKIE'],
                    'categories' => ['api'],
                    'logFile' => '@runtime/logs/' . date('Y') . '/' . date('m') . '/' . date('d') . '/api.log',
                ]
            ]],
        'errorHandler' => [
            'errorAction' => 'main/manage/error',
        ],
        'urlManager' => [
//'class' => \common\components\extend\UrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                [
                    'pattern' => '',
                    'route' => 'main/manage/index'
                ],
                [
                    'pattern' => 'translatemanager',
                    'route' => 'translatemanager'
                ],
                [
                    'pattern' => 'email-templates',
                    'route' => 'email-templates'
                ],
                [
                    'pattern' => 'api/v1/<controller:[\w-]+>/<action:[\w-]+>',
                    'route' => 'api-v1/<controller>/<action>'
                ],
                [
                    'pattern' => '<action:(login|logout|request-password-reset|reset-password)>',
                    'route' => 'user/auth/<action>'
                ],
                [
                    'pattern' => '<controller>/<action>/<id:\d+>',
                    'route' => '<controller>/<action>',
                    'suffix' => ''
                ],
                [
                    'pattern' => '<controller>/<action>',
                    'route' => '<controller>/<action>',
                    'suffix' => ''
                ],
                [
                    'pattern' => '<action:(reports|get-statistics|mt4|mt5|report)>',
                    'route' => 'reporting/reporting/<action>'
                ],
                [
                    'pattern' => '<module>/<controller>/<action>/<id:\d+>',
                    'route' => '<module>/<controller>/<action>',
                    'suffix' => ''
                ],
                [
                    'pattern' => '<module>/<controller>/<action>',
                    'route' => '<module>/<controller>/<action>',
                    'suffix' => ''
                ],
            ],
        ]
    ],
    'params' => $params,
];
