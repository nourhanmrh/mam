<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\country\CountrySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'geoname_id') ?>

    <?= $form->field($model, 'capital_geoname_id') ?>

    <?= $form->field($model, 'language_code') ?>

    <?= $form->field($model, 'currency_code') ?>

    <?php // echo $form->field($model, 'timezone_code') ?>

    <?php // echo $form->field($model, 'latitude') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'iso3') ?>

    <?php // echo $form->field($model, 'numcode') ?>

    <?php // echo $form->field($model, 'phonecode') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
