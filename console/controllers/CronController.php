<?php

namespace console\controllers;

use Yii;
use common\models\ReportServer\Mt4Reportserver;
use common\models\ReportServer\Mt5Reportserver;
use common\models\ReportServer\ReportServer;
use yii\console\Controller;
use fedemotta\cronjob\models\CronJob;
use common\models\tradersinfo;
use common\models\brokers;
use common\models\relations\Relations;
use common\models\mamaccounts\MamAccounts;

class CronController extends Controller {

    public function actionInit($from, $to) {
        $endDate = date('Y-m-d H:i:s');
        $startDate = date('Y-m-d H:i:s', strtotime($endDate . "-3 months"));
        $yesterday = date('Y-m-d H:i:s', strtotime($endDate . "-1 day"));
        $dates = CronJob::getDateRange($from, $to);
        $command = CronJob::run($this->id, $this->action->id, 0, CronJob::countDateRange($dates));
        if ($command === false) {
            return Controller::EXIT_CODE_ERROR;
        } else {
            foreach ($dates as $date) {
                $traders_list = \Yii::$app->db->createCommand("SELECT login,mam_accounts.id,mam_accounts.name as name,mam_accounts.platform as platform,trader_name,balance,brokers.broker_name,traders_info.maxpeak,traders_info.minpeak,traders_info.maxddvalue  FROM mam_accounts inner join brokers on brokers.id=mam_accounts.brokers_id  inner join traders_info on traders_info.trader_id= mam_accounts.id where account_type='master' and login is not null ")->queryAll();
                foreach ($traders_list as $k => $v) {
                    $drawdown = 0;
                    $maxpeak = $v['maxpeak'];
                    $minpeak = $v['minpeak'];
                    $balance = $v['balance'];
                    $maxdrawd = $v['maxddvalue'];
                    $data = new MamAccounts();
                    $info = $data->getDataBase($v['name']);
                    $report_server_class = (new ReportServer($v['platform']))->getClass();
                    $reportserver = new $report_server_class($info['database'], $v['login']);
                    $res = $reportserver->get_yesterday_profit($yesterday, $endDate);
                    $yesterday_profit = $res[0]["profit"];
                    $balance = $balance + $yesterday_profit;
                    if ($balance > $maxpeak) {
                        $drawDown = $maxpeak - $minpeak;
                        if ($maxdrawd < $drawDown) {
                            $maxdrawd = $drawDown;
                            $query_insert = \Yii::$app->db->createCommand("insert into drawdown(drawdown,trader_id) values (" . $maxdrawd . "," . $v["id"] . ")")->execute();
                            $DCMaxDDownValue = $maxdrawd;
                            $query = \Yii::$app->db->createCommand("update traders_info set maxddvalue=" . $maxdrawd . " where trader_id =" . $v["id"])->execute();
                            if ($maxpeak != 0.0) {
                                $maxdrawdownPer = ($maxdrawd / $maxpeak * 100.0);
                                $drawDown = $maxdrawdownPer;
                                $maxdrawdown = $maxdrawdownPer;
                                $query = \Yii::$app->db->createCommand("insert into drawdown(drawdown,trader_id) values (" . $maxdrawd . "," . $v["id"] . ")")->execute();
                                echo "77 \n ";
                            } else {
                                $maxdrawdownPer = 100.0;
                                $drawDown = $maxdrawdownPer;
                                $maxdrawdown = $maxdrawdownPer;
                                $query = \Yii::$app->db->createCommand("update drawdown set drawdown=" . $maxdrawd . " where trader_id =" . $v["id"])->execute();
                            }
                        }
                        $maxpeak = $balance;
                        $minpeak = $balance;
                        $query = \Yii::$app->db->createCommand("update traders_info set minpeak=" . $minpeak . ",maxpeak  =" . $maxpeak . " where trader_id =" . $v["id"])->execute();
                    }
                    if ($balance < $minpeak) {
                        $minpeak = $balance;
                        $query_ex = \Yii::$app->db->createCommand("update traders_info set minpeak=" . $minpeak . " where trader_id =" . $v["id"])->execute();
                    }
                    $drawDown = $maxpeak - $minpeak;
                    if ($maxdrawd < $drawDown) {
                        $maxdrawd = $drawDown;
                        $DCMaxDDownValue = $maxdrawd;
                        $query_up = \Yii::$app->db->createCommand("update traders_info set maxddvalue=" . $maxdrawd . " where trader_id =" . $v["id"])->execute();
                        if ($maxpeak != 0.0) {
                            $maxdrawdownPer = ($maxdrawd / $maxpeak * 100.0);
                            $drawDown = $maxdrawdownPer;
                            $maxdrawdown = $maxdrawdownPer;
                            $query_dr = \Yii::$app->db->createCommand("insert into drawdown(drawdown,trader_id) values (" . $maxdrawd . "," . $v["id"] . ")")->execute();
                        } else {
                            $maxdrawdownPer = 100.0;
                            $drawDown = $maxdrawdownPer;
                            $maxdrawdown = $maxdrawdownPer;
                            $query_ins_d = \Yii::$app->db->createCommand("insert into drawdown(drawdown,trader_id) values (" . $maxdrawd . "," . $v["id"] . ")")->execute();
                        }
                    }
                    $total_profit = 0;
                    $current_equity = 0;
                    $sql_threemonths_profit = $reportserver->get_threemonths_profit($startDate, $endDate);
                    $total_profit = $sql_threemonths_profit[0]['profit'];
                    $sql_current_equity = $reportserver->get_user_equity();
                    $current_equity = $sql_current_equity;
                    $past_equity = $current_equity - $total_profit;
                    if ($past_equity == 0)
                        $ROI =0.0;
                    else {
                        $ROI = ($total_profit / $past_equity) * 100;
                        $ROI = round($ROI, 2);
                    }
                    $investors_update = \Yii::$app->db->createCommand("update traders_info set last_updated='" . date('Y-m-d H:i:s') . "' where trader_id =" . $v["id"])->execute();

                    $query = \Yii::$app->db->createCommand("update traders_info set roi=" . $ROI . " where trader_id =" . $v["id"])->execute();
                }
            }
            $command->finish();
            return Controller::EXIT_CODE_NORMAL;
        }
    }

    public function actionIndex() {
        return $this->actionInit(date("Y-m-d"), date("Y-m-d"));
    }

    public function actionYesterday() {
        return $this->actionInit(date("Y-m-d", strtotime("-1 days")), date("Y-m-d", strtotime("-1 days")));
    }

}
