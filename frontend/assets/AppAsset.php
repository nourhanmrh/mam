<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/metronic/perfect-scrollbar.css',
        'css/metronic/tether.css',
        'css/line-awesome/css/line-awesome.css',
        'css/metronic-font/css/styles.css',
        'css/flaticon/css/flaticon.css',
        'css/metronic/styles.css',
        'css/metronic/style.bundle.css',
        'css/msdropdown/dd.css',
        'css/msdropdown/flags.css',
        'css/kendo/kendo.common.min.css',
        'css/kendo.bootstrap.min.css',
        'css/kendo.common-bootstrap.min.css',
        'css/kendo/kendo.material.min.css',
        'css/metronic/login.css',
        'css/metronic/plugins.bundle.css',
        'css/animate.css',
//        'css/kendo/kendo.rtl.min.css',
//        'css/kendo/kendo.common-material.min.css',
//        'css/kendo/kendo.material.min.css',
        'css/kendo/kendo.mobile.all.min.css',
//        'css/chartjs/style.css'
    ];
    public $js = [
        'js/metronic/popper.js',
        'js/metronic/bootstrap.min.js',
        'js/metronic/js.cookie.js',
        'js/metronic/moment.min.js',
       // 'js/metronic/tooltip.min.js',
//        'js/metronic/plugins.bundle.js',
        'js/metronic/scripts.bundle.js',
        'js/bootstrap-notify.min.js',
        'js/metronic/perfect-scrollbar.js',
        'js/metronic/wNumb.js',
        'js/metronic/bootstrap.min.js',
        'js/metronic/switchbut.js',
        'js/msdropdown/jquery.dd.min.js',
        'js/kendo-new.js',
//        'js/kendo.all.min.js',
        'js/chartjs/chart.min.js',
        'js/chartjs/utils.js',
        'js/chartjs/analyser.js',
        'js/metronic/login.js',
        'js/loadingoverlay.min.js',
        'js/app.js',
        'js/owl.js'
       // 'js/metronic/buttonswitch.js',
        //'js/metronic/datatables.bundle.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
