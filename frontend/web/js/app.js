var Loader = {
    show: function (id) {
        var customElement = $("<img src='/media/loader-mamgram.gif'>");
        $(id).LoadingOverlay("show", {
            image: "",
            custom: customElement,
            progress: false,
            background: "#d3d3d361"
        });
    },
    stop: function (id) {
        $(id).LoadingOverlay("hide");
    }
}

var Notify = {
    show: function (message, type) {
        $.notify({
            message: message
        }, {
            type: type
        });
    }
}