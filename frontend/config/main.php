<?php

$params = array_merge(
        require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

$config = [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'main' => [
            'class' => 'frontend\modules\main\Module',
        ],
        'user' => [
            'class' => 'frontend\modules\user\Module'
        ],
        'traders' => [
            'class' => 'frontend\modules\traders\Module'
        ],
        'leaderboard' => [
            'class' => 'frontend\modules\leaderboard\Module',
        ],
        'profile' => [
            'class' => 'frontend\modules\profile\Module',
        ],
        'accounts' => [
            'class' => 'frontend\modules\accounts\Module',
        ],
        'brokers' => [
            'class' => 'frontend\modules\brokers\Module',
        ],
        'webservices' => [
            'class' => 'frontend\modules\webservices\Module',
        ],
        'email-templates' => [
            'class' => \ymaker\email\templates\Module::class,
            'languageProvider' => [
                'class' => \motion\i18n\ConfigLanguageProvider::class,
                'languages' => [
                    [
                        'locale' => 'en-US',
                        'label' => 'English',
                    ]
                ],
                'defaultLanguage' => [
                    'locale' => 'en-US',
                    'label' => 'English',
                ],
            ],
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ], 'templateManager' => [
            'class' => \ymaker\email\templates\components\TemplateManager::class,
        ],
        'emailTemplates' => [
            'class' => bl\emailTemplates\components\TemplateManager::class
        ],
    

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '<alias:all-traders|traders-info|profiles>' => 'site/<alias>',
                [
                    'pattern' => '<action:(login|logout|confirm|reset-request|register|reset-password|change-password)>',
                    'route' => 'user/auth/<action>'
                ],
                ['pattern' => '<action:(statistics|join-as-trader|get-statistics)>',
                    'route' => 'traders/trader/<action>'],
                [
                    'pattern' => '<action:(|index|top-traders|sse)>',
                    'route' => 'main/main/<action>'
                ],
                [
                    'pattern' => '<action:(traders|get-traders|follow|get-own-traders|update-following-configuration|rating|countryz)>',
                    'route' => 'leaderboard/traders/<action>'
                ],
                [
                    'pattern' => '<action:(trader-info|profit-chart|pie-data|drawdown-chart|trades)>',
                    'route' => 'leaderboard/trader-info/<action>'
                ],
                [
                    'pattern' => '<action:(profile|accounts|activate-account|profile-trades)>',
                    'route' => 'profile/main/<action>'
                ],
                [
                    'pattern' => '<action:(open-account|open-live|open-demo|accounts-request)>',
                    'route' => 'accounts/main/<action>'
                ],
                [
                    'pattern' => '<action:(brokers|get-brokers|to-broker)>',
                    'route' => 'brokers/main/<action>'
                ],
                [
                    'pattern' => 'webservices/images/<action>',
                    'route' => 'webservices/images/<action>'
                ]
            ],
        ],
    ],
    'params' => $params,
];
if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '80.81.147.106'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}
return $config;
