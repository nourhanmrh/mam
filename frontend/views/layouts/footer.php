<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class=" kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer" style="">
    <!--    <div class="container kt-footer__top">
                <div class="">
                    <div class="col-lg-4">
                        <div class="kt-footer__section">
                            <h3 class="kt-footer__title">About</h3>
                            <div class="kt-footer__content">
                                Lorem Ipsum is simply dummy text of the printing<br> 
                                and typesetting and typesetting industry has been the <br>
                                industry's standard dummy text ever since the 1500s,<br>
                                when an unknown printer took a galley of type.								
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="kt-footer__section">
                            <h3 class="kt-footer__title">Quick Links</h3>
                            <div class="kt-footer__content">
                                <div class="kt-footer__nav">
                                    <div class="kt-footer__nav-section">
                                        <a href="#">General Reports</a>
                                        <a href="#">Dashboart Widgets</a>
                                        <a href="#">Custom Pages</a>
                                    </div>
                                    <div class="kt-footer__nav-section">
                                        <a href="#">User Setting</a>
                                        <a href="#">Custom Pages</a>
                                        <a href="#">Intranet Settings</a>
                                    </div>
                                </div>	
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="kt-footer__section">
                            <h3 class="kt-footer__title">Get In Touch</h3>
                            <div class="kt-footer__content">
                                <form action="" class="kt-footer__subscribe">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Enter Your Email">
                                        <div class="input-group-append">
                                            <button class="btn btn-brand" type="button">Join</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>				
                </div>				
        </div> 	 -->
    <div class="container kt-footer__bottom">
        <div class="kt-footer__wrapper">
            <a class="kt-header__brand-logo" href="?page=index&amp;demo=demo2">
                <img alt="Logo" src="/logo/logo.png" class="kt-header__brand-logo-sticky">
            </a>
            <div class="kt-footer__logo">
                <div class="kt-footer__copyright" style="margin: auto;">
                    2020&nbsp;©&nbsp;MamGram  
                </div>
            </div>				 
            <div class="kt-footer__menu">
                <a href="" target="_blank">Purchase Lisence</a>
                <a href="" target="_blank">Team</a>
                <a href="" target="_blank">Contact</a>
            </div>	
        </div>
    </div> 
</div>