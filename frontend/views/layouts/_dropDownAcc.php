<?php

$read = new \Kendo\Data\DataSourceTransportRead();
$read->url('/accounts')
        ->dataType('JSON')
        ->type('GET');

$transport = new \Kendo\Data\DataSourceTransport();
$transport->read($read);

$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data.data')
        ->total('data.total');

$dataSource = new \Kendo\Data\DataSource();
$dataSource->transport($transport)
        ->schema($schema);

$dropDownList = new \Kendo\UI\DropDownList('head_accs');
$dropDownList->dataTextField('name')->dataValueField('id')->value($active_account['id'])
        ->select('function(e) {
                if(source)
                    source.close();
                var name = e.dataItem["name"];
                $.ajax({
                    url: "/activate-account",
                    type: "post",
                    data: {name: name},
                    success: function (response) {
                        if (response) {                          
                        }
                    }
                    
                }); }')
        ->dataSource($dataSource)
        ->dataBound(new \Kendo\JavaScriptFunction('function (e) {$(".accounts-list").show()}'))
        ->attr('style', 'width:100%')
        ->height(400)
        ->headerTemplate('')
        ->template('<h6>#: data.name #</h6><p style="font-size:9px">#: data.description #</p></span>')
        ->valueTemplate('<span class="selected-value" style="") /><img src="/media/traders/live-#: data.is_live #.svg" width="35px" />&nbsp;&nbsp;<span>#:data.name#</span>'
);

echo $dropDownList->render();
?>
