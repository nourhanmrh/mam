<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="container site-error" id="elements-form-block" style="margin-top: 25px;">

        <h1><?= Html::encode($this->title) ?></h1>

        <div class="alert alert-danger">
            <?= nl2br(Html::encode($message)) ?>
        </div>

    <p>
        <?= Yii::t('frontend', 'error..'); ?>
    </p>
    <p>
        <?= Yii::t('frontend', 'error-contact..'); ?>
    </p>

</div>
