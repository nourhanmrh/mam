<style>
    .subs {
        font-size: 17px;padding-left: 50px;padding-right: 50px;
    }
    .btn-brand:hover {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    .btn-brand {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    @media screen and (min-width: 768px){
        .card {
            min-height: 600px;
        }
        .icon {
            height: 35%;
        }
        .text {
            height: 10%;
        }
        .fields {
            height: 20%;
        }
        .open-button {
            height: 20%;
        }
        .signup-text {
            height: 15%;
        }
    }
    
    b {
    font-weight: 550;
}
</style>
<div class="container" style="margin-top: 25px;">
    <div class="row">
        <div class="kt-portlet card" style="">
            <div class="kt-portlet__body" style="height: 100%"  >
                <div class="row" style="border-bottom: 1px solid #47b2a1 ;">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-8" style="font-size: 20px;line-height:2">
                        <b><?= Yii::t('frontend', 'Open Real Account'); ?></b>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-4">
                        <img src='/media/fxgrow.png' style=" max-width: 80px;float: right;    padding-bottom: 15px;" />
                    </div>
                </div>
                <div class="row" style="height: 100%;padding-top: 30px">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row icon" style="text-align: center;">
                            <div class="col-12">
                                <img src="/media/Real.png" />
                            </div>
                        </div>
                        <div class="row text" style="text-align: center;">
                            <div class="col-12 subs">
                                <b><?= Yii::t('frontend', 'Open your account with FxGrow Broker'); ?></b>
                            </div>
                        </div>
                        <div class="row fields" style="width: 70%;margin: auto;">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="form-group">
                                    <label><?= Yii::t('frontend', 'Login'); ?></label>
                                    <input type="text" class="form-control" name="login" value="" placeholder="Login ID" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="form-group">
                                    <label><?= Yii::t('frontend', 'Password'); ?></label>
                                    <input type="text" class="form-control" name="password" value="" placeholder="Account Password" />
                                </div>
                            </div>

                        </div>
                        <div class="row open-button" style="text-align: center;">
                            <div class="col-12" style="line-height: 5;">
                                <button type="reset" class="btn btn-brand"><?= Yii::t('frontend', 'Open Real Account'); ?></button>
                            </div>
                        </div>
                        <div class="row signup-text" style="text-align: center">
                            <div class="col-12">
                                <p><b><?= Yii::t('frontend', 'You dont have an account yet?'); ?></b></p>
                                <p>
                                    <a style="color: red;font-weight: bold;text-decoration: underline" href="https://fxgrow.com">Sign up now</a>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
