<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
?>
<style>
    .trader-img{
        width:100%;
        max-width: 100px;
        border-radius: 4px;
    }
    /*    .trader-row {
            height:115px;
        }*/
    .btn-primary {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    .star-ratings-css {
        unicode-bidi: bidi-override;
        color: #c5c5c5;
        font-size: 25px;
        position: relative;
        padding: 0;
        display: inline-block;
    }
    .star-ratings-css-top {
        color: #f39f25;
        padding: 0;
        position: absolute;
        z-index: 1;
        display: block;
        top: 0;
        left: 20% ;
        left: 0;
        overflow: hidden;
    }
    .star-ratings-css-bottom {
        padding: 0;
        display: block;
        z-index: 0;
        left: 20%;
    }
    svg {
        cursor: pointer;
    }
    .kt-portlet__head-title{
        text-align: left;
    }
    #windowForFilter{display:none}
    #undo {
        text-align: center;
        position: absolute;
        white-space: nowrap;
        padding: 1em;
        cursor: pointer;
    }
    .armchair {
        float: left;
        margin: 30px 30px 120px 30px;
        text-align: center;
    }
    .armchair img {
        display: block;
        margin-bottom: 10px;
    }
    .k-window-content a {
        color: #BBB;
    }
    .k-window-content p {
        margin-bottom: 1em;
    }
    div.k-window-content{
        overflow:hidden !important;
    }

    @media screen and (max-width: 1023px) {
        div.k-window {
            display: none !important;
        }
    }

    #windowForFilter .btn-brand:hover {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    #windowForFilter .btn-brand {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    #windowForFilter .btn-secondary:hover {
        border-color: #47b2a1 !important;
        background-color: #fff !important;
    }
    #windowForFilter .btn-secondary {
        border-color: #47b2a1 !important;
        background-color: #fff !important;
    }
    .k-window .k-button.k-bare {
        color: black !important;
    }
    .k-window .k-header {
        background-color: #fff !important;
    }
    b {
        font-weight: 550;
    }
    .card {
            min-height: 600px;
        }
        .broker-header div[class^="col"] {
            padding: 0 !important;
        }
</style>
<div class="container" style="margin-top: 25px;">
    <div class="row trader-row">
        <div class="kt-portlet card">
            <div class="" style='border-bottom: 1px solid rgba(0, 0, 0, .1);padding-left: 10px;'>
                <div class=" col-lg-3 col-md-3 col-sm-6 col-6 hidden-xs" style="font-size: 20px;line-height: 1.9;">
                    <b>Brokers</b>
                </div>

                <div class="kt-input-icon kt-input-icon--right col-lg-2 offset-lg-7 col-md-2 offset-md-7 col-sm-12 offset-sm-0 col-12 offset-0" >
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12" style="border-left: 1px solid lightgray;">
                            <div class="kt-input-icon kt-input-icon--right">
                                <input type="text" class="form-control" placeholder="Search..." id="generalSearch" style="border:0" >
                                    <span class="kt-input-icon__icon kt-input-icon__icon--right" style="cursor: pointer">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="row broker-header" style="text-align: center;font-weight: 550;    padding-bottom: 10px;    border-bottom: 1px solid #e5e5e5;">
                    <div class="col-lg-2 col-md-2 col-sm-3 col-3"><?= Yii::t('frontend', 'Broker'); ?></div>
                    <div class="col-2 hidden-sm hidden-xs"><?= Yii::t('frontend', 'Broker Name'); ?></div>
                    <div class="col-lg-1 col-md-1 col-sm-3 col-3"><?= Yii::t('frontend', 'Platform'); ?></div>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-3"><?= Yii::t('frontend', 'Min Deposit'); ?></div>
                    <div class="col-1  hidden-sm hidden-xs"><?= Yii::t('frontend', 'Currency'); ?></div>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-3"><?= Yii::t('frontend', 'Commission'); ?></div>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-3 hidden-sm hidden-xs"></div>
                </div>
                <?php for ($i = 0; $i < 10; $i++) { ?>
                <div class="row" style="text-align: center;font-weight: 550;    padding-bottom: 10px;    border-bottom: 1px solid #e5e5e5;padding-top: 10px;line-height: 4;">
                <div class="col-lg-2 col-md-2 col-sm-3 col-3">
                    <img src="/media/fxgrow.png" style="max-width: 90px" />
                </div>
                    <div class="col-2  hidden-sm hidden-xs">FxGrow</div>
                    <div class="col-lg-1 col-md-1 col-sm-3 col-3">Mt4</div>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-3">500$</div>
                    <div class="col-1 hidden-sm hidden-xs">USD</div>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-3">12$</div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                        <button type="button" class="btn btn-primary" style="width: 100%;padding: 2px;"><?= Yii::t('frontend', 'Open Live Account'); ?></button>
                    </div>
                </div>
                    <?php } ?>
            </div>
        </div>
    </div>
</div>
