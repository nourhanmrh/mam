<?php
/* @var $this yii\web\View */

use kv4nt\owlcarousel\OwlCarouselWidget;

$this->title = 'My Yii Application';
?>
<style>
    g [fill] {
        fill : #47b2a1;
    }

    #special-services svg {
        width:100px;
        height: 100px;
        display: block;
        margin: auto;
    }

    #special-services {
        text-align: center;
    }

    .kt-portlet {
        margin-bottom: 0px !important;
    }

    .title {
        margin-top: 15px;
        padding: 0;
        font-size: 1.5rem;
        font-weight: 600;
        line-height: 3.5;
        text-align: center;
    }
    /*    #special-services,#steps{
            margin-top: 50px;
            margin-bottom: 50px;
        }*/
    #registration-form button,#cards button {
        width: 100%;
        padding: 5px;
    }
    #registration-form .btn-primary,#cards .btn-primary {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }

    .card {
        font-size: 15px;
    }

    .card{
        box-shadow: 0px 0px 10px 1px rgba(185, 185, 185, 0.35);
        border-radius: 6px;
        padding-right: 10px;
        padding-left: 10px;
        //padding: 25px 8px;
    }
    #cards .row {
        text-align: center;
        margin-top:20px;
    }
    #steps button,#registration-form button {
        border: 1px solid #47b2a1;
    }
    .carousel-wrap {
        width: 1000px;
        margin: auto;
        position: relative;
    }
    .owl-carousel .owl-nav{
        overflow: hidden;
        height: 0px;
    }

    .owl-theme .owl-dots .owl-dot.active span, 
    .owl-theme .owl-dots .owl-dot:hover span {
        background: #2caae1;
    }

    .owl-dots {
        display: none;
    }
    .owl-carousel .item {
        text-align: center;
    }
    .owl-carousel .nav-btn{
        height: 47px;
        position: absolute;
        width: 26px;
        cursor: pointer;
        top: 100px !important;
    }

    .owl-carousel .owl-prev.disabled,
    .owl-carousel .owl-next.disabled{
        pointer-events: none;
        opacity: 0.2;
    }

    .owl-carousel .prev-slide{
        background: url(/media/nav-icon.png) no-repeat scroll 0 0;
        left: -33px;
    }
    @media screen and (max-width: 768px){
        .owl-carousel .prev-slide{
            left: 20px !important;
        }
        .owl-carousel .next-slide{
            right: 20px !important;
        }
        .owl-carousel .nav-btn{
            top : 20px !important;
        }
        .title {
            line-height: 1.5;
            padding-bottom: 10px;
        }
    }
    .owl-carousel .next-slide{
        background: url(/media/nav-icon.png) no-repeat scroll -24px 0px;
        right: -33px;
    }
    .owl-carousel .prev-slide:hover{
        background-position: 0px -53px;
    }
    .owl-carousel .next-slide:hover{
        background-position: -24px -53px;
    }

    span.img-text {
        text-decoration: none;
        outline: none;
        transition: all 0.4s ease;
        -webkit-transition: all 0.4s ease;
        -moz-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        cursor: pointer;
        width: 100%;
        font-size: 23px;
        display: block;
        text-transform: capitalize;
    }
    span.img-text:hover {
        color: #2caae1;
    }
    b {
        font-weight: 550;
    }

</style>
<div class="container home">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-12" style="color:#000;margin-top: 50px;">

            <h1 style="font-size: 3rem;"  class="kt-invoice__title">
                <?= Yii::t('frontend', 'Automate Your Trading'); ?></h1>
            <br>
                <h4 style="font-size: 1.5rem;"><?= Yii::t('frontend', 'Analyze Professional Strategies'); ?> </h4>
                <h4 style="font-size: 1.5rem;"><?= Yii::t('frontend', 'Choose Among Many Forex Brokers'); ?> </h4>
                <h4 style="font-size: 1.5rem;margin-bottom: 20px"><?= Yii::t('frontend', 'Follow Professional Traders'); ?> </h4>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-12 "><img style="width:100%;margin-bottom: 40px;" src="media/computer.png" /></div></div>
    <div class="row" id="special-services">
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <p class="animated  jackInTheBox">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <g id="automated"><path fill="" d="M59.06,44.925A29.508,29.508,0,0,0,62,32,29.931,29.931,0,0,0,51.73,9.41,3.959,3.959,0,0,0,52,8a3.989,3.989,0,0,0-6.739-2.9A30.019,30.019,0,0,0,32,2,29.508,29.508,0,0,0,19.075,4.94,9.995,9.995,0,1,0,4.94,19.075,29.508,29.508,0,0,0,2,32,29.931,29.931,0,0,0,12.27,54.59,3.959,3.959,0,0,0,12,56a3.989,3.989,0,0,0,6.739,2.9A30.019,30.019,0,0,0,32,62a29.508,29.508,0,0,0,12.925-2.94A9.995,9.995,0,1,0,59.06,44.925ZM48,6a2,2,0,1,1-2,2A2,2,0,0,1,48,6ZM4,12a8,8,0,1,1,8,8A8.009,8.009,0,0,1,4,12ZM16,58a2,2,0,1,1,2-2A2,2,0,0,1,16,58Zm16,2a27.594,27.594,0,0,1-12.2-2.807A3.963,3.963,0,0,0,20,56a4,4,0,0,0-4-4,3.962,3.962,0,0,0-2.563.955A27.941,27.941,0,0,1,4,32,27.535,27.535,0,0,1,6.539,20.367,9.988,9.988,0,0,0,20.367,6.539,27.922,27.922,0,0,1,44.2,6.807,3.963,3.963,0,0,0,44,8a4,4,0,0,0,4,4,3.962,3.962,0,0,0,2.563-.955A27.941,27.941,0,0,1,60,32a27.535,27.535,0,0,1-2.539,11.633A9.988,9.988,0,0,0,43.633,57.461,27.535,27.535,0,0,1,32,60Zm20,0a8,8,0,1,1,8-8A8.009,8.009,0,0,1,52,60Z"/><path fill="" d="M32,12A20.022,20.022,0,0,1,51.863,29.741l-1.47-1.234-1.286,1.532,3.25,2.727a1,1,0,0,0,1.409-.123l2.727-3.25-1.532-1.286-1.115,1.33A22.01,22.01,0,0,0,23.6,11.659l.764,1.849A19.869,19.869,0,0,1,32,12Z"/>
                        <path fill="" d="M32,52A20.012,20.012,0,0,1,13.5,24.381l-1.849-.762A22.012,22.012,0,0,0,32,54a22.242,22.242,0,0,0,4.142-.389l-.374-1.965A20.2,20.2,0,0,1,32,52Z"/>
                        <path fill="" d="M37.732,51.167l.572,1.916c.687-.2,1.367-.445,2.022-.713l-.759-1.851C38.973,50.762,38.355,50.98,37.732,51.167Z"/>
                        <path fill="" d="M19,41H45a3,3,0,0,0,3-3V26a3,3,0,0,0-3-3H19a3,3,0,0,0-3,3V38A3,3,0,0,0,19,41ZM18,26a1,1,0,0,1,1-1H45a1,1,0,0,1,1,1V38a1,1,0,0,1-1,1H19a1,1,0,0,1-1-1Z"/>
                        <path fill="" d="M32,37a5,5,0,1,0-5-5A5.006,5.006,0,0,0,32,37Zm0-8a3,3,0,1,1-3,3A3,3,0,0,1,32,29Z"/>
                        <path fill="" d="M22,29h2V27H21a1,1,0,0,0-1,1v3h2Z"/>
                        <path fill="" d="M42,31h2V28a1,1,0,0,0-1-1H40v2h2Z"/>
                        <path fill="" d="M21,37h3V35H22V33H20v3A1,1,0,0,0,21,37Z"/>
                        <path fill="" d="M44,36V33H42v2H40v2h3A1,1,0,0,0,44,36Z"/>
                        <path fill="" d="M13,15H11a1,1,0,0,1-1-1H8a3,3,0,0,0,3,3v1h2V17a3,3,0,0,0,0-6H11a1,1,0,0,1,0-2h2a1,1,0,0,1,1,1h2a3,3,0,0,0-3-3V6H11V7a3,3,0,0,0,0,6h2a1,1,0,0,1,0,2Z"/>
                        <path fill="" d="M51,49h2a1,1,0,0,1,1,1h2a3,3,0,0,0-3-3V46H51v1a3,3,0,0,0,0,6h2a1,1,0,0,1,0,2H51a1,1,0,0,1-1-1H48a3,3,0,0,0,3,3v1h2V57a3,3,0,0,0,0-6H51a1,1,0,0,1,0-2Z"/>
                    </g></svg>
            </p>
            <p><h4><b><?= Yii::t('frontend', 'Automated Trading'); ?></b></h4></p>
            <p><?= Yii::t('frontend', 'Terms-agreement'); ?></p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <p class="animated  jackInTheBox ">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 512.001 512.001" style="enable-background:new 0 0 512.001 512.001;" xml:space="preserve">
                    <g>
                        <g>
                            <g>
                                <path fill="" d="M466.395,88.411C395.95,69.109,325.091,39.054,261.478,1.496c-3.379-1.995-7.572-1.995-10.95,0
                                      C185.08,40.133,118.05,68.562,45.605,88.411c-4.68,1.281-7.924,5.535-7.924,10.388v110.046
                                      c0,113.323,52.279,188.335,96.137,231.306c47.216,46.265,102.216,71.85,122.185,71.85c19.967,0,74.967-25.585,122.183-71.85
                                      c43.857-42.97,96.133-117.982,96.133-231.306V98.798C474.319,93.946,471.075,89.692,466.395,88.411z M452.779,208.844
                                      c0,105.843-48.761,175.838-89.669,215.92c-46.431,45.495-96.074,65.695-107.107,65.695c-11.033,0-60.679-20.2-107.111-65.695
                                      c-40.907-40.083-89.67-110.077-89.67-215.92V106.974C128.5,87.304,193.018,59.853,256.005,23.25
                                      c61.414,35.632,129.151,64.448,196.774,83.72V208.844z"/>
                                <path fill="" d="M160.538,105.769c-2.18-5.535-8.433-8.254-13.969-6.073c-19.24,7.581-38.988,14.559-58.695,20.741
                                      c-4.491,1.41-7.547,5.57-7.547,10.276v41.591c0,5.948,4.823,10.77,10.77,10.77s10.77-4.822,10.77-10.77v-33.72
                                      c17.679-5.72,35.339-12.047,52.598-18.848C160,117.557,162.719,111.304,160.538,105.769z"/>
                                <path fill="" d="M180.997,107.812c1.445,0,2.912-0.291,4.319-0.905l0.198-0.086c5.449-2.388,7.903-8.731,5.515-14.178
                                      c-2.39-5.449-8.769-7.914-14.212-5.528l-0.174,0.075c-5.452,2.381-7.914,8.719-5.533,14.169
                                      C172.877,105.405,176.842,107.812,180.997,107.812z"/>
                                <path fill="" d="M384.322,347.283c-4.977-3.253-11.651-1.854-14.908,3.125c-8.875,13.584-19.287,26.592-30.951,38.659
                                      c-9.592,9.922-19.986,19.17-30.893,27.485c-4.729,3.606-5.639,10.364-2.034,15.095c2.121,2.779,5.328,4.241,8.572,4.241
                                      c2.278,0,4.573-0.719,6.523-2.207c11.765-8.971,22.975-18.944,33.317-29.642c12.611-13.044,23.881-27.124,33.499-41.849
                                      C390.702,357.21,389.301,350.536,384.322,347.283z"/>
                                <path fill="" d="M282.558,433.443l-0.618,0.364c-5.147,2.981-6.906,9.569-3.926,14.716c1.997,3.45,5.612,5.376,9.331,5.376
                                      c1.83,0,3.688-0.467,5.385-1.452l0.713-0.419c5.133-3.006,6.857-9.603,3.851-14.736
                                      C294.286,432.161,287.688,430.44,282.558,433.443z"/>
                                <path fill="" d="M182.589,234.019c-6.613-6.614-15.408-10.254-24.762-10.254s-18.15,3.641-24.766,10.254
                                      c-13.653,13.656-13.653,35.876,0,49.531l63.596,63.594c6.614,6.612,15.409,10.253,24.764,10.253s18.15-3.641,24.765-10.255
                                      L378.947,214.38c13.652-13.659,13.652-35.876-0.002-49.527c-6.614-6.614-15.409-10.254-24.765-10.254
                                      c-9.355,0-18.15,3.641-24.765,10.254L221.42,272.848L182.589,234.019z M344.647,180.085c2.545-2.545,5.932-3.946,9.534-3.946
                                      c3.604,0,6.988,1.401,9.535,3.946c5.255,5.255,5.255,13.809-0.002,19.066l-132.759,132.76c-2.545,2.545-5.932,3.946-9.534,3.946
                                      s-6.989-1.401-9.535-3.946l-63.594-63.592c-5.257-5.257-5.257-13.811-0.002-19.066c2.546-2.545,5.933-3.948,9.536-3.948
                                      s6.988,1.401,9.533,3.946l46.445,46.446c2.021,2.019,4.759,3.154,7.616,3.154s5.595-1.134,7.614-3.154L344.647,180.085z"/>
                            </g>
                        </g>

                </svg>

            </p>
            <p><h4><b><?= Yii::t('frontend', 'Secure Payment'); ?></b></h4></p>
            <p><?= Yii::t('frontend', 'Terms-agreement'); ?></p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <p class="animated  jackInTheBox">
                <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 368.029 368.029" style="enable-background:new 0 0 368.029 368.029;" xml:space="preserve">
                    <g>
                        <g>
                            <path fill="" d="M363.568,204.926c-2.056-7.448-3.992-14.488-3.992-20.912c0-6.424,1.936-13.464,3.992-20.912
                                  c3.272-11.856,6.648-24.12,2.608-36.04c-4.192-12.4-14.64-20.504-24.744-28.328c-6.056-4.696-11.776-9.128-15.488-14.008
                                  c-3.776-4.976-6.448-11.616-9.264-18.648c-4.648-11.552-9.448-23.504-20.2-31.008c-10.576-7.392-23.776-8.104-36.52-8.784
                                  c-7.952-0.424-15.472-0.832-21.768-2.792c-5.888-1.832-11.984-5.632-18.432-9.656c-10.376-6.48-22.144-13.824-35.752-13.824
                                  c-13.608,0-25.376,7.344-35.752,13.832c-6.448,4.024-12.528,7.832-18.424,9.656c-6.304,1.96-13.824,2.368-21.776,2.792
                                  c-12.752,0.688-25.936,1.392-36.52,8.784c-10.752,7.504-15.552,19.448-20.2,31c-2.832,7.032-5.504,13.68-9.28,18.656
                                  c-3.712,4.896-9.432,9.328-15.488,14.032c-10.096,7.832-20.528,15.92-24.72,28.304c-4.04,11.928-0.664,24.184,2.6,36.048
                                  c2.056,7.448,3.992,14.48,3.992,20.904s-1.936,13.464-3.992,20.912c-3.264,11.856-6.64,24.12-2.6,36.056
                                  c4.192,12.384,14.632,20.488,24.728,28.312c6.056,4.688,11.776,9.128,15.48,14.008c3.784,4.976,6.456,11.616,9.28,18.656
                                  c4.648,11.552,9.456,23.504,20.216,31.024c10.568,7.376,23.76,8.08,36.504,8.76c7.952,0.424,15.472,0.832,21.768,2.792
                                  c5.888,1.832,11.976,5.632,18.416,9.648c10.384,6.472,22.152,13.824,35.768,13.824c13.616,0,25.392-7.352,35.784-13.832
                                  c6.44-4.016,12.52-7.816,18.416-9.648c6.296-1.96,13.816-2.368,21.768-2.792c12.752-0.68,25.936-1.384,36.504-8.76
                                  c10.776-7.52,15.576-19.488,20.224-31.048c2.824-7.024,5.488-13.664,9.248-18.624c3.712-4.88,9.432-9.32,15.488-14.008
                                  c10.104-7.832,20.544-15.928,24.744-28.32C370.224,229.046,366.84,216.79,363.568,204.926z M351.023,235.854
                                  c-2.648,7.824-10.784,14.128-19.392,20.8c-6.6,5.12-13.432,10.416-18.424,16.976c-5.064,6.688-8.264,14.64-11.36,22.344
                                  c-3.944,9.824-7.672,19.096-14.528,23.88c-6.832,4.768-17.208,5.32-28.2,5.904c-8.584,0.456-17.472,0.928-25.68,3.488
                                  c-7.848,2.456-15.112,6.976-22.128,11.36c-9.4,5.872-18.272,11.408-27.304,11.408c-9.032,0-17.896-5.536-27.296-11.408
                                  c-7.008-4.376-14.264-8.904-22.12-11.36c-8.208-2.552-17.096-3.032-25.68-3.488c-10.992-0.584-21.376-1.144-28.2-5.904
                                  c-6.848-4.784-10.584-14.056-14.528-23.872c-3.096-7.704-6.304-15.664-11.376-22.352c-4.984-6.56-11.816-11.856-18.424-16.984
                                  c-8.6-6.672-16.728-12.968-19.376-20.792c-2.472-7.288,0.128-16.712,2.872-26.68c2.24-8.152,4.56-16.568,4.56-25.168
                                  c0-8.6-2.32-17.008-4.56-25.16c-2.752-9.968-5.344-19.384-2.872-26.672c2.648-7.824,10.768-14.12,19.368-20.792
                                  c6.608-5.128,13.448-10.432,18.424-17c5.088-6.688,8.288-14.656,11.392-22.36c3.936-9.816,7.664-19.08,14.504-23.848
                                  c6.832-4.776,17.224-5.328,28.224-5.912c8.576-0.464,17.464-0.936,25.664-3.496c7.864-2.448,15.128-6.984,22.152-11.368
                                  c9.384-5.848,18.248-11.384,27.272-11.384s17.896,5.536,27.288,11.4c7.024,4.392,14.28,8.92,22.144,11.368
                                  c8.2,2.552,17.08,3.024,25.664,3.488c11,0.584,21.384,1.152,28.216,5.92c6.848,4.784,10.576,14.048,14.512,23.848
                                  c3.104,7.712,6.296,15.672,11.384,22.36c4.984,6.56,11.816,11.856,18.416,16.976c8.608,6.68,16.744,12.984,19.392,20.824
                                  c2.472,7.272-0.136,16.688-2.88,26.656c-2.248,8.152-4.568,16.568-4.568,25.168s2.312,17.016,4.568,25.16
                                  C350.896,219.15,353.495,228.566,351.023,235.854z"/>
                        </g>
                    </g>
                    <g>
                        <g>
                            <path fill="" d="M184.008,64.014c-66.168,0-120,53.832-120,120s53.832,120,120,120s120-53.832,120-120S250.176,64.014,184.008,64.014z
                                  M184.008,288.014c-57.344,0-104-46.656-104-104s46.656-104,104-104c29.576,0,56.24,12.472,75.2,32.36l-76.24,107.288
                                  l-41.304-41.304c-3.128-3.128-8.184-3.128-11.312,0s-3.128,8.184,0,11.312l48,48c1.504,1.512,3.544,2.344,5.656,2.344
                                  c0.216,0,0.448-0.008,0.672-0.032c2.344-0.192,4.488-1.416,5.848-3.336l79.176-111.424c11.52,16.736,18.304,36.976,18.304,58.792
                                  C288.008,241.358,241.352,288.014,184.008,288.014z"/>
                        </g>
                </svg>

            </p>
            <p><h4><b><?= Yii::t('frontend', 'Guaranteed Investment'); ?></b></h4></p>
            <p><?= Yii::t('frontend', 'Terms-agreement'); ?></p>
        </div>
    </div>
    <div class="row title" ><div class="col-12"><?= Yii::t('frontend', 'Below are some statistics of our professional traders'); ?></div></div>
    <div class="row" id="cards">
        <?php
        OwlCarouselWidget::begin([
            'container' => 'div',
            'containerOptions' => [
                'id' => 'container-id',
                'class' => 'container-class'
            ],
            'pluginOptions' => [
                'autoplay' => false,
                'autoplayTimeout' => 0,
                'items' => 3,
                'nav' => true,
                'navText' => ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"],
                'responsive' => [
                    "0" => [
                        "items" => 1
                    ],
                    "600" => [
                        "items" => 3
                    ],
                    "1000" => [
                        "items" => 3
                    ]
                ],
                'loop' => false,
                'itemsDesktop' => [1199, 3],
                'itemsDesktopSmall' => [979, 3]
            ]
        ]);

        for ($i = 0; $i < 6; $i++) {
            ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 item-class">
                <div class="card">
                    <div class="row">
                        <div class="col-12">
                            <h5><b>Master<?= $i ?></b></h5>
                            <p>Hassan Harb</p>
                        </div>
                    </div>
                    <div class="row">
                        <div style="height:200px;" class="wrapper col-12"><canvas style="max-height: 200px" id="chart-<?= $i ?>"></canvas></div>
                    </div>
                    <div class="row" style="margin-top:30px">
                        <div class="col-6">
                            <h5><b>Amount Following</b></h5>
                            <p>$51978.9</p>  
                        </div>
                        <div class="col-6">
                            <h5><b>Investors</b></h5>
                            <p>2</p>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <h5><b>Max DD</b></h5>
                            <p>5.2</p>  
                        </div>
                        <div class="col-6">
                            <h5><b>ROI</b></h5>
                            <p>12.2%</p>  
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-12">
                            <button type="button" class="btn btn-primary btn-lg">Go to trader</button>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>

        <?php OwlCarouselWidget::end(); ?>
    </div>
    <div class="row title"><div class="col-12">Open an account and Copy professional strategies</div></div>
    <div class="row" id="steps">
        <div class="kt-portlet card">
            <!--            <div class="kt-portlet__head" style='border:0'>
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                                Open an account and Copy professional strategies
                                </h3>
                            </div>
                        </div>-->
            <div class="kt-portlet__body">
                <ul class="nav nav-tabs nav-fill" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#kt_tabs_2_1">Step 1</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tabs_2_2">Step 2</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tabs_2_3">Step 3</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tabs_2_4">Step 4</a>
                    </li>
                </ul>                    

                <div class="tab-content">
                    <div class="tab-pane active" id="kt_tabs_2_1" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-12 col-12"><br>
                                    <h2>Register Now!</h2><br>
                                        <h5>Register to MamGram and create a profile</h5><br>
                                            <p>
                                                <button onclick="window.location = '/#registration-form'" class="btn btn-secondary btn-lg"> Register </button>
                                                
                                            </p>
                                            </div>
                                            <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                                                <img src="/media/index/verify.png" />
                                            </div>
                                            </div>
                                            </div>
                                            <div class="tab-pane" id="kt_tabs_2_2" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                                                        <br>
                                                            <h2>Choose a trader</h2><br>
                                                                <h5>Analyze the professional traders' strategies</h5><br>
                                                                    <p>
                                                                        <button type="button" class="btn btn-secondary btn-lg">Traders</button>
                                                                    </p>
                                                                    </div>
                                                                    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                                                                        <img src="/media/index/follow.png" />
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="kt_tabs_2_3" role="tabpanel">
                                                                        <div class="row">
                                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-12">   <br>
                                                                                    <h2>Open an account</h2><br>
                                                                                        <h5>Choose a broker and open a live account</h5><br>
                                                                                            <p>
                                                                                                <button type="button" class="btn btn-secondary btn-lg">Open an account</button>
                                                                                            </p>
                                                                                            </div>
                                                                                            <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                                                                                                <img src="/media/index/share.png" />
                                                                                            </div>
                                                                                            </div>                    </div>
                                                                                            <div class="tab-pane" id="kt_tabs_2_4" role="tabpanel">
                                                                                                <div class="row">
                                                                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-12">   <br>
                                                                                                            <h2><?= Yii::t('frontend', 'Automate Your Trading'); ?></h2><br>
                                                                                                                <h5>Let MamGram automate on your behalf</h5><br>
                                                                                                                    <p>
                                                                                                                        <button type="button" class="btn btn-secondary btn-lg">Open an account</button>
                                                                                                                    </p>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                                                                                                                        <img src="/media/index/register.png" />
                                                                                                                    </div>
                                                                                                                    </div> 
                                                                                                                    </div>
                                                                                                                    </div>      
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    <div class="row title"  ><div class="col-12">Register Now & Join Our Traders</div></div>
                                                                                                                    <div class="row" id="registration-form">
                                                                                                                        <div class="kt-portlet card">
                                                                                                                            <!--                                                                                                                            <div class="kt-portlet__head" style='border:0'>
                                                                                                                                                                                                        <div class="kt-portlet__head-label">
                                                                                                                                                                                                        <h3 class="kt-portlet__head-title">
                                                                                                                                                                                                        Register Now & Join Our Trader
                                                                                                                                                                                                        </h3>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>-->
                                                                                                                            <div class="kt-portlet__body">
                                                                                                                                <div class="row">
                                                                                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                                                                                                                        <form class="kt-form">
                                                                                                                                            <div class="row">
                                                                                                                                                <div class="col-lg-6">
                                                                                                                                                    <div class="form-group">
                                                                                                                                                        <label>First Name:</label>
                                                                                                                                                        <input type="email" class="form-control" placeholder="Enter first name">
                                                                                                                                                            <!--<span class="form-text text-muted">Please enter your full name</span>-->
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                                <div class="col-lg-6">
                                                                                                                                                    <div class="form-group">
                                                                                                                                                        <label>Last Name:</label>
                                                                                                                                                        <input type="email" class="form-control" placeholder="Enter last name">
                                                                                                                                                            <!--<span class="form-text text-muted">Please enter your full name</span>-->
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="row">
                                                                                                                                                <div class="col-lg-12">
                                                                                                                                                    <div class="form-group">
                                                                                                                                                        <label>Email:</label>
                                                                                                                                                        <input type="email" class="form-control" placeholder="Enter email">
                                                                                                                                                            <!--<span class="form-text text-muted">Please enter your full name</span>-->
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="row">
                                                                                                                                                <div class="col-lg-12">
                                                                                                                                                    <div class="form-group">
                                                                                                                                                        <label>Password:</label>
                                                                                                                                                        <input type="email" class="form-control" placeholder="Enter Password">
                                                                                                                                                            <!--<span class="form-text text-muted">Please enter your full name</span>-->
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="row">
                                                                                                                                                <div class="col-lg-4">
                                                                                                                                                    <div class="form-group">
                                                                                                                                                        <label>Country:</label>
                                                                                                                                                        <select name="countries" id="countries" style="width:100%;">
                                                                                                                                                            <option value='ad' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ad" data-title="Andorra">Andorra</option>
                                                                                                                                                            <option value='ae' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ae" data-title="United Arab Emirates">United Arab Emirates</option>
                                                                                                                                                            <option value='af' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag af" data-title="Afghanistan">Afghanistan</option>
                                                                                                                                                            <option value='ag' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ag" data-title="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                                                                                                            <option value='ai' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ai" data-title="Anguilla">Anguilla</option>
                                                                                                                                                            <option value='al' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag al" data-title="Albania">Albania</option>
                                                                                                                                                            <option value='am' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag am" data-title="Armenia">Armenia</option>
                                                                                                                                                            <option value='an' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag an" data-title="Netherlands Antilles">Netherlands Antilles</option>
                                                                                                                                                            <option value='ao' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ao" data-title="Angola">Angola</option>
                                                                                                                                                            <option value='aq' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag aq" data-title="Antarctica">Antarctica</option>
                                                                                                                                                            <option value='ar' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ar" data-title="Argentina">Argentina</option>
                                                                                                                                                            <option value='as' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag as" data-title="American Samoa">American Samoa</option>
                                                                                                                                                            <option value='at' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag at" data-title="Austria">Austria</option>
                                                                                                                                                            <option value='au' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag au" data-title="Australia">Australia</option>
                                                                                                                                                            <option value='aw' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag aw" data-title="Aruba">Aruba</option>
                                                                                                                                                            <option value='ax' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ax" data-title="Aland Islands">Aland Islands</option>
                                                                                                                                                            <option value='az' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag az" data-title="Azerbaijan">Azerbaijan</option>
                                                                                                                                                            <option value='ba' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ba" data-title="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                                                                                                                            <option value='bb' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bb" data-title="Barbados">Barbados</option>
                                                                                                                                                            <option value='bd' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bd" data-title="Bangladesh">Bangladesh</option>
                                                                                                                                                            <option value='be' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag be" data-title="Belgium">Belgium</option>
                                                                                                                                                            <option value='bf' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bf" data-title="Burkina Faso">Burkina Faso</option>
                                                                                                                                                            <option value='bg' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bg" data-title="Bulgaria">Bulgaria</option>
                                                                                                                                                            <option value='bh' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bh" data-title="Bahrain">Bahrain</option>
                                                                                                                                                            <option value='bi' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bi" data-title="Burundi">Burundi</option>
                                                                                                                                                            <option value='bj' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bj" data-title="Benin">Benin</option>
                                                                                                                                                            <option value='bm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bm" data-title="Bermuda">Bermuda</option>
                                                                                                                                                            <option value='bn' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bn" data-title="Brunei Darussalam">Brunei Darussalam</option>
                                                                                                                                                            <option value='bo' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bo" data-title="Bolivia">Bolivia</option>
                                                                                                                                                            <option value='br' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag br" data-title="Brazil">Brazil</option>
                                                                                                                                                            <option value='bs' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bs" data-title="Bahamas">Bahamas</option>
                                                                                                                                                            <option value='bt' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bt" data-title="Bhutan">Bhutan</option>
                                                                                                                                                            <option value='bv' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bv" data-title="Bouvet Island">Bouvet Island</option>
                                                                                                                                                            <option value='bw' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bw" data-title="Botswana">Botswana</option>
                                                                                                                                                            <option value='by' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag by" data-title="Belarus">Belarus</option>
                                                                                                                                                            <option value='bz' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag bz" data-title="Belize">Belize</option>
                                                                                                                                                            <option value='ca' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ca" data-title="Canada">Canada</option>
                                                                                                                                                            <option value='cc' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cc" data-title="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                                                                                                                            <option value='cd' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cd" data-title="Democratic Republic of the Congo">Democratic Republic of the Congo</option>
                                                                                                                                                            <option value='cf' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cf" data-title="Central African Republic">Central African Republic</option>
                                                                                                                                                            <option value='cg' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cg" data-title="Congo">Congo</option>
                                                                                                                                                            <option value='ch' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ch" data-title="Switzerland">Switzerland</option>
                                                                                                                                                            <option value='ci' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ci" data-title="Cote D'Ivoire (Ivory Coast)">Cote D'Ivoire (Ivory Coast)</option>
                                                                                                                                                            <option value='ck' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ck" data-title="Cook Islands">Cook Islands</option>
                                                                                                                                                            <option value='cl' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cl" data-title="Chile">Chile</option>
                                                                                                                                                            <option value='cm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cm" data-title="Cameroon">Cameroon</option>
                                                                                                                                                            <option value='cn' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cn" data-title="China">China</option>
                                                                                                                                                            <option value='co' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag co" data-title="Colombia">Colombia</option>
                                                                                                                                                            <option value='cr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cr" data-title="Costa Rica">Costa Rica</option>
                                                                                                                                                            <option value='cs' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cs" data-title="Serbia and Montenegro">Serbia and Montenegro</option>
                                                                                                                                                            <option value='cu' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cu" data-title="Cuba">Cuba</option>
                                                                                                                                                            <option value='cv' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cv" data-title="Cape Verde">Cape Verde</option>
                                                                                                                                                            <option value='cx' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cx" data-title="Christmas Island">Christmas Island</option>
                                                                                                                                                            <option value='cy' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cy" data-title="Cyprus">Cyprus</option>
                                                                                                                                                            <option value='cz' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag cz" data-title="Czech Republic">Czech Republic</option>
                                                                                                                                                            <option value='de' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag de" data-title="Germany">Germany</option>
                                                                                                                                                            <option value='dj' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag dj" data-title="Djibouti">Djibouti</option>
                                                                                                                                                            <option value='dk' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag dk" data-title="Denmark">Denmark</option>
                                                                                                                                                            <option value='dm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag dm" data-title="Dominica">Dominica</option>
                                                                                                                                                            <option value='do' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag do" data-title="Dominican Republic">Dominican Republic</option>
                                                                                                                                                            <option value='dz' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag dz" data-title="Algeria">Algeria</option>
                                                                                                                                                            <option value='ec' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ec" data-title="Ecuador">Ecuador</option>
                                                                                                                                                            <option value='ee' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ee" data-title="Estonia">Estonia</option>
                                                                                                                                                            <option value='eg' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag eg" data-title="Egypt">Egypt</option>
                                                                                                                                                            <option value='eh' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag eh" data-title="Western Sahara">Western Sahara</option>
                                                                                                                                                            <option value='er' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag er" data-title="Eritrea">Eritrea</option>
                                                                                                                                                            <option value='es' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag es" data-title="Spain">Spain</option>
                                                                                                                                                            <option value='et' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag et" data-title="Ethiopia">Ethiopia</option>
                                                                                                                                                            <option value='fi' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag fi" data-title="Finland">Finland</option>
                                                                                                                                                            <option value='fj' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag fj" data-title="Fiji">Fiji</option>
                                                                                                                                                            <option value='fk' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag fk" data-title="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                                                                                                                            <option value='fm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag fm" data-title="Federated States of Micronesia">Federated States of Micronesia</option>
                                                                                                                                                            <option value='fo' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag fo" data-title="Faroe Islands">Faroe Islands</option>
                                                                                                                                                            <option value='fr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag fr" data-title="France">France</option>
                                                                                                                                                            <option value='fx' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag fx" data-title="France, Metropolitan">France, Metropolitan</option>
                                                                                                                                                            <option value='ga' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ga" data-title="Gabon">Gabon</option>
                                                                                                                                                            <option value='gb' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gb" data-title="Great Britain (UK)">Great Britain (UK)</option>
                                                                                                                                                            <option value='gd' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gd" data-title="Grenada">Grenada</option>
                                                                                                                                                            <option value='ge' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ge" data-title="Georgia">Georgia</option>
                                                                                                                                                            <option value='gf' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gf" data-title="French Guiana">French Guiana</option>
                                                                                                                                                            <option value='gh' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gh" data-title="Ghana">Ghana</option>
                                                                                                                                                            <option value='gi' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gi" data-title="Gibraltar">Gibraltar</option>
                                                                                                                                                            <option value='gl' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gl" data-title="Greenland">Greenland</option>
                                                                                                                                                            <option value='gm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gm" data-title="Gambia">Gambia</option>
                                                                                                                                                            <option value='gn' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gn" data-title="Guinea">Guinea</option>
                                                                                                                                                            <option value='gp' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gp" data-title="Guadeloupe">Guadeloupe</option>
                                                                                                                                                            <option value='gq' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gq" data-title="Equatorial Guinea">Equatorial Guinea</option>
                                                                                                                                                            <option value='gr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gr" data-title="Greece">Greece</option>
                                                                                                                                                            <option value='gs' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gs" data-title="S. Georgia and S. Sandwich Islands">S. Georgia and S. Sandwich Islands</option>
                                                                                                                                                            <option value='gt' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gt" data-title="Guatemala">Guatemala</option>
                                                                                                                                                            <option value='gu' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gu" data-title="Guam">Guam</option>
                                                                                                                                                            <option value='gw' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gw" data-title="Guinea-Bissau">Guinea-Bissau</option>
                                                                                                                                                            <option value='gy' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag gy" data-title="Guyana">Guyana</option>
                                                                                                                                                            <option value='hk' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag hk" data-title="Hong Kong">Hong Kong</option>
                                                                                                                                                            <option value='hm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag hm" data-title="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
                                                                                                                                                            <option value='hn' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag hn" data-title="Honduras">Honduras</option>
                                                                                                                                                            <option value='hr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag hr" data-title="Croatia (Hrvatska)">Croatia (Hrvatska)</option>
                                                                                                                                                            <option value='ht' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ht" data-title="Haiti">Haiti</option>
                                                                                                                                                            <option value='hu' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag hu" data-title="Hungary">Hungary</option>
                                                                                                                                                            <option value='id' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag id" data-title="Indonesia">Indonesia</option>
                                                                                                                                                            <option value='ie' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ie" data-title="Ireland">Ireland</option>
                                                                                                                                                            <option value='in' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag in" data-title="India" >India</option>
                                                                                                                                                            <option value='io' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag io" data-title="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                                                                                                            <option value='iq' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag iq" data-title="Iraq">Iraq</option>
                                                                                                                                                            <option value='ir' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ir" data-title="Iran">Iran</option>
                                                                                                                                                            <option value='is' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag is" data-title="Iceland">Iceland</option>
                                                                                                                                                            <option value='it' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag it" data-title="Italy">Italy</option>
                                                                                                                                                            <option value='jm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag jm" data-title="Jamaica">Jamaica</option>
                                                                                                                                                            <option value='jo' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag jo" data-title="Jordan">Jordan</option>
                                                                                                                                                            <option value='jp' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag jp" data-title="Japan">Japan</option>
                                                                                                                                                            <option value='ke' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ke" data-title="Kenya">Kenya</option>
                                                                                                                                                            <option value='kg' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag kg" data-title="Kyrgyzstan">Kyrgyzstan</option>
                                                                                                                                                            <option value='kh' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag kh" data-title="Cambodia">Cambodia</option>
                                                                                                                                                            <option value='ki' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ki" data-title="Kiribati">Kiribati</option>
                                                                                                                                                            <option value='km' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag km" data-title="Comoros">Comoros</option>
                                                                                                                                                            <option value='kn' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag kn" data-title="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                                                                                                            <option value='kp' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag kp" data-title="Korea (North)">Korea (North)</option>
                                                                                                                                                            <option value='kr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag kr" data-title="Korea (South)">Korea (South)</option>
                                                                                                                                                            <option value='kw' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag kw" data-title="Kuwait">Kuwait</option>
                                                                                                                                                            <option value='ky' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ky" data-title="Cayman Islands">Cayman Islands</option>
                                                                                                                                                            <option value='kz' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag kz" data-title="Kazakhstan">Kazakhstan</option>
                                                                                                                                                            <option value='la' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag la" data-title="Laos">Laos</option>
                                                                                                                                                            <option value='lb' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag lb" data-title="Lebanon" selected="selected">Lebanon</option>
                                                                                                                                                            <option value='lc' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag lc" data-title="Saint Lucia">Saint Lucia</option>
                                                                                                                                                            <option value='li' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag li" data-title="Liechtenstein">Liechtenstein</option>
                                                                                                                                                            <option value='lk' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag lk" data-title="Sri Lanka">Sri Lanka</option>
                                                                                                                                                            <option value='lr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag lr" data-title="Liberia">Liberia</option>
                                                                                                                                                            <option value='ls' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ls" data-title="Lesotho">Lesotho</option>
                                                                                                                                                            <option value='lt' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag lt" data-title="Lithuania">Lithuania</option>
                                                                                                                                                            <option value='lu' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag lu" data-title="Luxembourg">Luxembourg</option>
                                                                                                                                                            <option value='lv' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag lv" data-title="Latvia">Latvia</option>
                                                                                                                                                            <option value='ly' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ly" data-title="Libya">Libya</option>
                                                                                                                                                            <option value='ma' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ma" data-title="Morocco">Morocco</option>
                                                                                                                                                            <option value='mc' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mc" data-title="Monaco">Monaco</option>
                                                                                                                                                            <option value='md' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag md" data-title="Moldova">Moldova</option>
                                                                                                                                                            <option value='mg' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mg" data-title="Madagascar">Madagascar</option>
                                                                                                                                                            <option value='mh' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mh" data-title="Marshall Islands">Marshall Islands</option>
                                                                                                                                                            <option value='mk' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mk" data-title="Macedonia">Macedonia</option>
                                                                                                                                                            <option value='ml' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ml" data-title="Mali">Mali</option>
                                                                                                                                                            <option value='mm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mm" data-title="Myanmar">Myanmar</option>
                                                                                                                                                            <option value='mn' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mn" data-title="Mongolia">Mongolia</option>
                                                                                                                                                            <option value='mo' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mo" data-title="Macao">Macao</option>
                                                                                                                                                            <option value='mp' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mp" data-title="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                                                                                                            <option value='mq' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mq" data-title="Martinique">Martinique</option>
                                                                                                                                                            <option value='mr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mr" data-title="Mauritania">Mauritania</option>
                                                                                                                                                            <option value='ms' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ms" data-title="Montserrat">Montserrat</option>
                                                                                                                                                            <option value='mt' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mt" data-title="Malta">Malta</option>
                                                                                                                                                            <option value='mu' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mu" data-title="Mauritius">Mauritius</option>
                                                                                                                                                            <option value='mv' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mv" data-title="Maldives">Maldives</option>
                                                                                                                                                            <option value='mw' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mw" data-title="Malawi">Malawi</option>
                                                                                                                                                            <option value='mx' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mx" data-title="Mexico">Mexico</option>
                                                                                                                                                            <option value='my' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag my" data-title="Malaysia">Malaysia</option>
                                                                                                                                                            <option value='mz' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag mz" data-title="Mozambique">Mozambique</option>
                                                                                                                                                            <option value='na' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag na" data-title="Namibia">Namibia</option>
                                                                                                                                                            <option value='nc' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag nc" data-title="New Caledonia">New Caledonia</option>
                                                                                                                                                            <option value='ne' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ne" data-title="Niger">Niger</option>
                                                                                                                                                            <option value='nf' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag nf" data-title="Norfolk Island">Norfolk Island</option>
                                                                                                                                                            <option value='ng' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ng" data-title="Nigeria">Nigeria</option>
                                                                                                                                                            <option value='ni' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ni" data-title="Nicaragua">Nicaragua</option>
                                                                                                                                                            <option value='nl' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag nl" data-title="Netherlands">Netherlands</option>
                                                                                                                                                            <option value='no' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag no" data-title="Norway">Norway</option>
                                                                                                                                                            <option value='np' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag np" data-title="Nepal">Nepal</option>
                                                                                                                                                            <option value='nr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag nr" data-title="Nauru">Nauru</option>
                                                                                                                                                            <option value='nu' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag nu" data-title="Niue">Niue</option>
                                                                                                                                                            <option value='nz' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag nz" data-title="New Zealand (Aotearoa)">New Zealand (Aotearoa)</option>
                                                                                                                                                            <option value='om' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag om" data-title="Oman">Oman</option>
                                                                                                                                                            <option value='pa' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pa" data-title="Panama">Panama</option>
                                                                                                                                                            <option value='pe' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pe" data-title="Peru">Peru</option>
                                                                                                                                                            <option value='pf' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pf" data-title="French Polynesia">French Polynesia</option>
                                                                                                                                                            <option value='pg' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pg" data-title="Papua New Guinea">Papua New Guinea</option>
                                                                                                                                                            <option value='ph' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ph" data-title="Philippines">Philippines</option>
                                                                                                                                                            <option value='pk' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pk" data-title="Pakistan">Pakistan</option>
                                                                                                                                                            <option value='pl' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pl" data-title="Poland">Poland</option>
                                                                                                                                                            <option value='pm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pm" data-title="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                                                                                                                            <option value='pn' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pn" data-title="Pitcairn">Pitcairn</option>
                                                                                                                                                            <option value='pr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pr" data-title="Puerto Rico">Puerto Rico</option>
                                                                                                                                                            <option value='ps' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ps" data-title="Palestinian Territory">Palestinian Territory</option>
                                                                                                                                                            <option value='pt' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pt" data-title="Portugal">Portugal</option>
                                                                                                                                                            <option value='pw' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag pw" data-title="Palau">Palau</option>
                                                                                                                                                            <option value='py' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag py" data-title="Paraguay">Paraguay</option>
                                                                                                                                                            <option value='qa' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag qa" data-title="Qatar">Qatar</option>
                                                                                                                                                            <option value='re' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag re" data-title="Reunion">Reunion</option>
                                                                                                                                                            <option value='ro' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ro" data-title="Romania">Romania</option>
                                                                                                                                                            <option value='ru' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ru" data-title="Russian Federation">Russian Federation</option>
                                                                                                                                                            <option value='rw' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag rw" data-title="Rwanda">Rwanda</option>
                                                                                                                                                            <option value='sa' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sa" data-title="Saudi Arabia">Saudi Arabia</option>
                                                                                                                                                            <option value='sb' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sb" data-title="Solomon Islands">Solomon Islands</option>
                                                                                                                                                            <option value='sc' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sc" data-title="Seychelles">Seychelles</option>
                                                                                                                                                            <option value='sd' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sd" data-title="Sudan">Sudan</option>
                                                                                                                                                            <option value='se' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag se" data-title="Sweden">Sweden</option>
                                                                                                                                                            <option value='sg' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sg" data-title="Singapore">Singapore</option>
                                                                                                                                                            <option value='sh' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sh" data-title="Saint Helena">Saint Helena</option>
                                                                                                                                                            <option value='si' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag si" data-title="Slovenia">Slovenia</option>
                                                                                                                                                            <option value='sj' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sj" data-title="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                                                                                                                            <option value='sk' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sk" data-title="Slovakia">Slovakia</option>
                                                                                                                                                            <option value='sl' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sl" data-title="Sierra Leone">Sierra Leone</option>
                                                                                                                                                            <option value='sm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sm" data-title="San Marino">San Marino</option>
                                                                                                                                                            <option value='sn' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sn" data-title="Senegal">Senegal</option>
                                                                                                                                                            <option value='so' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag so" data-title="Somalia">Somalia</option>
                                                                                                                                                            <option value='sr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sr" data-title="Suriname">Suriname</option>
                                                                                                                                                            <option value='st' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag st" data-title="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                                                                                                            <option value='su' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag su" data-title="USSR (former)">USSR (former)</option>
                                                                                                                                                            <option value='sv' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sv" data-title="El Salvador">El Salvador</option>
                                                                                                                                                            <option value='sy' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sy" data-title="Syria">Syria</option>
                                                                                                                                                            <option value='sz' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag sz" data-title="Swaziland">Swaziland</option>
                                                                                                                                                            <option value='tc' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tc" data-title="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                                                                                                                            <option value='td' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag td" data-title="Chad">Chad</option>
                                                                                                                                                            <option value='tf' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tf" data-title="French Southern Territories">French Southern Territories</option>
                                                                                                                                                            <option value='tg' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tg" data-title="Togo">Togo</option>
                                                                                                                                                            <option value='th' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag th" data-title="Thailand">Thailand</option>
                                                                                                                                                            <option value='tj' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tj" data-title="Tajikistan">Tajikistan</option>
                                                                                                                                                            <option value='tk' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tk" data-title="Tokelau">Tokelau</option>
                                                                                                                                                            <option value='tl' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tl" data-title="Timor-Leste">Timor-Leste</option>
                                                                                                                                                            <option value='tm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tm" data-title="Turkmenistan">Turkmenistan</option>
                                                                                                                                                            <option value='tn' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tn" data-title="Tunisia">Tunisia</option>
                                                                                                                                                            <option value='to' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag to" data-title="Tonga">Tonga</option>
                                                                                                                                                            <option value='tp' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tp" data-title="East Timor">East Timor</option>
                                                                                                                                                            <option value='tr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tr" data-title="Turkey">Turkey</option>
                                                                                                                                                            <option value='tt' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tt" data-title="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                                                                                                            <option value='tv' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tv" data-title="Tuvalu">Tuvalu</option>
                                                                                                                                                            <option value='tw' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tw" data-title="Taiwan">Taiwan</option>
                                                                                                                                                            <option value='tz' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag tz" data-title="Tanzania">Tanzania</option>
                                                                                                                                                            <option value='ua' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ua" data-title="Ukraine">Ukraine</option>
                                                                                                                                                            <option value='ug' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ug" data-title="Uganda">Uganda</option>
                                                                                                                                                            <option value='uk' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag uk" data-title="United Kingdom">United Kingdom</option>
                                                                                                                                                            <option value='um' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag um" data-title="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                                                                                                            <option value='us' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag us" data-title="United States">United States</option>
                                                                                                                                                            <option value='uy' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag uy" data-title="Uruguay">Uruguay</option>
                                                                                                                                                            <option value='uz' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag uz" data-title="Uzbekistan">Uzbekistan</option>
                                                                                                                                                            <option value='va' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag va" data-title="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                                                                                                                                                            <option value='vc' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag vc" data-title="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                                                                                                                                            <option value='ve' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ve" data-title="Venezuela">Venezuela</option>
                                                                                                                                                            <option value='vg' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag vg" data-title="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                                                                                                            <option value='vi' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag vi" data-title="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                                                                                                                                                            <option value='vn' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag vn" data-title="Viet Nam">Viet Nam</option>
                                                                                                                                                            <option value='vu' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag vu" data-title="Vanuatu">Vanuatu</option>
                                                                                                                                                            <option value='wf' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag wf" data-title="Wallis and Futuna">Wallis and Futuna</option>
                                                                                                                                                            <option value='ws' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ws" data-title="Samoa">Samoa</option>
                                                                                                                                                            <option value='ye' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag ye" data-title="Yemen">Yemen</option>
                                                                                                                                                            <option value='yt' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag yt" data-title="Mayotte">Mayotte</option>
                                                                                                                                                            <option value='yu' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag yu" data-title="Yugoslavia (former)">Yugoslavia (former)</option>
                                                                                                                                                            <option value='za' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag za" data-title="South Africa">South Africa</option>
                                                                                                                                                            <option value='zm' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag zm" data-title="Zambia">Zambia</option>
                                                                                                                                                            <option value='zr' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag zr" data-title="Zaire (former)">Zaire (former)</option>
                                                                                                                                                            <option value='zw' data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag zw" data-title="Zimbabwe">Zimbabwe</option>
                                                                                                                                                        </select>
                                                                                                                                                    </div></div>
                                                                                                                                                <div class="col-lg-8">
                                                                                                                                                    <div class="form-group">
                                                                                                                                                        <label>Phone Number:</label>
                                                                                                                                                        <input type="email" class="form-control" placeholder="Enter phone number">
                                                                                                                                                            <!--<span class="form-text text-muted">Please enter your full name</span>-->
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="row" style="line-height: 3;">
                                                                                                                                                <div class="col-lg-6">
                                                                                                                                                    <button type="button" class="btn btn-primary btn-lg">Register Now</button>
                                                                                                                                                </div>
                                                                                                                                                <div class="col-lg-6">
                                                                                                                                                    <button type="button" class="btn btn-secondary btn-lg">Reset</button>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </form>
                                                                                                                                    </div>
                                                                                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12  hidden-sm hidden-xs"><img src="/media/map2.png" style="max-width:100%" /></div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    </div>


                                                                                                                    <script>
                                                                                                                        $(document).ready(function () {
                                                                                                                            $("#countries").msDropdown();
                                                                                                                            var presets = window.chartColors;
                                                                                                                            var utils = Samples.utils;
                                                                                                                            var inputs = {
                                                                                                                                min: -100,
                                                                                                                                max: 100,
                                                                                                                                count: 8,
                                                                                                                                decimals: 2,
                                                                                                                                continuity: 1
                                                                                                                            };
                                                                                                                            function generateData(config) {
                                                                                                                                return utils.numbers(Chart.helpers.merge(inputs, config || {}));
                                                                                                                            }

                                                                                                                            function generateLabels(config) {
                                                                                                                                return utils.months(Chart.helpers.merge({
                                                                                                                                    count: inputs.count,
                                                                                                                                    section: 3
                                                                                                                                }, config || {}));
                                                                                                                            }


                                                                                                                            var options = {
                                                                                                                                responsive: true,
                                                                                                                                maintainAspectRatio: false,
                                                                                                                                spanGaps: false,
                                                                                                                                legend: {
                                                                                                                                    display: false
                                                                                                                                },
                                                                                                                                elements: {
                                                                                                                                    line: {
                                                                                                                                        tension: 0.4
                                                                                                                                    }
                                                                                                                                },
                                                                                                                                plugins: {
                                                                                                                                    filler: {
                                                                                                                                        propagate: false
                                                                                                                                    }
                                                                                                                                },
                                                                                                                                scales: {
                                                                                                                                    x: {
                                                                                                                                        ticks: {
                                                                                                                                            autoSkip: false,
                                                                                                                                            maxRotation: 0,
                                                                                                                                        },
                                                                                                                                    },
                                                                                                                                    xAxes: [{
                                                                                                                                            display: false
                                                                                                                                        }],
                                                                                                                                    yAxes: [{
                                                                                                                                            display: false
                                                                                                                                        }]
                                                                                                                                }
                                                                                                                            };
                                                                                                                            utils.srand(8);

                                                                                                                            //gradient.addColorStop(2, '#fff');
                                                                                                                            for (var i = 0; i < 6; i++) {
                                                                                                                                var ctx = document.getElementById("chart-" + i).getContext("2d");

                                                                                                                                /*** Gradient ***/
                                                                                                                                var gradient = ctx.createLinearGradient(0, 0, 0, 400);
                                                                                                                                gradient.addColorStop(0, '#47b2a1ab');
                                                                                                                                gradient.addColorStop(1, '#fff');
                                                                                                                                new Chart('chart-' + i, {
                                                                                                                                    type: 'line',
                                                                                                                                    data: {
                                                                                                                                        labels: generateLabels(),
                                                                                                                                        datasets: [{
                                                                                                                                                backgroundColor: gradient,
                                                                                                                                                borderColor: '#47b2a1',
                                                                                                                                                pointBorderColor: "#fff",
                                                                                                                                                pointBackgroundColor: "#47b2a1",
                                                                                                                                                pointHoverBackgroundColor: "#47b2a1",
                                                                                                                                                pointHoverBorderColor: "#47b2a1",
                                                                                                                                                pointBorderWidth: 2,
                                                                                                                                                pointHoverRadius: 8,
                                                                                                                                                pointHoverBorderWidth: 1,
                                                                                                                                                pointRadius: 5,
                                                                                                                                                data: generateData(),
                                                                                                                                                label: '',
                                                                                                                                                fill: 'start'
                                                                                                                                            }]
                                                                                                                                    },
                                                                                                                                    options: options
                                                                                                                                });
                                                                                                                            }
                                                                                                                        })
                                                                                                                    </script>