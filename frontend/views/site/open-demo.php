<style>
    .subs {
        font-size: 17px;padding-left: 50px;padding-right: 50px;
    }
    .btn-brand:hover {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    .btn-brand {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    @media screen and (min-width: 768px){
        .card {
            min-height: 550px;
        }
        .icon {
            height: 40%;
        }
        .text {
            height: 15%;
        }
        .fields {
            height: 30%;
        }
        .open-button {
            height: 15%;
        }
        b {
            font-weight: 550;
        }
    }
</style>
<div class="container" style="margin-top: 25px;">
    <div class="row">
        <div class="kt-portlet card" style="">
            <div class="kt-portlet__body" style="height: 100%">
                <div class="row">
                    <div class="col-12" style="border-bottom: 1px solid #47b2a1 ;font-size:20px;line-height:2;padding-bottom: 15px;">
                        <b><?= Yii::t('frontend', 'Open Demo Account'); ?></b>
                    </div>
                </div>
                <div class="row" style="height: 100%;padding-top: 30px">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row icon" style="text-align: center;">
                            <div class="col-12">
                                <img src="/media/Demo.png" />
                            </div>
                        </div>
                        <div class="row text" style="text-align: center;">
                            <div class="col-12 subs">
                                <b><?= Yii::t('frontend', 'Specify account details'); ?></b>
                            </div>
                        </div>
                        <div class="row fields" style="width: 70%;margin: auto;">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="form-group">
                                    <label><?= Yii::t('frontend', 'Base Currency'); ?></label>
                                    <select class="form-control" id="base-currency-text-input">
                                        <option>USD</option>
                                        <option>EUR</option>
                                        <option>GBP</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="form-group">
                                    <label>Leverage</label>
                                    <select class="form-control" id="leverage-text-input">
                                        <option>1:100</option>
                                        <option>1:200</option>
                                        <option>1:300</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="balance-text-input">Balance</label>
                                    <select class="form-control" id="balance-text-input">
                                        <option>500</option>
                                        <option>1000</option>
                                        <option>5000</option>
                                    </select>
                                    <label class="kt-checkbox kt-checkbox--success" style="margin-top: 5px;">
                                        <input type="checkbox" data-type="boolean" /><?= Yii::t('frontend', 'Enter custom amount'); ?> 
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row open-button" style="text-align: center;">
                            <div class="col-12">
                                <button type="reset" class="btn btn-brand"><?= Yii::t('frontend', 'Open Demo Account'); ?></button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
