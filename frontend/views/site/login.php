<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>

    .kt-login__btn-primary {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
        border-radius: 5px !important;
    }
    .kt-login__btn-secondary {
        border-radius: 5px !important;
        border-color: #47b2a1 !important;
        color: #47b2a1 !important;
    }
    .kt-login__container .form-control {
        border-radius: 5px !important;
        border-color: #47b2a1 !important;
        color: #47b2a1 !important;
        height: 46px;
        width:100%;
        padding-left: 1.5rem;
        padding-right: 1.5rem;
        margin-top: 1.5rem;
        border: 1px solid #47b2a1 !important; 
    }
    .kt-checkbox span {
        border: 1px solid #47b2a1 !important; 
    }
    .kt-checkbox>span:after {
        border: solid #47b2a1;
    }
    .card {
            min-height: 640px;
        }
  
</style>
<div class="container" style="margin-top: 25px;">
    <div class="row trader-row">
        <div class="kt-portlet card">
            <div class="kt-portlet__body">
                <div class="kt-grid kt-grid--ver kt-grid--root">
                    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
                        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(/metronic/themes/metronic/theme/default/demo1/dist/assets/media/bg/bg-2.jpg);">
                            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                                <div class="kt-login__container">
                                    <div class="kt-login__logo">
                                        <a href="#">
                                            <img src="logo/logo.png">  	
                                        </a>
                                    </div>
                                    <div class="kt-login__signin">
                                        <div class="kt-login__head">
                                            <h3 class="kt-login__title"><?= Yii::t('frontend', 'Log In To Admin'); ?></h3>
                                        </div>
                                        <form class="kt-form" action="">
                                            <div class="input-group">
                                                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
                                            </div>
                                            <div class="input-group">
                                                <input class="form-control" type="password" placeholder="Password" name="password">
                                            </div>
                                            <div class="row kt-login__extra" style="text-align: center;">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <label class="kt-checkbox">
                                                        <input type="checkbox" name="remember"><?= Yii::t('frontend', 'Remember me'); ?>
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-12 ">
                                                    <a href="javascript:;" id="kt_login_forgot" class="kt-login__link"><?= Yii::t('frontend', 'Forget Password ?'); ?></a>
                                                </div>
                                            </div>
                                            <div class="kt-login__actions">
                                                <button id="kt_login_signin_submit" class="btn btn-brand btn-pill kt-login__btn-primary"><?= Yii::t('frontend', 'Log In'); ?></button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                    <div class="kt-login__forgot">
                                        <div class="kt-login__head">
                                            <h3 class="kt-login__title"><?= Yii::t('frontend', 'Forgotten Password ?'); ?></h3>
                                            <div class="kt-login__desc"><?= Yii::t('frontend', 'Enter your email to reset your password:'); ?></div>
                                        </div>
                                        <form class="kt-form" action="">
                                            <div class="input-group">
                                                <input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
                                            </div>
                                            <div class="kt-login__actions">
                                                <button id="kt_login_forgot_submit" class="btn btn-brand btn-pill kt-login__btn-primary"><?= Yii::t('frontend', 'Request'); ?></button>&nbsp;&nbsp;
                                                <button id="kt_login_forgot_cancel" class="btn btn-secondary btn-pill kt-login__btn-secondary"><?= Yii::t('frontend', 'Cancel'); ?></button>
                                            </div>
                                        </form>
                                    </div>
                                   
                                </div>	
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
</div>
