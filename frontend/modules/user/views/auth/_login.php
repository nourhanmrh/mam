<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="kt-login__signin" id="login-form-container" >
    <div class="kt-login__head">
        <h3 class="kt-login__title">Log In To MamGram</h3>
    </div>
    <?php
    $form = ActiveForm::begin(
                    [
                        'id' => 'login-form',
                        'action' => '/login',
                        'options' => [
                            'class' => 'kt-form',
                            'data-pjax' => true
                        ]
                    ]
    );
    ?>
    <?=
            $form->field($model, 'email')->textInput([
                'autofocus' => true,
                'class' => 'form-control',
                "placeholder" => $model->getAttributeLabel('email')
            ])
            ->label(false)
    ?>

    <?=
            $form->field($model, 'password')->passwordInput([
                'class' => 'form-control',
                "placeholder" => $model->getAttributeLabel('password')
            ])
            ->label(false)
    ?>
    <div class="row kt-login__extra" style="text-align: center;">
        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
            <label class="kt-checkbox">
                <?= $form->field($model, 'rememberMe')->checkbox()->label(false) ?>
                Remember me
                <span></span>
            </label>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-12 ">
            <a href="javascript:;" id="kt_login_forgot" class="kt-login__link">Forget Password ?</a>
        </div>
    </div>
    <div class="kt-login__actions">
        <?= Html::submitButton('Login', ['class' => 'btn btn-brand btn-pill kt-login__btn-primary', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$js = <<< JS
           
$('#login-form').on('beforeSubmit', function () {
            Loader.show(".kt-portlet");
            var form = $(this);
                $.pjax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: new FormData($('#login-form')[0]),
                    container: "#login-form-container",
                    push: false,
                    scrollTo: false,
                    cache: false,
                    contentType: false,
                    timeout: 10000,
                    processData: false
                })
                .done(function(data) {
                    Loader.stop(".kt-portlet");
                    console.log('ok');
                })
                .fail(function () {
                    Loader.stop(".kt-portlet");
                })
            return false; // prevent default form submission
        });
        
JS;
$this->registerJs($js);
?>