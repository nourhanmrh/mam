<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .form-group {
        margin: 0 !important;
    }
  .kt-checkbox span {
        display: none;
    }
    .kt-login__btn-primary {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
        border-radius: 5px !important;
    }
    .kt-login__btn-secondary {
        border-radius: 5px !important;
        border-color: #47b2a1 !important;
        color: #47b2a1 !important;
    }
    .kt-login__container .form-control {
        border-radius: 5px !important;
        border-color: #47b2a1 !important;
        color: #47b2a1 !important;
        height: 46px;
        width:100%;
        padding-left: 1.5rem;
        padding-right: 1.5rem;
        margin-top: 1.5rem;
        border: 1px solid #47b2a1 !important; 
    }
    .kt-checkbox span {
        border: 1px solid #47b2a1 !important; 
    }
    .kt-checkbox>span:after {
        border: solid #47b2a1;
    }
    .card {
        min-height: 640px;
    }

</style>
<div class="container" id="elements-form-block" style="margin-top: 25px;">
    <div class="row trader-row">
        <div class="kt-portlet card">
            <div class="kt-portlet__body">
                <div class="kt-grid kt-grid--ver kt-grid--root">
                    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
                        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="">
                            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                                <div class="kt-login__container">
                                    <div class="kt-login__logo">
                                        <a href="#">
                                            <img src="logo/logo.png">  	
                                        </a>
                                    </div>
                                    <?= $this->render('_login', ['model' => $loginModel]) ?>
                                    <?= $this->render('_resetPasswordRequest', ['model' => $resetPasswordRequestModel]) ?>
                                </div>	
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
</div>
