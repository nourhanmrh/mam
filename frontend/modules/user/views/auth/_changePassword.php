<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\Notification;

?>
<div id="change-password-form-container">
    <p><b>Change Your Password</b></p>
    <p>
        Please input a secure and memorable password.
    </p>
    <p>
        It is important to change your password frequently and keep it private since anybody who knows your password may access your account. Do not save your password in your browser’s memory. Please note that this password will apply to any active accounts associated with your email address.
    </p>
    <?php
    $form = ActiveForm::begin([
                'id' => 'change-password-form',
                'action' => '/change-password',
                'options' => [
                    'data-pjax' => true
                ]
    ]);
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <p><b>Current Password</b></p>
                <p>
                    Enter your current password.
                </p>
                <?= $form->field($model, 'old_password')->passwordInput(['maxlength' => true])->label(false) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <p><b>New Password</b></p>
                <p>
                    Please enter a new password.
                </p>
                <p>
                    Use any combination of letters, numbers and special characters (minimum 8 characters). 
                </p>
                <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
            </div></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <p><b>Conﬁrm New Password</b></p>
                <p>
                    Please re-enter your new password.
                </p>
                <?= $form->field($model, 'confirm_password')->passwordInput()->label(false) ?>
            </div></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-12">
            <?= Html::submitButton('Change Password', ['class' => 'btn btn-brand', 'style' => 'width:100%']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php
$js = <<< JS
           
$('#change-password-form').on('beforeSubmit', function () {
            Loader.show("#change-password-form-container");
            var form = $(this);
                $.pjax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: new FormData($('#change-password-form')[0]),
                    container: "#change-password-form-container",
                    push: false,
                    scrollTo: false,
                    cache: false,
                    contentType: false,
                    timeout: 10000,
                    processData: false
                })
                .done(function(data) {
                    Loader.stop("#change-password-form-container");
                })
                .fail(function () {
                    // request failed
                    console.log('request failed');
                })
            return false; // prevent default form submission
        });
        
JS;
$this->registerJs($js);
?>