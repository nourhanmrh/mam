<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="kt-login__forgot" id="forgot-login">
    <div class="kt-login__head">
        <h3 class="kt-login__title">Forgotten Password ?</h3>
        <div class="kt-login__desc">Enter your email to reset your password:</div>
    </div>
    <?php
    $form = ActiveForm::begin(
                    [
                        'id' => 'reset-password-request-form',
                        'action' => '/reset-request',
                        'options' => [
                            'class' => 'kt-form',
                            'data-pjax' => true
                        ]
                    ]
    );
    ?>
    <?=
            $form->field($model, 'email')->textInput([
                'autofocus' => true,
                'class' => 'form-control',
                "placeholder" => $model->getAttributeLabel('email')
            ])
            ->label(false)
    ?>
    <div class="kt-login__actions">
        <?= Html::submitButton('Request', ['class' => 'btn btn-brand btn-pill kt-login__btn-primary', 'name' => 'forget-button', 'id' => 'forgot_submit']) ?> 
        <button id="kt_login_forgot_cancel" class="btn btn-secondary btn-pill kt-login__btn-secondary">Cancel</button>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$js = <<< JS
        
$('#reset-password-request-form').on('beforeSubmit', function () {
            Loader.show(".kt-portlet");
            var form = $(this);
                $.pjax({
                    type: form.attr('method'),
                    url: form.attr('action'), 
                    data: new FormData($('#reset-password-request-form')[0]),
                    container: "#forgot-login",
                    push: false,
                    scrollTo: false,
                    cache: false,
                    contentType: false,
                    timeout: 10000,
                    processData: false
                })
                .done(function(data) {
                    Loader.stop(".kt-portlet");
                })
                .fail(function () {
                    // request failed
                    console.log('request failed');
                })
            return false; // prevent default form submission
        });
        
JS;
$this->registerJs($js);
?>
