<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php
$form = ActiveForm::begin(
                [
                    'id' => 'register-form',
                    'action' => '/register',
                    'options' => [
                        'class' => 'kt-form',
                        'data-pjax' => true
                    ]
                ]
);
?>
<style>
    .showme {
        display: none;
    }

    .showhim:hover .showme {
        display: block;
    }
</style>
<div id="register-form-container">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Full Name:</label>
                <?=
                        $form->field($model, 'name')->textInput([
                            'class' => 'form-control',
                            "placeholder" => $model->getAttributeLabel('name')
                        ])
                        ->label(false)
                ?>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Username:</label>
                <?=
                        $form->field($model, 'username')->textInput([
                            'class' => 'form-control',
                            "placeholder" => $model->getAttributeLabel('username')
                        ])
                        ->label(false)
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <label>Email:</label>
                <?=
                        $form->field($model, 'email')->textInput([
                            'class' => 'form-control',
                            "placeholder" => $model->getAttributeLabel('email')
                        ])
                        ->label(false)
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Password:</label>


                <br>
                <?=
                        $form->field($model, 'password')->passwordInput([
                            'class' => 'form-control password',
                        ])
                        ->label(false)
                ?>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="showhim" ><img class="lazyload lazyload--done" height="20px" alt="234813383" src="https://t4.ftcdn.net/jpg/02/34/81/33/240_F_234813383_bE3zVSXclqYEDKxRtDazfB54jfSRsHCK.jpg">
                <div class="showme">          
                    <span class="disp" style="font-size:10px;font-weight: bold;display:none">a minimum of 1 lower case,1 upper case letter ,and 1 numeric character</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <label>Country:</label>
                <select name="SignupForm[country_code]" id="countries" style="width:100%;">
                    <?php
                    foreach ($countries as $key => $value) {
                        $selected = "";
                        if (strtoupper($model->country_code) == $key) {
                            $selected = "selected='selected'";
                        }
                        ?>
                        <option <?= $selected ?> value="<?= $key ?>" data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag flag-<?= strtolower($key) ?>" data-title="<?= $value ?>"><?= $value ?></option>
                    <?php } ?>    
                </select>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="form-group">
                <label>Phone Number:</label>
                <?=
                        $form->field($model, 'phone')->textInput([
                            'class' => 'form-control',
                            "placeholder" => $model->getAttributeLabel('phone')
                        ])
                        ->label(false)
                ?>
            </div>
        </div>
    </div>
    <div class="row" style="line-height: 3;">
        <div class="col-lg-6">
            <?= Html::submitButton('Register Now', ['class' => 'btn btn-primary btn-lg', 'name' => 'register-button']) ?>
        </div>
        <div class="col-lg-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-secondary btn-lg', 'name' => 'register-button']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php
$js = <<< JS
        
$('#register-form').on('beforeSubmit', function () {
            Loader.show("#registration-form");
            var form = $(this);
                $.pjax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: new FormData($('#register-form')[0]),
                    container: "#register-form-container",
                    push: false,
                    scrollTo: false,
                    cache: false,
                    contentType: false,
                    timeout: 10000,
                    processData: false
                })
                .done(function(data) {
                    Loader.stop("#registration-form");
                })
                .fail(function () {
                    // request failed
                    console.log('request failed');
                })
            return false; // prevent default form submission
        });
        
   $('.showhim').hover(function () {
    $(".disp").css("display", "block");
        $('.password').attr('type', 'text');
    }, function () {
        $('.password').attr('type', 'password');
    });
        
JS;
$this->registerJs($js);
?>
