<?php

/**
 * Created by PhpStorm.
 * User: Vladimir Baranov <phpnt@yandex.ru>
 * Git: <https://github.com/phpnt>
 * VK: <https://vk.com/phpnt>
 * Date: 19.08.2018
 * Time: 8:43
 */

namespace frontend\modules\user\controllers;

use Yii;
use common\models\forms\PasswordResetRequestForm;
use common\models\forms\ResetPasswordForm;
use common\models\forms\ConfirmationForm;
use common\models\forms\LoginForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Cookie;
use common\models\Constants;
use common\models\forms\SignupForm;
use common\models\users\Users;
use common\models\mamaccounts\MamAccounts;
use common\helpers\Notification;
use common\models\forms\PasswordResetForm;
use common\models\forms\ChangePasswordForm;
use common\models\country\Country;

class AuthController extends Controller {
    //public $layout = 'main-login';

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'register', 'request-password', 'reset-password', 'reset-request', 'confirm'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model = new RegisterForm();

        if ($model->load(Yii::$app->request->post())) {
            return $this->render('/users/profile', ['model' => $model]);
        }
        return $this->render('index', ['model' => $model]);
    }

    public function actionConfirm($key) {
        $model = new SignupForm();
        $query = Users::find()->where("confirmation_key ='" . $key . "'")->one();
        if ($query) {
            $x = Users::findOne($query['id']);
            $x->confirmed_at = date('Y-m-d H:i:s');
            $x->status = "active";
            $x->confirmation_key = '';
            if ($x->save()) {
                $cookies = Yii::$app->response->cookies;
                unset($cookies['verification']);
            } else {
                print_r($x->errors);die;
            }
        }
        $notification = new Notification("Your Profile Is Confirmed");
        $notification->success();
        return $this->goHome();
    }

    public function actionResetRequest() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->sendPasswordResetEmail()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
                $notification = new Notification("Kindly check your email to reset your password");
                $notification->success();
                return $this->redirect('/login');
            }
        }
        if ($model->errors) {
            return $this->renderAjax('@frontend/modules/user/views/auth/_resetPassword', [
                        'model' => $model
            ]);
        }
        return $this->redirect('/login');
    }

    public function actionResetPassword($token) {
        $message = "";
        $model = new PasswordResetForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($query = Users::find()->where("password_reset_token ='" . $token . "'")->one()) {
                $query->password = md5($model['password']);
                if ($query->save()) {
                    $query->password_reset_token = null;
                    $query->save();
                    $notification = new Notification("Your password is reset");
                    $notification->success();
                    return $this->redirect('/login');
                }
            } else {
                $notification = new Notification("Error reset password");
                $notification->error();
            }
            return $this->redirect('/login');
        }
        $model->password = "";
        return $this->render('reset-password', ['model' => $model]);
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect("/");
        }
        $resetPasswordRequestModel = new PasswordResetRequestForm();
        $loginModel = new LoginForm();
        $loginModel->types = Constants::USERS_FRONTEND_TYPES;
        if ($loginModel->load(Yii::$app->request->post()) && $loginModel->login()) {
                      
            $loginModel = Users::findByEmail($loginModel->email, $loginModel->types);
            $loginModel->generateAccessTokenAfterUpdatingClientInfo(true);
            $user= Users::findOne(['email'=>$loginModel->email]);           
            $active_acc= MamAccounts::find()->where(['user_id'=>$user['id']])->andWhere(['active_account'=>1])->one();     
            $session = \yii::$app->session;
            $session->open();
            $session->set('active_account', $active_acc['name']);          
            $id = implode(',', array_values($loginModel->getPrimaryKey(true)));
            $notification = new Notification("Login success, welcome " . $loginModel->username);
            $notification->success();
            if ($loginModel->updated_at == null) {
                $loginModel->password = '';
                $loginModel->updated_at = date("Y-m-d H:i:s");
                return $this->redirect("/profile");
            } else if (($loginModel->updated_at) !== null) {
                return $this->redirect("/profile");
                return $this->goHome();
            }
        }
        if ($loginModel->errors) {
            return $this->renderAjax('@frontend/modules/user/views/auth/_login', [
                        'model' => $loginModel,
                        'resetPasswordRequestModel' => $resetPasswordRequestModel
            ]);
        }
        $loginModel->password = '';
        return $this->render('login', [
                    'loginModel' => $loginModel,
                    'resetPasswordRequestModel' => $resetPasswordRequestModel
                        ]
        );
    }

    public function actionRegister() {
        $model = new SignupForm();
        $confirm = new ConfirmationForm();
        $countries = Country::countriesDropdown();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->signup()) {
                if ($model->getUser()) {
                    $user = $model->getUser();
                    $user->type = Constants::USER_TYPE_CLIENT;
                    $id = $model->getUser()->id;
                    $demo = new MamAccounts();
                    $r = $demo->createDemo($id);
                    if ($r) {
                        $notification = new Notification("Registration success");
                        $notification->success();
                        $confirm->sendRegistrationEmail($user);
                    } else {
                        $notification = new Notification("Registration Failed");
                        $notification->error();
                    }
                    $this->redirect('/login');
                }
            }
        }
        if ($model->errors) {
            return $this->renderAjax('@frontend/modules/user/views/auth/_signup', [
                        'model' => $model, 'countries' => $countries
            ]);
        }
    }

    public function actionLogout() {
        $session = Yii::$app->session;
        Yii::$app->user->logout();
        $session->setFlash('success', 'You have been successfully logged out!');
        $session->set('logout', __METHOD__ . ' at ' . date('Y-m-d H:i:s'));       
        return $this->goHome();
    }

    public function actionError() {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }

    public function actionChangePassword() {
        $model = new ChangePasswordForm(\Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->changePassword()) {
                    $notification = new Notification("Change Password Success");
                    $notification->success();
                    return $this->redirect("/profile");
                }
            }

            return $this->renderAjax('@frontend/modules/user/views/auth/_changePassword', [
                        'model' => $model
            ]);
        }
    }

}
