<?php

namespace frontend\modules\log;

/**
 * log module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\log\controllers';
    /**
     * @inheritdoc
     */   
    public $defaultRoute = 'browse';
    /**
     * @var integer the maximum lenght of the log messages displayed in index
     */
    public $messageLen = 200;
    /**
     * @var string the DB application component ID of the DB connection.
     */
    public $db = 'db';
}
