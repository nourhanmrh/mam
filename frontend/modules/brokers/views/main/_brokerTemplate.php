<div class="row" style="text-align: center;font-weight: 550;padding-bottom: 10px;border-bottom: 1px solid \\#e5e5e5;padding-top: 10px;line-height: 4;">
    <div class="col-lg-2 col-md-2 col-sm-3 col-3">
        <img src="" class="brokers-img" style="max-width: 90px;max-height: 50px" />
    </div>
    <div class="col-2  hidden-sm hidden-xs">#: BrokerName #</div>
    <div class="col-lg-1 col-md-1 col-sm-3 col-3">#: Platforms #</div>
    <div class="col-lg-2 col-md-2 col-sm-3 col-3">#: MinDeposit # $</div>
    <div class="col-1 hidden-sm hidden-xs">#: SupportedCurrencies #</div>
    <div class="col-lg-2 col-md-2 col-sm-3 col-3">#: Commission #$</div>
    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
        <a href="/to-broker?broker=#: Id #" class="btn btn-primary" style="width: 100%;padding: 2px;">Open Live Account</a>
    </div>
</div>