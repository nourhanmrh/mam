<?php
$transport = new \Kendo\Data\DataSourceTransport();

$read = new \Kendo\Data\DataSourceTransportRead();

$read->url('/get-brokers')
        ->dataType('json')
        ->type('POST');


$transport->read($read)
        ->parameterMap('function (data, type) { Loader.show(".kt-portlet__body"); return {models: kendo.stringify(data)};}');

$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data.data')
        ->errors(new \Kendo\JavaScriptFunction("function(data){if(data.data.errors){kendo.alert(data.data.errors)}}"))
        ->total('data.total');

$live_bk = new \Kendo\Data\DataSourceFilterItem();
$live_bk->field('is_live');
$live_bk->operator('eq');
$live_bk->value(1);
$dataSource = new \Kendo\Data\DataSource();
$dataSource->transport($transport)
        ->pageSize(20)
         ->addFilterItem($live_bk)
        ->schema($schema)
        ->serverPaging(true)
        ->serverFiltering(true)
        ->serverSorting(true);


$listview = new \Kendo\UI\ListView('brokers-list-view');
$listview->dataSource($dataSource)->pageable(true)->dataBound(new \Kendo\JavaScriptFunction('function () {
        var list = this;
	jQuery("#brokers-list-view").removeClass("k-widget k-listview");Loader.stop(".kt-portlet__body");
        jQuery(".brokers-img").each(function () {
            var tr = $(this).closest("div");
            var model = list.dataItem(tr);
            $(this).attr("src", model.Image);
        });
}'))->templateId('brokers-template');
echo $listview->render();
?>
<script id="brokers-template" type="text/x-kendo-template">
    <?= $this->render('_brokerTemplate') ?>
</script>
