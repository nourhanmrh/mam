<?php

namespace frontend\modules\brokers\controllers;

use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\brokers\Brokers;
use common\models\mamaccounts\MamAccounts;
use common\helpers\Notification;
use common\models\Constants;
/**
 * Default controller for the `brokers` module
 */
class MainController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['brokers', 'get-brokers', 'to-broker'],
                'rules' => [
                    [
                        'actions' => ['brokers', 'get-brokers', 'to-broker'],
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ]
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionBrokers() {
        return $this->render('index');
    }

    public function actionGetBrokers() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $brokers = new Brokers();
        $request = json_decode($_POST['models']);
        return $brokers->getBrokersByKendo($request);
    }

    public function actionToBroker($broker) {
        $live_account = new MamAccounts();
        $live_requests = $live_account->getLiveAccountRequests();
        if ($live_requests) {          
            $notification = new Notification("You have a live account request, kindly complete it");
            $notification->error();
            return $this->redirect(['/open-live?name='.$live_requests['name']]);
        }
        $live_account->brokers_id = $broker;

        $x=$live_account->createLive(\yii::$app->user->id, $broker);         
            if($x){
            $notification = new Notification("Create Live Account Request Success");
            $notification->success();
            return $this->redirect(['/open-live?name='.$x]);
        }
        if ($live_account->name) {
            $notification = new Notification("Create Live Account Request Success");
            $notification->success();
            return $this->redirect('/open-live?name=' . $live_account->name);
        } else {
            $notification = new Notification("Create Live Account Request Error");
            $notification->error();
            return $this->redirect('/brokers');
        }
    }

}
