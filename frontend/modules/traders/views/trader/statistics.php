<?php

use yii\helpers\Html;
?>
<style>
    .tit{
        color:#47b2a1 
    }
    .trader-row svg {
        width: 25%;
    }
    @media(max-width: 754px){
        .trader-row svg {
            width: 17% !important;
        }
    }
    canvas {
        height: 100px;
    }
    .trader-img{
        width:100%;
        /*max-width: 100px;*/
        border-radius: 4px;
    }
    .trader-name{
        font-size: 20px;
    }
    .btn-primary {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    .star-ratings-css {
        unicode-bidi: bidi-override;
        color: #c5c5c5;
        font-size: 25px;
        position: relative;
        padding: 0;
        display: inline-block;
    }
    .star-ratings-css-top {
        color: #f39f25;
        padding: 0;
        position: absolute;
        z-index: 1;
        display: block;
        top: 0;
        left: 20% ;
        left: 0;
        overflow: hidden;
    }
    .star-ratings-css-bottom {
        padding: 0;
        display: block;
        z-index: 0;
        left: 20%;
    }
    svg {
        cursor: pointer;
    }
    .kt-portlet__head-title{
        text-align: left;
    }

    #undo {
        text-align: center;
        position: absolute;
        white-space: nowrap;
        padding: 1em;
        cursor: pointer;
    }
    .armchair {
        float: left;
        margin: 30px 30px 120px 30px;
        text-align: center;
    }
    .armchair img {
        display: block;
        margin-bottom: 10px;
    }
    .k-window-content a {
        color: #BBB;
    }
    .k-window-content p {
        margin-bottom: 1em;
    }
    div.k-window-content{
        overflow:hidden !important;
    }

    @media screen and (max-width: 1023px) {
        div.k-window {
            display: none !important;
        }
    }

    #windowForFilter .btn-brand:hover {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    #windowForFilter .btn-brand {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    #windowForFilter .btn-secondary:hover {
        border-color: #47b2a1 !important;
        background-color: #fff !important;
    }
    #windowForFilter .btn-secondary {
        border-color: #47b2a1 !important;
        background-color: #fff !important;
    }
    .k-window .k-button.k-bare {
        color: black !important;
    }
    .k-window .k-header {
        background-color: #fff !important;
    }
    b {
        font-weight: 550;
    }
</style>
<?php
$transport = new \Kendo\Data\DataSourceTransport();
$read = new \Kendo\Data\DataSourceTransportRead();
$read->url('/get-statistics')
        ->dataType('json')
        ->type('POST');
$transport->read($read)
        ->parameterMap('function (data, type) { return {models: kendo.stringify(data)};}');
$model = new \Kendo\Data\DataSourceSchemaModel();
$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data');
$filterItem_trader = new \Kendo\Data\DataSourceFilterItem();
$filterItem_trader->field('user_id');
$filterItem_trader->operator('eq');
$filterItem_trader->value(\Yii::$app->user->identity->id);
$filterItem_type = new \Kendo\Data\DataSourceFilterItem();
$filterItem_type->field('is_live');
$filterItem_type->operator('eq');
$filterItem_type->value(1);
$dataSource = new \Kendo\Data\DataSource();

$dataSource->transport($transport)
        ->schema($schema)
        ->addFilterItem($filterItem_trader,$filterItem_type)
        ->serverFiltering(true)
        ->serverSorting(true);
$grid = new \Kendo\UI\ListView('grid');
$grid->dataSource($dataSource)->pageable(false)->templateId('ms-template');
?>

<?php
$transport = new \Kendo\Data\DataSourceTransport();

$read = new \Kendo\Data\DataSourceTransportRead();

$read->url('/get-traders')
        ->dataType('json')
        ->type('POST');


$transport->read($read)
        ->parameterMap('function (data, type) { Loader.show(".kt-portlet__body"); data.id =' . $trader_id . ';return {models: kendo.stringify(data)};}');

$model = new \Kendo\Data\DataSourceSchemaModel();

$id_field = new \Kendo\Data\DataSourceSchemaModelField('trader_id');
$id_field->type('number');

$model->addField($id_field);

$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data.data')
        ->errors(new \Kendo\JavaScriptFunction("function(data){if(data.data.errors){kendo.alert(data.data.errors)}}"))
        ->model($model)
        ->total('data.total');

$filterItem_trader = new \Kendo\Data\DataSourceFilterItem();
$filterItem_trader->field('user_id');
$filterItem_trader->operator('eq');
$filterItem_trader->value(\Yii::$app->user->id);

$filterItem_type = new \Kendo\Data\DataSourceFilterItem();
$filterItem_type->field('is_live');
$filterItem_type->operator('eq');
$filterItem_type->value(1);

$dataSource = new \Kendo\Data\DataSource();

$dataSource->transport($transport)
        ->pageSize(20)
        ->schema($schema)
        ->serverPaging(true)
        ->addFilterItem($filterItem_trader)
        ->serverFiltering(true)
        ->serverSorting(true);


$listview = new \Kendo\UI\ListView('traders-list-view');
$listview->dataSource($dataSource)->pageable(true)
        ->dataBound(new \Kendo\JavaScriptFunction('function (e) {
             Loader.stop(".kt-portlet__body");
	jQuery("#traders-list-view").removeClass("k-widget k-listview");
	var grid = this;
        var data = this.dataSource.view(); 
        if(data.length == 0){
            jQuery("#traders-list-view").text("No Traders");
        }
        jQuery(".trader-img").each(function () {
            var tr = $(this).closest("div");
            var model = grid.dataItem(tr);
            $(this).attr("src", "/webservices/images/traders?id="+model.Id+"&size=SM");
        });
        jQuery(".rating").each(function () {
            var tr = $(this).closest("div");
            var model = grid.dataItem(tr);
            if(!model.IsLogged){
            var rating;
            if(model.AverrageRate > 0){
               $(this).kendoRating({value: model.AverrageRate ,label: false}).getKendoRating().enable(false);
            }else if(model.AverrageRate == 0){
               $(this).kendoRating().getKendoRating().enable(false);
            }
            }else{
                if(model.CanVote){
                    if(model.ActiveAccountVotes > 0){
                        $(this).kendoRating({value: model.ActiveAccountVotes ,label: false}).getKendoRating().enable(false);
                    }else{
                        var rat = $(this).kendoRating({label: false}).getKendoRating();
                        jQuery(".rating").click(function() {
                        if(rat.value()){
                                $.ajax({
                                    url : \'/rating\',
                                    type : \'post\',
                                    data : {
                                        \'rating\' : rat.value(),
                                        \'trader_name\' : model.TraderName
                                    },
                                    dataType:\'json\',
                                    success : function(data) {
                                        if(data.success){
                                            rat.enable(false); 
                                             var myWindow = $("#starswindow"),
                undo = $("#undo");

        undo.click(function () {
            myWindow.data("kendoWindow").open();
            undo.fadeOut();
        });

        function onClose() {
            undo.fadeIn();
        }

        myWindow.kendoWindow({
            width: "400px",
            title: "",
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Close"
            ],
            close: onClose
        }).data("kendoWindow").center().open();
        $("div.k-window").removeClass("k-widget");
                                        }
                                    },
                                });
                            }
                        });
                    }
                }else {
                    $(this).hide();
                }
            }
        });
        
	jQuery(".trader-chart").each(function() {
				var chart = $(this);
				var tr = chart.closest("div");
				var model = grid.dataItem(tr);
                            //  console.log(model)
				var options = {
					responsive: true,
					maintainAspectRatio: false,
					spanGaps: false,
					legend: {
						display: false
					},
					elements: {
						line: {
							tension: 0.4
						}
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						x: {
							ticks: {
								autoSkip: false,
								maxRotation: 0,
							},
						},
						xAxes: [{
							display: false
						}],
						yAxes: [{
							display: false
						}]
					}
				};
                                
				var ctx = this.getContext("2d");
				var gradient = ctx.createLinearGradient(0, 0, 0, 0);
				gradient.addColorStop(0, \'#47b2a1ab\');
					gradient.addColorStop(1, \'#fff\');
						new Chart(chart, {
							type: \'line\',
							data: {
								labels: ["","","","","","","","","",""],
								datasets: [{
									backgroundColor: gradient,
									borderColor: \'#47b2a1\',
									pointHoverBackgroundColor: "#47b2a1",
									pointHoverBorderColor: "#47b2a1",
									pointHoverRadius: 8,
									pointHoverBorderWidth: 1,
//									pointRadius: 0,
									data: model.SparklinePoints,
									label: \'pips\',
									fill: \'start\'
								}]
							},
							options: options
						});

					});
                                        
                 $(".edit-configuration-button").click(function(){
                    var tr = $(this).closest("div");
                    var clicked = grid.dataItem(tr);
                    var title = "Edit Following Configuration For Trader \'"+clicked.TraderName+"\'";
                    popupForm(clicked,title);
                });                           
                    $(".follow-button").click(function () {
	var tr = $(this).closest("div");
	var clicked = grid.dataItem(tr);
        if(!clicked.IsLogged){
            registerPopup();
        }else{
            if(clicked.ActiveAccountStatus == \'completed\'){
                if(clicked.FollowStatusText == \'Follow\'){
                    var title = clicked.FollowStatusText + " \'" + clicked.TraderName + "\' through (" + clicked.ActiveAccountName +")"
                    popupForm(clicked,title);
                }
                if(clicked.FollowStatusText == \'Unfollow\'){
                    kendo.confirm("Are you sure to Unfollow \'"+ clicked.TraderName +"\' using account : \'"+clicked.ActiveAccountName+"\' ?").then(function () {
                        sendProcess(clicked.TraderName,"unfollow","","","true");
                    }, function () {
                    });
                }
            }else{
                nonActiveAccountPopup(clicked.ActiveAccountName);
            }
        }
        });
			}'))
        ->templateId('traders-template');
?>



<div class="container" style="margin-top: 25px;">
    <div class="row">
        <div class="kt-portlet card">
            <div class="" style='border-bottom: 1px solid rgba(0, 0, 0, .1);padding-left: 10px;'>
                <div class=" col-lg-3 col-md-3 col-sm-6 col-6 hidden-xs" style="font-size: 20px;line-height: 1.9;">
                    <b>Traders</b>
                </div>
            </div>

            <div class="kt-portlet__body" style="min-height: 300px">
                <?php
                echo $listview->render();
                ?>

            </div>
            <?php
            echo $grid->render();
            ?>
        </div>

    </div>
</div>



<script id="traders-template" type="text/x-kendo-template">

    <?= $this->render("_traderTemplate") ?>

</script>

<script id="ms-template" type="text/x-kendo-template">
<div class="col-lg-12 col-md-12 col-sm-6 col-6" style="text-align: center">
<div class="row">
    <div  class="col-lg-5 col-md-5 col-sm-6 col-6" style="text-align: center">
		<div class="row" style="height: 50%"><div class="col-lg-12 tit" ><b>Total commission received </b></div></div>
		<div class="row" style="height: 50%"><div class="col-lg-12">  #: sum_rcom # </div></div>
    </div>
    <div class="col-lg-5 col-md-5 col-sm-6 col-6" style="text-align: center">
		<div class="row" style="height: 50%"><div class="col-lg-12 tit" ><b>Total commission pending  </b></div></div>
		<div class="row" style="height: 50%"><div class="col-lg-12">  #: sum_pcom # </div></div>
    </div>
</div>

<div class="row">
     <div class="col-lg-5 col-md-5 col-sm-6 col-6" style="text-align: center">
		<div class="row" style="height: 50%"><div class="col-lg-12 tit" ><b>Total profit </b></div></div>
		<div class="row" style="height: 50%"><div class="col-lg-12">  #: profit_sum # </div></div>
    </div>
     <div class="col-lg-5 col-md-5 col-sm-6 col-6" style="text-align: center">
		<div class="row" style="height: 50%"><div class="col-lg-12 tit" ><b>Average ROI </b></div></div>
		<div class="row" style="height: 50%"><div class="col-lg-12">#: roi_avg #%  </div></div>
    </div>
</div>
<div class="row">
     <div class="col-lg-5 col-md-5 col-sm-6 col-6" style="text-align: center">
		<div class="row" style="height: 50%"><div class="col-lg-12 tit" ><b>Average Profit </b></div></div>
		<div class="row" style="height: 50%"><div class="col-lg-12">  #: profit_avg # </div></div>
    </div>
    <div class="col-lg-5 col-md-5 col-sm-6 col-6" style="text-align: center">
		<div class="row" style="height: 50%"><div class="col-lg-12 tit" ><b>Total Amount following </b></div></div>
		<div class="row" style="height: 50%"><div class="col-lg-12">#: total_amount #  </div></div>
    </div>
</div>
</div>

    </script>