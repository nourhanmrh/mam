<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<style>.showme {
        display: none;
    }

    .showhim:hover .showme {
        display: block;
    }</style>
<div id="become-form" >
    <div class="row" style="background-color:#e6e7e8;text-align: center;font-weight: bold;">
        <div class="col-lg-12"style="margin-bottom:20px;">
            A Trader is any user that shares his trading strategy in Mamgram. 
            <br>
            As a Trader you will compete on the Traders page and allow Investors to receive your signals into their trading account. 
            <br>The more proﬁtable your Trader strategy is, the more Investors will receive your signals!
        </div>
    </div>
    <?php
    $form = ActiveForm::begin(
                    [
                        'id' => 'trader-form',
                        'action' => '/join-as-trader',
                        'options' => [
                            'class' => 'kt-form',
                            'data-pjax' => true
                        ]
                    ]
    );
    ?>
    <div id="trader-form-container">
        <div class="row" id="trader-name" style="margin-top: 30px">
            <div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-12"> <h3>Trader Details</h3></div></div>
            <div class="row">
                <div class="col-lg-8 col-md-6 col-sm-12 col-12">
                    <div class="col-lg-2 col-md-12 col-sm-12 col-12" style="margin: 30px 0px">Trader Name:</div>
                    <div class="col-lg-6 col-md-6 col-sm-10 col-10"style="margin:10px" > <?= $form->field($model, 'name')->label(false)->textInput(['style' => 'margin: 10px'])?></div> 
                    <div class="col-lg-10 col-md-12 col-sm-12 col-12" style="height:120px;font-size:12px">This will be your Trader name, and that's how the MamGram users will know you. Please note that your Trader name should not include contact information (such as e-mail, website address or phone number), inappropriate language or registered trademarks</div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-12"> <h3>Personal Details</h3></div>
            <div class="row" id="trader-info" style="margin-top: 30px">

                <div class="col-lg-6 col-md-6 col-sm-12 col-12 align-self-center"> Email<br>
                    <?= $form->field($model, 'email')->label(false) ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-6 align-self-center">Password <br>

                    <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-6" style="margin-top:10px;"> <div class="showhim" ><img class="lazyload lazyload--done" height="20px" alt="234813383" src="https://t4.ftcdn.net/jpg/02/34/81/33/240_F_234813383_bE3zVSXclqYEDKxRtDazfB54jfSRsHCK.jpg">
                        <div class="showme">          
                            <span class="disp" style="font-size:10px;font-weight: bold;display:none">a minimum of 1 lower case,1 upper case letter ,and 1 numeric character</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 align-self-center">

                    <?= $form->field($model, 'username')->input('Username') ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 align-self-center">
                    <?= $form->field($model, 'phone')->input('Phone Number') ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 align-self-center">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label>Country:</label>
                            <select name="SignupForm[country_code]" id="countries" style="width:100%;">
                                <?php
                                foreach ($countries as $key => $value) {
                                    $selected = "";
                                    if (strtoupper($model->country_code) == $key) {
                                        $selected = "selected='selected'";
                                    }
                                    ?>
                                    <option <?= $selected ?> value="<?= $key ?>" data-image="/media/msdropdown/icons/blank.gif" data-imagecss="flag flag-<?= strtolower($key) ?>" data-title="<?= $value ?>"><?= $value ?></option>
                                <?php } ?>     
                            </select>
                        </div>
                    </div>
                </div>
                <?php foreach ($array as $key => $val) { ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">  
                        <?php echo $form->field($model_info, $val)->textInput(); ?>
                    </div>
                <?php } ?>
            </div>

             <div class="col-lg-12 col-md-12 col-sm-12 col-12" style="margin:30px 0px">
                <div class="field field-full-width">
                    <label><a href='' style='color:#47b2a1'> Terms &amp; Conditions</a></label><br>
<!--                    <span class="description">
                        You must read and accept the Terms and Conditions to create a new account.</span>-->
<!--                    <div  class="row"><div class="col-lg-12" style="border:1px solid black;width:500px;height:150px;overflow: scroll;">



                            THE LEGAL AGREEMENT SET OUT BELOW GOVERNS YOUR USE OF THE mamgram LTD SERVICES. TO AGREE TO THESE TERMS, CLICK "AGREE". IF YOU DO NOT AGREE TO THESE TERMS, DO NOT CLICK "AGREE," AND DO NOT USE THE SERVICES.
                            SIGNAL PROVIDER SERVICES AGREEMENT

                            THIS SIGNAL PROVIDER SERVICES AGREEMENT (this "Agreement") is made and entered by and between mamgram, LTD. ("mamgram") and you ("Signal Provider" or "Trader") executing this Agreement.

                            WHEREAS, mamgram maintains and operates a web-based and mobile electronic trading platform (the "mamgram Platform");

                            WHEREAS, The mamgram Platform provides functionality that allows participating traders to browse or follow information related to the trading activity of persons who trade retail off-exchange foreign currency spot contracts and CFDs (collectively “Forex”) and then trade automatically or manually based upon such information, or manually alone;

                            WHEREAS, certain customers of mamgram, its subsidiaries and affiliates (collectively, "mamgram Group") may wish to utilize the trading signals generated by systems developed by the Signal Provider;

                            WHEREAS, the Trader desires to receive compensation for providing mamgram Group customers with access to trading signals generated by systems developed by the Trader;

                            WHEREAS, the parties desire to memorialize their agreement regarding the compensation to be received by the Trader as the result of mamgram Group customers having access to trading signals generated by systems developed by the Trader;

                            NOW, THEREFORE, , in consideration of the mutual covenants hereinafter recited, the receipt and sufficiency of which are hereby acknowledged, and other good and valuable consideration, the parties agree as follows:

                            Incorporation of Recitals
                            Each of the foregoing recitals is incorporated by reference herein and made a part hereof.
                            Access to Trading Signals
                            The Signal Provider has developed certain trading systems ("System") designed to generate trading signals to be used in trading Forex products. The Signal Provider shall provide mamgram access to its trading signals for the purpose of trading customer accounts introduced by mamgram Group pursuant to such signals. The trading signals shall be provided to mamgram in the manner acceptable to mamgram.
                            Identification Confirmation
                            Within five (5) business days of the Effective Date of this Agreement, the Trader shall provide mamgram with sufficient evidence of the Trader's identity as proof, as required by mamgram's Compliance Department and as necessary for account completion. For the purpose of confirming the Trader's identity, the Trader shall produce a copy of the Trader's valid passport, driver's license, government identification card or any other government-issued document evidencing nationality or residence and bearing a photograph or other safeguard. In the event the Trader is a corporate entity, the Trader shall provide its formation documents and information sufficient for mamgram to determine the corporate or business entity's identity, and the authority of its business representative to act on its behalf, as well as the business representative's identity.
                            Compensation
                            In consideration for providing mamgram with access to the trading signals generated by the System, mamgram agrees to pay the Trader the compensation fees as posted on mamgram’s website and described on mamgram’s Trader Program Guide here https://www.mamgram.com/trader-guide. The remuneration paid to the Trader will constitute full payment for access to the trading signals generated by the System, and the Trader will not receive any additional benefits or compensation under this Agreement. Trader hereby agrees that mamgram shall have the sole discretion to amend, replace, vary, and cancel the compensation fees due and payable to the Signal provider by providing a notice. The payment of compensation fees will be processed by mamgram or other affiliate entity.
                            Trader's compensation is calculated on a monthly calendar basis. Payment requests can include accrued revenues for only the previous month(s), up to the end of the calendar month prior to the request.
                            mamgram values the safety of its followers' capital as an utmost priority. For this reason, Traders who at mamgram’s sole discretion apply abusive trading behavior, introducing high risks to their followers, will be examined carefully and if deemed malicious, they will be refused compensation and may be banned from the mamgram services. Traders hereby acknowledge and agree that they are required to comply with trading compliance rules as described and amended from time to time in the Trader Program Guide found here https://www.mamgram.com/trader-guide in order to be eligible for compensation.
                            The compensation fees set forth in this section 4 shall not be payable to Traders if a) mamgram determines at its sole discretion that such payment would violate any laws or rules to which mamgram Group or the Trader is subject; b) mamgram at its sole discretion deems it necessary to withhold the compensation fees for reasons arising from, but not limited to, customer complaints, any government body investigation or complaint, or any legal issue; or c) mamgram has reason to believe that the Trader's activity is in breach of this Agreement.
                            For the avoidance of doubt, mamgram will not compensate any Trader that resides in Japan, unless such Trader is registered with the FSA as an Investment Advisory and Agency Business Operator. For the purposes of this Agreement, “Japan Resident” shall mean any natural person resident of Japan; any company, partnership or other legal entity created or organized under the laws of any jurisdiction of Japan
                            It is agreed that the Trader may not receive any payment from mamgram in the event that the Trader has not completed the verification requirements of mamgram, including the provision of the identification information and/or documents required by mamgram, as amended and/or updated from time to time. It is further agreed that no payment hereunder shall be made to any entity other than the Trader and/or to any account and/or payment method not registered in the name of the Trader and/or to any account and/or payment method the details of which are not identical to those provided to mamgram by the Trader.
                            As an independent contractor, the Trader is responsible for all taxes that are payable as a result of the compensation paid to the Affiliate from mamgram.
                            The Signal Provider's Exempt Status
                            The Trader represents and warrants that it does not direct or guide any customer accounts or provide trading advice based on, or tailored to, the commodity or Forex interests or cash market positions or other circumstances or characteristics of any customer. The Trader further represents that it has obtained all required domestic and foreign governmental and regulatory licenses or registrations as may be necessary to carry out its obligations and duties under this Agreement, or is exempt from such registration or licensure.
                            Method of Performing Services
                            In performing the services set forth herein, the Trader shall comply fully at all times with all applicable laws, rules and regulations governing the trading of Forex and any other governmental and self-regulatory authorities or organizations having jurisdiction over it.
                            Warranties
                            The Trader understands that mamgram respects proprietary rights and does not desire to acquire from the Trader any trade secrets or confidential information. The Trader represents and warrants to mamgram that: (a) it is not under any pre-existing obligation inconsistent with the terms of this Agreement; and (b) the services to be performed under this Agreement will be the Trader's original work, free and clear of any claims or encumbrances of any kind, and will not infringe any patent, copyright, trademark or other proprietary right or infringe upon a trade secret of any person or entity.
                            Promotional Material
                            The Trader undertakes to ensure that all promotional material is of a type and character, and is disseminated in a manner, that will not cause disrepute or harm to mamgram and comply with all applicable laws and regulations, including without limitation relating to anti-spam laws and regulations. Immediately upon notice from mamgram that, in mamgram’s opinion, any promotional material (content or method of use) does not comply with this standard, the Trader will cease use of such materials or manner of use. The Trader agrees that it will not use or disseminate any promotional material referencing mamgram without obtaining mamgram's written permission.
                            Disabling of Account
                            A Trader account is considered inactive if there is no trading activity for a certain period of time. All Provider accounts that have been inactive for more than 3 months will be permanently suspended. Especially for Provider accounts linked with MT4 terminals, the permitted inactivity period is limited to 1 month.
                            mamgram may at its sole discretion disable Trader's account at anytime for any of the following reasons: abuse of the system wherein the Trader sends large amounts of trades, suspicious activity regarding false personal identification or other reasons not described in this Agreement.
                            Termination
                            Either party may terminate this Agreement at any time without cause at any time by prior written notice to the non-terminating party of such termination.
                            In the event either party defaults in the performance of its obligations under this Agreement, the non-defaulting party may terminate this Agreement effective immediately upon the giving written notice of the default to the other party.
                            Termination of this Agreement, however caused, shall not release either party from any liability or responsibility to the other with respect to all terms, covenants and conditions contained herein, all of which shall survive the termination of this Agreement. In addition, the termination of this Agreement shall not affect any of the rights or obligations of either party arising prior to or at the time of termination of this Agreement, or which may arise by any event causing the termination of this Agreement.
                            Indemnification

                            The Trader shall indemnify, hold harmless and defend mamgram, its principals, shareholders, officers, directors, employees, representatives, agents or affiliates from and against any and all losses, claims, damages and liabilities to which any person indemnified herein may become subject under any state, provincial or national law, any rule or regulation promulgated under any of such acts or laws, including those of any financial self-regulatory agency or organization, or otherwise, insofar as such losses, claims, damages or liabilities (or actions in respect thereof) arise out of, or are based upon:
                            a material breach by the Signal Provider of this Agreement; or
                            a violation by the Trader of any applicable law, rule or regulation, except to the extent damages claimed result from the actions of mamgram, or any of its respective principals, officers, directors, employees, representatives, agents or affiliates if such actions constitute: (A) a violation by such person or entity of any applicable law, rule or regulation; or (B) gross negligence, bad faith, or willful misconduct.

                            The Trader shall reimburse any and all persons indemnified herein for any legal or other expenses (including attorney's fees) reasonably incurred by any of them in connection with investigating or defending any action or claim covered by this indemnity.
                            Independent Contractor
                            For purposes of this Agreement, the Trader is an independent contractor, and not an employee or agent of mamgram, nor shall anything herein be construed as making the Trader a partner or co-venturer with mamgram or any of its affiliates or other clients. Except as provided in this Agreement, the Trader shall have no authority to bind, obligate or represent mamgram.
                            Miscellaneous
                            This Agreement shall be binding upon and inure to the benefit of the Parties hereto and their respective successors and assigns, mamgram may assign or novate any of its rights, benefits or obligations under this Agreement . Trader may not assign, novate, transfer. encumber, license all or any part of this Agreement or any rights, benefits, or obligations under this Agreement without the prior written consent of mamgram, which consent will not be unreasonably withheld.
                            The Agreement shall be governed by and construed in accordance with the laws of Cyprus. For purpose of any action or proceeding involving any matter arising out of or relating to this Agreement, the Parties hereto agree to submit to the exclusive jurisdiction of the courts of Cyprus.
                            All captions used in this Agreement are for convenience only, are not a part hereof, and are not to be used in construing or interpreting any aspect hereof
                            This Agreement may be executed in counterparts, each such counterpart to be deemed an original, but which all together shall constitute one and the same instrument.
                            This Agreement constitutes the entire agreement between the parties hereto with respect to the matters referred to herein, and no other agreement, verbal or otherwise, shall be binding among the parties unless it is in writing and signed by the party against whom enforcement is sought.
                            mamgram shall have the right, at any time and under its sole and absolute discretion, to change and/or amend the terms and conditions of this Agreement. The Trader agrees that any new format of this Agreement which shall be posted on mamgram’s Website shall be considered as sufficient provision of notice for the changes and/or amendments made in such new format and shall become effective as of the date of posting it as aforesaid.
                            No waiver of any provision of this Agreement may be implied from any course of dealing between or among any of the parties hereto or from any failure by any party hereto to assert its rights under this Agreement on any occasion or series of occasions.
                            The provisions of this Agreement shall survive the termination of this Agreement with respect to any matter arising while this Agreement was in effect.
                            Any invalid or unenforceable provision of this Agreement shall not affect any other provision hereunder and the remainder of the Agreement shall be valid and enforceable to the fullest extent permitted by law. If any provision of this Agreement is held to be overbroad, invalid or unenforceable by a court of competent jurisdiction, the parties agree that the court may modify or amend such provision to allow for enforcement to the maximum extent permitted under the law.



                        </div>

                    </div>-->
                    <span class="label-fix"><input type="checkbox" id="agreement" name="agreement" >I accept the Terms &amp; Conditions</label></span>

                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="col-lg-2">   <?= Html::submitButton(' Save Settings', ['class' => 'btn trader-btn ']) ?></div>
                <div class="col-lg-3">   <?= Html::resetButton('Reset', ['class' => 'btn btn-primary btnReset', 'id' => 'reset']); ?></div>
            </div>
        </div>  
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$js = <<< JS
        
$('#trader-form').on('beforeSubmit', function () {
            Loader.show("#become-form");
            var form = $(this);
                $.pjax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: new FormData($('#trader-form')[0]),
                    container: "#trader-form-container",
                    push: false,
                    scrollTo: false,
                    cache: false,
                    contentType: false,
                    timeout: 10000,
                    processData: false
                })
                .done(function(data) {
                    Loader.stop("#become-form");
                })
                .fail(function () {
                    // request failed
                    console.log('request failed');
                })
            return false; // prevent default form submission
        });
        
JS;
$this->registerJs($js);
?>
<script>
    $('.showhim').hover(function () {
        $(".disp").css("display", "block");
        $('.password').attr('type', 'text');
    }, function () {
        $('.password').attr('type', 'password');
    });
</script>