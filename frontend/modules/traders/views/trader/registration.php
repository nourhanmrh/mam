<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<style>
    #trader-reg .text{font-size: 16px;}

    #trader-reg p{text-align: center;font-size: 14px;margin: 20px 0px}
    .trader-btn{background-color: #47b2a1 !important;
                border-color: #47b2a1 !important;color:white!important}
    .reset-btn{
        border-color: #47b2a1 !important;
        color: #47b2a1 !important;
    }
    .btnReset{background-color: white !important;
              border-color: #47b2a1 !important;
              color: #47b2a1 !important;}  
    .dd {
        border: 1px solid #47b2a1;
    }
</style>

<div class="container"  style="margin-top: 25px;">
    <div class="row trader-row">
        <div class="kt-portlet card">
            <div class="kt-portlet__body">
                <div class="kt-grid kt-grid--ver kt-grid--root">
                    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
                        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="">
                            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                                <div class="row" style="font-size: 30px;text-align:center"><div class="col-lg-12 col-md-12 col-sm-2 col-12">Become a Trader.</div></div>
                                <div class="text-center"> <img src="trader-registration/globe.png" class="img-fluid" alt="Responsive image"></div>
                                <div class="text">Traders can earn from $100 up to $1 million per month depending on their performance and popularity.</div>                                
                                <div class="text-center" style="margin: 20px 0px"><button  id ="trader-btn" onclick="window.location = '/join-as-trader#become-form'" class="btn btn-primary trader-btn">Become a trader </button></div>
                                <div class="text">Submit your trading signals to all Demo and Live accounts, using your preferred trading method!</div>                                
                                <div class="row"id="trader-reg"  style="margin-top: 20px ;    border: 5px solid #e6e7e8;">
                                    <div class="col-lg-6" style="    border-right: 1px solid #e6e7e8;">
                                        <div class="text-center" >
                                            <img  src="logo/logo.png"class="img-fluid" >
                                        </div>
                                        <div style="height:200px">

                                            <p>
                                                - Fully-ﬂedged Trading Station. Demo and Live accounts supported.
                                            </p>
                                            <p>
                                                - Technical charts and indicators
                                            </p>
                                            <p>
                                                - ZuluScripts - create your own custom Script and Indicator!
                                            </p>
                                            <p>
                                                - Web-based, no connectivity issues, no need for a VPS
                                            </p>


                                        </div>
                                        <div class="text-center">
                                            <img src="logo/logo.png">
                                        </div>
                                        <div style="height:140px">

                                            <p>
                                                - Our own REST API
                                            </p>

                                            <p>
                                                - Conﬁgure your own algorithm
                                            </p>
                                            <p>
                                                - Over 7 base currencies   
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-center">
                                            <img src="logo/logo.png">
                                        </div>
                                        <div style="height:200px">
                                            <p>
                                                - Demo and Live accounts supported
                                            </p>
                                            <p>
                                                - Over 50 Brokers supported
                                            </p>
                                            <p>
                                                - Trade from your own custom program
                                            </p>
                                            <p>
                                                - Web-based, no connectivity issues, no need for a VPS
                                            </p>
                                        </div>
                                        <div class="text-center">
                                            <img src="logo/logo.png">
                                        </div>
                                        <div style="height:140px">

                                            <p>
                                                - Host your MT4 trading strategy for free                              
                                            </p>

                                            <p>
                                                - Access your VPS via any Browse 
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            <?= $this->render('_become', ['model' => $model, 'model_info' => $model_info, 'array' => $model_info->array, 'countries' => $countries])?>

                                </div>
                            </div>
                        </div>
                    </div>	
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#countries").msDropdown();
        var presets = window.chartColors;
        var utils = Samples.utils;
        var inputs = {
            min: -100,
            max: 100,
            count: 8,
            decimals: 2,
            continuity: 1
        };
    });
</script>

