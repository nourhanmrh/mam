<?php

namespace frontend\modules\traders\controllers;

use common\models\ReportServer\Mt4Reportserver;
use common\models\ReportServer\Mt5Reportserver;
use Yii;
use common\models\forms\PasswordResetRequestForm;
use common\models\forms\ResetPasswordForm;
use common\models\forms\LoginForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use common\models\Constants;
use common\models\forms\SignupForm;
use common\models\forms\TraderRegistrationForm;
use common\models\users\Users;
use common\models\usersinfo\UsersInfo;
use common\helpers\AuthManager;
use common\models\ReportServer\ReportServer;
use common\models\country\Country;
use common\models\tradersinfo\TradersInfo;
use common\models\mamaccounts\MamAccounts;
use common\models\forms\ConfirmationForm;

class TraderController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['statistics', 'get-statistics', 'join-as-trader'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['join-as-trader'],
                        'allow' => true,
                        'roles' => ['?'],
                    ]
                ],
            ]
        ];
    }

    public function actionTrader() {

        return $this->render('statisctics');
    }

    public function actionStatistics() {


        $trader_id = \Yii::$app->user->getId();
        if ($trader_id) {
            return $this->render('statistics', ['trader_id' => $trader_id]);
        } else {
            Yii::warning('user not logged in', 'traders');
        }
    }

    public function actionJoinAsTrader() {
        $ip = Yii::$app->geoip->ip();
        $model = new SignupForm();
        $default_country_code = \Yii::$app->params['DefaultCountryCode'];
        if ($ip->isoCode) {
            $model->country_code = $ip->isoCode;
        } else {
            $model->country_code = $default_country_code;
        }
        $countries = Country::countriesDropdown();
        $model->type = Constants::USER_TYPE_MONEY_MANAGER;
        $model_info = new UsersInfo();
        $confirm = new ConfirmationForm();
        $manager = Yii::$app->authManager;
        $infos = Yii::$app->request->post('UsersInfo');

        if ($model->load(Yii::$app->request->post())) {
           
                if (Yii::$app->request->post('agreement') == 'on') {
                    if ($model->validate() && $model->signup()) {
                        $user = Users::findByUsername($model->username);
                        $demo = new MamAccounts();
                        $demo->createDemo($user->id, 1);
                        $confirm->sendRegistrationEmail($user);
                        $response = \Yii::$app->getResponse();
                        $response->setStatusCode(201);
                        if ($infos) {
                            foreach ($infos as $key => $val) {
                                $model_info = new UsersInfo();
                                $model_info->user_id = $user->id;
                                $model_info->key = $key;
                                $model_info->value = $val;
                                $model_info->save(false);
                            }
                        }
                        $response = \Yii::$app->getResponse();
                        $response->setStatusCode(201);
                        Yii::info($response, 'traders');
                        $this->redirect('/login');
                    }
                }
            Yii::error('signup falied', 'traders');
            Yii::warning($model, 'traders');
            if ($model->errors) {
                return $this->renderAjax('_become', [
                            'model' => $model, 'countries' => $countries, 'model_info' => $model_info, 'array' => $model_info->array, 'countries' => $countries
                ]);
            }
        }
        return $this->render('registration', ['model' => $model, 'model_info' => $model_info, 'array' => $model_info->array, 'countries' => $countries]);
    }

    public function actionGetStatistics() {        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = json_decode($_POST['models']);
        $result = new \DataSourceResult(Yii::$app->db->dsn . ";charset=" . Yii::$app->db->charset, Yii::$app->db->username, Yii::$app->db->password);
        $trader_info = new TradersInfo();
        $res = $trader_info->getTradersStatistics($request, $result);
        if ($res) {
            return $res;
        }
        Yii::warning('no result ', 'traders');
    }

    public function actionRating() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->post()) {
            $data = Yii::$app->getRequest()->getBodyParams();
            $model = new Rates();
            $c = (new \yii\db\Query())->select(['user_id'])->from('mam_accounts')->where(['id' => $data['trader_id']])->one();
            $active = Traders::get_active();
            $model->ib_id = $c['user_id'];
            $model->client_id = Yii::$app->user->id;
            $model->master_account = $data['trader_id'];
            $model->slave_account = $active['id'];
            $model->rate = $data['vote'];
            $model->save();
            return 'done';
        }
    }

}
