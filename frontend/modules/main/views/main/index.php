<?php
/* @var $this yii\web\View */

use kv4nt\owlcarousel\OwlCarouselWidget;
use common\sse\MessageEventHandler;

$this->title = 'MamGram';
?>

<style>
    g [fill] {
        fill : #47b2a1;
    }

    #special-services svg {
        width:100px;
        height: 100px;
        display: block;
        margin: auto;
    }

    #special-services {
        text-align: center;
    }

    .kt-portlet {
        margin-bottom: 0px !important;
    }

    .title {
        padding: 0;
        font-size: 1.5rem;
        font-weight: 600;
        line-height: 3.5;
        text-align: center;
    }
    /*    #special-services,#steps{
            margin-top: 50px;
            margin-bottom: 50px;
        }*/
    #registration-form button,#top-traders-cards button,#top-traders-cards .btn-primary {
        width: 100%;
        padding: 5px;
    }
    #registration-form .btn-primary,#top-traders-cards .btn-primary {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }

    .card{
        box-shadow: 0px 0px 10px 1px rgba(185, 185, 185, 0.35);
        border-radius: 6px;
        padding-right: 10px;
        padding-left: 10px;
    }
    #top-traders-cards .row {
        text-align: center;
        margin-top:20px;
    }

    #steps button,#registration-form button {
        border: 1px solid #47b2a1;
    }
    .carousel-wrap {
        width: 1000px;
        margin: auto;
        position: relative;
    }
    .owl-carousel .owl-nav{
        overflow: hidden;
        height: 0px;
    }

    .owl-item img {
        border-radius: 50%;
    }

    .owl-theme .owl-dots .owl-dot.active span,
    .owl-theme .owl-dots .owl-dot:hover span {
        background: #2caae1;
    }

    .owl-dots {
        display: none;
    }
    .owl-carousel .item {
        text-align: center;
    }
    .owl-carousel .nav-btn{
        height: 47px;
        position: absolute;
        width: 26px;
        cursor: pointer;
        top: 100px !important;
    }

    .owl-carousel .owl-prev.disabled,
    .owl-carousel .owl-next.disabled{
        pointer-events: none;
        opacity: 0.2;
    }

    .owl-carousel .prev-slide{
        background: url(/media/nav-icon.png) no-repeat scroll 0 0;
        left: -33px;
    }
    @media screen and (max-width: 768px){
        .owl-carousel .prev-slide{
            left: 20px !important;
        }
        .owl-carousel .next-slide{
            right: 20px !important;
        }
        .owl-carousel .nav-btn{
            top : 20px !important;
        }
        .title {
            line-height: 1.5;
            padding-bottom: 10px;
        }
    }
    .owl-carousel .next-slide{
        background: url(/media/nav-icon.png) no-repeat scroll -24px 0px;
        right: -33px;
    }
    .owl-carousel .prev-slide:hover{
        background-position: 0px -53px;
    }
    .owl-carousel .next-slide:hover{
        background-position: -24px -53px;
    }

    span.img-text {
        text-decoration: none;
        outline: none;
        transition: all 0.4s ease;
        -webkit-transition: all 0.4s ease;
        -moz-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        cursor: pointer;
        width: 100%;
        font-size: 23px;
        display: block;
        text-transform: capitalize;
    }
    span.img-text:hover {
        color: #2caae1;
    }
    b {
        font-weight: 550;
    }

</style>
<div class="container home">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-12" style="color:#000;margin-top: 50px;">

            <h1 style="font-size: 3rem;"  class="kt-invoice__title">
                Automate Your Trading </h1>
            <br>
            <h4 style="font-size: 1.5rem;"> Analyze Professional Strategies</h4>
            <h4 style="font-size: 1.5rem;"> Choose Among Many Forex Brokers</h4>
            <h4 style="font-size: 1.5rem;margin-bottom: 20px"> Follow Professional Traders</h4>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-12 "><img style="width:100%;margin-bottom: 40px;" src="media/computer.png" /></div></div>
    <div class="row" id="special-services">
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <p class="animated  jackInTheBox">
                <img src="/media/home/automated-trading.svg" />
            </p>
            <p><h4><b>Automated Trading</b></h4></p>
            <p>I have read and I accept the Terms and Conditions and the Privacy Policy of Mamgram</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <p class="animated  jackInTheBox ">
                <img src="/media/home/secure-payment.svg" />
            </p>
            <p><h4><b>Secure Payment</b></h4></p>
            <p>I have read and I accept the Terms and Conditions and the Privacy Policy of Mamgram</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <p class="animated  jackInTheBox">
                <img src="/media/home/guaranteed-investment.svg" />
            </p>
            <p><h4><b>Guaranteed Investment</b></h4></p>
            <p>I have read and I accept the Terms and Conditions and the Privacy Policy of Mamgram</p>
        </div>
    </div>
    <?= $this->render('_topTraders') ?>
    <?= $this->render('_joinAsTrader') ?>
    <?= $this->render('_steps') ?><br>
    <?= $this->render('_platforms') ?>
    <?php if (Yii::$app->user->isGuest) { ?>
        <?= $this->render('_register', ['signupForm' => $signupForm, 'countries' => $countries]) ?>
    <?php } ?>
</div>
