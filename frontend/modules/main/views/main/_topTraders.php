<?php

use kv4nt\owlcarousel\OwlCarouselWidget;
?>
<?php
$transport = new \Kendo\Data\DataSourceTransport();

$read = new \Kendo\Data\DataSourceTransportRead();

$read->url('/get-traders')
        ->dataType('json')
        ->type('POST');


$transport->read($read)
        ->parameterMap('function (data, type) { Loader.show("#top-traders-cards"); return {models: kendo.stringify(data)};}');

$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data.data')
        ->errors(new \Kendo\JavaScriptFunction("function(data){if(data.data.errors){kendo.alert(data.data.errors)}}"))
        ->total('data.total');


$dataSource = new \Kendo\Data\DataSource();

$dataSource->transport($transport)
        ->pageSize(6)
        ->schema($schema)
        ->serverSorting(true);


$listview = new \Kendo\UI\ListView('top-trader-list-view');
$listview->dataSource($dataSource)->pageable(false)->attr('class', 'owl-carousel owl-theme')
        ->dataBound(new \Kendo\JavaScriptFunction('function () {
	jQuery("#top-trader-list-view").removeClass("k-widget k-listview");
	var grid = this;
	jQuery("#top-trader-list-view").owlCarousel({
		"autoplay": false,
		"autoplayTimeout": 0,
		"items": 3,
		"nav": true,
		"navText": ["<div class=\'nav-btn prev-slide\'></div>", "<div class=\'nav-btn next-slide\'></div>"],
		"responsive": {
			"0": {
				"items": 1
			},
			"600": {
				"items": 3
			},
			"1000": {
				"items": 3
			}
		},
		"loop": false,
		"itemsDesktop": [1199, 3],
		"itemsDesktopSmall": [979, 3]
	});
	$(".top-traders-chart").each(function () {
				var chart = $(this);
				var tr = chart.closest("div");
				var model = grid.dataItem(tr);
                                
				var options = {
					responsive: true,
					maintainAspectRatio: false,
					spanGaps: false,
					legend: {
						display: false
					},
					elements: {
						line: {
							tension: 0.4
						}
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						x: {
							ticks: {
								autoSkip: false,
								maxRotation: 0,
							},
						},
						xAxes: [{
							display: false
						}],
						yAxes: [{
							display: false
						}]
					}
				};
				var ctx = this.getContext("2d");
				var gradient = ctx.createLinearGradient(0, 0, 0, 400);
				gradient.addColorStop(0, \'#47b2a1ab\');
					gradient.addColorStop(1, \'#fff\');
						new Chart(chart, {
							type: \'line\',
							data: {
								labels: ["","","","","","","","","",""],
								datasets: [{
									backgroundColor: gradient,
									borderColor: \'#47b2a1\',
									pointBorderColor: "#fff",
									pointBackgroundColor: "#47b2a1",
									pointHoverBackgroundColor: "#47b2a1",
									pointHoverBorderColor: "#47b2a1",
									pointBorderWidth: 2,
									pointHoverRadius: 8,
									pointHoverBorderWidth: 1,
									pointRadius: 5,
									data: model.SparklinePoints,
									label: \'pips\',
									fill: \'start\'
								}]
							},
							options: options
						});

					});
                                     
                        Loader.stop("#top-traders-cards");

			}'))
        ->templateId('top-traders-template');
?>
<div class="row title" ><div class="col-12" style="margin-top:20px">Below are some statistics of our professional traders</div></div>
<div class="row" id="top-traders-cards" style="min-height: 250px">
    <?php
    echo $listview->render();
    ?>
</div>

<script id="top-traders-template" type="text/x-kendo-template" >
<?= $this->render("_topTradersTemplate") ?>
</script>