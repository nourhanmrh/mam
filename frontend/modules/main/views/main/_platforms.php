<style>
    #mamgram-platforms div {
        text-align: center;
    }
</style>
<div class="row title" ><div class="col-12">Trade and manage your account from any platform</div></div>
<div class="row" id="mamgram-platforms" style="padding-top: 20px;padding-bottom: 20px">
    <div class="col-lg-3 col-md-3 col-sm-3 col-12" style="line-height: 50px;">
        <p><b>iPad & iPhone</b></p>
        <p><img src="/media/home/app-store.png" /></p>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
        <img src="/media/home/platforms.png" width="100%" />
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-12" style="line-height: 50px;">
        <p><b>Android Tablets & Phones</b></p>
        <p><img src="/media/home/google-play.png" /></p>
    </div>
</div>