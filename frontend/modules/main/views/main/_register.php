<div class="row" id="registration-form">
    <div class="kt-portlet card">
        <div class="row title"><div class="col-12" style="border-bottom: 1px solid rgba(0, 0, 0, .1)">Register Now & Join Our Traders</div></div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <?= $this->render('@frontend/modules/user/views/auth/_signup', ['model' => $signupForm,'countries'=>$countries]) ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12  hidden-sm hidden-xs">
                    <img src="/media/map2.png" style="max-width:100%" />
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$js = <<< JS
           
$("#countries").msDropdown();
        
JS;
$this->registerJs($js);
?>