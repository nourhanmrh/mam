    <div class="col-lg-12 col-md-12 col-sm-12 col-12 item">
    <div class="card">
    <div class="row">
        <div class="col-2"><img class="trader-img" src="/webservices/images/traders?id=#= Id #&size=XS" alt=""> </div>
    <div class="col-6" style="text-align: left">
    <a target="_blank" href="/trader-info?name=#= TraderName #"><h5><b>#: TraderName #</b></h5></a>
    <p>#: TraderName #</p>
    </div>
    </div>
    <div class="row">
    <div style="height:200px;" class="wrapper col-12"><canvas style="max-height: 200px" class="top-traders-chart"></canvas></div>
    </div>
    <div class="row" style="margin-top:30px">
    <div class="col-6">
    <h5><b>Amount Following</b></h5>
    <p>$#: AmountFollowing #</p>  
    </div>
    <div class="col-6">
    <h5><b>Investors</b></h5>
    <p>#: Investors #</p>  
    </div>
    </div>
    <div class="row">
    <div class="col-6">
    <h5><b>Profit</b></h5>
    <p>#: Profit #</p>  
    </div>
    <div class="col-6">
    <h5><b>ROI</b></h5>
    <p>#: Roi #%</p>  
    </div>
    </div>
    <div class="row" style="margin-bottom: 20px;">
    <div class="col-12">
    <a target="_blank" href="/trader-info?name=#= TraderName #" class="btn btn-primary btn-lg">Go to trader</a>
    </div>
    </div>
    </div>
    </div>