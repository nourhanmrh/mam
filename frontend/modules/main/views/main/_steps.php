<div class="row" id="steps">
    <div class="kt-portlet card">
        <div class="row title"><div class="col-12" style="border-bottom: 1px solid rgba(0, 0, 0, .1)">Open an account and Copy professional strategies</div></div>
        <div class="kt-portlet__body">
            <ul class="nav nav-tabs nav-fill" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#kt_tabs_2_1">Step 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_2_2">Step 2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_2_3">Step 3</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_2_4">Step 4</a>
                </li>
            </ul>                    

            <div class="tab-content">
                <div class="tab-pane active" id="kt_tabs_2_1" role="tabpanel">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-12"><br>
                            <h2>Register Now!</h2><br>
                            <h5>Register to MamGram and create a profile</h5><br>
                           <?php if ( Yii::$app->user->isGuest ){?>
                            <p>
                                <button onclick="window.location = '/#registration-form'" class="btn btn-secondary btn-lg"> Register </button>
                            </p>
                           <?php }else{?>
                            <p>
                                 <p>
                                <button type="button" onclick="window.location = '/open-account'" class="btn btn-secondary btn-lg">Open an account</button>
                            </p>
                            </p>
                           <?php }?>
                        </div>
                        <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                            <img src="/media/home/verify.png" />
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="kt_tabs_2_2" role="tabpanel">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                            <br>
                            <h2>Choose a trader</h2><br>
                            <h5>Analyze the professional traders' strategies</h5><br>
                            <p>
                                <button type="button" onclick="window.location = '/traders'"  class="btn btn-secondary btn-lg">Traders</button>
                            </p>
                        </div>
                        <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                            <img src="/media/home/follow.png" />
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="kt_tabs_2_3" role="tabpanel">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-12">   <br>
                            <h2>Open an account</h2><br>
                            <h5>Choose a broker and open a live account</h5><br>
                            <p>
                                <button type="button" onclick="window.location = '/open-account'" class="btn btn-secondary btn-lg">Open an account</button>
                            </p>
                        </div>
                        <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                            <img src="/media/home/share.png" />
                        </div>
                    </div>                    </div>
                <div class="tab-pane" id="kt_tabs_2_4" role="tabpanel">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-12">   <br>
                            <h2>Automate your trading</h2><br>
                            <h5>Let MamGram automate on your behalf</h5><br>
                            <p>
                                <button type="button"onclick="window.location = '/open-account'"   class="btn btn-secondary btn-lg">Open an account</button>
                            </p>
                        </div>
                        <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                            <img src="/media/home/register.png" />
                        </div>
                    </div> 
                </div>
            </div>      
        </div>
    </div>
</div>