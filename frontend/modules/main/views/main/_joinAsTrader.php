<style>
    #join-as-trader img {
        width: 120px;
        height: 120px;
        display: block;
        margin: auto;
    }

    #join-as-trader {
        text-align: center;
    }
</style>
<div class="row title" style="" ><div class="col-12">Start your own journey to become a professional trader</div></div>
<div class="row" id="join-as-trader" style="padding-top: 20px;padding-bottom: 30px">
    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
        <p class="animated  jackInTheBox"><img src="/media/home/register.svg" /></p>
        <p><h5>Register for an account</h5></p>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
        <p class="animated  jackInTheBox"><img src="/media/home/fill.svg" /></p>
        <p><h5>Fill your profile</h5></p>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
        <p class="animated  jackInTheBox"><img src="/media/home/journey.svg" /></p>
        <p><h5>Start your journey</h5></p>
    </div>
</div>
 <?php if (Yii::$app->user->isGuest) { ?>
<div class="row" style="padding-bottom: 30px">
    <div class="col-12" style="text-align: center;">
        <a href="/join-as-trader" type="button" class="btn btn-primary btn-lg" style="border:0;background-color: #47b2a1;padding: 5px;min-width: 300px">
            Join As Trader
        </a>
    </div>
</div>
 <?php }?>