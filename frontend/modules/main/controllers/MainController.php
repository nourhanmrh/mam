<?php

namespace frontend\modules\main\controllers;

use Yii;
use yii\base\InlineAction;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use common\models\forms\SignupForm;
use common\models\Constants;
use common\models\country\Country;
use common\sse\TraderTradesHandler;

/**
 * Default controller for the `main` module
 */
class MainController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'top-traders','sse'],
                        'allow' => true
                    ]
                ]
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'error'
            ],
        ];
    }

    public function actionSse($trader_name,$tickets) {
        $sse = Yii::$app->sse;
        $sse->addEventListener('message', new TraderTradesHandler($trader_name,$tickets));
        $sse->start();
    }

    public function actionIndex() {
        $ip = Yii::$app->geoip->ip();
        $signupForm = new SignupForm();
        $default_country_code = \Yii::$app->params['DefaultCountryCode'];
        if ($ip->isoCode) {
            $signupForm->country_code = $ip->isoCode;
        } else {
            $signupForm->country_code = $default_country_code;
        }
        $countries = Country::countriesDropdown();

        return $this->render('index', ['signupForm' => $signupForm, 'countries' => $countries]);
    }

}
