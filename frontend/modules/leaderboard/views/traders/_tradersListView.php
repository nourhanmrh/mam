<style>
    .trader-row svg {
        width: 25%;
    }

    @media(max-width: 754px){
        .trader-row svg {
            width: 17% !important;
        }
/*        .rating-input-row {
            margin-top: 20px;
        }*/
        .last-trader-column{
            line-height: 3;
        }
        .k-rating {
            margin-top: 10px;
    margin-bottom: 10px;
        }
    }
    canvas {
        height: 100px;
    }
    .trader-img{
        width:100%;
        /*max-width: 100px;*/
        border-radius: 50%;
    }
    .trader-name{
        font-size: 20px;
    }
    .btn-primary {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    .star-ratings-css {
        unicode-bidi: bidi-override;
        color: #c5c5c5;
        font-size: 25px;
        position: relative;
        padding: 0;
        display: inline-block;
    }
    .star-ratings-css-top {
        color: #f39f25;
        padding: 0;
        position: absolute;
        z-index: 1;
        display: block;
        top: 0;
        left: 20% ;
        left: 0;
        overflow: hidden;
    }
    .star-ratings-css-bottom {
        padding: 0;
        display: block;
        z-index: 0;
        left: 20%;
    }
    svg {
        cursor: pointer;
    }
    .kt-portlet__head-title{
        text-align: left;
    }
    #undo {
        text-align: center;
        position: absolute;
        white-space: nowrap;
        padding: 1em;
        cursor: pointer;
    }
    .armchair {
        float: left;
        margin: 30px 30px 120px 30px;
        text-align: center;
    }
    .armchair img {
        display: block;
        margin-bottom: 10px;
    }

    b {
        font-weight: 550;
    }
    .k-rating .k-state-selected, .k-rating .k-state-hover {
        box-shadow: none!important;
        color: #f39f25!important;
    }
    .k-rating.k-widget {
        border-color: transparent!important;
    }
    .k-rating {
        cursor: pointer;
        display: initial;
    }
    .k-rating-container .k-rating-item {
        padding: 0!important;
    }
    #starswindow{
        display:none;height:50px;font-weight: bold;font-size:17px; text-align:center;color:#47b2a1 
    }
</style>
<div id="starswindow" >you have successfully rated this trader.</div>
<?php
$transport = new \Kendo\Data\DataSourceTransport();

$read = new \Kendo\Data\DataSourceTransportRead();

$read->url($url)
        ->dataType('json')
        ->type('POST');


$transport->read($read)
        ->parameterMap('function (data, type) { Loader.show("' . $loader_id . '"); return {models: kendo.stringify(data)};}');

$model = new \Kendo\Data\DataSourceSchemaModel();

$id_field = new \Kendo\Data\DataSourceSchemaModelField('trader_id');
$id_field->type('number');

$model->addField($id_field);

$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data.data')
        ->errors(new \Kendo\JavaScriptFunction("function(data){if(data.data.errors){kendo.alert(data.data.errors)}}"))
        ->model($model)
        ->total('data.total');


$dataSource = new \Kendo\Data\DataSource();

$dataSource->transport($transport)
        ->pageSize(20)
        ->schema($schema)
        ->serverPaging(true)
        ->serverFiltering(true)
        ->serverSorting(true);


$listview = new \Kendo\UI\ListView('traders-list-view');
$listview->dataSource($dataSource)->pageable($pageable)
        ->dataBound(new \Kendo\JavaScriptFunction('function (e) {
             Loader.stop("' . $loader_id . '");
	jQuery("#traders-list-view").removeClass("k-widget k-listview");
	var grid = this;
        var data = this.dataSource.view(); 
        if(data.length == 0){
            jQuery("#traders-list-view").text("No Traders");
        }
        jQuery(".trader-img").each(function () {
            var tr = $(this).closest("div");
            var model = grid.dataItem(tr);
            $(this).attr("src", "/webservices/images/traders?id="+model.Id+"&size=SM");
        });
        jQuery(".rating").each(function () {
            var tr = $(this).closest("div");
            var model = grid.dataItem(tr);
            if(!model.IsLogged){
            var rating;
            if(model.AverrageRate > 0){
               $(this).kendoRating({value: model.AverrageRate ,label: false}).getKendoRating().enable(false);
            }else if(model.AverrageRate == 0){
               $(this).kendoRating().getKendoRating().enable(false);
            }
            }else{
                if(model.CanVote){
                    if(model.ActiveAccountVotes > 0){
                        $(this).kendoRating({value: model.ActiveAccountVotes ,label: false}).getKendoRating().enable(false);
                    }else{
                        var rat = $(this).kendoRating({label: false}).getKendoRating();
                        jQuery(".rating").click(function() {
                        if(rat.value()){
                                $.ajax({
                                    url : \'/rating\',
                                    type : \'post\',
                                    data : {
                                        \'rating\' : rat.value(),
                                        \'trader_name\' : model.TraderName
                                    },
                                    dataType:\'json\',
                                    success : function(data) {
                                        if(data.success){
                                            rat.enable(false); 
                                             var myWindow = $("#starswindow"),
                undo = $("#undo");

        undo.click(function () {
            myWindow.data("kendoWindow").open();
            undo.fadeOut();
        });

        function onClose() {
            undo.fadeIn();
        }

        myWindow.kendoWindow({
            width: "400px",
            title: "",
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Close"
            ],
            close: onClose
        }).data("kendoWindow").center().open();
        $("div.k-window").removeClass("k-widget");
                                        }
                                    },
                                });
                            }
                        });
                    }
                }else {
                    $(this).hide();
                }
            }
        });
        
	jQuery(".trader-chart").each(function() {
				var chart = $(this);
				var tr = chart.closest("div");
				var model = grid.dataItem(tr);
                            //  console.log(model)
				var options = {
					responsive: true,
					maintainAspectRatio: false,
					spanGaps: false,
					legend: {
						display: false
					},
					elements: {
						line: {
							tension: 0.4
						}
					},
					plugins: {
						filler: {
							propagate: false
						}
					},
					scales: {
						x: {
							ticks: {
								autoSkip: false,
								maxRotation: 0,
							},
						},
						xAxes: [{
							display: false
						}],
						yAxes: [{
							display: false
						}]
					}
				};
                                
				var ctx = this.getContext("2d");
				var gradient = ctx.createLinearGradient(0, 0, 0, 0);
				gradient.addColorStop(0, \'#47b2a1ab\');
					gradient.addColorStop(1, \'#fff\');
						new Chart(chart, {
							type: \'line\',
							data: {
								labels: ["","","","","","","","","",""],
								datasets: [{
									backgroundColor: gradient,
									borderColor: \'#47b2a1\',
									pointHoverBackgroundColor: "#47b2a1",
									pointHoverBorderColor: "#47b2a1",
									pointHoverRadius: 8,
									pointHoverBorderWidth: 1,
//									pointRadius: 0,
									data: model.SparklinePoints,
									label: \'pips\',
									fill: \'start\'
								}]
							},
							options: options
						});

					});
                                        
                 $(".edit-configuration-button").click(function(){
                    var tr = $(this).closest("div");
                    var clicked = grid.dataItem(tr);
                    var title = "Edit Following Configuration For Trader \'"+clicked.TraderName+"\'";
                    popupForm(clicked,title);
                });                           
                    $(".follow-button").click(function () {
                    var tr = $(this).closest("div");
                    var clicked = grid.dataItem(tr);
                    if(!clicked.IsLogged){
                    registerPopup();
              }else{
            if(clicked.ActiveAccountStatus == \'completed\'){
                if(clicked.FollowStatusText == \'Follow\'){
                    var title = clicked.FollowStatusText + " \'" + clicked.TraderName + "\' through (" + clicked.ActiveAccountName +")"
                    popupForm(clicked,title);
                }
                if(clicked.FollowStatusText == \'Unfollow\'){
                    kendo.confirm("Are you sure to Unfollow \'"+ clicked.TraderName +"\' using account : \'"+clicked.ActiveAccountName+"\' ?").then(function () {
                        sendProcess(clicked.TraderName,"unfollow","","","true");
                    }, function () {
                    });
                }
            }else{
                nonActiveAccountPopup(clicked.ActiveAccountName);
            }
        }
        });
			}'))
        ->templateId('traders-template');

echo $listview->render();
?>
<script id="traders-template" type="text/x-kendo-template">
<?= $this->render("_traderTemplate") ?>
</script>
<?= $this->render("_tradersListViewScripts") ?>
<style>
    .btn-danger,.btn-danger:hover {
        color: #47b2a1;
        background-color: transparent!important;
        border-color: #47b2a1;
        color: #47b2a1!important; 
    }
</style>