<style>
    #windowTraderFilter .k-dropdown {
        display: block;
        width: 100%;
    }
    #windowTraderFilter{display:none}
    #windowTraderFilter .btn-brand:hover {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    #windowTraderFilter .btn-brand {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    #windowTraderFilter .btn-secondary:hover {
        border-color: #47b2a1 !important;
        background-color: #fff !important;
    }
    #windowTraderFilter .btn-secondary {
        border-color: #47b2a1 !important;
        background-color: #fff !important;
    }
</style>
<div id="windowTraderFilter">
    <div class="form" style="padding: 20px;">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Name</label>
                    <input id="name" type="text" class='form-control' data-bind=" value: name" />
                </div>
            </div>
            <div class="col-lg-6"> 
                <div class="form-group">
                    <label>Country</label>
                    <select data-role="dropdownlist" class='form-control' 
                            data-value-field="iso" data-text-field="name"
                            data-bind="source: countryz, value: selectedCntry" style="width: 100%;"></select> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Sort By:</label>
                    <select id="sort_fields" class='form-control' data-role="dropdownlist" data-bind="source: sort_fields, value: sortField" data-text-field="name" data-value-field="value"></select>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <select id="sort_types" class='form-control' data-role="dropdownlist" data-bind="source: sort_types, value: sortType" data-text-field="name" data-value-field="value"></select>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-4" style="text-align: center;">
                <div class="form-group">
                    <label class="kt-checkbox kt-checkbox--success">
                        <input type="checkbox" alue="ea"
                               data-bind="checked: eaValue" data-type="boolean" /> EXPERT ADVISOR
                        <span></span>
                    </label>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-4" style="text-align: center;">
                <label class="kt-checkbox kt-checkbox--success">
                    <input type="checkbox" value="live" data-bind="checked: is_live" data-type="boolean" />LIVE<span></span>
                </label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-4" style="text-align: center;">
                <label class="kt-checkbox kt-checkbox--success">
                    <input type="checkbox" value="demo" data-bind="checked: is_demo" data-type="boolean" />DEMO<span></span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6"><button id="create" data-bind="click: search" style="width:100%" class="btn btn-brand">Search</button></div>
            <div class="col-lg-6 col-md-6"><button type="reset" data-bind="click: cancel" style="width:100%" class="btn btn-secondary">Cancel</button></div>
        </div>
    </div>
</div>
<?php
$js = <<< JS
        
        jQuery("#filter-button").on('click',function(){
        showTrades();
   });
        jQuery("#generalSearch").on('keyup', function() { 
            
   });
        
          function showTrades() {
        var myWindow = $("#windowTraderFilter"),
          undo = $("#undo");

        undo.click(function () {
            myWindow.data("kendoWindow").open();
            undo.fadeOut();
        });

        function onClose() { 
       
          undo.fadeIn();
        }

        myWindow.kendoWindow({
            width: "600px",
            title: "",
            visible: false,
            modal: true,
            actions: [
                "Pin",
                "Close"
            ],
            close: onClose
        }).data("kendoWindow").center().open();
        $("div.k-window").removeClass("k-widget");
    }
        
    var viewModel = kendo.observable({
            countryz: new kendo.data.DataSource({
        transport: {
            read: {
                url:"/countryz",
                dataType: "json",
                 type:"post"
            },
            
            parameterMap: function(data, type) {
            return {models: kendo.stringify(data)};
              
            }
        },
        batch: true,
        height: 550,
                        scrollable: true,
                        sortable: true,
                        filterable: true,
                        pageable: {
                            input: true,
                            numeric: false
                        },
        schema: {
            model: {
                code: "code",
         fields: {
                        code: { type: "string" },
                      
                    }
            }
        }
    }),  
        pageSize: 20,
    selectedCntry: null,
    hasChanges: false,
    save: function() {
        this.productsSource.sync();
        this.set("hasChanges", false);
    },
    
    showForm: function() {
       return this.get("selectedCntry") !== null;
    },
    change: function() {
        this.set("hasChanges", true);
    },
        isVisible: true,
        isEnabled: false,
        checkboxValue: false,
        radioVal:'',
        items: "desc",
        eaValue: false,
        is_live:false,
        is_demo:false,
        expenses: [],
        cname: [],
        sort_fields: [{name: "", value: ""}, {name: "roi", value: "roi"}, {name: "investors", value: "investors"}, {name: "profit", value: "profit"}, {name: "rate", value: "rate"}, {name: "amount_following", value: "amount_following"}],
        sort_types: [{name: "", value: ""},{name: "Ascending", value: "asc"},{name: "Descending", value: "desc"}],
        currencies: [{name: "EUR/USD", value: "EURUSD"}, {name: "GBP/USD", value: "GBPUSD"}, {name: "USD/CAD", value: "USDCAD"}],
        sortField: "",
        sortType:"",
        currency: [],
        name: "",
        country:"",
      search: function (e) {
           var myWindow = $("#windowTraderFilter"); myWindow.data("kendoWindow").close();
            var filter = {logic: "and", filters: []};
            var orfilter = {logic: "or", filters: []};
            if (this.name != '') {
                filter.filters.push({field: "trader_name", operator: "contains", value: this.name});
            }
            if (this.selectedCntry != null) { 
                filter.filters.push({field: "country_name", operator: "contains", value: this.selectedCntry});
            }
            if (this.eaValue) {
                filter.filters.push({field: "ea", operator: "eq", value: 1});
            }
            if (this.is_live) {
                filter.filters.push({field: "is_live", operator: "eq", value: 1});
            }
            if (this.is_demo) {
                filter.filters.push({field: "is_live", operator: "eq", value: 0});
            }
            if (this.currency.length > 0) {
                for (var i = 0; i < this.currency.length; i++)
                {
                    orfilter.filters.push({field: 'currency', operator: "contains", value: this.currency[i].value});
                }
                filter.filters.push(orfilter);
            }
            if ((this.sortField != '') && (this.sortType !='')) {
                var sort = [{field: this.sortField, dir: this.sortType}];
                $("#traders-list-view").data("kendoListView").dataSource._sort = sort;
            }
        
            if ((filter.filters).length > 0) {
                $("#traders-list-view").data("kendoListView").dataSource._filter = filter;
            }
            $("#traders-list-view").data("kendoListView").dataSource.read();
            delete $("#traders-list-view").data("kendoListView").dataSource._filter;
            delete $("#traders-list-view").data("kendoListView").dataSource._sort;
             
            viewModel.set("name", "");
            viewModel.set("selectedCntry", "");
            viewModel.set("currency", "");
            viewModel.set("eaValue", false);
            viewModel.set("sortField", "");
            viewModel.set("sortType", "");
            viewModel.set("is_live", "");
            viewModel.set("is_demo", "");
        },
        cancel:function (e) {
        var myWindow = $("#windowTraderFilter"); myWindow.data("kendoWindow").close();
           viewModel.set("name", "");
            viewModel.set("selectedCntry", "");
            viewModel.set("currency", "");
            viewModel.set("eaValue", false);
            viewModel.set("sortField", "");
            viewModel.set("sortType", "");
            viewModel.set("is_live", "");
            viewModel.set("is_demo", "");
        }
    });
    kendo.bind($("#windowTraderFilter"), viewModel);
        
JS;
$this->registerJs($js);
?>