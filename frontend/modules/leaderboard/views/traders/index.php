<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
?>
<div class="container" style="margin-top: 25px;">
    <div class="row">
        <div class="kt-portlet card">
            <div class="" style='border-bottom: 1px solid rgba(0, 0, 0, .1);padding-left: 10px;'>
                <div class=" col-lg-3 col-md-3 col-sm-6 col-6 hidden-xs" style="font-size: 20px;line-height: 1.9;">
                    <b>Traders</b>
                </div>
                <div class="kt-input-icon kt-input-icon--right col-lg-3 offset-lg-6 col-md-3 offset-md-6 col-sm-12 offset-sm-0 col-12 offset-0" >
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-3 col-3" id="filter-button" style="text-align: center;border-right: 0.5px solid lightgray;border-left: 0.5px solid lightgray;">
                            <svg xmlns="http://www.w3.org/2000/svg" style="padding-top: 8px;" width="20px" viewBox="0 0 512 512" >
                                <g id="Filter"><path d="M447.009,12.143A8,8,0,0,0,440,8H72a8,8,0,0,0-6.759,12.28L216,258.32V448a8,8,0,0,0,3.2,6.4l64,48A8,8,0,0,0,296,496V258.32L446.759,20.28A8,8,0,0,0,447.009,12.143ZM281.241,251.72A8,8,0,0,0,280,256V480l-48-36V256a8,8,0,0,0-1.241-4.28L86.536,24H425.464Z"/><path d="M128,56h80a8,8,0,0,0,0-16H128a8,8,0,0,0,0,16Z"/><path d="M232,56h8a8,8,0,0,0,0-16h-8a8,8,0,0,0,0,16Z"/></g></svg>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-9 col-9">
                            <div class="kt-input-icon kt-input-icon--right">
                                <input type="text" class="form-control" placeholder="Search..." id="generalSearch" style="border:0" />
                                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                    <span><i class="la la-search"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="traders-list-view-container" class="kt-portlet__body" style="min-height: 300px">
                <?= $this->render("_tradersListView", ['url' => '/get-traders', 'pageable' => true, 'loader_id' => '.kt-portlet__body']) ?>
            </div>
        </div>
    </div>
</div>
<?= $this->render("_traderFilterPopup") ?>
<script>
    $('#generalSearch').keydown(function (e) {
        if (e.keyCode == 13) {
            var filter = {logic: "and", filters: []};
            filter.filters.push({field: "trader_name", operator: "contains", value: $('#generalSearch').val()});
            if ((filter.filters).length > 0) {
                $("#traders-list-view").data("kendoListView").dataSource._filter = filter;
            }
            $("#traders-list-view").data("kendoListView").dataSource.read();
        }

    }
    )
</script>
