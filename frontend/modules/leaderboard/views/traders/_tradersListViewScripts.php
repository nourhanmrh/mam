<style>
    .k-window-content a {
        color: #BBB;
    }
    .k-window-content p {
        margin-bottom: 1em;
    }
    div.k-window-content{
        overflow:hidden !important;
        padding: 0;
    }

    /*    @media screen and (max-width: 1023px) {
            div.k-window {
                display: none !important;
            }
        }*/

    .k-window .k-button.k-bare {
        color: black !important;
    }
    .k-window .k-button a{
        color: #47b2a1 !important;
    }
    .k-window .k-header {
        background-color: #fff !important;
        color: black;
    }
    .k-window-titlebar {
        padding: 10px;
    }
    .k-window-actions{
        padding: 5px;
    }
    @media(max-width: 754px){
        .k-window {
            width: 100%!important;
            top: 0!important;
            left: 0!important;
        }}
    </style>
    <div id="dialog"></div>
<script id="scriptTemplate" type="text/x-kendo-template">
    <div class="row" style="text-align: center;padding: 10px;border-bottom: 1px solid lightgray;">
    <div class="col-lg-6 col-md-6 col-sm-6 col-6">
    <label class="kt-radio kt-radio--success" name="radio5">
    <input type="radio" checked="checked" name="copyType" value="percentage"> Percentage
    
    <span></span>
    </label>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-6">
    <label class="kt-radio kt-radio--success" name="radio5">
    <input type="radio" name="copyType" value="lot"> Fixed Lot
    <span></span>
    </label>
    </div>
    </div>
    <div class="row" style="padding-top: 30px;padding-right: 30px;padding-left: 30px;">
    <div class="col-lg-3" id="copy-type-label" style="line-height: 3;font-weight:500">Volume:</div>
    <div class="col-lg-8 input-group">
    <input type="number" step="0.01" class="form-control" style="border-color: \\#47b2a1;" id="copy-value"  value="#= val #" placeholder="For example: 0.01" />
    <div class="input-group-append" style="margin-bottom: 1px;"><span class="input-group-text" style="border-color: \\#47b2a1;" id="indice-type">Lot</span></div>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-8" id="copy-type-description" style="padding: 15px;">This is fixed lot that will be executed for each individual trade received by this trader.</div>
    </div>
    <div class="row">
    # if(data.FollowingDetails) { #
    <button class="btn btn-success" id="popup-follow-button" onclick="updateFollowConfig('#= data.FollowingDetails.Id #','#= refresh #')" style="width:100%;background:\\#47b2a1">Update</button></div> 
    # }else{ #
    <button class="btn btn-success" id="popup-follow-button" onclick="follow('#= data.TraderName #','#= data.FollowStatusText #','#= refresh #')" style="width:100%;background:\\#47b2a1">Follow</button></div> 
    # } #
</script>
<script>

function registerPopup() {
var dialog = $('#dialog');
dialog.kendoDialog({
width: "550px",
title: "Please register or login",
        closable: true,
        modal: true,
        content: "You must be authenticated to add Traders in your portfolio. If you already have a Real or a Demo Investor account with MamGram please login, otherwise you may create a new MamGram account!",
        actions: [
                {text: '<a href="/login">Login</a>'},
                {text: '<a href="/\\#registration-form">Register</a>'}
        ]
});
dialog.data("kendoDialog").open();
}

function nonActiveAccountPopup(name) {
var dialog = $('#dialog');
dialog.kendoDialog({
width: "550px",
        title: "Your Account (" + name + ") Is Not Completed",
        closable: true,
        modal: true,
        content: "You must complete your account to follow a trader!",
        actions: [
        {text: '<a href="/profile">Complete Your Account</a>'}
        ]});
dialog.data("ke n doDialog").open();
}

function popupForm(clicked, title, refreshList = "true") {
var kendoWindowAssign = $("#dialog");
kendoWindowAssign.kendoWindow({
width: "550px",
        visible: false,
        title: title,
        modal: true,
        position: {
        top: 100,
                left: "35%"
        },
        actions:["Pin", "Close"]
        }); var popup = $("#dialog").data('kendoWindow');
popup.title(title);
popup.wrapper.removeClass("k-widget");
popup.open();
<!--popup.center();-->
var scriptTemplate = kendo.template($("#scriptTemplate").html());
        var value = "";
        if(clicked.FollowingDetails){value = clicked.FollowingDetails.Value}
        var scriptData = {data:clicked, val: value, refresh: refreshList};
        $("#dialog").html(scriptTemplate(scriptData));
        
                if(clicked.FollowingDetails){
        $("input[name='copyType'][value=" + clicked.FollowingDetails.CopyType + "]").prop('checked', true);
popupContent(clicked.FollowingDetails.CopyType);
}

$("input[name='copyType']").change(function(e){
        popupContent($(this).val());
        });    
        }
        
        function popupContent(value){
        if (value == 'percentage') {
$("#indice-type").text("%");
$("#copy-type-label").text("Percentage:");
$("#copy-type-description").text("This is fixed percentage that will be executed for each individual trade received by this trader.");
$("#copy-value").attr("placeholder", "For example: 50");
} else {
        $("#indice-type").text("Lot");
$("#copy-type-label").text("Volume:");
$("#copy-type-description").text("This is fixed lot that will be executed for each individual trade received by this trader.");
$("#copy-value").attr("placeholder", "For example: 0.01");
        }
        }
        
        function follow(name,status,refreshList){
        var copy = $("input[name='copyType']:checked").val();
var copy_value = $("#copy-value").val();
if (copy_value != ''){
sendProcess(name, "follow", copy, copy_value, refreshList);
        }
        }
        
        function updateFollowConfig(id,refreshList){
        var copy = $("input[name='copyType']:checked").val();
var copy_value = $("#copy-value").val();
if (copy_value != ''){
request = {id: id, copy: copy, value: copy_value, status: 'updatefollow'};
        $.ajax({
        url: '/follow',
        type: 'post',
        data: request,
        dataType: 'json',
        success: function (response) {
        if (response){
        $("#dialog").data("kendoWindow").close();
        if (refreshList == 'true'){
        $("#traders-list-view").data("kendoListView").dataSource.read();
                        }else{
                         location.reload();
                    }
                    }else{
                        alert("Error update configuration");
                    }
                },
                error: function (error) {
                    console.log(error)
                }
            });
        }
    }

    function sendProcess(name,status,copy,value,refreshList){
        var request = "";
        if(status == "follow"){
            request = {name: name, status: status, copy: copy, value: value};
        }
        if(status == "unfollow"){
            request = {name: name, status: status};
        }
        console.log(refreshList)
        $.ajax({
                url: '/follow',
                type: 'post',
                data: request,
                dataType: 'json',
                success: function (response) {
                    if(status == "follow"){
                        $("#dialog").data("kendoWindow").close();
                    }
                    if(refreshList=="true"){
                        $("#traders-list-view").data("kendoListView").dataSource.read();
                    }else{
                         location.reload();
                    }
                },
                error: function (error) {
                    console.log(error)
                }
            });
    }
</script>