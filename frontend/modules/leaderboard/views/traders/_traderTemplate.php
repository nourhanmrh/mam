<div class="row trader-row" style="">
    <div class="col-lg-1 col-md-1 col-sm-3 col-3">
        <div class="row" style="position: relative; left: 0; top: 0;">
            <img class="trader-img" src="" alt="">  
            <span class="mamgram-avatar__icon mamgram-flag mamgram-flag-#: CountryCode.toLowerCase() #" style="position: absolute;left: 0;top: 0;"></span>
        </div>
        <div class="row" style=""><div class="col-12">
                <img src="#= BrokerPic #" style="width:100%" class=""></div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-8 col-8">
        <div class="row" style="font-size: 17px;font-weight: 500;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12" class="trader-name">
                <a id="trader_name" style="color: black!important;" ><b>#: TraderName #</b></a>
            	<a href="/trader-info?name=#= TraderName #" style='font-size:9px'>view details</a>
            </div>
        </div>

        <div class="row" style="">
            <div class="col-4">
                # if(data.Trusted > -1) {#
                <img src="/media/traders/trusted-#: data.Trusted #.svg" style="width: 25%" />                                
                # } #
                # if(data.Live > -1) {#
                <img src="/media/traders/live-#: data.Live #.svg" style="width: 25%" />
                # } #
                # if(data.ExpertAdvisor == 1) {#
                <img src="/media/traders/ea.svg" style="width: 25%" />
                # } #
            </div>

        </div>
        <div class="row">
            <div class="col-12" style="margin-top: 5px;">Hello dear traders
                My name is Sam and I am a professional quant and a data...</div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2" style="margin-bottom: 10px;margin-top: 10px;">
        <div class="wrapper col-12" >
            <canvas class="trader-chart"></canvas>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-6" style="text-align: center;line-height: 2;">
        <div class="row" style="height: 25%"><div class="col-lg-12"><b>Amount Following </b></div></div>
        <div class="row" style="height: 25%"><div class="col-lg-12">$#: AmountFollowing #</div></div>
        <div class="row" style="height: 25%"><div class="col-lg-12"><b>Live Proﬁt</b></div></div>
        <div class="row" style="height: 25%"><div class="col-lg-12"> $#: Profit # </div></div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-6" style="text-align: center;line-height: 2;">
        <div class="row" style="height: 25%"><div class="col-lg-12"><b>Investors </b></div></div>
        <div class="row" style="height: 25%"><div class="col-lg-12">#: Investors #  </div></div>
        <div class="row" style="height: 25%"><div class="col-lg-12"><b>ROI </b></div></div>
        <div class="row" style="height: 25%"><div class="col-lg-12">  #: Roi #% </div></div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-12 col-12 last-trader-column" style="text-align: center">
        
        # if(!FollowingDetails) { var first=32.333 , second=34.333 , third=33.333 }else{var first=22.333 , second=28.333 , third=33.333} #
        
        # if((!IsLogged && AverrageRate >= 0) || CanVote) { #
        <div class="row rating-input-row" style="height: #= third #%">
            <input class="rating" name="rating" style="width: 100%;" />
        </div>
        # }#
        
        <div class="row" style="height: #= third #%">
            <div class="col-lg-12">
                # if(FollowingDetails) { #
                <button type="button" class="btn btn-danger edit-configuration-button" style="width: 100%;padding: 2px;">#: FollowingDetailsLabel #</button>
                # } #
            </div>
        </div>
        
        <div class="row" style="height: #= third #%">
            <div class="col-lg-12">
                # if(ActiveAccountType != "master") { #
                <button type="button" class="btn btn-primary follow-button" style="width: 100%;padding: 2px;">#: FollowStatusText #</button>
                # } #
            </div>
        </div>
    </div>
    <hr style="width: 100%;">
</div>
