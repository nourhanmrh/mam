<canvas id="chart-profit"></canvas>
<?php
$js = <<< JS
        
        var presets = window.chartColors;

            var options = {
                responsive: true,
                maintainAspectRatio: false,
                spanGaps: false,
                legend: {
                    display: false
                },
                elements: {
                    line: {
                        tension: 0.4
                    }
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                },
                scales: {
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 0,
                        },
                    },
                    xAxes: [{
                type: 'time',
                time: {
                 unit: 'day',
                 unitStepSize: 6
            }
            }],
                    yAxes: [
            {
                ticks: {
                    callback: function(label, index, labels) {
        if(label >= 1000 || label <= -1000){
                        return label/1000+'k';
        }else{
        return label;
   }
                    }
                }
            }
        ]
                }
            };
            var ctx = document.getElementById("chart-profit").getContext("2d");
            var gradient = ctx.createLinearGradient(0, 0, 0, 400);
            gradient.addColorStop(0, '#47b2a1ab');
            gradient.addColorStop(1, '#fff');
        profit_chart = new Chart("chart-profit", {
                    type: 'line',
                    data: {
                        labels: [],
                        datasets: [{
                                backgroundColor: gradient,
                                borderColor: '#47b2a1',
                                pointBorderColor: "#fff",
                                pointBackgroundColor: "#47b2a1",
                                pointHoverBackgroundColor: "#47b2a1",
                                pointHoverBorderColor: "#47b2a1",
                                pointBorderWidth: 2,
                                pointHoverRadius: 8,
                                pointHoverBorderWidth: 2,
                                pointRadius: 6,
                                data: [],
                                label: '',
                                fill: 'start'
                            }]
                    },
                    options: options
                });
        
  function reloadProfitChart(){
    Loader.show("#profit");
    $.ajax({
        url: '/profit-chart',
        data: {name: $("#trader-name").text(), time: $("#filters-dropdown").val()},
        type: "GET",
        success: function (data) {
            Loader.stop("#profit");
            profit_chart.data.labels = data.data.date;
            profit_chart.data.datasets[0].data = data.data.profit;
            profit_chart.update();
        },
        error: function () {
            console.log("failure");
        }
    });
        }
        
        reloadProfitChart();

JS;
$this->registerJs($js);
?>