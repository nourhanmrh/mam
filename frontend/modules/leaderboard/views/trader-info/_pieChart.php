
<div class="demo-section k-content wide">
    <?php
    $series = new \Kendo\Dataviz\UI\ChartSeriesItem();
    $series->field('value')
            ->categoryField('symbol')
            ->labels(array(
                'visible' => true,
                "position" => "outsideEnd",
                'background' => 'transparent',
                'distance' => 20,
                'padding' => 0,
                'template' => '#= category #: \n #= value#%'
    ));

    $model = new \Kendo\Data\DataSourceSchemaModel();

    $schema = new \Kendo\Data\DataSourceSchema();
    $schema->data('data.data')
            ->model($model);
    $read = new \Kendo\Data\DataSourceTransportRead();

    $read->url('/pie-data')
            ->dataType('json')
            ->type('GET');

    $transport = new \Kendo\Data\DataSourceTransport();
    $transport->read($read)
            ->parameterMap('function (data, type) { data.time=$("#filters-dropdown").val();data.name =$("#trader-name").text();return {models: kendo.stringify(data)};}');

    $dataSource = new \Kendo\Data\DataSource();

    $dataSource->transport($transport)->schema($schema)->serverFiltering(true);

    $chart_pie = new \Kendo\Dataviz\UI\Chart("chart_pie");

    $chart_pie
            ->dataSource($dataSource)
            ->addSeriesItem($series)
            ->title(array('position' => 'bottom', 'text' => ''))
            ->theme("material")
            ->legend(array('visible' => false))
            ->tooltip(array('visible' => true, 'template' => '#= category # : #= value #%'))
            ->seriesDefaults(array('type' => 'pie', 'startAngle' => 150))
            ->chartArea(array('background' => 'transparent'))
            ->attr('class', 'small-chart');

    echo $chart_pie->render();
    ?>
</div>	
<script>
//jQuery(window).on("resize", function(event) {
    //var chartDiv = jQuery("#chart_pie");
//       var chart = chartDiv.data("kendoChart");

    // Temporarily disable animation, yo!
//       chart.options.transitions = false;

    // Temporarily hide, then set size of chart to container (which will naturally resize itself), then show it again
//       chartDiv.css({ display:"none" });
    //chartDiv.css({width: chartDiv.parent().innerWidth(), display: "block"});

//       chart.redraw();  
//   });
</script>

