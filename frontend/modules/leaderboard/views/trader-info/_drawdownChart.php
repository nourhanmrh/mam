<canvas id="chart-drawdown"></canvas>
<?php
$js = <<< JS
        
        var presets = window.chartColors;

            var options = {
                responsive: true,
                maintainAspectRatio: false,
                spanGaps: false,
                legend: {
                    display: false
                },
                elements: {
                    line: {
                        tension: 0.4
                    }
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                },
                scales: {
                    x: {
                        ticks: {
                            autoSkip: false,
                            maxRotation: 0,
                        },
                    },
                    xAxes: [],
                    yAxes: [
            {
                ticks: {
                    callback: function(label, index, labels) {
                        if(label >= 1000 || label <= -1000){
                        return label/1000+'k';
        }else{
        return label;
   }
                    }
                }
            }
        ]
                }
            };
            var ctx = document.getElementById("chart-drawdown").getContext("2d");
            var gradient = ctx.createLinearGradient(0, 0, 0, 400);
            gradient.addColorStop(0, '#47b2a1ab');
            gradient.addColorStop(1, '#fff');
        drawdown_chart = new Chart("chart-drawdown", {
                    type: 'bar',
                    data: {
                        labels: [],
                        datasets: [{
                                backgroundColor: gradient,
                                borderColor: '#47b2a1',
                                pointBorderColor: "#fff",
                                pointBackgroundColor: "#47b2a1",
                                pointHoverBackgroundColor: "#47b2a1",
                                pointHoverBorderColor: "#47b2a1",
                                pointBorderWidth: 2,
                                pointHoverRadius: 8,
                                pointHoverBorderWidth: 2,
                                pointRadius: 6,
                                data: [],
                                label: '',
                                fill: 'start'
                            }]
                    },
                    options: options
                });
        
  function reloadDrawDownChart(){
        Loader.show("#drawdown");
    $.ajax({
        url: '/drawdown-chart',
        data: {name: $("#trader-name").text(), time: $("#filters-dropdown").val()},
        type: "GET",
        success: function (data) {
        Loader.stop("#drawdown");
            drawdown_chart.data.labels = data.data.date;
            drawdown_chart.data.datasets[0].data = data.data.drawdown;
            drawdown_chart.update();
        },
        error: function () {
            console.log("failure");
        }
    });
        }
        
        reloadDrawDownChart();

JS;
$this->registerJs($js);
?>