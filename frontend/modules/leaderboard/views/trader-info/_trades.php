<?php
$transport = new \Kendo\Data\DataSourceTransport();

$read = new \Kendo\Data\DataSourceTransportRead();

$read->url($url)
        ->dataType('json')
        ->type('GET');

$transport->read($read)
        ->parameterMap('function (data, type) { if(source)
            source.close(); if(!data.type) { data.type = $(".trades-nav .nav-item .nav-link.active").attr("data-selected")}; data.time=$("#filters-dropdown").val() ? $("#filters-dropdown").val() : "year";data.name ="' . $name . '";if(!data.type){data.type = "history"};return {models: kendo.stringify(data)};}');

$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data.data')
        ->errors(new \Kendo\JavaScriptFunction("function(data){if(data.data.errors){kendo.alert(data.data.errors)}}"))
        ->total('data.total');


$dataSource = new \Kendo\Data\DataSource();

$dataSource->transport($transport)
        ->pageSize(20)
        ->schema($schema)
        ->serverPaging(true)
        ->serverFiltering(true)
        ->serverSorting(true);


$listview = new \Kendo\UI\ListView('trades-list-view');
$listview->dataSource($dataSource)->pageable(true)
        ->dataBound(new \Kendo\JavaScriptFunction('function (e) {
            
	jQuery("#trades-list-view").removeClass("k-widget k-listview");
        var type = $(".trades-nav .nav-item .nav-link.active").attr("data-selected");
        if(!type){type="history";}
        if(type == "history"){
        if(source)
            source.close();
            if ($(window).width() <= 754) {
            $(".open-column").hide();
            }
        }else{
            $(".open-column").show();
            var trades = $("#trades-list-view").data("kendoListView");
             var arrayTickets = [];
        var data = trades.dataSource._data;
        for(i=0; i<data.length; i++){
         arrayTickets.push(data[i].Ticket);
            }
        var name = "";
        if(TraderData.TraderName){
            name = TraderData.TraderName;
        }else{
            name = TraderData.name;
        }
        source = new EventSource("/sse?trader_name="+ name + "&tickets=" + JSON.stringify(arrayTickets));
        source.onmessage = function(event) {
            //event.target.close();
            var result = JSON.parse(event.data);
            
            $.each(data,function(index, value) {
                data[index].set("Profit",result[value.Ticket].profit);
                data[index].set("Pips",result[value.Ticket].pips);
                $("#equity").text(result[value.Ticket].equity);
            });
        };
            }
        }'
        ))
        ->templateId('trades-template');
echo $listview->render();
?>
<style>
    .open-time-label {display: none}
</style>
<script id="trades-template" type="text/x-kendo-template">
    <div class="row trade-row" style="border-bottom: 1px solid \\#e6e7e8;  padding-top: 8px;padding-bottom: 8px;">
    <div class="col-lg-2 col-md-2 col-sm-2 col-2"  style="font-weight: 500">#: Currency #</div>
    <div class="col-lg-1 col-md-1 col-sm-2 col-2">#: Type #</div>
    <div class="col-lg-1 col-md-1 col-sm-2 col-2">#: Volume #</div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-2 hidden-sm hidden-xs ">#: OpenTime #</div>
    # if( CloseTime ) { #
    <div class="col-lg-2 col-md-2 col-sm-2 col-2 hidden-sm hidden-xs ">#: CloseTime #</div>
    # } #
    <div class="col-lg-1 col-md-1 col-sm-2 col-2 open-column"><b>#: OpenPrice #</b></div>
    # if( CloseTime ) { #
    <div class="col-lg-1 col-md-1 col-sm-2 col-2 close-time-label"><b>#: ClosePrice #</b></div>
    # } #
    # if( !CloseTime ) { #
    <div class="col-lg-1 col-md-1 col-sm-1 col-1 hidden-sm hidden-xs"><b>#: TakeProfit #</b></div>
    <div class="col-lg-1 col-md-1 col-sm-1 col-1 hidden-sm hidden-xs"><b>#: StopLoss #</b></div>
    # } #    


    <div class="col-lg-2 col-md-2 col-sm-4 col-4" style="color:# if(Profit<0)  { # red  # }else{ # \\#47b2a1 # } #;font-weight: 500">
    <div class="row">
    <div class="col-12">
    #: Pips # pips
    </div>
    </div>
    <div class="row">
    <div class="col-12">
    #: Profit # $
    </div>
    </div>
    </div>     
    </div>
</script>

<?php
$js = <<< JS
window.onbeforeunload = function() {
     if(source)
            source.close();
};
$(".trades-nav .nav-item .nav-link").click(function(){
    var type ="";
    var trades = $("#trades-list-view").data("kendoListView");
    if(!$(this).hasClass("active")){
        type = $(this).attr("data-selected");
    }

    if(type == "history"){
        if(source)
            source.close();
        if ($(window).width() <= 754) {
            $(".open-column").hide();
                }else{
                    $(".open-column").show();
                }
        $(".open-time-label").hide();
        $(".close-time-label").show();
    }else if(type == "positions"){
        $(".close-time-label").hide();
        $(".open-time-label").show();
    }
    
    trades.dataSource.read({type:type,take:20,skip:0,page:1});

});
JS;
$this->registerJs($js);
?>

<script>
    $(document).ready(function () {
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

// store the currently selected tab in the hash value
        $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
            if (id == 'positions') {
                $(".close-time-label").hide();
                $(".open-time-label").show();
            } else if (id == 'history') {
                $(".open-time-label").hide();
                $(".close-time-label").show();
            }
       
// on load of the page: switch to the currently selected tab
      
        var trades = $("#trades-list-view").data("kendoListView");
          trades.dataSource.read({type:id,take:20,skip:0,page:1});
    });  var hash = window.location.hash;
        $('#myTab a[href="' + hash + '"]').tab('show'); });
</script>