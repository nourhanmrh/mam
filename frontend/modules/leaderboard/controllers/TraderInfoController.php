<?php

namespace frontend\modules\leaderboard\controllers;

use yii;
use yii\web\Controller;
use common\models\tradersinfo\TradersInfo;
use common\models\mamaccounts\MamAccounts;
use common\models\ReportServer\ReportServer;
use common\models\traders\Traders;

/**
 * Default controller for the `leaderboard` module
 */
class TraderInfoController extends Controller {

    
    public function actionTraderInfo($name) {
        $trader_info = new TradersInfo();
        $trader = TradersInfo::findOne(['trader_name' => $name]);
        $acc = MamAccounts::findOne(['id' => $trader['trader_id']]);
        $list = [];
        $list[] = $trader['trader_id'];
        $sum_profit = 0;
        $winning_trades = 0;
        $profit_by_pips = 0;
        $trader = $trader_info->getTraderInfoByName($name);
        if ($trader) {
            $trader_info = new TradersInfo();
            $reportserver = $trader_info->getRS($trader['trader_id']);
            $reques = [];
            $request = (object) $reques;
            $request->time = '';
            $request->type = '';
            $request->take = 20;
            $request->skip = 0;
            $request->pagesize = 20;
            $request->page = 1;
            $request->name = '';
            $data = json_decode($reportserver->getTrades($request));
            $trades_number = $data->total;
            foreach ($data->data as $k => $v) {
                $sum_profit += $v->Profit;
                $profit_by_pips += $v->Pips;
                if (($v->Profit) > 0) {
                    $winning_trades++;
                }
            }
            if($trades_number>0){
               $avg_pips = round(($profit_by_pips / $trades_number), 2);  
            }else{
                $avg_pips=0;
            }
           
            $opened_pos = $trader_info->getTotalPositions($trader['trader_id']);


            $trader = $trader_info->completeTradersInfo([$trader]);          
            return $this->render('index', [
                        'trader' => $trader[0],
                        'trades' => $trades_number,
                        'profit' => $sum_profit,
                        'profit_by_pips' => $profit_by_pips,
                        'winning_trades' => $winning_trades,
                        'opened_positions' => $opened_pos,
                        'avg_pips' => $avg_pips]);
        } else {
            return $this->redirect("/traders");
        }
    }

    public function actionProfitChart($name, $time) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = new MamAccounts();
        $res = array();
        $item = [];

        $info = $data->getTraderDatabase($name);
        $report_server_class = (new ReportServer($info['platform']))->getClass();

        $reportserver = new $report_server_class($info['database'], $info['login']);
        $data = $reportserver->getChartCustom($time);
        foreach ($data as $key => $val) {
            $item['profit'][] = number_format((float) $val, 2, '.', '');
            $item['date'][] = $key;
        }

        return $item;
    }

    public function actionPieData($models) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $models = json_decode($models);
        if ($models->name && $models->time) {
            $trader_name = $models->name;
            $data = new MamAccounts();
            $total = 0;
            $info = $data->getTraderDatabase($trader_name);
            $report_server_class = (new ReportServer($info['platform']))->getClass();
            $reportserver = new $report_server_class($info['database'], $info['login']);
            $data = $reportserver->getSymbolsData($models->time);
            if ($data) {
                foreach ($data as $item) {
                    $total += $item['count'];
                }
                foreach ($data as $k => $v) {
                    $frequency = $v['count'];
                    $p['symbol'] = $v['SYMBOL'];
                    $p['value'] = number_format((float) (($frequency / $total) * 100), 2, '.', '');
                    $array[] = $p;
                    $x['data'] = $array;
                }
                return $x;
            }
        }
        return [];
    }

    public function actionDrawdownChart($name, $time) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $traders = new Traders();
        $res = $traders->getDrawdown($name, $time);
        $data = [];
        foreach ($res as $k => $v) {
            $data['date'][] = substr($v['time'], 0, 10);
            $data['drawdown'][] = $v['drawdown'];
        }
        return $data;
    }

    public function actionTrades($models) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = json_decode($models);
        $data = new MamAccounts();
        $info = $data->getTraderDatabase($request->name);
        $report_server_class = (new ReportServer($info['platform']))->getClass();
        $reportserver = new $report_server_class($info['database'], $info['login']);
        return $reportserver->getTrades($request);
    }

}
