<?php

namespace frontend\modules\leaderboard\controllers;

use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\tradersinfo\TradersInfo;
use common\models\mamaccounts\MamAccounts;
use common\models\ReportServer\ReportServer;
use common\models\traders\Traders;
use common\helpers\Notification;
use common\services\MamAccountAction;
use common\models\brokers\Brokers;
use common\services\Response;
use common\models\relations\Relations;
use common\models\rates\Rates;
use \common\models\country\Country;

/**
 * Default controller for the `leaderboard` module
 */
class TradersController extends Controller {

    private $datasource;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['traders', 'get-traders', 'countryz'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['follow', 'get-own-traders', 'update-following-configuration', 'rating'],
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'follow' => ['post'],
                    'get-own-traders' => ['post']
                ],
            ],
        ];
    }

    function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
        $this->datasource = new \DataSourceResult(Yii::$app->db->dsn . ";charset=" . Yii::$app->db->charset, Yii::$app->db->username, Yii::$app->db->password);
    }

    public function actionTraders() {
        return $this->render('index');
    }

    public function actionCountryz() {
        header('Content-Type: application/json');

        $request = json_decode($_POST['models']);

        $result = new \DataSourceResult(Yii::$app->db->dsn . ";charset=" . Yii::$app->db->charset, Yii::$app->db->username, Yii::$app->db->password);

        $res = $result->read(Country::tableName(), array(
            'code', 'language_code', 'name'), $request);

        $data = json_encode($res['data']);

        return $data;
    }

    public function actionRating() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->post()) {
            $data = Yii::$app->getRequest()->getBodyParams();
            $account = new MamAccounts();
            $user = [];
            $user['id'] = \Yii::$app->user->id;
            $active = $account->getActiveAccountInfo($user);
            $trader_info = TradersInfo::find()->where("trader_name='" . $data['trader_name'] . "'")->one();
            $c = (new \yii\db\Query())->select(['name'])
                    ->from('mam_accounts')
                    ->where(['id' => $trader_info['trader_id']])
                    ->one();
            $data['master'] = $c['name'];
            $model = new Rates();
            $rate = $model->setRating(\Yii::$app->user->getId(), $active['id'], $data);
            if ($rate == 'done') {

                $response = \Yii::$app->getResponse();

                $response->setStatusCode(200);

                $response->content = "Rating succesfull";

                return $rate;
            } else {

                $response = \Yii::$app->getResponse();

                $response->setStatusCode(400);

                $response->content = "Rating failed";
            }
        }
    }

    public function actionGetOwnTraders() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = json_decode($_POST['models']);
        $trader_info = new TradersInfo();
        $active = Traders::get_active();
        $res = [];

        if ($active) {
            $condition = Traders::get_followed_trader_details($active['id']);
            if ($condition) {
                return $condition;
            }
            return [];
        }
    }

    public function actionGetTraders() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = json_decode($_POST['models']);
        $trader_info = new TradersInfo();

        $res = $trader_info->getTradersInfo($request, $this->datasource);
        return $res;
    }

    public function actionProfitChart() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = json_decode($_POST['models']);
        $trader_name = $request->name;
        $val = $request->time;
        $data = new MamAccounts();
        $res = array();
        $item = [];

        $info = $data->getDataBase($trader_name);
        $report_server_class = new ReportServer($info['plstform']);
        $reportserver = new $report_server_class($info['database'], $info['login']);
        $data = $mt4reportserver->getChartCustom($request->time);
        foreach ($data as $key => $val) {
            $res['profit'] = $val;
            $res['date'] = $key;

            $item[] = $res;
        }
        $array['data'] = $item;

        return $array;
    }

    public function actionUpdateFollowingConfiguration() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $res = Yii::$app->request->post();
        print_r($res);
        die;
        $slave_account = Traders::get_active();
        $mam_account = new MamAccounts();
        if ($res['id'] && $res['copy'] && $res['value']) {
            $relation = Relations::findOne($res['id']);
            $trader_info = TradersInfo::find()->joinWith("mamAccount")->where(['trader_id' => $relation['master_account']])->one();
            $broker = Brokers::findOne($trader_info['mamAccount']['brokers_id']);
            $relation->copy_type = $res['copy'];
            $relation->value = $res['value'];
            if (!$relation->save()) {
                return false;
            }
            try {
                $message = json_encode($mam_account->getSlave($relation['master_account'], $relation['slave_account'], $trader_info['mamAccount']['platform'], $broker->broker_name));
                echo $message;
                die;
                $mam_account_update = new MamAccountUpdate($slave_account['platform'], MamAccountUpdate::UPDATE_TYPE);
                $mam_account_update->account = $message;
                $operation_callback = $mam_account_update->send_message();
                $operation_response = new Response($operation_callback);
            } catch (Exception $ex) {
                $res['errors'] = "Cannot connect to service";
            }
            return true;
        }
    }

    public function actionFollow() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $res = Yii::$app->request->post();
        $slave_account = Traders::get_active();
        $trader = new Traders();
        $mam_account = new MamAccounts();
        $map = "";
        $result = "";

        if ($res['status']) {
            if ($res['status'] == MamAccountAction::FOLLOW_TYPE) {
                $type = MamAccountAction::FOLLOW_TYPE;
                $result = $trader->follow($res['name'], $slave_account['name'], $res['copy'], $res['value']);
            } else if ($res['status'] == MamAccountAction::UNFOLLOW_TYPE) {
                $type = MamAccountAction::UNFOLLOW_TYPE;
                $result = $trader->unfollow($res['name'], $slave_account['name']);
            } else if ($res['status'] == MamAccountAction::UPDATE_FOLLOW_TYPE) {
                $type = MamAccountAction::UNFOLLOW_TYPE;
                $result = $trader->updatefollow($res['id'], $res['copy'], $res['value']);
            }

            if ($result) {
                $notification = new Notification(Yii::t('app', 'Success ' . $type));
                $notification->success();
            } else {
                $notification = new Notification(Yii::t('app', $type . " Operation Failed"));
                $notification->error();
            }
        } else {
            $notification = new Notification(Yii::t('app', "Operation Failed"));
            $notification->error();
        }
        return $this->redirect("/traders");
    }

}
