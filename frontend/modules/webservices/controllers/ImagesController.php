<?php

namespace frontend\modules\webservices\controllers;

use yii;
use yii\web\Controller;
use common\models\users\Users;
use common\models\tradersinfo\TradersInfo;
use common\models\brokers\Brokers;

class ImagesController extends Controller {

    public function actionUsers($id, $size = null) {
        header("Content-Type: image/png");
        $user = Users::findIdentity($id);
        $s = $this->getSize($size);
        if ($user && $user->image_location) {
            $file = Yii::getAlias('@common/uploads/users/' . $id . '/' . $user->image_location);
            if (!file_exists($file)) {
                $file = Yii::getAlias('@common/uploads/users/default.png');
            }
        } else {
            $file = Yii::getAlias('@common/uploads/users/default.png');
        }

        $image = Yii::$app->image->load($file);
        echo $image->resize($s, $s)->render();
    }

    public function actionBrokers($id, $size) {
        header("Content-Type: image/png");
        $broker = Brokers::findOne($id);
        $s = $this->getSize($size);
        if ($broker && $broker->image_location) {
            $file = Yii::getAlias('@common/uploads/brokers/' . $id . '/' . $broker->image_location);
            if (!file_exists($file)) {
                $file = Yii::getAlias('@common/uploads/brokers/default.png');
            }
        } else {
            $file = Yii::getAlias('@common/uploads/brokers/default.png');
        }

        $image = Yii::$app->image->load($file);
        echo $image->resize($s, $s)->render();
    }

    public function actionTraders($id, $size = null) {
       header("Content-Type: image/png");
        $file = "";
        $s = $this->getSize($size);

        if ($trader = TradersInfo::findOne($id)) {
            $user = Users::find()->where(['id' => $trader->user_id])->one();
            if ($trader) {
                $trader_folder_path = Yii::getAlias('@common/uploads/traders/' . $id);
                if (is_dir($trader_folder_path) && $trader->profile_pic) {
                    $file = Yii::getAlias('@common/uploads/traders/' . $id . '/' . $trader->profile_pic);
                } else {
                    $user_folder_path = Yii::getAlias('@common/uploads/users/' . $trader->user_id);
                    if (is_dir($user_folder_path)) {
                        if ($user->image_location)
                            $file = Yii::getAlias('@common/uploads/users/' . $trader->user_id . '/' . $user->image_location);
                        else {
                            $file = Yii::getAlias('@common/uploads/users/default.png');
                        }
                    }
                }
            }
        }


        if (!file_exists($file)) {

            $file = Yii::getAlias('@common/uploads/users/default.png');
        }
        $image = Yii::$app->image->load($file);
        echo $image->resize($s, $s)->render();
    }

    private function getSize($size) {
        switch ($size) {
            case "XS":
                return "50";
                break;

            case "SM":
                return "100";
                break;

            case "L":
                return "200";
                break;

            case "XL":
                return "300";
                break;

            default:
                return "100";
        }
    }

}
