<p><b>Email Address</b></p>
<p>
    This is the email created upon opening an account. If any changes required to be done in it please contact us at
    <a href="mail:support@mamgram.com">support@mamgram.com</a>
</p>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
        <input type="text" name="email" disabled="disabled" class="form-control" value="<?= $user_info['email'] ?>" />
    </div>
</div>
<p></p>
<p><b>Full Name</b></p>
<p>
    This must be your real full name (first name, middle name and last name)
</p>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
        <input type="text" class="form-control"  disabled="disabled" name="name" value="<?= $user_info['name'] ?>" />
    </div>
</div>
<p></p>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
        <p><b>Country of Residence</b></p>
        <p>Choose your country of residence</p>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                <input type="text" class="form-control"  disabled="disabled" name="country" value="<?= $user_info['country']['name'] ?>" />
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
        <p><b>Phone Number</b></p>
        <p>Enter your phone number without any leading zeros or prefixes.</p>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                <input type="phone" class="form-control" disabled="disabled" name="phone" value="+<?= $user_info['country']['phonecode'] . $user_info['phone'] ?>" />
            </div>
        </div>
    </div>
</div>
<p></p>
<p><b>Daily Analysis</b></p>
<p>Allow MamGram to send you daily analysis. Your email will not be used for any kind of third party advertisements.</p>
<label class="kt-checkbox kt-checkbox--success" style="font-weight: normal" >
    <input type="checkbox" /> Yes, I would like to receive newsletters
    <span></span>
</label>
<p></p>
<div class="row">
    <div class="col-lg-3 col-md-3"><button type="reset" style="width:100%" class="btn btn-brand">Save Settings</button></div>
    <div class="col-lg-3 col-md-3"><button type="reset" style="width:100%" class="btn btn-secondary">Reset Changes</button></div>
</div>
<hr>
<p></p>
<?= $this->render('@frontend/modules/user/views/auth/_changePassword', ['model' => $change_password_form]) ?>