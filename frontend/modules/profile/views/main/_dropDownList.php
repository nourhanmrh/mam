<?php

$read = new \Kendo\Data\DataSourceTransportRead();
$read->url('/accounts')
        ->dataType('JSON')
        ->type('GET');

$transport = new \Kendo\Data\DataSourceTransport();
$transport->read($read);

$schema = new \Kendo\Data\DataSourceSchema();
$schema->data('data.data')
        ->total('data.total');

$dataSource = new \Kendo\Data\DataSource();
$dataSource->transport($transport)
        ->schema($schema);

$dropDownList = new \Kendo\UI\DropDownList('accounts');
$dropDownList->dataTextField('name')->dataValueField('id')->value($active_account['id'])
        ->select('function(e) {
                var name = e.dataItem["name"];
                $.ajax({
                    url: "/activate-account",
                    type: "post",
                    data: {name: name},
                    success: function (response) {
                        if (response) {                          
                        }
                    }
                    
                }); }')
        ->dataSource($dataSource)
        ->attr('style', 'width:100%')
        ->height(400)
        ->headerTemplate('')
        ->template('<span class="k-state-default" style="background-image: url(\'/media/traders/live-#: data.is_live #.svg\')"></span><span class="k-state-default"><h3>#: data.name #</h3><p>#: data.description #</p></span>')
        ->valueTemplate('<span class="selected-value" style="") /><img src="/media/traders/live-#: data.is_live #.svg" width="35px" />&nbsp;&nbsp;<span>#:data.name#</span>'
);

echo $dropDownList->render();
?>
