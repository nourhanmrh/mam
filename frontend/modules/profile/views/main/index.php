<?php

use common\models\Constants;
?>
<style>
    .profile-img{
        max-width:80px;
        border-radius: 4px;
    }
    .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup,.k-dropdown .k-state-default {
        /*height: 50px;*/
    }
    /*    .k-header .k-icon {
            bottom: 15px;
            position: absolute;
            left: 0;
        }*/
    span.selected-value + span {
        bottom: 15px;
        position: absolute;
        left: 0;
    }
    #complete,.k-dropdown .k-state-default,.k-dropdown .k-state-default:hover {
        /*border-color: #47b2a1 !important;*/
        border-radius: 5px;
        background-color: #ffffff !important;
        box-shadow: none !important;
        color: black !important;
    }
    .k-list>.k-state-selected.k-state-focused {
        color: #47b2a1 !important;
    }
    .btn-brand:hover {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    .btn-brand {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    .btn-secondary:hover {
        border-color: #47b2a1 !important;
        background-color: #fff !important;
    }
    .btn-secondary {
        border-color: #47b2a1 !important;
        background-color: #fff !important;
    }
    .nav-tabs.nav-tabs-line.nav-tabs-line-brand a.nav-link.active, .nav-tabs.nav-tabs-line.nav-tabs-line-brand a.nav-link:hover, .nav-tabs.nav-tabs-line.nav-tabs-line-brand.nav.nav-tabs .nav-link.active, .nav-tabs.nav-tabs-line.nav-tabs-line-brand.nav.nav-tabs .nav-link:hover {
        color: #47b2a1;
        border-bottom: 1px solid #47b2a1;
    }
    b {
        font-weight: 550;
    }
    .kt-header__topbar .kt-header__topbar-item.kt-header__topbar-item--user .kt-header__topbar-wrapper img {
        height: 35px;
    }
</style>
<script>
    var TraderData = <?php echo json_encode($active_account); ?>;
</script>
<div class="container" style="margin-top: 25px;">
    <div class="row trader-row">
        <div class="kt-portlet card" style="min-height: 500px;">
            <div class="kt-portlet__body">
                <div class="row" style="border-bottom: 1px solid #47b2a1;padding-bottom: 20px;">
                    <div class="col-lg-1 col-md-1 col-sm-4 col-4">
                        <img class="profile-img" src="<?= $account_image_path ?>" alt="">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-8 col-8">
                        <div class="row" style="height: 40%">
                            <div class="col-12" style="font-size:20px;">
                                <b><?= $user_info['name'] ?></b>
                            </div>
                        </div>
                        <div class="row" style="height: 20%">
                            <div class="col-12" style="font-weight: 500">
                                <?= $active_account['is_live'] ? $active_account['platform'] . " Live" : "Demo" ?> Account
                            </div>
                        </div>
                        <div class="row" style="height: 40%">
                            <div class="col-12">
                                <img class="profile-img" src="<?= $broker_image_path ?>" class="" style="position: absolute;bottom: 0;max-height: 100%;" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-top: 30px;">
                    <div class="col-lg-2 col-md-2 col-sm-6 col-6" style="text-align: center">
                        <div class="row" style="height: 50%"><div class="col-lg-12"><h5><b>Balance</b></h5></div></div>
                        <div class="row" style="height: 50%;font-size: 15px;"><div class="col-lg-12"><?= $active_account['balance'] ?></div></div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-6" style="text-align: center">
                        <div class="row" style="height: 50%"><div class="col-lg-12"><h5><b>Equity</b></h5></div></div>
                        <div class="row" style="height: 50%;font-size: 15px;"><div class="col-lg-12" id="equity"><?= $active_account['equity'] ?></div></div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-6 col-6" style="text-align: center">
                        <div class="row" style="height: 50%"><div class="col-lg-12"><h5><b>Currency</b></h5></div></div>
                        <div class="row" style="height: 50%;font-size: 15px;"><div class="col-lg-12"><?= $active_account['currency'] ?></div></div>
                    </div>
                    <div class="offset-lg-3 col-lg-3 offset-md-1 col-md-3 " style="text-align: center;line-height: 4">
                        <?php if ($active_account['account_status'] == Constants::ACCOUNT_STATUS_NOT_COMPETED) { ?>
                            <a href="<?php
                            $name = '?name=' . $active_account['name'];
                            if ($active_account['is_live'])
                                echo '/open-live' . $name;
                            else
                                echo '/open-demo' . $name;
                            ?>" type="reset" class="btn btn-brand" style="width: 100%"><?= Yii::t("app", "Complete Your account") ?></button>
                           <?php }elseif ($active_account['account_status'] == Constants::ACCOUNT_STATUS_WAITING_CONFIRMATION) { ?>
                                <button class="btn btn-brand" style="width: 100%"><?= Yii::t("app", "Account Waiting Confirmation") ?></button>
                            <?php } elseif ($active_account['account_status'] == Constants::ACCOUNT_STATUS_COMPLETED) { ?>
                                <button class="btn btn-brand" style="width: 100%"><?= Yii::t("app", "Your account is completed") ?></button>
                            <?php } ?>
                            <a href="/open-account" class="btn btn-secondary" style="width: 100%"><?= Yii::t("app", "Open Account") ?></a>
                    </div>
                </div>
  

                <div class="row">
                    <div class="col-12 kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand trades-nav" role="tablist" id="myTab">
                            <?php if ($user_info['type'] != Constants::USER_TYPE_MONEY_MANAGER) { ?>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#following" role="tab" aria-selected="true">
                                        <?= Yii::t("app", "Following") ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#account" role="tab" aria-selected="true">
                                    <?= Yii::t("app", "Account") ?>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" data-selected="history" href="#history" role="tab" aria-selected="false">
                                    <?= Yii::t("app", "History") ?>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" data-selected="positions" href="#positions" role="tab" aria-selected="false">
                                    <?= Yii::t("app", "Positions") ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 tab-content">
                        <div class="tab-pane" id="following" style="min-height: 300px">
                            <?= $this->render("@frontend/modules/leaderboard/views/traders/_tradersListView", ['url' => '/get-own-traders', 'pageable' => false, 'loader_id' => '#following']) ?>
                        </div>
                        <div class="tab-pane" id="account">
                            <?= $this->render('_accountTab', ['change_password_form' => $change_password_form, 'user_info' => $user_info]) ?>
                        </div>
                        <div class="tab-pane active" id="history"  style="text-align: center;font-size: 12px;color: black;font-weight: 500;">
                            <div class="row"  style="padding-bottom: 15px;    background: #e6e7e8;    padding-top: 15px;">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-2"><b>Currency</b></div>
                                <div class="col-lg-1 col-md-1 col-sm-2 col-2"><b>Type</b></div>
                                <div class="col-lg-1 col-md-1 col-sm-2 col-2"><b>Lot</b></div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-2 hidden-sm hidden-xs"><b>Date Opened</b></div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-2 hidden-sm hidden-xs close-time-label"><b>Date Closed</b></div>
                                <div class="col-lg-1 col-md-1 col-sm-2 col-2 open-column"><b>Open Price</b></div>
                                <!--<div class="col-lg-2 col-md-2 col-sm-2 col-2 hidden-sm hidden-xs"><b>Open/Close</b></div>-->
                                <div class="col-lg-1 col-md-1 col-sm-2 col-2 close-time-label"><b>Close Price</b></div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-2 hidden-sm hidden-xs open-time-label"><b>TP</b></div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-2 open-time-label hidden-sm hidden-xs"><b>SL</b></div>
                                <!--<div class="col-lg-1 col-md-1 hidden-sm hidden-xs"><b>High</b></div>
                                <div class="col-lg-1 col-md-1 hidden-sm hidden-xs"><b>Low</b></div>-->
                                <div class="col-lg-2 col-md-2 col-sm-4 col-4"><b>Profit</b></div>
                                <!--<div class="col-lg-2 col-md-2 hidden-sm hidden-xs"><b>Total</b></div>-->
                            </div>
                           <?php $acc=\yii::$app->session->get('active_account'); ?>
                            <?= $this->render('@frontend/modules/leaderboard/views/trader-info/_trades', ['name' => $acc, 'url' => '/profile-trades']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
