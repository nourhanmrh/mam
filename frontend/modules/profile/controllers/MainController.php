<?php

namespace frontend\modules\profile\controllers;

use yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\users\Users;
use common\helpers\Notification;
use common\models\traders\Traders;
use common\models\forms\ChangePasswordForm;
use common\models\Constants;
use common\models\brokers\Brokers;
use common\models\mamaccounts\MamAccounts;
use common\models\ReportServer\ReportServer;
use common\models\ReportServer\Mt5Reportserver;
use common\models\ReportServer\Mt4Reportserver;

class MainController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['profile', 'accounts', 'activate-account', 'profile-trades', 'ss'],
                'rules' => [
                    [
                        'actions' => ['profile', 'accounts', 'activate-account', 'profile-trades', 'ss'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionProfile() {
        $user = new Users();
        $brokers = new Brokers();
        $t = new Traders();
        $currency = '';
        $change_password_form = new ChangePasswordForm(\Yii::$app->user->id);
        $active_account = Traders::get_active();
        $trader = Users::findIdentity(\Yii::$app->user->identity->id);
        $user_info = $user->findIdentityWithCountry(\Yii::$app->user->identity->id);
        $account_image_path = $user_info->findImagePath(Constants::IMAGE_SIZE_LARGE);
        $broker_image_path = $brokers->findImagePath($active_account['brokers_id'], Constants::IMAGE_SIZE_LARGE);
        $acs = $t->get_trader_info($active_account['id']);
        $session = \yii::$app->session;
        $session->open();
        $session->set('active_account', $active_account['name']);
        return $this->render('index', [
                    'active_account' => $active_account,
                    'user_info' => $user_info,
                    'change_password_form' => $change_password_form,
                    'account_image_path' => $account_image_path,
                    'broker_image_path' => $broker_image_path,
        ]);
    }

    public function actionAccounts() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $res = (new \yii\db\Query())
                ->select(['id', 'name', 'is_live'])
                ->from('mam_accounts')
                ->andWhere(['user_id' => \yii::$app->user->id])
                ->andWhere(['<>', 'account_status', 'deactivated']);

        $res = $res->all();
        foreach ($res as $key => $value) {
            if ($res[$key]['is_live']) {
                $res[$key]['description'] = "Live Account";
            } else {
                $res[$key]['description'] = "Demo Account";
            }
        }
        return array("total" => count($res), "data" => $res);
    }

    public function actionActivateAccount() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->post()) {
            $log = Yii::$app->getRequest()->getBodyParams();
            $name = $log['name'];

            Yii::$app->db->createCommand()
                    ->update('mam_accounts', ['active_account' => 0], ['user_id' => \yii::$app->user->id])
                    ->execute();
            Yii::$app->db->createCommand()
                    ->update('mam_accounts', ['active_account' => 1], ['AND', 'user_id' => \yii::$app->user->id, ['name' => $name]])
                    ->execute();
            $session = \yii::$app->session;
            $session->set('active_account', $name);
//          $notification = new Notification("Activation Success");
//          $notification->success();

            return $this->redirect("/profile");
        }
    }

    public function actionProfileTrades($models) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = json_decode($models);
        $data = new MamAccounts();
        $info = $data->getDataBase($request->name);
        if ($info['login']) {
            $report_server_class = (new ReportServer($info['platform']))->getClass();
            $reportserver = new $report_server_class($info['database'], $info['login']);
            return $reportserver->getTrades($request);
        }
        return [];
    }

}
