<?php 


use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<style>
    .subs {
        font-size: 17px;
        padding-left: 50px;
        padding-right: 50px;
    }
    .btn-brand:hover {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    .btn-brand {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    @media screen and (min-width: 768px){
        .card {
            min-height: 550px;
        }
        .icon {
            height: 40%;
        }
        .text {
            height: 15%;
        }
        .fields {
            height: 30%;
        }
        .open-button {
            height: 15%;
        }
        b {
            font-weight: 550;
        }
    }
    #open-demo-form {
        height: 100%;
    }
</style>
<?php 
$title = Yii::t("app","Open");
$account_name = "";
if($account && $account->name){
    $title = Yii::t("app","Complete");
    $account_name = "(".$account->name.")";
}
?>
<div class="container" id="open-demo-form-container" style="margin-top: 25px;">
    <div class="row">
        <div class="kt-portlet card" style="">
            <div class="kt-portlet__body" style="height: 100%">
                <div class="row">
                    <div class="col-12" style="border-bottom: 1px solid #47b2a1 ;font-size:20px;line-height:2;padding-bottom: 15px;">
                        <b><?= $title ?> Demo Account <?= $account_name ?></b>
                    </div>
                </div>
                <?php
                $form = ActiveForm::begin(
                                [
                                    'id' => 'open-demo-form',
                                    'action' => '/open-demo',
                                    'options' => [
                                        'data-pjax' => true
                                    ]
                                ]
                );
                ?>
                <?=  $form->field($model, 'name')->hiddenInput()->label(false); ?>
                <div class="row" style="height: 100%;padding-top: 30px">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row icon" style="text-align: center;">
                            <div class="col-12">
                                <img src="/media/Demo.png" />
                            </div>
                        </div>
                        <div class="row text" style="text-align: center;">
                            <div class="col-12 subs">
                                <b>Specify account details</b>
                            </div>
                        </div>
                        <div class="row fields" style="width: 70%;margin: auto;">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="form-group">
                                    <?=
                                    $form->field($model, 'currency')->dropDownList(
                                            ["USD" => "USD", "GBP" => "GBP", "EUR" => "EUR", "PLN" => "PLN"],
                                            ['prompt' => 'Select Curency'], 
                                            ['class' => 'form-control']
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="form-group">
                                    <?=
                                    $form->field($model, 'leverage')->dropDownList(
                                            ["100" => "1:100", "200" => "1:200", "300" => "1:300"],
                                            ['prompt' => 'Select Leverage'], 
                                            ['class' => 'form-control']
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="form-group">
                                     <?=
                                    $form->field($model, 'balance')->dropDownList(
                                            ["500" => "500", "1000" => "1000", "5000" => "5000", "10000" => "10000"],
                                            ['prompt' => 'Select Balance'], 
                                            ['class' => 'form-control']
                                    );
                                    ?>
<!--                                    <label class="kt-checkbox kt-checkbox--success">
                                        <input type="checkbox" data-type="boolean" /> Enter custom amount
                                        <span></span>
                                    </label>-->
                                </div>
                            </div>
                        </div>
                        <div class="row open-button" style="text-align: center">
                            <div class="col-12">
                                 <?= Html::submitButton('Open Demo Account', ['class' => 'btn btn-brand', 'name' => 'open-demo-button']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
$js = <<< JS
           
$('#open-demo-form').on('beforeSubmit', function () {
            Loader.show(".kt-portlet");
            var form = $(this);
                $.pjax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: new FormData($('#open-demo-form')[0]),
                    container: "#open-demo-form-container",
                    push: false,
                    scrollTo: false,
                    cache: false,
                    contentType: false,
                    timeout: 10000,
                    processData: false
                })
                .done(function(data) {
                    Loader.stop(".kt-portlet");
                })
                .fail(function () {
                    Loader.stop(".kt-portlet");
                })
            return false; // prevent default form submission
        });
        
JS;
$this->registerJs($js);
?>
