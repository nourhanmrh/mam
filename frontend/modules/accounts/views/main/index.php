<style>
    @media(min-width: 755px){
        .subs {
            font-size: 17px;padding-left: 50px;padding-right: 50px;
        }
        .img-row {
            height: 50%!important;
        }
        .desc-row,.btn-row{
            height: 25%!important;
        }
    }
    @media(max-width: 754px){
        .img-row,.desc-row,.btn-row{
            margin-bottom: 25px;
        }
    }

    .btn-brand:hover {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    .btn-brand {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    b {
        font-weight: 550;
    }
</style>
<div class="container" style="margin-top: 25px;">
    <div class="row">
        <div class="kt-portlet card" style="min-height: 500px;">
            <div class="kt-portlet__body" style="height: 100%">
                <div class="row">
                    <div class="col-12" style="border-bottom: 1px solid #47b2a1 ;font-size:20px;padding-bottom: 15px;">
                        <b>Open Account with MamGram</b>
                    </div>
                </div>
                <div class="row" style="text-align: center;height: 100%;padding-top: 30px">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="row img-row" style="">
                            <div class="col-12">
                                <img src="/media/Real.png" />
                            </div>
                        </div>
                        <div class="row desc-row" style="">
                            <div class="col-12 subs">
                                Trade with MamGram and invest with in the best Traders in the market through your preferred Broker’s account.
                            </div>
                        </div>
                        <div class="row btn-row" style="">
                            <div class="col-12">
                                <a href="/brokers" class="btn btn-brand">Open Live Account</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="row img-row" style="">
                            <div class="col-12">
                                <img src="/media/Demo.png" />
                            </div>
                        </div>
                        <div class="row desc-row" style="">
                            <div class="col-12 subs">
                                Open for free demo account that works exactly like the real one and learn how to trade with no risk.
                            </div>
                        </div>
                        <div class="row btn-row" style="">
                            <div class="col-12">
                                <a href="/open-demo" class="btn btn-brand">Open Demo Account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>