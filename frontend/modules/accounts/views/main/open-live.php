<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<style>
    .subs {
        font-size: 17px;padding-left: 50px;padding-right: 50px;
    }
    .btn-brand:hover {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    .btn-brand {
        background-color: #47b2a1 !important;
        border-color: #47b2a1 !important;
    }
    @media screen and (min-width: 768px){
        .card {
            min-height: 600px;
        }
        .icon {
            height: 40%;
        }
        .text {
            height: 10%;
        }
        .fields {
            height: 20%;
        }
        .open-button {
            height: 20%;
        }
        .signup-text {
            height: 10%;
        }
    }

    b {
        font-weight: 550;
    }
</style>
<div class="container" id="open-live-form-container" style="margin-top: 25px;">
    <div class="row">
        <div class="kt-portlet card" style="">
            <div class="kt-portlet__body" style="height: 100%">
                <div class="row" style="border-bottom: 1px solid #47b2a1;">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-8" style="font-size: 20px;line-height:2">
                        <b>Complete Real Account (<?= $account->name ?>)</b>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-4">
                        <img src='/webservices/images/brokers?id=<?= $account['brokers_id'] ?>&size=SM' style=" max-width: 80px;float: right;padding-bottom: 15px;" />
                    </div>
                </div>
                <?php
                $form = ActiveForm::begin(
                                [
                                    'id' => 'open-live-form',
                                    'options' => [
                                        'data-pjax' => true
                                    ]
                                ]
                );
                ?>
                <?= $form->field($model, 'name')->hiddenInput()->label(false); ?>
                <div class="row" style="height: 100%;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row icon" style="text-align: center;">
                            <div class="col-12">
                                <img src="/media/Real.png" />
                            </div>
                        </div>
                        <div class="row text" style="text-align: center;">
                            <div class="col-12 subs">
                                <b>Complete your account with <?= $account['brokers']['broker_name'] ?> Broker</b>
                            </div>
                        </div>
                        <div class="row fields" style="width: 70%;margin: auto;">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="form-group">
                                    <?= $form->field($model, 'login')->textInput(['class' => 'form-control', "placeholder" => $model->getAttributeLabel('login')]) ?>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="form-group">
                                    <?=
                                    $form->field($model, 'platform')->dropDownList(
                                            [/*"mt4" => "mt4",*/ "mt5" => "mt5"], ['prompt' => 'Select Platform'], ['class' => 'form-control']
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="form-group">
                                    <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control', "placeholder" => $model->getAttributeLabel('password')]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row open-button" style="text-align: center;">
                            <div class="col-12" style="line-height: 5;">
                                <?= Html::submitButton('Complete Real Account', ['class' => 'btn btn-brand', 'name' => 'open-real-button','value'=>'complete']) ?>

                            </div>
                        </div>
                        <div class="row signup-text" style="text-align: center">
                            <div class="col-12">
                                <p><b>You don't have an account yet?</b></p>
                                <p>
                                    <a style="color: red;font-weight: bold;text-decoration: underline" target="_blank" href="https://fxgrow.com">Sign up now</a>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
$js = <<< JS
           
$('#open-live-form').on('beforeSubmit', function () {
            Loader.show(".kt-portlet");
            var form = $(this);
                $.pjax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: new FormData($('#open-live-form')[0]),
                    container: "#open-live-form-container",
                    push: false,
                    scrollTo: false,
                    cache: false,
                    contentType: false,
                    timeout: 10000,
                    processData: false
                })
                .done(function(data) {
                    Loader.stop(".kt-portlet");
                })
                .fail(function () {
                    Loader.stop(".kt-portlet");
                })
            return false; // prevent default form submission
        });
        
JS;
$this->registerJs($js);
?>
