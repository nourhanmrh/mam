<?php

namespace frontend\modules\accounts\controllers;

use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\forms\DemoAccountCreateForm;
use common\models\forms\LiveAccountCreateForm;
use common\models\mamaccounts\MamAccounts;
use common\helpers\Notification;

/**
 * Default controller for the `accounts` module
 */
class MainController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['open-account', 'open-live', 'open-demo'],
                'rules' => [
                    [
                        'actions' => ['open-account', 'open-live', 'open-demo'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['accounts-request'],
                        'ips' => [yii::$app->params['accounts_request_ip']]
                    ]
                ],
            ]
        ];
    }

    public function actionAccountsRequest() {
        $mam_account = new MamAccounts();
        $result = $mam_account->getAllAccountsMap();
        return json_encode($result);
    }

    public function actionOpenAccount() {
        return $this->render('index');
    }

    public function actionOpenLive($name = null) {
        $live_account = new LiveAccountCreateForm();
        $live_account->name = $name;
        $mam_account = new MamAccounts();
        $account = [];
        if ($name) {
            $account = $mam_account->getOwnNotCompleteAccountByName($name, \Yii::$app->user->identity->id);          
            if (!$account) {
                $notification = new Notification("Account Not Found");
                $notification->error();
                return $this->redirect('/profile');
            }
            if ($account && !$account->isActive()) {
                $notification = new Notification("Account Not Active");
                $notification->error();
                return $this->redirect('/profile');
            }
        } else {
            $notification = new Notification("Account Not Found");
            $notification->error();
            return $this->redirect('/profile');
        }

        $live_account->broker = $account['brokers']['broker_name'];
        if ($live_account->load(\Yii::$app->request->post()) && $live_account->validate()) {
            if ($live_account->name) {
                if (!$mam_account->completeLiveAccount($live_account)) {
                    $notification = new Notification("Failed Complete Account");
                    $notification->error();
                    return $this->refresh();
                } else {
                    $notification = new Notification("Complete Account Success, your account is waiting confirmation");
                    $notification->success();
                }
            }
            return $this->redirect(['/profile']);
        }

        return $this->render('open-live', ['account' => $account, 'model' => $live_account]);
    }

    public function actionOpenDemo($name = null) {
        $demo_account = new DemoAccountCreateForm();
        $demo_account->name = $name;
        $mam_account = new MamAccounts();
        $account = [];
        if ($name) {
            $account = $mam_account->getOwnNotCompleteAccountByName($name, \Yii::$app->user->identity->id);
            if (!$account) {
                $notification = new Notification("Account Not Found");
                $notification->error();
                return $this->redirect('/profile');
            }
        }

        if ($demo_account->load(\Yii::$app->request->post())) {
            if ($demo_account->name) {
                if (!$mam_account->completeAccount($demo_account)) { 
                    $notification = new Notification("Failed Complete Account");
                    $notification->error();
                    return $this->refresh();
                } else {
                    $notification = new Notification("Complete Account Success");
                    $notification->success();
                }
            } else { 
                if (!$mam_account->createDemoAccount($demo_account)) {
                    $notification = new Notification("Failed Create Demo Account");
                    $notification->error();
                    return $this->refresh();
                } else {
                    $notification = new Notification("Create Demo Account Success");
                    $notification->success();
                }
            }
            return $this->redirect(['/profile']);
        }

        return $this->render('open-demo', ['account' => $account, 'model' => $demo_account]);
    }

}
